.PHONY: all clean library examples test

LIBRARY_TARGET = \
	embedded_stm32f4_library \
	embedded_stm32f429disco_library \
	light_stm32f4_library \
	light_tasking_stm32f4_library \
	light_tasking_stm32f429disco_library \


all: library examples

library: $(LIBRARY_TARGET)

$(LIBRARY_TARGET):
	gprbuild -P library/gpr/$@.gpr

clean: $(LIBRARY_TARGET:%=%-clean)

%-clean:
	gprclean -P library/gpr/$*.gpr

examples:
	cd examples; $(MAKE) all

test:
	echo $(TARGET)
	echo $(LIBRARY_TARGET)