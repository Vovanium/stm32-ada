generic
	type Element_Type is private;
package STM32.Circular_Buffers is

	type Circular_Buffer (Capacity : Positive) is private;

	function Is_Empty (B : Circular_Buffer) return Boolean;

	function Is_Full (B : Circular_Buffer) return Boolean;

	procedure Enqueue (B : in out Circular_Buffer; E : in Element_Type)
	with Pre => not Is_Full (B), Post => not Is_Empty (B);

	procedure Dequeue (B : in out Circular_Buffer; E : out Element_Type)
	with Pre => not Is_Empty (B), Post => not Is_Full (B);

	procedure Clear (B : in out Circular_Buffer);

private
	type Element_Array is array (Natural range <>) of Element_Type;
	type Circular_Buffer (Capacity : Positive) is record
		Head : Natural := 0;
		Tail : Natural := 0;
		Data : Element_Array (0 .. Capacity);
	end record;

	function Is_Empty (B : Circular_Buffer) return Boolean
	is (B.Head = B.Tail);

	function Is_Full (B : Circular_Buffer) return Boolean
	is ((B.Head + 1) mod (B.Capacity + 1) = B.Tail);

end STM32.Circular_Buffers;