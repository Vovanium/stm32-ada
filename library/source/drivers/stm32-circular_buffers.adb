package body STM32.Circular_Buffers is

	procedure Enqueue (B : in out Circular_Buffer; E : in Element_Type) is
	begin
		B.Data (B.Head) := E;
		B.Head := (B.Head + 1) mod (B.Capacity + 1);
	end Enqueue;

	procedure Dequeue (B : in out Circular_Buffer; E : out Element_Type) is
	begin
		E := B.Data (B.Tail);
		B.Tail := (B.Tail + 1) mod (B.Capacity + 1);
	end Dequeue;

	procedure Clear (B : in out Circular_Buffer) is
	begin
		B.Head := 0;
		B.Tail := 0;
	end Clear;

end STM32.Circular_Buffers;
