with Ada.Streams;
use  Ada.Streams;

package STM32.Stream_Drivers is
	type Stream_Driver is synchronized interface;

	procedure Read (
		Driver : in out Stream_Driver;
		Item   :    out Stream_Element_Array;
		Last   :    out Stream_Element_Offset) is abstract;
	-- Reads some data from input stream
	-- Will block until any data available or some other specified event occur

	procedure Read_Immediate (
		Driver : in out Stream_Driver;
		Data   :    out Stream_Element_Array;
		Last   :    out Stream_Element_Offset) is abstract;
	-- Reads some data from input stream
	-- Will not block

	procedure Write (
		Driver : in out Stream_Driver;
		Data   : in     Stream_Element_Array;
		Last   :    out Stream_Element_Offset) is abstract;
	-- Writes data to output stream
	-- Will block until it can write some data (bay be not all)

private

end STM32.Stream_Drivers;