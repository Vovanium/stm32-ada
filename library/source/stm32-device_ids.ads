package STM32.Device_IDs with Pure is

	-- Device identifiers are retrived through DBG unit.
	-- See Debug package.

	type Device_ID is mod 2**12;

	-- IDCODE
	STM32F4x5_4x7  : constant Device_ID := 16#413#;
	STM32F405      : constant Device_ID := STM32F4x5_4x7;
	STM32F407      : constant Device_ID := STM32F4x5_4x7;
	STM32F415      : constant Device_ID := STM32F4x5_4x7;
	STM32F417      : constant Device_ID := STM32F4x5_4x7;
	STM32F42x_43x  : constant Device_ID := 16#419#;
	STM32F427      : constant Device_ID := STM32F42x_43x;
	STM32F429      : constant Device_ID := STM32F42x_43x;
	STM32F437      : constant Device_ID := STM32F42x_43x;
	STM32F439      : constant Device_ID := STM32F42x_43x;
	STM32F446      : constant Device_ID := 16#421#;
	STM32F401xB_xC : constant Device_ID := 16#423#;
	STM32F401xD_xE : constant Device_ID := 16#433#;
	STM32F411xC_xE : constant Device_ID := 16#431#;
	STM32F46x_F47x : constant Device_ID := 16#434#;
	STM32F412      : constant Device_ID := 16#441#;
	STM32F410      : constant Device_ID := 16#458#;
	STM32F413_F423 : constant Device_ID := 16#463#;
	STM32F413      : constant Device_ID := STM32F413_F423;
	STM32F423      : constant Device_ID := STM32F413_F423;

end STM32.Device_IDs;
