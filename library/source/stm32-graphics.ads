with Interfaces;

package STM32.Graphics is
	-- Commons for graphics
	-- Color register and pixel formats

	pragma Pure;

	type ARGB8888_Pixel is record
		B : Integer range 0 .. 2**8 - 1;
		G : Integer range 0 .. 2**8 - 1;
		R : Integer range 0 .. 2**8 - 1;
		A : Integer range 0 .. 2**8 - 1 := 0;
	end record with Size => 32;
	for ARGB8888_Pixel use record
		B at 0 range  0 ..  7;
		G at 0 range  8 .. 15;
		R at 0 range 16 .. 23;
		A at 0 range 24 .. 31;
	end record;

	type RGB888_Pixel is record
		B : Integer range 0 .. 2**8 - 1;
		G : Integer range 0 .. 2**8 - 1;
		R : Integer range 0 .. 2**8 - 1;
	end record with Size => 24;
	for RGB888_Pixel use record
		B at 0 range  0 ..  7;
		G at 0 range  8 .. 15;
		R at 0 range 16 .. 23;
	end record;

	type RGB565_Pixel is record
		B : Integer range 0 .. 2**5 - 1;
		G : Integer range 0 .. 2**6 - 1;
		R : Integer range 0 .. 2**5 - 1;
	end record with Size => 16;
	for RGB565_Pixel use record
		B at 0 range  0 ..  4;
		G at 0 range  5 .. 10;
		R at 0 range 11 .. 15;
	end record;

	type ARGB1555_Pixel is record
		B : Integer range 0 .. 2**5 - 1;
		G : Integer range 0 .. 2**5 - 1;
		R : Integer range 0 .. 2**5 - 1;
		A : Integer range 0 .. 1 := 0;
	end record with Size => 16;
	for ARGB1555_Pixel use record
		B at 0 range  0 ..  4;
		G at 0 range  5 ..  9;
		R at 0 range 10 .. 14;
		A at 0 range 15 .. 15;
	end record;

	type ARGB4444_Pixel is record
		B : Integer range 0 .. 2**4 - 1;
		G : Integer range 0 .. 2**4 - 1;
		R : Integer range 0 .. 2**4 - 1;
		A : Integer range 0 .. 2**4 - 1 := 0;
	end record with Size => 16;
	for ARGB4444_Pixel use record
		B at 0 range  0 ..  3;
		G at 0 range  4 ..  7;
		R at 0 range  8 .. 11;
		A at 0 range 12 .. 15;
	end record;

	subtype L8_Pixel is Interfaces.Unsigned_8;

	type AL44_Pixel is record
		L : Integer range 0 .. 2**4 - 1;
		A : Integer range 0 .. 2**4 - 1;
	end record with Size => 8;
	for AL44_Pixel use record
		L at 0 range  0 ..  3;
		A at 0 range  4 ..  7;
	end record;

	type AL88_Pixel is record
		L : Integer range 0 .. 2**8 - 1;
		A : Integer range 0 .. 2**8 - 1;
	end record with Size => 16;
	for AL88_Pixel use record
		L at 0 range  0 ..  7;
		A at 0 range  8 .. 15;
	end record;

	type Color_Mode is (
		ARGB8888,
		RGB888,
		RGB565,
		ARGB1555,
		ARGB4444,
		L8,
		AL44,
		AL88,
		L4,
		A8,
		A4
	);

	for Color_Mode use (
		ARGB8888 => 2#0000#,
		RGB888   => 2#0001#,
		RGB565   => 2#0010#,
		ARGB1555 => 2#0011#,
		ARGB4444 => 2#0100#,
		L8       => 2#0101#,
		AL44     => 2#0110#,
		AL88     => 2#0111#,
		L4       => 2#1000#,
		A8       => 2#1001#,
		A4       => 2#1010#
	);

end STM32.Graphics;
