with System;
with Interfaces;
use  Interfaces;

package STM32 is
	pragma Pure;

	subtype Address_Register is System.Address;

	type Unused_1_Bit   is mod 2**1  with Size =>  1;
	type Unused_2_Bits  is mod 2**2  with Size =>  2;
	type Unused_3_Bits  is mod 2**3  with Size =>  3;
	type Unused_4_Bits  is mod 2**4  with Size =>  4;
	type Unused_5_Bits  is mod 2**5  with Size =>  5;
	type Unused_6_Bits  is mod 2**6  with Size =>  6;
	type Unused_7_Bits  is mod 2**7  with Size =>  7;
	type Unused_8_Bits  is mod 2**8  with Size =>  8;
	type Unused_9_Bits  is mod 2**9  with Size =>  9;
	type Unused_10_Bits is mod 2**10 with Size => 10;
	type Unused_12_Bits is mod 2**12 with Size => 12;
	type Unused_13_Bits is mod 2**13 with Size => 13;
	type Unused_14_Bits is mod 2**14 with Size => 14;
	type Unused_15_Bits is mod 2**15 with Size => 15;
	type Unused_16_Bits is mod 2**16 with Size => 16;
	type Unused_17_Bits is mod 2**17 with Size => 17;
	type Unused_18_Bits is mod 2**18 with Size => 18;
	type Unused_19_Bits is mod 2**19 with Size => 19;
	type Unused_20_Bits is mod 2**20 with Size => 20;
	type Unused_21_Bits is mod 2**21 with Size => 21;
	type Unused_22_Bits is mod 2**22 with Size => 22;
	type Unused_23_Bits is mod 2**23 with Size => 23;
	type Unused_24_Bits is mod 2**24 with Size => 24;
	type Unused_25_Bits is mod 2**25 with Size => 25;
	type Unused_28_Bits is mod 2**28 with Size => 28;
	type Unused_29_Bits is mod 2**29 with Size => 29;
	type Unused_30_Bits is mod 2**30 with Size => 30;

	type Logarithmic is (
		Value_1,  Value_2,  Value_4,  Value_8,
		Value_16, Value_32, Value_64, Value_128);

end STM32;
