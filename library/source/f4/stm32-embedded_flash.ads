with STM32.Address_Map;

-- Embedded Flash Memory Interface

package STM32.Embedded_Flash is

	-- ACR
	type Access_Control_Register is record
		LATENCY   : Integer range 0 .. 15; -- 0 .. 7 for F405/07/15/17
		Unused_4  : Unused_4_Bits;
		PRFTEN    : Boolean;
		ICEN      : Boolean;
		DCEN      : Boolean;
		ICRST     : Boolean;
		DCRST     : Boolean;
		Unused_13 : Unused_19_Bits;
	end record with Size => 32;

	for Access_Control_Register use record
		LATENCY   at 0 range  0 ..  3;
		Unused_4  at 0 range  4 ..  7;
		PRFTEN    at 0 range  8 ..  8;
		ICEN      at 0 range  9 ..  9;
		DCEN      at 0 range 10 .. 10;
		ICRST     at 0 range 11 .. 11;
		DCRST     at 0 range 12 .. 12;
		Unused_13 at 0 range 13 .. 31;
	end record;

	-- KEYR
	type Key_Register is mod 2**32 with Size => 32;

	KEY1 : constant Key_Register := 16#45670123#;
	KEY2 : constant Key_Register := 16#CDEF90AB#;

	-- OPTKEYR
	type Option_Key_register is mod 2**32 with Size => 32;

	OPTKEY1 : constant Option_Key_Register := 16#08192A3B#;
	OPTKEY2 : constant Option_Key_Register := 16#4C5D6E7F#;

	-- SR
	type Status_Register is record
		EOP       : Boolean;
		OPERR     : Boolean;
		Unused_2  : Unused_2_Bits;
		WRPERR    : Boolean;
		PGAERR    : Boolean;
		PGPERR    : Boolean;
		PGSERR    : Boolean;
		RDERR     : Boolean; -- Only in F42xxx, F43xxx
		Unused_9  : Unused_6_Bits;
		BSY       : Boolean;
		Unused_17 : Unused_15_Bits;
	end record with Size => 32;

	for Status_Register use record
		EOP       at 0 range  0 ..  0;
		OPERR     at 0 range  1 ..  1;
		Unused_2  at 0 range  2 ..  3;
		WRPERR    at 0 range  4 ..  4;
		PGAERR    at 0 range  5 ..  5;
		PGPERR    at 0 range  6 ..  6;
		PGSERR    at 0 range  7 ..  7;
		RDERR     at 0 range  8 ..  8;
		Unused_9  at 0 range  9 .. 15;
		BSY       at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 31;
	end record;

	-- CR
	type Sector_Number is (
		Sector_0,
		Sector_1,
		Sector_2,
		Sector_3,
		Sector_4,
		Sector_5,
		Sector_6,
		Sector_7,
		Sector_8,
		Sector_9,
		Sector_10,
		Sector_11,
		Sector_12,
		Sector_13,
		Sector_14,
		Sector_15,
		Sector_16,
		Sector_17,
		Sector_18,
		Sector_19,
		Sector_20,
		Sector_21,
		Sector_22,
		Sector_23
	) with Size => 5;

	for Sector_Number use (
		Sector_0  => 2#00000#,
		Sector_1  => 2#00001#,
		Sector_2  => 2#00010#,
		Sector_3  => 2#00011#,
		Sector_4  => 2#00100#,
		Sector_5  => 2#00101#,
		Sector_6  => 2#00110#,
		Sector_7  => 2#00111#,
		Sector_8  => 2#01000#,
		Sector_9  => 2#01001#,
		Sector_10 => 2#01010#,
		Sector_11 => 2#01011#,
		Sector_12 => 2#10000#, -- this and further in F42xxx, F43xxx only
		Sector_13 => 2#10001#,
		Sector_14 => 2#10010#,
		Sector_15 => 2#10011#,
		Sector_16 => 2#10100#,
		Sector_17 => 2#10101#,
		Sector_18 => 2#10110#,
		Sector_19 => 2#10111#,
		Sector_20 => 2#11000#,
		Sector_21 => 2#11001#,
		Sector_22 => 2#11010#,
		Sector_23 => 2#11011#
	);

	type Program_Size is (
		Program_X8,
		Program_X16,
		Program_X32,
		Program_X64
	) with Size => 2;

	for Program_Size use (
		Program_X8  => 2#00#,
		Program_X16 => 2#01#,
		Program_X32 => 2#10#,
		Program_X64 => 2#11#
	);

	type Control_Register is record
		PG        : Boolean;
		SER       : Boolean;
		MER       : Boolean;
		SNB       : Sector_Number;
		PSIZE     : Program_Size;
		Unused_10 : Unused_5_Bits;
		MER1      : Boolean; -- in F42xxx, F43xxx only
		STRT      : Boolean;
		Unused_17 : Unused_7_Bits;
		EOPIE     : Boolean;
		ERRIE     : Boolean;
		Unused_26 : Unused_5_Bits;
		LOCK      : Boolean;
	end record with Size => 32;

	for Control_Register use record
		PG        at 0 range  0 ..  0;
		SER       at 0 range  1 ..  1;
		MER       at 0 range  2 ..  2;
		SNB       at 0 range  3 ..  7;
		PSIZE     at 0 range  8 ..  9;
		Unused_10 at 0 range 10 .. 14;
		MER1      at 0 range 15 .. 15;
		STRT      at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 23;
		EOPIE     at 0 range 24 .. 24;
		ERRIE     at 0 range 25 .. 25;
		Unused_26 at 0 range 26 .. 30;
		LOCK      at 0 range 31 .. 31;
	end record;

	-- OPTCR
	type BOR_Level is (
		BOR_Level_3,
		BOR_Level_2,
		BOR_Level_1,
		BOR_Off
	) with Size => 2;

	for BOR_Level use (
		BOR_Level_3 => 2#00#,
		BOR_Level_2 => 2#01#,
		BOR_Level_1 => 2#10#,
		BOR_Off     => 2#11#
	);

	type Read_Protect is mod 2**8;
	Read_Protect_Level_0 : constant Read_Protect := 16#AA#;
	Read_Protect_Level_2 : constant Read_Protect := 14#CC#;
	-- other values: Level 1

	type Not_Write_Protect is array (Integer range 0 .. 11) of Boolean
	 with Pack, Size => 12;

	type Option_Control_Register is record
		OPTLOCK     : Boolean;
		OPTSTRT     : Boolean;
		BOR_LEV     : BOR_Level;
		BFB2        : Boolean; -- In F42xxx, F43xxx only
		WDG_SW      : Boolean;
		nRST_STOP   : Boolean;
		nRST_STDBY  : Boolean;
		RDP         : Read_Protect;
		nWRP        : Not_Write_Protect;
		Unused_28   : Unused_2_Bits;
		DB1M        : Boolean; -- In F42xxx, F43xxx only
		SPRMOD      : Boolean; -- In F42xxx, F43xxx only
	end record with Size => 32;

	for Option_Control_Register use record
		OPTLOCK     at 0 range  0 ..  0;
		OPTSTRT     at 0 range  1 ..  1;
		BOR_LEV     at 0 range  2 ..  3;
		BFB2        at 0 range  4 ..  4;
		WDG_SW      at 0 range  5 ..  5;
		nRST_STOP   at 0 range  6 ..  6;
		nRST_STDBY  at 0 range  7 ..  7;
		RDP         at 0 range  8 .. 15;
		nWRP        at 0 range 16 .. 27;
		Unused_28   at 0 range 28 .. 29;
		DB1M        at 0 range 30 .. 30;
		SPRMOD      at 0 range 31 .. 31;
	end record;

	-- OPTCR1
	type Option_Control_Register_1 is record -- In F42xxx, F43xxx only
		Unused_0  : Unused_16_Bits;
		nWRP      : Not_Write_Protect;
		Unused_28 : Unused_4_Bits;
	end record with Size => 32;

	for Option_Control_Register_1 use record
		Unused_0  at 0 range  0 .. 15;
		nWRP      at 0 range 16 .. 27;
		Unused_28 at 0 range 28 .. 31;
	end record;

	type Flash_Registers is record
		ACR     : Access_Control_Register;
		KEYR    : Key_Register;
		OPTKEYR : Option_Key_Register;
		SR      : Status_Register;
		CR      : Control_Register;
		OPTCR   : Option_Control_Register;
		OPTCR1  : Option_Control_Register_1;
	end record with Volatile;

	for Flash_Registers use record
		ACR     at 16#00# range 0 .. 31;
		KEYR    at 16#04# range 0 .. 31;
		OPTKEYR at 16#08# range 0 .. 31;
		SR      at 16#0C# range 0 .. 31;
		CR      at 16#10# range 0 .. 31;
		OPTCR   at 16#14# range 0 .. 31;
		OPTCR1  at 16#18# range 0 .. 31;
	end record;

	FLASH : Flash_Registers with Volatile, Import, Address => Address_Map.Flash_IR;

end STM32.Embedded_Flash;
