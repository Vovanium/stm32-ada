with STM32.Address_Map;
package body STM32.Reset_and_Clock.Frequencies is

	type Product is range 0 .. 500_000_000_000; -- Big enough

	function PLL_Input_Frequency return Natural is (
		case RCC.PLLCFGR.SRC is
		when PLLSRC_HSI => HSI_Frequency,
		when PLLSRC_HSE => HSE_Frequency);

	function VCO_Input_Frequency return Natural is (PLL_Input_Frequency / RCC.PLLCFGR.M);

	function PLL_VCO_Frequency return Natural is (
		Natural (
			Product (PLL_Input_Frequency) * Product (RCC.PLLCFGR.N) / Product (RCC.PLLCFGR.M)
		)
	);

	function PLLCLK_Frequency return Natural is
	begin
		return PLL_VCO_Frequency / PLLP_Value (RCC.PLLCFGR.P);
	end;

	function USB_Frequency return Natural is
	begin
		return PLL_VCO_Frequency / RCC.PLLCFGR.Q;
	end;

	function SYSCLK_Frequency return Natural is (
		case RCC.CFGR.SWS is
		when System_Clock_HSI => HSI_Frequency,
		when System_Clock_HSE => HSE_Frequency,
		when System_Clock_PLL => PLLCLK_Frequency);

	function HCLK_Frequency return Natural is
	begin
		return SYSCLK_Frequency / HPRE_Value (RCC.CFGR.HPRE);
	end;

	function System_Timer_Clock_Frequency return Natural is
	begin
		return HCLK_Frequency / 8;
	end;

	function PCLK1_Frequency return Natural is
	begin
		return HCLK_Frequency / PPRE_Value (RCC.CFGR.PPRE1);
	end;

	function PCLK2_Frequency return Natural is
	begin
		return HCLK_Frequency / PPRE_Value (RCC.CFGR.PPRE2);
	end;

	function APB_Timer_Clock_Frequency (
		PPRE : APB_Prescaler_Factor)
		return Natural
	is
		HCLK : Natural := HCLK_Frequency;
		PPRE_Val : Natural := PPRE_Value (PPRE);
		TIMPRE_Val : Natural := (case RCC.DCKCFGR.TIMPRE is
			when Timer_2PCLK => 2,
			when Timer_4PCLK => 4);
	begin
		return Natural'Min (HCLK, HCLK * TIMPRE_Val / PPRE_Val);
	end;

	function APB1_Timer_Clock_Frequency return Natural is
	begin
		return APB_Timer_Clock_Frequency (RCC.CFGR.PPRE1);
	end;

	function APB2_Timer_Clock_Frequency return Natural is
	begin
		return APB_Timer_Clock_Frequency (RCC.CFGR.PPRE2);
	end;

	function PLLI2S_Frequency return Natural is (
		Natural (
			Product (PLL_Input_Frequency) * Product (RCC.PLLI2SCFGR.N) / Product (RCC.PLLCFGR.M)
		)
	);

	function PLLSAI_Frequency return Natural is (
		Natural (
			Product (PLL_Input_Frequency) * Product (RCC.PLLSAICFGR.N) / Product (RCC.PLLCFGR.M)
		)
	);

	function I2S_Clock_Frequency (External_Frequency : Natural := 0) return Natural is
	begin
		case RCC.CFGR.I2SSRC is
		when I2S_Clock_PLLI2S => return PLLI2S_Frequency / RCC.PLLI2SCFGR.R;
		when I2S_Clock_CLKIN  => return External_Frequency;
		end case;
	end;

	function SAI1A_Clock_Frequency (External_Frequency : Natural := 0) return Natural is
	begin
		case RCC.DCKCFGR.SAI1ASRC is
		when SAI_Clock_PLLSAI =>
			return PLLSAI_Frequency / (RCC.PLLSAICFGR.Q * (RCC.DCKCFGR.PLLSAIDIVQ + 1));
		when SAI_Clock_PLLI2S =>
			return PLLI2S_Frequency / (RCC.PLLI2SCFGR.Q * (RCC.DCKCFGR.PLLI2SDIVQ + 1));
		when SAI_Clock_AF_Input =>
			return External_Frequency;
		end case;
	end;

	function SAI1B_Clock_Frequency (External_Frequency : Natural := 0) return Natural is
	begin
		case RCC.DCKCFGR.SAI1BSRC is
		when SAI_Clock_PLLSAI =>
			return PLLSAI_Frequency / (RCC.PLLSAICFGR.Q * (RCC.DCKCFGR.PLLSAIDIVQ + 1));
		when SAI_Clock_PLLI2S =>
			return PLLI2S_Frequency / (RCC.PLLI2SCFGR.Q * (RCC.DCKCFGR.PLLI2SDIVQ + 1));
		when SAI_Clock_AF_Input =>
			return External_Frequency;
		end case;
	end;

	function LCD_Clock_Frequency return Natural is
	begin
		return PLLSAI_Frequency / (RCC.PLLSAICFGR.R * PLLSAIDIVR_Value (RCC.DCKCFGR.PLLSAIDIVR));
	end;

	function PLLI2S_N (Frequency : Natural) return Natural is
		F : Product := Product (Frequency) * Product (RCC.PLLCFGR.M);
		P : Product := Product (PLL_Input_Frequency);
	begin
		return Natural ((F + P / 2) / P);
	end;

	function PLLSAI_N (Frequency : Natural) return Natural renames PLLI2S_N;

	procedure LCD_R (Frequency : Natural; R : out Natural; DIVR : out PLLSAI_Division_Factor) is
		R_Total : constant Natural := PLLSAI_Frequency / Frequency;
	begin
		case R_Total is
		when 3 .. 2 * 7 =>
			DIVR := PLLSAI_Div_2;
			R := (R_Total + 1) / 2;
		when 1 + 2 * 7 .. 1 + 4 * 7 =>
			DIVR := PLLSAI_Div_4;
			R := (R_Total + 2) / 4;
		when 2 + 4 * 7 .. 3 + 8 * 7 =>
			DIVR := PLLSAI_Div_8;
			R := (R_Total + 4) / 8;
		when 4 + 8 * 7 .. 7 + 16 * 7 =>
			DIVR := PLLSAI_Div_16;
			R := (R_Total + 8) / 16;
		when others =>
			raise Constraint_Error;
		end case;
	end;

end STM32.Reset_and_Clock.Frequencies;