with System.Storage_Elements;
use  System.Storage_Elements;

-- USB On-the-Go controllers

package STM32.USB_OTG is

	-- Both USB_OTG_FS and USB_OTG_HS have the same register structure.

	type OTG_Role is (USB_Device, USB_Host) with Size => 1;

	type Device_Address is range 0 .. 127; -- USB device address

	-- Channels

	type Extended_Channel_Number is range 0 .. 31; -- Channel / Tx FIFO / Endpoint numbers
	Non_Periodic_Tx_FIFO : constant Extended_Channel_Number := 2#00000#; -- Non-periodic Tx FIFO in host mode
	Periodic_Tx_FIFO     : constant Extended_Channel_Number := 2#00001#; -- Periodic Tx FIFO in host mode
	All_Tx_FIFOs         : constant Extended_Channel_Number := 2#10000#; -- Flush all Tx FIFOs

	subtype Channel_Number is Extended_Channel_Number range 0 .. 15;

	type Channel_Set is array (Channel_Number) of Boolean with Pack, Default_Component_Value => False;

	-- Used in DCTL and HPRT registers

	type Test_Mode is (
		No_Test,
		Test_J,
		Test_K,
		Test_SE0_NAK,
		Test_Packet,
		Test_Force_Enable)
	with Size => 3;
	for Test_Mode use (
		No_Test           => 2#000#,
		Test_J            => 2#001#,
		Test_K            => 2#010#,
		Test_SE0_NAK      => 2#011#,
		Test_Packet       => 2#100#,
		Test_Force_Enable => 2#101#);

	-- Used in DIEPCTLx, HCCHARx

	type Endpoint_Type is (
		Control_Endpoint,
		Isochronous_Endpoint,
		Bulk_Endpoint,
		Interrupt_Endpoint)
	with Size => 2;

	-- GOTGCTL

	type Connector_Status is (
		A_Device,
		B_Device)
	with Size => 1;

	type Debounce_Time is (
		Long_Debounce,
		Short_Debounce)
	with Size => 1;

	type General_Control_and_Status_Register is record
		SRQSCS    : Boolean          := False; -- Session request success
		SRQ       : Boolean          := False; -- Session request
		Unused_2  : Unused_6_Bits    := 0;
		HNGSCS    : Boolean          := False; -- Host negotiation success
		HNPRQ     : Boolean          := False; -- HNP request
		HSHNPEN   : Boolean          := False; -- Host set HNP enable
		DHNPEN    : Boolean          := True; -- Device HNP enabled
		Unused_12 : Unused_4_Bits    := 0;
		CIDSTS    : Connector_Status := A_Device; -- Connector ID status
		DBCT      : Debounce_Time    := Long_Debounce; -- Long/short debounce time
		ASVLD     : Boolean          := False; -- A-session valid
		BSVLD     : Boolean          := False; -- B-session valid
		Unused_20 : Unused_12_Bits;
	end record with Size => 32;
	for General_Control_and_Status_Register use record
		SRQSCS    at 0 range  0 ..  0;
		SRQ       at 0 range  1 ..  1;
		Unused_2  at 0 range  2 ..  7;
		HNGSCS    at 0 range  8 ..  8;
		HNPRQ     at 0 range  9 ..  9;
		HSHNPEN   at 0 range 10 .. 10;
		DHNPEN    at 0 range 11 .. 11;
		Unused_12 at 0 range 12 .. 15;
		CIDSTS    at 0 range 16 .. 16;
		DBCT      at 0 range 17 .. 17;
		ASVLD     at 0 range 18 .. 18;
		BSVLD     at 0 range 19 .. 19;
		Unused_20 at 0 range 20 .. 31;
	end record;

	-- GOTGINT

	type General_Interrupt_Register is record
		Unused_0  : Unused_2_Bits  := 0;
		SEDET     : Boolean        := False; -- Session end detected
		Unused_3  : Unused_5_Bits  := 0;
		SRSSCHG   : Boolean        := False; -- Session request success status
		HNSSCHG   : Boolean        := False; -- Host negotiation success statu
		Unused_10 : Unused_7_Bits  := 0;
		HNGDET    : Boolean        := False; -- Host negotiation detected
		ADTOCHG   : Boolean        := False; -- A-device timeout change
		DBCDNE    : Boolean        := False; -- Debounce done
		Unused_20 : Unused_12_Bits := 0;
	end record with Size => 32;
	for General_Interrupt_Register use record
		Unused_0  at 0 range  0 ..  1;
		SEDET     at 0 range  2 ..  2;
		Unused_3  at 0 range  3 ..  7;
		SRSSCHG   at 0 range  8 ..  8;
		HNSSCHG   at 0 range  9 ..  9;
		Unused_10 at 0 range 10 .. 16;
		HNGDET    at 0 range 17 .. 17;
		ADTOCHG   at 0 range 18 .. 18;
		DBCDNE    at 0 range 19 .. 19;
		Unused_20 at 0 range 20 .. 31;
	end record;

	-- GAHBCFG

	type Burst_Length is (
		Single,
		INCR,
		INCR4,
		INCR8)
	with Size => 4;
	for Burst_Length use (
		Single => 2#0000#,
		INCR   => 2#0001#,
		INCR4  => 2#0011#,
		INCR8  => 2#0101#);

	type FIFO_Empty_Level is (
		Half_Empty,
		Completely_Empty)
	with Size => 1;

	type General_AHB_Configuration_Register is record
		GINTMSK   : Boolean          := False;  -- Global interrupt mask (also GINT)
		HBSTLEN   : Burst_Length     := Single; -- Burst length/type (HS only)
		DMAEN     : Boolean          := False;  -- DMA enable (HS only)
		Unused_6  : Unused_1_Bit     := 0;
		TXFELVL   : FIFO_Empty_Level := Half_Empty; -- Non-periodic TxFIFO empty level
		PTXFELVL  : FIFO_Empty_Level := Half_Empty; -- Periodic TxFIFO empty
		Unused_9  : Unused_23_Bits;
	end record with Size => 32;
	for General_AHB_Configuration_Register use record
		GINTMSK   at 0 range  0 ..  0;
		HBSTLEN   at 0 range  1 ..  4;
		DMAEN     at 0 range  5 ..  5;
		Unused_6  at 0 range  6 ..  6;
		TXFELVL   at 0 range  7 ..  7;
		PTXFELVL  at 0 range  8 ..  8;
		Unused_9  at 0 range  9 .. 31;
	end record;

	-- GUSBCFG

	type PHY_Select is (
		External_ULPI,
		Internal)
	with Size => 1;

	subtype Turnaround_Time is Integer range 0 .. 15;

	type DLine_Pulsing is (
		UTMI_TxValid,
		UTMI_TermSel)
	with Size => 1;

	type General_USB_Configuration_Register is record
		TOCAL       : Integer range 0 .. 7  := 0;  -- FS timeout calibration
		Unused_3    : Unused_3_Bits      := 0;
		PHYSEL      : PHY_Select;                  -- USB 2.0 high-speed ULPI PHY or
		Unused_7    : Unused_1_Bit       := 0;
		SRPCAP      : Boolean            := False; -- SRP-capable
		HNPCAP      : Boolean            := False; -- HNP-capable
		TRDT        : Integer range 0 .. 15 := 5;  -- USB turnaround time
		Unused_14   : Unused_1_Bit       := 0;
		PHYLPCS     : Boolean            := False; -- PHY Low-power clock select
		Unused_16   : Unused_1_Bit       := 0;
		ULPIFSLS    : Boolean            := False; -- ULPI FS/LS interface select (HS only)
		ULPIAR      : Boolean            := False; -- ULPI Auto-resume (HS only)
		ULPICSM     : Boolean            := False; -- ULPI Clock SuspendM (HS only)
		ULPIEVBUSD  : Boolean            := False; -- ULPI External VBUS Drive (HS only)
		ULPIEVBUSI  : Boolean            := False; -- ULPI external VBUS Comparator (HS only)
		TSDPS       : DLine_Pulsing   := UTMI_TxValid; -- TermSel DLine pulsing selection (HS only)
		PCCI        : Boolean            := False; -- Indicator complement (HS only)
		PTCI        : Boolean            := False; -- Indicator pass through (HS only)
		ULPIIPD     : Boolean            := False; -- ULPI interface protection disable (HS only)
		Unused_26   : Unused_3_Bits      := 0;
		FHMOD       : Boolean            := False; -- Forced host mode
		FDMOD       : Boolean            := False; -- Forced peripheral device mode
		CTXPKT      : Boolean            := False; -- Corrupt Tx packet (used only for debug purposes!)
	end record with Size => 32;
	for General_USB_Configuration_Register use record
		TOCAL       at 0 range  0 ..  2;
		Unused_3    at 0 range  3 ..  5;
		PHYSEL      at 0 range  6 ..  6;
		Unused_7    at 0 range  7 ..  7;
		SRPCAP      at 0 range  8 ..  8;
		HNPCAP      at 0 range  9 ..  9;
		TRDT        at 0 range 10 .. 13;
		Unused_14   at 0 range 14 .. 14;
		PHYLPCS     at 0 range 15 .. 15;
		Unused_16   at 0 range 16 .. 16;
		ULPIFSLS    at 0 range 17 .. 17;
		ULPIAR      at 0 range 18 .. 18;
		ULPICSM     at 0 range 19 .. 19;
		ULPIEVBUSD  at 0 range 20 .. 20;
		ULPIEVBUSI  at 0 range 21 .. 21;
		TSDPS       at 0 range 22 .. 22;
		PCCI        at 0 range 23 .. 23;
		PTCI        at 0 range 24 .. 24;
		ULPIIPD     at 0 range 25 .. 25;
		Unused_26   at 0 range 26 .. 28;
		FHMOD       at 0 range 29 .. 29;
		FDMOD       at 0 range 30 .. 30;
		CTXPKT      at 0 range 31 .. 31;
	end record;

	-- OTG_HS_GRSTCTL

	type General_Reset_Register is record
		CSRST     : Boolean        := False; -- Core soft reset
		HSRST     : Boolean        := False; -- HCLK soft reset (may be not available)
		FCRST     : Boolean        := False; -- Host frame counter reset
		Unused_3  : Unused_1_Bit   := 0;
		RXFFLSH   : Boolean        := False; -- RxFIFO flush
		TXFFLSH   : Boolean        := False; -- TxFIFO flush
		TXFNUM    : Extended_Channel_Number := 0; -- TxFIFO number that must be flushed
		Unused_11 : Unused_19_Bits := 0;
		DMAREQ    : Boolean        := False; -- DMA request signal (HS only, debug)
		AHBIDL    : Boolean        := True;  -- AHB master idle
	end record with Size => 32;
	for General_Reset_Register use record
		CSRST     at 0 range  0 ..  0;
		HSRST     at 0 range  1 ..  1;
		FCRST     at 0 range  2 ..  2;
		Unused_3  at 0 range  3 ..  3;
		RXFFLSH   at 0 range  4 ..  4;
		TXFFLSH   at 0 range  5 ..  5;
		TXFNUM    at 0 range  6 .. 10;
		Unused_11 at 0 range 11 .. 29;
		DMAREQ    at 0 range 30 .. 30;
		AHBIDL    at 0 range 31 .. 31;
	end record;

	-- GINTSTS, GINTMSK

	type General_Core_Interrupt_Register is record
		CMOD               : OTG_Role      := USB_Host; -- Current mode of operation
		MMIS               : Boolean       := False; -- Mode mismatch interrupt
		OTGINT             : Boolean       := False; -- OTG interrupt
		SOF                : Boolean       := False; -- Start of frame
		RXFLVL             : Boolean       := False; -- Rx FIFO non-empty
		NPTXFE             : Boolean       := False; -- Nonperiodic Tx FIFO empty
		GINAKEFF           : Boolean       := False; -- Global IN nonperiodic NAK effective
		GONAKEFF           : Boolean       := False; -- Global OUT NAK effective
		Unused_8           : Unused_2_Bits := 0;
		ESUSP              : Boolean       := False; -- Early suspend
		USBSUSP            : Boolean       := False; -- USB suspend
		USBRST             : Boolean       := False; -- USB reset
		ENUMDNE            : Boolean       := False; -- Enumeration done
		ISOODRP            : Boolean       := False; -- Isochronous OUT packet dropped
		EOPF               : Boolean       := False; -- End of periodic frame
		Unused_16          : Unused_2_Bits := 0;
		IEPINT             : Boolean       := False; -- IN endpoint interrupt
		OEPINT             : Boolean       := False; -- OUT endpoint interrupt
		IISOIXFR           : Boolean       := False; -- Incomplete isochronous IN transfer
		IPXFR_INCOMPISOOUT : Boolean       := False; -- Incomplete periodic transfer
		DATAFSUSP          : Boolean       := False; -- Data fetch suspended (HS only)
		RSTDET             : Boolean       := False; -- Reset detected (FS only)
		HPRTINT            : Boolean       := False; -- Host port interrupt
		HCINT              : Boolean       := False; -- Host channels interrupt
		PTXFE              : Boolean       := False; -- Periodic TxFIFO empty
		LPMINT             : Boolean       := False; -- LPM interrupt (HS only?)
		CIDSCHG            : Boolean       := False; -- Connector ID status change
		DISCINT            : Boolean       := False; -- Disconnect detected
		SRQINT             : Boolean       := False; -- Session request/new session detected
		WKUPINT            : Boolean       := False; -- Resume/remote wakeup detected
	end record with Size => 32;
	for General_Core_Interrupt_Register use record
		CMOD               at 0 range  0 ..  0;
		MMIS               at 0 range  1 ..  1;
		OTGINT             at 0 range  2 ..  2;
		SOF                at 0 range  3 ..  3;
		RXFLVL             at 0 range  4 ..  4;
		NPTXFE             at 0 range  5 ..  5;
		GINAKEFF           at 0 range  6 ..  6;
		GONAKEFF           at 0 range  7 ..  7;
		Unused_8           at 0 range  8 ..  9;
		ESUSP              at 0 range 10 .. 10;
		USBSUSP            at 0 range 11 .. 11;
		USBRST             at 0 range 12 .. 12;
		ENUMDNE            at 0 range 13 .. 13;
		ISOODRP            at 0 range 14 .. 14;
		EOPF               at 0 range 15 .. 15;
		Unused_16          at 0 range 16 .. 17;
		IEPINT             at 0 range 18 .. 18;
		OEPINT             at 0 range 19 .. 19;
		IISOIXFR           at 0 range 20 .. 20;
		IPXFR_INCOMPISOOUT at 0 range 21 .. 21;
		DATAFSUSP          at 0 range 22 .. 22;
		RSTDET             at 0 range 23 .. 23;
		HPRTINT            at 0 range 24 .. 24;
		HCINT              at 0 range 25 .. 25;
		PTXFE              at 0 range 26 .. 26;
		LPMINT             at 0 range 27 .. 27;
		CIDSCHG            at 0 range 28 .. 28;
		DISCINT            at 0 range 29 .. 29;
		SRQINT             at 0 range 30 .. 30;
		WKUPINT            at 0 range 31 .. 31;
	end record;

	-- GRXSTSR, GRXSTSP

	subtype Packet_Byte_Count is Storage_Count range 0 .. 2**11 - 1;

	type Data_PID is (DATA0, DATA1, DATA2, MDATA) with Size => 2;
	SETUP : constant Data_PID := MDATA;

	type Packet_Status is (
		No_Packet_Status,
		Global_Out_NAK,
		Data_Packet_Received,
		Transfer_Completed,
		SETUP_Transaction_Completed,
		Data_Toggle_Error,
		SETUP_Data_Packet_Received,
		Channel_Halted)
	with Size => 4;
	for Packet_Status use (
		No_Packet_Status            => 2#0000#,
		Global_Out_NAK              => 2#0001#,
		Data_Packet_Received        => 2#0010#,
		Transfer_Completed          => 2#0011#,
		SETUP_Transaction_Completed => 2#0100#,
		Data_Toggle_Error           => 2#0101#,
		SETUP_Data_Packet_Received  => 2#0110#,
		Channel_Halted              => 2#0111#);

	type General_Receive_Status_Register is record
		CHNUM_EPNUM : Channel_Number;        -- Channel / Endpoint number
		BCNT        : Packet_Byte_Count;     -- Byte count
		DPID        : Data_PID;              -- Data PID
		PKTSTS      : Packet_Status;         -- Packet status
		FRMNUM      : Integer range 0 .. 15; -- Frame number 4 modulo 16 (device only)
		Unused_25   : Unused_2_Bits;
		STSPHST     : Boolean;               -- Status phase start (not present on most models?)
		Unused_28   : Unused_4_Bits;
	end record with Size => 32;
	for General_Receive_Status_Register use record
		CHNUM_EPNUM at 0 range 0 .. 3;
		BCNT        at 0 range 4 .. 14;
		DPID        at 0 range 15 .. 16;
		PKTSTS      at 0 range 17 .. 20;
		FRMNUM      at 0 range 21 .. 24;
		Unused_25   at 0 range 25 .. 26;
		STSPHST     at 0 range 27 .. 27;
		Unused_28   at 0 range 28 .. 31;
	end record;

	-- GNPTXFSIZ aka TX0FSIZ, HPTTXFSIZ, DIEPTXFx

	type Transmit_FIFO_Size_Register is record
		FSA : Integer range 0 .. 2**16 - 1; -- Transmit RAM start
		FD  : Integer range 0 .. 2**16 - 1; -- TxFIFO depth
	end record with Size => 32;
	for Transmit_FIFO_Size_Register use record
		FSA at 0 range 0 .. 15;
		FD  at 0 range 16 .. 31;
	end record;

	type Endpoint_Transmit_FIFO_Size_Registers is array (Channel_Number range 1 .. 7)
	of Transmit_FIFO_Size_Register;

	-- GNPTXSTS

	type Transmit_Info is (
		IN_OUT_Token,
		Zero_Length_Transmit_Packet,
		Channel_Halt_Command)
	with Size => 2;
	for Transmit_Info use (
		IN_OUT_Token                => 2#00#,
		Zero_Length_Transmit_Packet => 2#01#,
		Channel_Halt_Command        => 2#11#);

	type Non_Periodic_Transmit_Top is record
		Term      : Boolean;
		Info      : Transmit_Info; -- I really don't know how to name this field
		Channel   : Channel_Number;
		Frame     : Integer range 0 .. 1; -- Frame number modulo 2 (may not be available)
	end record with Size => 8;
	for Non_Periodic_Transmit_Top use record
		Term      at 0 range 0 .. 0;
		Info      at 0 range 1 .. 2;
		Channel   at 0 range 3 .. 6;
		Frame     at 0 range 7 .. 7;
	end record;

	type General_Non_Periodic_Tx_Status_Register is record
		NPTXFSAV  : Integer range 0 .. 2**16 - 1; -- Nonperiodic TxFIFO space available
		NPTQXSAV  : Integer range 0 .. 2**8 - 1; -- Nonperiodic transmit request queue space available
		NPTXQTOP  : Non_Periodic_Transmit_Top; -- Top of the nonperiodic transmit queue TODO:
	end record with Size => 32;
	for General_Non_Periodic_Tx_Status_Register use record
		NPTXFSAV  at 0 range 0 .. 15;
		NPTQXSAV  at 0 range 16 .. 23;
		NPTXQTOP  at 0 range 24 .. 31;
	end record;

	-- OTG_HS_GCCFG

	type Core_Configuration_Flag is range 0 .. 31;

	GCCR_PWRDWN : constant Core_Configuration_Flag := 16; -- Transceiver enable (True = enabled!)

	type General_Core_Configuration_Register is array (Core_Configuration_Flag) of Boolean
	with Pack, Size => 32;

	-- Note: Fields in GCCR are very specific to device.
	-- Always consult the manual no what to program. Do not change not defined fields.

	-- CID

	type Core_ID is new Unsigned_32;

	package Cores is
		F405    : constant Core_ID := 16#0000_1100#; -- Applies to whole family covered by RM0090
		F469_FS : constant Core_ID := 16#0000_2000#; -- Applies to many others
		F469_HS : constant Core_ID := 16#0000_2100#; -- Applies to many others
		-- Seems like contents of GCCR depends on this value
	end Cores;

	-- GLPMCFG

	type BES_Latency is (
		Latency_125_Microseconds,
		Latency_150_Microseconds,
		Latency_200_Microseconds,
		Latency_300_Microseconds,
		Latency_400_Microseconds,
		Latency_500_Microseconds,
		Latency_1_Millisecond,
		Latency_2_Milliseconds,
		Latency_3_Milliseconds,
		Latency_4_Milliseconds,
		Latency_5_Milliseconds,
		Latency_6_Milliseconds,
		Latency_7_Milliseconds,
		Latency_8_Milliseconds,
		Latency_9_Milliseconds,
		Latency_10_Milliseconds)
	with Size => 4;

	type BESL_Threshold is (
		Threshold_75_Microseconds,
		Threshold_100_Microseconds,
		Threshold_150_Microseconds,
		Threshold_250_Microseconds,
		Threshold_350_Microseconds,
		Threshold_450_Microseconds,
		Threshold_950_Microseconds)
	with Size => 4;
	for BESL_Threshold use (
		Threshold_75_Microseconds  => 2#0000#,
		Threshold_100_Microseconds => 2#0001#,
		Threshold_150_Microseconds => 2#0010#,
		Threshold_250_Microseconds => 2#0011#,
		Threshold_350_Microseconds => 2#0100#,
		Threshold_450_Microseconds => 2#0101#,
		Threshold_950_Microseconds => 2#0110#);

	type LPM_Response is (
		LPM_ACK,
		LPM_NYET,
		LPM_STALL,
		LPM_ERROR)
	with Size => 2;

	type Core_LPM_Configuration_Register is record
		LPMEN       : Boolean := False; -- LPM support enable
		LPMACK      : Boolean := False; -- LPM token acknowledge enable
		BESL        : BES_Latency := Latency_125_Microseconds; -- Best effort_service latency
		REMWAKE     : Boolean := False; -- bRemoteWake value
		L1SSEN      : Boolean := False; -- L1 Shallow Sleep enable
		BESLTHRS    : BESL_Threshold := Threshold_75_Microseconds; -- BESL threshold
		L1DSEN      : Boolean := False; -- L1 Deep Sleep enable
		LPMRST      : LPM_Response := LPM_ACK; -- LPM response
		SLPSTS      : Boolean := False; -- L1 Sleep status
		L1RSMOK     : Boolean := False; -- L1 Sleep state resume OK
		LPMCHIDX    : Channel_Number := 0; -- LPM channel index (host mode)
		LPMRCNT     : Integer range 0 .. 7 := 0; -- LPM retry count
		SNDLPM      : Boolean := False; -- Send LPM transaction
		LPMRCNTSTS  : Integer range 0 .. 7 := 0; -- LPM retry count status
		ENBESL      : Boolean      := False; -- Enable BESL
		Unused_29   : Unused_3_Bits := 0;
	end record with Size => 32;
	for Core_LPM_Configuration_Register use record
		LPMEN       at 0 range  0 ..  0;
		LPMACK      at 0 range  1 ..  1;
		BESL        at 0 range  2 ..  5;
		REMWAKE     at 0 range  6 ..  6;
		L1SSEN      at 0 range  7 ..  7;
		BESLTHRS    at 0 range  8 .. 11;
		L1DSEN      at 0 range 12 .. 12;
		LPMRST      at 0 range 13 .. 14;
		SLPSTS      at 0 range 15 .. 15;
		L1RSMOK     at 0 range 16 .. 16;
		LPMCHIDX    at 0 range 17 .. 20;
		LPMRCNT     at 0 range 21 .. 23;
		SNDLPM      at 0 range 24 .. 24;
		LPMRCNTSTS  at 0 range 25 .. 27;
		ENBESL      at 0 range 28 .. 28;
		Unused_29   at 0 range 29 .. 31;
	end record;

	--

	type OTG_General_Registers is record
		GOTGCTL  : General_Control_and_Status_Register;
		GOTGINT  : General_Interrupt_Register;
		GAHBCFG  : General_AHB_Configuration_Register;
		GUSBCFG  : General_USB_Configuration_Register;
		GRSTCTL  : General_Reset_Register;
		GINTSTS  : General_Core_Interrupt_Register;
		GINTMSK  : General_Core_Interrupt_Register;
		GRXSTSR  : General_Receive_Status_Register;
		GRXSTSP  : General_Receive_Status_Register;
		GRXFSIZ  : Integer range 0 .. 2**16 - 1;
		TX0FSIZ  : Transmit_FIFO_Size_Register; -- also GNPTXFSIZ
		GNPTXSTS : General_Non_Periodic_Tx_Status_Register;
		GCCFG    : General_Core_Configuration_Register;
		CID      : Core_ID;
		GLPMCFG  : Core_LPM_Configuration_Register;
		HPTXFSIZ : Transmit_FIFO_Size_Register;
		DIEPTXF  : Endpoint_Transmit_FIFO_Size_Registers;
	end record;
	for OTG_General_Registers use record
		GOTGCTL  at 16#00# range 0 .. 31;
		GOTGINT  at 16#04# range 0 .. 31;
		GAHBCFG  at 16#08# range 0 .. 31;
		GUSBCFG  at 16#0C# range 0 .. 31;
		GRSTCTL  at 16#10# range 0 .. 31;
		GINTSTS  at 16#14# range 0 .. 31;
		GINTMSK  at 16#18# range 0 .. 31;
		GRXSTSR  at 16#1C# range 0 .. 31;
		GRXSTSP  at 16#20# range 0 .. 31;
		GRXFSIZ  at 16#24# range 0 .. 31;
		TX0FSIZ  at 16#28# range 0 .. 31;
		GNPTXSTS at 16#2C# range 0 .. 31;
		GCCFG    at 16#38# range 0 .. 31;
		CID      at 16#3C# range 0 .. 31;
		GLPMCFG  at 16#54# range 0 .. 31;
		HPTXFSIZ at 16#100# range 0 .. 31;
		DIEPTXF  at 16#104# range 0 .. 32 * 7 - 1;
	end record;

	package Power_and_Clock is

		type Power_and_Clock_Gating_Register is record
			STPPCLK   : Boolean        := False; -- Stop PHY clock
			GATEHCLK  : Boolean        := False; -- Gate HCLK
			Unused_2  : Unused_2_Bits  := 0;
			PHYSUSP   : Boolean        := False; -- PHY suspended
			ENL1GTG   : Boolean        := False; -- Enable sleep clock gating
			PHYSLEEP  : Boolean        := False; -- PHY in sleep mode
			SUSP      : Boolean        := False; -- Deep sleep
			Unused_8  : Unused_24_Bits := 16#200B_80#;
		end record with Size => 32;
		for Power_and_Clock_Gating_Register use record
			STPPCLK   at 0 range  0 ..  0;
			GATEHCLK  at 0 range  1 ..  1;
			Unused_2  at 0 range  2 ..  3;
			PHYSUSP   at 0 range  4 ..  4;
			ENL1GTG   at 0 range  5 ..  5;
			PHYSLEEP  at 0 range  6 ..  6;
			SUSP      at 0 range  7 ..  7;
			Unused_8  at 0 range  8 .. 31;
		end record;

		type OTG_PWRCLK_Registers is record
			PCGCR : Power_and_Clock_Gating_Register;
		end record;
		for OTG_PWRCLK_Registers use record
			PCGCR at 16#00# range 0 .. 31;
		end record;

	end Power_and_Clock;

end STM32.USB_OTG;
