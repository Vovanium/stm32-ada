with STM32.Address_Map;

-- Analog to Digital Convertors
--
-- Units exist in all devices. Fx0, Fx1, Fx2, Fx3 devices have 1 ADC, others 3.

package STM32.Analog_to_Digital_Convertors is

	-- SR

	type Status_Register is record
		AWD         : Boolean := True; -- Analog watchdog flag
		EOC         : Boolean := True; -- Regular channel end of conversion
		JEOC        : Boolean := True; -- Injected channel end of conversion
		JSTRT       : Boolean := True; -- Injected channel start flag
		STRT        : Boolean := True; -- Regular channel start flag
		OVR         : Boolean := True; -- Overrun
		Reserved    : Integer range 0 .. 2**26 - 1 := 0;
	end record with Size => 32;

	for Status_Register use record
		AWD         at 0 range  0 ..  0;
		EOC         at 0 range  1 ..  1;
		JEOC        at 0 range  2 ..  2;
		JSTRT       at 0 range  3 ..  3;
		STRT        at 0 range  4 ..  4;
		OVR         at 0 range  5 ..  5;
		Reserved    at 0 range  6 .. 31;
	end record;

	-- CR1
	type Resolution is (
		Resolution_12_Bits,
		Resolution_10_Bits,
		Resolution_8_Bits,
		Resolution_6_Bits
	) with Size => 2;
	for Resolution use (
		Resolution_12_Bits => 2#00#,
		Resolution_10_Bits => 2#01#,
		Resolution_8_Bits  => 2#10#,
		Resolution_6_Bits  => 2#11#
	);

	type Control_Register_1 is record
		AWDCH       : Integer range 0 .. 18       := 0; -- Analog watchdog channel select
		EOCIE       : Boolean                     := False;    -- Interrupt enable for EOC
		AWDIE       : Boolean                     := False;    -- Analog watchdog interrupt enable
		JEOCIE      : Boolean                     := False;    -- Interrupt enable for injected channels
		SCAN        : Boolean                     := False;    -- Scan mode enable
		AWDSGL      : Boolean                     := False;    -- Enable watchdog on a single channel in scan mode
		JAUTO       : Boolean                     := False;    -- Automatic injected group conversion
		DISCEN      : Boolean                     := False;    -- Discontinuous mode on regular channels enable
		JDISCEN     : Boolean                     := False;    -- Discontinuous mode on injected channels enable
		DISCNUM     : Integer range 0 .. 2**3 - 1 := 0; -- Discontinuous mode channel count minus 1
		Reserved_16 : Integer range 0 .. 2**6 - 1 := 0;
		JAWDEN      : Boolean                     := False;    -- Analog watchdog enable on injected channels
		AWDEN       : Boolean                     := False;   -- Analog watchdog enable on regular channels
		RES         : Resolution                  := Resolution_12_Bits; -- ADC resolution
		OVRIE       : Boolean                     := False;    -- Overrun interrupt enable
		Reserved_27 : Integer range 0 .. 2**5 - 1 := 0;
	end record with Size => 32;
	for Control_Register_1 use record
		AWDCH       at 0 range  0 ..  4;
		EOCIE       at 0 range  5 ..  5;
		AWDIE       at 0 range  6 ..  6;
		JEOCIE      at 0 range  7 ..  7;
		SCAN        at 0 range  8 ..  8;
		AWDSGL      at 0 range  9 ..  9;
		JAUTO       at 0 range 10 .. 10;
		DISCEN      at 0 range 11 .. 11;
		JDISCEN     at 0 range 12 .. 12;
		DISCNUM     at 0 range 13 .. 15;
		Reserved_16 at 0 range 16 .. 21;
		JAWDEN      at 0 range 22 .. 22;
		AWDEN       at 0 range 23 .. 23;
		RES         at 0 range 24 .. 25;
		OVRIE       at 0 range 26 .. 26;
		Reserved_27 at 0 range 27 .. 31;
	end record;

	-- CR2
	type DMA_Disable is (
		Stop_after_Last,
		Continue
	) with Size => 1;
	for DMA_Disable use (
		Stop_after_Last => 0,
		Continue        => 1
	);

	type End_of_Conversion_Select is (
		End_of_Sequence,
		End_of_Conversion
	) with Size => 1;
	for End_of_Conversion_Select use (
		End_of_Sequence   => 0,
		End_of_Conversion => 1
	);

	type Data_Alignment is (
		Right,
		Left
	) with Size => 1;
	for Data_Alignment use (
		Right => 0,
		Left  => 1
	);

	type External_Event_Injected is (
		Timer_1_CC4,
		Timer_1_TRGO,
		Timer_2_CC1,
		Timer_2_TRGO,
		Timer_3_CC2,
		Timer_3_CC4,
		Timer_4_CC1,
		Timer_4_CC2,
		Timer_4_CC3,
		Timer_4_TRGO,
		Timer_5_CC4,
		Timer_5_TRGO,
		Timer_8_CC2,
		Timer_8_CC3,
		Timer_8_CC4,
		EXTI_Line15
	) with Size => 4;
	for External_Event_Injected use (
		Timer_1_CC4  => 2#0000#,
		Timer_1_TRGO => 2#0001#,
		Timer_2_CC1  => 2#0010#,
		Timer_2_TRGO => 2#0011#,
		Timer_3_CC2  => 2#0100#,
		Timer_3_CC4  => 2#0101#,
		Timer_4_CC1  => 2#0110#,
		Timer_4_CC2  => 2#0111#,
		Timer_4_CC3  => 2#1000#,
		Timer_4_TRGO => 2#1001#,
		Timer_5_CC4  => 2#1010#,
		Timer_5_TRGO => 2#1011#,
		Timer_8_CC2  => 2#1100#,
		Timer_8_CC3  => 2#1101#,
		Timer_8_CC4  => 2#1110#,
		EXTI_Line15  => 2#1111#
	);

	type External_Event_Regular is (
		Timer_1_CC1,
		Timer_1_CC2,
		Timer_1_CC3,
		Timer_2_CC2,
		Timer_2_CC3,
		Timer_2_CC4,
		Timer_2_TRGO,
		Timer_3_CC1,
		Timer_3_TRGO,
		Timer_4_CC4,
		Timer_5_CC1,
		Timer_5_CC2,
		Timer_5_CC3,
		Timer_8_CC1,
		Timer_8_TRGO,
		EXTI_Line11
	) with Size => 4;
	for External_Event_Regular use (
		Timer_1_CC1  => 2#0000#,
		Timer_1_CC2  => 2#0001#,
		Timer_1_CC3  => 2#0010#,
		Timer_2_CC2  => 2#0011#,
		Timer_2_CC3  => 2#0100#,
		Timer_2_CC4  => 2#0101#,
		Timer_2_TRGO => 2#0110#,
		Timer_3_CC1  => 2#0111#,
		Timer_3_TRGO => 2#1000#,
		Timer_4_CC4  => 2#1001#,
		Timer_5_CC1  => 2#1010#,
		Timer_5_CC2  => 2#1011#,
		Timer_5_CC3  => 2#1100#,
		Timer_8_CC1  => 2#1101#,
		Timer_8_TRGO => 2#1110#,
		EXTI_Line11  => 2#1111#
	);

	type External_Trigger_Enable is (
		Disabled,
		Rising_Edge,
		Falling_Edge,
		Both_Edges
	) with Size => 2;
	for External_Trigger_Enable use (
		Disabled     => 2#00#,
		Rising_Edge  => 2#01#,
		Falling_Edge => 2#10#,
		Both_Edges   => 2#11#
	);

	type Control_Register_2 is record
		ADON        : Boolean                     := False;
		CONT        : Boolean                     := False;
		Reserved_2  : Integer range 0 .. 2**6 - 1 := 0;
		DMA         : Boolean                     := False;
		DDS         : DMA_Disable                 := Stop_after_Last;
		EOCS        : End_of_Conversion_Select    := End_of_Sequence;
		ALIGN       : Data_Alignment              := Right;
		Reserved_12 : Integer range 0 .. 15       := 0;
		JEXTSEL     : External_Event_Injected     := Timer_1_CC4;
		JEXTEN      : External_Trigger_Enable     := Disabled;
		JSWSTART    : Boolean                     := False; -- Start conversion of injected channels
		EXTSEL      : External_Event_Regular      := Timer_1_CC1;
		EXTEN       : External_Trigger_Enable     := Disabled; -- External trigger enable for regular channels
		SWSTART     : Boolean                     := False; -- Start conversion of regular channels
		Reserved_31 : Integer range 0 .. 1        := 0;
	end record with Size => 32;
	for Control_Register_2 use record
		ADON        at 0 range  0 ..  0;
		CONT        at 0 range  1 ..  1;
		Reserved_2  at 0 range  2 ..  7;
		DMA         at 0 range  8 ..  8;
		DDS         at 0 range  9 ..  9;
		EOCS        at 0 range 10 .. 10;
		ALIGN       at 0 range 11 .. 11;
		Reserved_12 at 0 range 12 .. 15;
		JEXTSEL     at 0 range 16 .. 19;
		JEXTEN      at 0 range 20 .. 21;
		JSWSTART    at 0 range 22 .. 22;
		EXTSEL      at 0 range 24 .. 27;
		EXTEN       at 0 range 28 .. 29;
		SWSTART     at 0 range 30 .. 30;
		Reserved_31 at 0 range 31 .. 31;
	end record;

	-- SMPRx
	type Sampling_Time is (
		Sample_3_Cycles,
		Sample_15_Cycles,
		Sample_28_Cycles,
		Sample_56_Cycles,
		Sample_84_Cycles,
		Sample_112_Cycles,
		Sample_144_Cycles,
		Sample_480_Cycles
	) with Size => 3;
	for Sampling_Time use (
		Sample_3_Cycles   => 2#000#,
		Sample_15_Cycles  => 2#001#,
		Sample_28_Cycles  => 2#010#,
		Sample_56_Cycles  => 2#011#,
		Sample_84_Cycles  => 2#100#,
		Sample_112_Cycles => 2#101#,
		Sample_144_Cycles => 2#110#,
		Sample_480_Cycles => 2#111#
	);

	type Sample_Time_Array is array (Integer range <>) of Sampling_Time with Pack;

	type Sample_Time_Register_1 is record
		SMP         : Sample_Time_Array (10 .. 18) := (others => Sample_3_Cycles);
		Reserved    : Integer range 0 .. 2**5 - 1  := 0;
	end record with Size => 32;
	for Sample_Time_Register_1 use record
		SMP         at 0 range  0 .. 26;
		Reserved    at 0 range 27 .. 31;
	end record;

	type Sample_Time_Register_2 is record
		SMP         : Sample_Time_Array (0 .. 9) := (others => Sample_3_Cycles);
		Reserved    : Integer range 0 .. 3       := 0;
	end record with Size => 32;
	for Sample_Time_Register_2 use record
		SMP         at 0 range  0 .. 29;
		Reserved    at 0 range 30 .. 31;
	end record;

	-- SQRx
	subtype Channel_Index is Integer range 0 .. 18;
	type Channel_Sequence_Array is array (Integer range <>) of Channel_Index with Pack, Component_Size => 5;
	type Regular_Sequence_Register_1 is record
		SQ          : Channel_Sequence_Array (13 .. 16) := (others => 0);
		L           : Integer range 0 .. 2**4 - 1       := 0; -- Regular channel sequence length minus 1
		Reserved    : Integer range 0 .. 2**8 - 1       := 0;
	end record with Size => 32;
	for Regular_Sequence_Register_1 use record
		SQ          at 0 range 0 .. 19;
		L           at 0 range 20 .. 23;
		Reserved    at 0 range 24 .. 31;
	end record;

	type Regular_Sequence_Register_2 is record
		SQ          : Channel_Sequence_Array (7 .. 12) := (others => 0);
		Reserved    : Integer range 0 .. 3             := 0;
	end record with Size => 32;
	for Regular_Sequence_Register_2 use record
		SQ          at 0 range  0 .. 29;
		Reserved    at 0 range 30 .. 31;
	end record;

	type Regular_Sequence_Register_3 is record
		SQ          : Channel_Sequence_Array (1 .. 6) := (others => 0);
		Reserved    : Integer range 0 .. 3            := 0;
	end record with Size => 32;
	for Regular_Sequence_Register_3 use record
		SQ          at 0 range  0 .. 29;
		Reserved    at 0 range 30 .. 31;
	end record;

	type Injected_Sequence_Register is record
		JSQ         : Channel_Sequence_Array (1 .. 4) := (others => 0);
		JL          : Integer range 0 .. 3            := 0; -- Injected channel sequence length minus 1
		Reserved    : Integer range 0 .. 2**10 - 1    := 0;
	end record with Size => 32;
	for Injected_Sequence_Register use record
		JSQ         at 0 range  0 .. 19;
		JL          at 0 range 20 .. 21;
		Reserved    at 0 range 22 .. 31;
	end record;


	type DR_Register is record
		DATA : Integer range 0 .. 2**16 - 1;

	end record with Size => 32;
	for DR_Register use record
		DATA at 0 range 0 .. 15;

	end record;

	type ADC_Registers is record
		SR    : Status_Register;
		pragma Volatile_Full_Access (SR);
		CR1   : Control_Register_1;
		pragma Volatile_Full_Access (CR1);
		CR2   : Control_Register_2;
		pragma Volatile_Full_Access (CR2);
		SMPR1 : Sample_Time_Register_1;
		pragma Volatile_Full_Access (SMPR1);
		SMPR2 : Sample_Time_Register_2;
		pragma Volatile_Full_Access (SMPR2);
		JOFR1 : Integer range 0 .. 2**12 - 1 := 0;
		pragma Volatile_Full_Access (JOFR1);
		JOFR2 : Integer range 0 .. 2**12 - 1 := 0;
		pragma Volatile_Full_Access (JOFR2);
		JOFR3 : Integer range 0 .. 2**12 - 1 := 0;
		pragma Volatile_Full_Access (JOFR3);
		JOFR4 : Integer range 0 .. 2**12 - 1 := 0;
		pragma Volatile_Full_Access (JOFR4);
		HTR   : Integer range 0 .. 2**12 - 1 := 16#0000_0FFF#;
		pragma Volatile_Full_Access (HTR);
		LTR   : Integer range 0 .. 2**12 - 1 := 16#0000_0000#;
		pragma Volatile_Full_Access (LTR);
		SQR1  : Regular_Sequence_Register_1;
		pragma Volatile_Full_Access (SQR1);
		SQR2  : Regular_Sequence_Register_2;
		pragma Volatile_Full_Access (SQR2);
		SQR3  : Regular_Sequence_Register_3;
		pragma Volatile_Full_Access (SQR3);
		JSQR  : Injected_Sequence_Register;
		pragma Volatile_Full_Access (JSQR);
		JDR1  : Integer range 0 .. 2**16 - 1  := 0;
		pragma Volatile_Full_Access (JDR1);
		JDR2  : Integer range 0 .. 2**16 - 1  := 0;
		pragma Volatile_Full_Access (JDR2);
		JDR3  : Integer range 0 .. 2**16 - 1  := 0;
		pragma Volatile_Full_Access (JDR3);
		JDR4  : Integer range 0 .. 2**16 - 1  := 0;
		pragma Volatile_Full_Access (JDR4);
		DR    : Integer range 0 .. 2**16 - 1  := 0;
		pragma Volatile_Full_Access (DR);
	end record;
	for ADC_Registers use record
		SR    at 16#00# range 0 .. 31;
		CR1   at 16#04# range 0 .. 31;
		CR2   at 16#08# range 0 .. 31;
		SMPR1 at 16#0C# range 0 .. 31;
		SMPR2 at 16#10# range 0 .. 31;
		JOFR1 at 16#14# range 0 .. 31;
		JOFR2 at 16#18# range 0 .. 31;
		JOFR3 at 16#1C# range 0 .. 31;
		JOFR4 at 16#20# range 0 .. 31;
		HTR   at 16#24# range 0 .. 31;
		LTR   at 16#28# range 0 .. 31;
		SQR1  at 16#2C# range 0 .. 31;
		SQR2  at 16#30# range 0 .. 31;
		SQR3  at 16#34# range 0 .. 31;
		JSQR  at 16#38# range 0 .. 31;
		JDR1  at 16#3C# range 0 .. 31;
		JDR2  at 16#40# range 0 .. 31;
		JDR3  at 16#44# range 0 .. 31;
		JDR4  at 16#48# range 0 .. 31;
		DR    at 16#4C# range 0 .. 31;
	end record;

	-- CSR

	type ADC_Status is record
		AWD         : Boolean;
		EOC         : Boolean;
		JEOC        : Boolean;
		JSTRT       : Boolean;
		STRT        : Boolean;
		OVR         : Boolean;
		Reserved    : Integer range 0 .. 3;
	end record with Size => 8;
	for ADC_Status use record
		AWD         at 0 range 0 .. 0;
		EOC         at 0 range 1 .. 1;
		JEOC        at 0 range 2 .. 2;
		JSTRT       at 0 range 3 .. 3;
		STRT        at 0 range 4 .. 4;
		OVR         at 0 range 5 .. 5;
		Reserved    at 0 range 6 .. 7;
	end record;

	type ADC_Status_Array is array (Integer range <>) of ADC_Status with Pack;

	type Common_Status_Register is record
		ADC         : ADC_Status_Array (1 .. 3);
		Reserved    : Integer range 0 .. 2**8 - 1;
	end record with Size => 32;
	for Common_Status_Register use record
		ADC         at 0 range 0 .. 23;
		Reserved    at 0 range 24 .. 31;
	end record;

	-- CCR
	type Multi_ADC_Mode is (
		Independent,
		Dual_Regular_and_Injected,
		Dual_Regular_and_Alternate_Trigger,
		Dual_Injected,
		Dual_Regular,
		Dual_Interleaved,
		Dual_Alternate_Trigger,
		Triple_Regular_and_Injected,
		Triple_Regular_and_Alternate_Trigger,
		Triple_Injected,
		Triple_Regular,
		Triple_Interleaved,
		Triple_Alternate_Trigger
	) with Size => 5;
	for Multi_ADC_Mode use (
		Independent                          => 2#00000#,
		Dual_Regular_and_Injected            => 2#00001#,
		Dual_Regular_and_Alternate_Trigger   => 2#00010#,
		Dual_Injected                        => 2#00101#,
		Dual_Regular                         => 2#00110#,
		Dual_Interleaved                     => 2#00111#,
		Dual_Alternate_Trigger               => 2#01001#,
		Triple_Regular_and_Injected          => 2#10001#,
		Triple_Regular_and_Alternate_Trigger => 2#10010#,
		Triple_Injected                      => 2#10101#,
		Triple_Regular                       => 2#10110#,
		Triple_Interleaved                   => 2#10111#,
		Triple_Alternate_Trigger             => 2#11001#
	);

	type DMA_Mode is (
		Disabled,
		Half_Words_Single,
		Half_Words_Pairs,
		Bytes_Pairs
	) with Size => 2;
	for DMA_Mode use (
		Disabled          => 2#00#,
		Half_Words_Single => 2#01#,
		Half_Words_Pairs  => 2#10#,
		Bytes_Pairs       => 2#11#
	);

	type ADC_Prescaler is (
		PCLK_Div_2,
		PCLK_Div_4,
		PCLK_Div_6,
		PCLK_Div_8
	) with Size => 2;
	for ADC_Prescaler use (
		PCLK_Div_2 => 2#00#,
		PCLK_Div_4 => 2#01#,
		PCLK_Div_6 => 2#10#,
		PCLK_Div_8 => 2#11#
	);

	type Common_Control_Register is record
		MULTI       : Multi_ADC_Mode              := Independent; -- Multi ADC mode selection
		Reserved_5  : Integer range 0 .. 7        := 0;
		DLAY        : Integer range 0 .. 2**4 - 1 := 0; -- Delay between 2 sampling phases minus 5
		Reserved_12 : Integer range 0 .. 1        := 0;
		DDS         : DMA_Disable                 := Stop_After_Last; -- DMA disable selection in multi ADC mode
		DMA         : DMA_Mode                    := Disabled; -- DMA mode
		ADCPRE      : ADC_Prescaler               := PCLK_Div_2; -- ADC prescaler
		Reserved_18 : Integer range 0 .. 15       := 0;
		VBATE       : Boolean                     := False; -- Vbat enable
		TSVREFE     : Boolean                     := False; -- Temperature sensor and Vrefint enable
		Reserved_24 : Integer range 0 .. 2**8 - 1 := 0;
	end record with Size => 32;
	for Common_Control_Register use record
		MULTI       at 0 range  0 ..  4;
		Reserved_5  at 0 range  5 ..  7;
		DLAY        at 0 range  8 .. 11;
		Reserved_12 at 0 range 12 .. 12;
		DDS         at 0 range 13 .. 13;
		DMA         at 0 range 14 .. 15;
		ADCPRE      at 0 range 16 .. 17;
		Reserved_18 at 0 range 18 .. 21;
		VBATE       at 0 range 22 .. 22;
		TSVREFE     at 0 range 23 .. 23;
		Reserved_24 at 0 range 24 .. 31;
	end record;

	type Common_Data_Register is record
		DATA1 : Integer range 0 .. 2**16 - 1;
		DATA2 : Integer range 0 .. 2**16 - 1;
	end record with Size => 32;
	for Common_Data_Register use record
		DATA1 at 0 range 0 .. 15;
		DATA2 at 0 range 16 .. 31;
	end record;

	type Common_ADC_Registers is record
		CSR : Common_Status_Register;
		pragma Volatile_Full_Access (CSR);
		CCR : Common_Control_Register;
		pragma Volatile_Full_Access (CCR);
		CDR : Common_Data_Register;
		pragma Volatile_Full_Access (CDR);
	end record with Volatile;
	for Common_ADC_Registers use record
		CSR at 16#00# range 0 .. 31;
		CCR at 16#04# range 0 .. 31;
		CDR at 16#08# range 0 .. 31;
	end record;

	ADC1      : aliased ADC_Registers with Volatile, Import, Address => Address_Map.ADC1;
	ADC2      : aliased ADC_Registers with Volatile, Import, Address => Address_Map.ADC2;
	ADC3      : aliased ADC_Registers with Volatile, Import, Address => Address_Map.ADC3;
	ADC_Common : Common_ADC_Registers with Volatile, Import, Address => Address_Map.ADC_Common;

end STM32.Analog_to_Digital_Convertors;
