with STM32.Address_Map;

-- Digital Camera Interface

package STM32.Digital_Camera_Interface is

	-- CR

	type Capture_Mode is (
		Continuous,
		Snapshot
	) with Size => 1;
	for Capture_Mode use (
		Continuous => 0,
		Snapshot   => 1
	);

	type Clock_Polarity is (
		Capture_on_Falling, -- Input pixel clock
		Capture_on_Rising -- Inverted pixel clock
	) with Size => 1;
	for Clock_Polarity use (
		Capture_on_Falling => 0,
		Capture_on_Rising  => 1
	);

	type Signal_Polarity is (
		Active_Low,
		Active_High
	) with Size => 1;
	for Signal_Polarity use (
		Active_Low  => 0,
		Active_High => 1
	);

	type Frame_Capture_Rate is (
		Every_Frame,
		Frames_1_of_2,
		Frames_1_of_4
	) with Size => 2;
	for Frame_Capture_Rate use (
		Every_Frame   => 2#00#,
		Frames_1_of_2 => 2#01#,
		Frames_1_of_4 => 2#10#
	);

	type Extended_Data_Mode is (
		Data_8_Bit,
		Data_10_Bit,
		Data_12_Bit,
		Data_14_Bit
	) with Size => 2;
	for Extended_Data_Mode use (
		Data_8_Bit  => 2#00#,
		Data_10_Bit => 2#01#,
		Data_12_Bit => 2#10#,
		Data_14_Bit => 2#11#
	);

	type Byte_Select is (
		All_Bytes,
		Bytes_1_of_2,
		Bytes_1_of_4,
		Bytes_2_of_4
	) with Size => 2;
	for Byte_Select use (
		All_Bytes    => 2#00#,
		Bytes_1_of_2 => 2#01#,
		Bytes_1_of_4 => 2#10#,
		Bytes_2_of_4 => 2#11#
	);

	type Line_Select is (
		Every_Line,
		Lines_1_of_2
	) with Size => 1;
	for Line_Select use (
		Every_Line   => 0,
		Lines_1_of_2 => 1
	);

	type Even_Odd_Select is (
		First,
		Second
	) with Size => 1;
	for Even_Odd_Select use (
		First  => 0,
		Second => 1
	);

	type Control_Register_1 is record
		CAPTURE     : Boolean            := False; -- Capture enable
		CM          : Capture_Mode       := Continuous; -- Capture mode
		CROP        : Boolean            := False; -- Crop feature enable
		JPEG        : Boolean            := False; -- JPEG format
		ESS         : Boolean            := False; -- Embedded synchronization
		PCKPOL      : Clock_Polarity     := Capture_on_Falling; -- Pixel clock polarity
		HSPOL       : Signal_Polarity    := Active_Low;  -- Horizontal synchronization
		VSPOL       : Signal_Polarity    := Active_Low;  -- Vertical synchronization
		FCRC        : Frame_Capture_Rate := Every_Frame; -- Frame capture rate control
		EDM         : Extended_Data_Mode := Data_8_Bit;  -- Extended data mode
		Unused_12   : Integer range 0 .. 3 := 0;
		ENABLE      : Boolean            := False;      -- DCMI enable
		Unused_15   : Integer range 0 .. 1 := 0;
		BSM         : Byte_Select        := All_Bytes;  -- Byte Select mode (available only on STM43F446)
		OEBS        : Even_Odd_Select    := First;      -- Odd/Even Byte Select (available only on STM43F446)
		LSM         : Line_Select        := Every_Line; -- Line Select mode (available only on STM43F446)
		OELS        : Even_Odd_Select    := First;      -- Odd/Even Line Select (available only on STM43F446)
		Unused_21   : Integer range 0 .. 2**11 - 1 := 0;
	end record with Size => 32;
	for Control_Register_1 use record
		CAPTURE     at 0 range  0 ..  0;
		CM          at 0 range  1 ..  1;
		CROP        at 0 range  2 ..  2;
		JPEG        at 0 range  3 ..  3;
		ESS         at 0 range  4 ..  4;
		PCKPOL      at 0 range  5 ..  5;
		HSPOL       at 0 range  6 ..  6;
		VSPOL       at 0 range  7 ..  7;
		FCRC        at 0 range  8 ..  9;
		EDM         at 0 range 10 .. 11;
		Unused_12   at 0 range 12 .. 13;
		ENABLE      at 0 range 14 .. 14;
		Unused_15   at 0 range 15 .. 15;
		BSM         at 0 range 16 .. 17;
		OEBS        at 0 range 18 .. 18;
		LSM         at 0 range 19 .. 19;
		OELS        at 0 range 20 .. 20;
		Unused_21   at 0 range 21 .. 31;
	end record;

	-- SR

	type Status_Register is record
		HSYNC       : Boolean; -- HSYNC in active state (synchronization between lines)
		VSYNC       : Boolean; -- VSYNC in active state (synchronization between frames)
		FNE         : Boolean; -- FIFO not empty
	end record with Size => 32;
	for Status_Register use record
		HSYNC       at 0 range  0 ..  0;
		VSYNC       at 0 range  1 ..  1;
		FNE         at 0 range  2 ..  2;
	end record;

	-- RIS, IER, MIS, ICR

	type Interrupt_Register is record
		FRAME       : Boolean := False; -- Capture complete raw interrupt
		OVR         : Boolean := False; -- Overrun raw interrupt
		ERR         : Boolean := False; -- Synchronization error raw inte
		VSYNC       : Boolean := False; -- VSYNC raw interrupt status
		LINE        : Boolean := False; -- Line raw interrupt status
		Unused_5    : Integer range 0 .. 2**27 - 1 := 0;
	end record with Size => 32;
	for Interrupt_Register use record
		FRAME       at 0 range  0 ..  0;
		OVR         at 0 range  1 ..  1;
		ERR         at 0 range  2 ..  2;
		VSYNC       at 0 range  3 ..  3;
		LINE        at 0 range  4 ..  4;
		Unused_5    at 0 range  5 .. 31;
	end record;

	-- ESCR, ESUR

	type Embedded_Synchronization_Register is record
		FS          : Unsigned_8 := 0; -- Frame start delimiter code / unmask
		LS          : Unsigned_8 := 0; -- Line start delimiter code / unmask
		LE          : Unsigned_8 := 0; -- Line end delimiter code / unmask
		FE          : Unsigned_8 := 0; -- Frame end delimiter code / unmask
	end record with Size => 32;
	for Embedded_Synchronization_Register use record
		FS          at 0 range  0 ..  7;
		LS          at 0 range  8 .. 15;
		LE          at 0 range 16 .. 23;
		FE          at 0 range 24 .. 31;
	end record;

	-- CWSTRT

	type Crop_Window_Start_Register is record
		HOFFCNT     : Integer range 0 .. 2**14 - 1 := 0; -- Horizontal offset count
		Unused_14   : Integer range 0 .. 3         := 0;
		VST         : Integer range 0 .. 2**13 - 1 := 0; -- Vertical start line count
		Unused_29   : Integer range 0 .. 7         := 0;

	end record with Size => 32;
	for Crop_Window_Start_Register use record
		HOFFCNT     at 0 range  0 .. 13;
		Unused_14   at 0 range 14 .. 15;
		VST         at 0 range 16 .. 28;
		Unused_29   at 0 range 29 .. 31;

	end record;

	-- CWSIZE

	type Crop_Window_Size_Register is record
		CAPCNT      : Integer range 0 .. 2**14 - 1 := 0; -- Capture byte count minus 1
		Unused_14   : Integer range 0 .. 3         := 0;
		VLINE       : Integer range 0 .. 2**14 - 1 := 0; -- Vertical line count minus 1
		Unused_30   : Integer range 0 .. 3         := 0;
	end record with Size => 32;
	for Crop_Window_Size_Register use record
		CAPCNT      at 0 range  0 .. 13;
		Unused_14   at 0 range 14 .. 15;
		VLINE       at 0 range 16 .. 29;
		Unused_30   at 0 range 30 .. 31;
	end record;

	-- DR

	type Data_Register is array (0 .. 3) of Unsigned_8 with Pack, Size => 32;

	type DCMI_Registers is record
		CR          : Control_Register_1;
		pragma Volatile_Full_Access (CR);
		SR          : Status_Register;
		pragma Volatile_Full_Access (SR);
		RIS         : Interrupt_Register;
		pragma Volatile_Full_Access (RIS);
		IER         : Interrupt_Register;
		pragma Volatile_Full_Access (IER);
		MIS         : Interrupt_Register;
		pragma Volatile_Full_Access (MIS);
		ICR         : Interrupt_Register;
		pragma Volatile_Full_Access (ICR);
		ESCR        : Embedded_Synchronization_Register;
		pragma Volatile_Full_Access (ESCR);
		ESUR        : Embedded_Synchronization_Register;
		pragma Volatile_Full_Access (ESUR);
		CWSTRT      : Crop_Window_Start_Register;
		pragma Volatile_Full_Access (CWSTRT);
		CWSIZE      : Crop_Window_Size_Register;
		pragma Volatile_Full_Access (CWSIZE);
		DR          : Data_Register;
		pragma Volatile_Full_Access (DR);
	end record;
	for DCMI_Registers use record
		CR          at 16#00# range  0 .. 31;
		SR          at 16#04# range  0 .. 31;
		RIS         at 16#08# range  0 .. 31;
		IER         at 16#0C# range  0 .. 31;
		MIS         at 16#10# range  0 .. 31;
		ICR         at 16#14# range  0 .. 31;
		ESCR        at 16#18# range  0 .. 31;
		ESUR        at 16#1C# range  0 .. 31;
		CWSTRT      at 16#20# range  0 .. 31;
		CWSIZE      at 16#24# range  0 .. 31;
		DR          at 16#28# range  0 .. 31;
	end record;

	DCMI : DCMI_Registers with Volatile, Import, Address => Address_Map.DCMI;

end STM32.Digital_Camera_Interface;
