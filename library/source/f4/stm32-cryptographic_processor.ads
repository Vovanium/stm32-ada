with STM32.Address_Map;

-- Cryptographic Processor

package STM32.Cryptographic_Processor is

	-- Register correspondence

	-- CSGCMCCMxR -- CSGCMCCMR (x)
	-- CSGCMxR    -- CSGCMR (x)
	-- IVxLR      -- IVR (x, Left)
	-- IVxRR      -- IVR (x, Right)
	-- KxLR       -- KR (x, Left)
	-- KxRR       -- KR (x, Right)

	-- Field correspondence

	-- INIM   -- INI
	-- INMIS  -- INI
	-- INRIS  -- INI
	-- OUTIM  -- OUTI
	-- OUTMIS -- OUTI
	-- OUTRIS -- OUTI

	-- CR

	type Algorithm_Direction is (
		Encrypt,
		Decrypt
	);
	for Algorithm_Direction use (
		Encrypt => 0,
		Decrypt => 1
	);

	type Algorithm_Mode_0 is mod 8;
	type Algorithm_Mode_3 is mod 2;

	type Algorithm_Mode is (
		TDES_ECB,
		TDES_CBC,
		DES_ECB,
		DES_CBC,
		AES_ECB,
		AES_CBC,
		AES_CTR,
		AES_Key_Preparation,
		GCM,
		Counter_With_CBC_MAC
	);
	for Algorithm_Mode use (
		TDES_ECB             => 2#0000#,
		TDES_CBC             => 2#0001#,
		DES_ECB              => 2#0010#,
		DES_CBC              => 2#0011#,
		AES_ECB              => 2#0100#,
		AES_CBC              => 2#0101#,
		AES_CTR              => 2#0110#,
		AES_Key_Preparation  => 2#0111#,
		GCM                  => 2#1000#,
		Counter_With_CBC_MAC => 2#1001#
	);

	function To_Mode_0 (Mode : Algorithm_Mode) return Algorithm_Mode_0 is (
		Algorithm_Mode_0 (Unsigned_32 (Algorithm_Mode'Pos (Mode)) and 2#0111#)
	);
	-- ALGOMODE0 field value of a given algorithm mode

	function To_Mode_3 (Mode : Algorithm_Mode) return Algorithm_Mode_3 is (
		Algorithm_Mode_3 (Shift_Right (Unsigned_32 (Algorithm_Mode'Pos (Mode)), 3))
	);
	-- ALGOMODE3 field value of a given algorithm mode

	function To_Mode (Mode_3 : Algorithm_Mode_3; Mode_0 : Algorithm_Mode_0) return Algorithm_Mode is (
		Algorithm_Mode'Val (Shift_Left (Unsigned_32 (Mode_3), 3) or Unsigned_32 (Mode_0))
	);
	-- Composes ALGOMODE0 and ALGOMODE3 to a single value of algorithm mode

	type Data_Type is (
		Data_32_Bit, -- No swapping of data
		Data_16_Bit, -- Each word consist of two half-words which are swapped with each other
		Data_8_Bit,  -- Each word consist of four bytes in reverse order
		Data_1_Bit   -- Each word is reversed bit-wise
	) with Size => 2;
	for Data_Type use (
		Data_32_Bit => 2#00#,
		Data_16_Bit => 2#01#,
		Data_8_Bit  => 2#10#,
		Data_1_Bit  => 2#11#
	);

	type Key_Size is (
		Key_128_Bit,
		Key_192_Bit,
		Key_256_Bit
	) with Size => 2;
	for Key_Size use (
		Key_128_Bit => 2#00#,
		Key_192_Bit => 2#01#,
		Key_256_Bit => 2#10#
	);

	type CCM_Phase is (
		Initialization,
		Header,
		Payload,
		Final
	) with Size => 2;
	for CCM_Phase use (
		Initialization => 2#00#,
		Header         => 2#01#,
		Payload        => 2#10#,
		Final          => 2#11#
	);

	type Control_Register is record
		ALGODIR     : Algorithm_Direction     := Encrypt; -- Direction
		ALGOMODE0   : Algorithm_Mode_0        := 0; -- Algorithm mode bits 0 .. 2;
		DATATYPE    : Data_Type               := Data_32_Bit; -- Data size
		KEYSIZE     : Key_Size                := Key_128_Bit; -- Size of cryptographic key
		Reserved_10 : Integer range 0 .. 15   := 0;
		FFLUSH      : Boolean                 := False; -- Induce FIFO flushes (only when CRYPEN = False)
		CRYPEN      : Boolean                 := False; -- Cryptographic processor enable
		GCM_CCMPH   : CCM_Phase               := Initialization; -- CCM phase (not available in some devices)
		Reserved_18 : Integer range 0 .. 1    := 0;
		ALGOMODE3   : Algorithm_Mode_3        := 0; -- Algorithm mode bit 3 (not available in some devices)
		Reserved_20 : Integer range 0 .. 2**12 - 1 := 0;
	end record with Size => 32;
	for Control_Register use record
		ALGODIR     at 0 range  2 ..  2;
		ALGOMODE0   at 0 range  3 ..  5;
		DATATYPE    at 0 range  6 ..  7;
		KEYSIZE     at 0 range  8 ..  9;
		Reserved_10 at 0 range 10 .. 13;
		FFLUSH      at 0 range 14 .. 14;
		CRYPEN      at 0 range 15 .. 15;
		GCM_CCMPH   at 0 range 16 .. 17;
		Reserved_18 at 0 range 18 .. 18;
		ALGOMODE3   at 0 range 19 .. 19;
		Reserved_20 at 0 range 20 .. 31;
	end record;

	-- SR

	type Status_Register is record
		IFEM        : Boolean; -- Input FIFO empty
		IFNF        : Boolean; -- Input FIFO not full
		OFNE        : Boolean; -- Output FIFO not empty
		OFFU        : Boolean; -- Output FIFO full
		BUSY        : Boolean; -- CRYP is currently processing
		Reserved    : Integer range 0 .. 2**27 - 1;
	end record with Size => 32;
	for Status_Register use record
		IFEM        at 0 range  0 ..  0;
		IFNF        at 0 range  1 ..  1;
		OFNE        at 0 range  2 ..  2;
		OFFU        at 0 range  3 ..  3;
		BUSY        at 0 range  4 ..  4;
		Reserved    at 0 range  5 .. 31;
	end record;

	-- DMACR

	type DMA_Control_Register is record
		DIEN        : Boolean := False; -- DMA input enable
		DOEN        : Boolean := False; -- DMA output enable
		Reserved    : Integer range 0 .. 2**30 - 1 := 0;
	end record with Size => 32;
	for DMA_Control_Register use record
		DIEN        at 0 range  0 ..  0;
		DOEN        at 0 range  1 ..  1;
		Reserved    at 0 range  2 .. 31;
	end record;

	-- IMSCR, RISR, MISR

	type Interrupt_Register is record
		INI         : Boolean := False; -- Input FIFO interrupt enable
		OUTI        : Boolean := False; -- Output FIFO interrupt enable
		Reserved    : Integer range 0 .. 2**30 - 1 := 0;
	end record with Size => 32;
	for Interrupt_Register use record
		INI         at 0 range  0 ..  0;
		OUTI        at 0 range  1 ..  1;
		Reserved    at 0 range  2 .. 31;
	end record;

	-- KxLR, KxRR

	type Left_Right is (
		Left,
		Right
	);

	subtype Key_Index is Integer range 0 .. 3;

	type Key_Registers is array (Key_Index, Left_Right) of Unsigned_32 with Component_Size => 32, Size => 32 * 8;

	-- IVxLR, IVxRR

	subtype Initialization_Vector_Index is Integer range 0 .. 1;

	type Initialization_Vector is array (Initialization_Vector_Index, Left_Right) of Unsigned_32 with Component_Size => 32, Size => 32 * 4;

	-- CSGCMCCMxR, CSGCMxR

	type Context_Swap_Array is array (0 .. 7) of Unsigned_32 with Component_Size => 32, Size => 32 * 8;

	type CRYP_Registers is record
		CR          : Control_Register;
		SR          : Status_Register;
		DIN         : Unsigned_32;
		DOUT        : Unsigned_32;
		DMACR       : DMA_Control_Register;
		IMSCR       : Interrupt_Register;
		RISR        : Interrupt_Register;
		MISR        : Interrupt_Register;
		KR          : Key_Registers;
		IVR         : Initialization_Vector;
		CSGCMCCMR   : Context_Swap_Array;
		CSGCMR      : Context_Swap_Array;
	end record;
	for CRYP_Registers use record
		CR          at 16#00# range 0 .. 31;
		SR          at 16#04# range 0 .. 31;
		DIN         at 16#08# range 0 .. 31;
		DOUT        at 16#0C# range 0 .. 31;
		DMACR       at 16#10# range 0 .. 31;
		IMSCR       at 16#14# range 0 .. 31;
		RISR        at 16#18# range 0 .. 31;
		MISR        at 16#1C# range 0 .. 31;
		KR          at 16#20# range 0 .. 32 * 8 - 1;
		IVR         at 16#40# range 0 .. 32 * 4 - 1;
		CSGCMCCMR   at 16#50# range 0 .. 32 * 8 - 1;
		CSGCMR      at 16#70# range 0 .. 32 * 8 - 1;
	end record;

	CRYP : CRYP_Registers with Volatile, Import, Address => Address_Map.CRYP;

end STM32.Cryptographic_Processor;
