with STM32.Address_Map;

-- Window watchdog

package STM32.Window_Watchdog is

	-- CR

	type Control_Register is record
		T           : Integer range 0 .. 2**7 - 1 := 16#7F#; -- 7-bit counter (MSB to LSB)
		WDGA        : Boolean := False; -- Activation bit
		Unused_8    : Integer range 0 .. 2**24 - 1;

	end record with Size => 32;
	for Control_Register use record
		T           at 0 range  0 ..  6;
		WDGA        at 0 range  7 ..  7;
		Unused_8    at 0 range  8 .. 31;
	end record;

	-- CFR

	type Timer_Base is (
		PCLK1_Div_4096,
		PCLK1_Div_8192,
		PCLK1_Div_16384,
		PCLK1_Div_32768
	) with Size => 2;
	for Timer_Base use (
		PCLK1_Div_4096  => 2#00#,
		PCLK1_Div_8192  => 2#01#,
		PCLK1_Div_16384 => 2#10#,
		PCLK1_Div_32768 => 2#11#
	);

	type Configuration_Register is record
		W           : Integer range 0 .. 2**7 - 1 := 16#7F#; -- 7-bit window value
		WDGTB       : Timer_Base                  := PCLK1_Div_4096; -- Timer base
		EWI         : Boolean                     := False; -- Early wakeup interrupt
		Unused_10   : Integer range 0 .. 2**22 - 1 := 0;
	end record with Size => 32;
	for Configuration_Register use record
		W           at 0 range  0 ..  6;
		WDGTB       at 0 range  7 ..  8;
		EWI         at 0 range  9 ..  9;
		Unused_10   at 0 range 10 .. 31;
	end record;

	-- SR

	type Status_Register is record
		EWIF        : Boolean := True; -- Early wakeup interrupt
		Unused_1    : Integer range 0 .. 2**31 - 1 := 0;
	end record with Size => 32;
	for Status_Register use record
		EWIF        at 0 range  0 ..  0;
		Unused_1    at 0 range  1 .. 31;
	end record;

	type WWDG_Registers is record
		CR          : Control_Register;
		pragma Volatile_Full_Access (CR);
		CFR         : Configuration_Register;
		pragma Volatile_Full_Access (CFR);
		SR          : Status_Register;
		pragma Volatile_Full_Access (SR);
	end record;
	for WWDG_Registers use record
		CR          at 16#00# range 0 .. 31;
		CFR         at 16#04# range 0 .. 31;
		SR          at 16#08# range 0 .. 31;
	end record;

	WWDG : WWDG_Registers with Volatile, Import, Address => Address_Map.WWDG;

end STM32.Window_Watchdog;
