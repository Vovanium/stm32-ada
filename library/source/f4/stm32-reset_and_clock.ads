with STM32.Address_Map;

-- Reset and Clock Controller

package STM32.Reset_and_Clock is

	-- CR
	HSITRIM_Default : constant Integer := 16#10#; -- This constant comes from HAL

	type Clock_Control_Register is record
		HSION       : Boolean;
		HSIRDY      : Boolean;
		Unused_2  : Integer range 0 .. 1;
		HSITRIM     : Integer range 0 .. 2**5 - 1;
		HSICAL      : Integer range 0 .. 2**8 - 1;
		HSEON       : Boolean;
		HSERDY      : Boolean;
		HSEBYP      : Boolean;
		CSSON       : Boolean;
		Unused_20 : Integer range 0 .. 2**4 - 1;
		PLLON       : Boolean;
		PLLRDY      : Boolean;
		PLLI2SON    : Boolean;
		PLLI2SRDY   : Boolean;
		PLLSAION    : Boolean; -- Available only in STM32F42xxx, F43xxx
		PLLSAIRDY   : Boolean; -- --"--
		Unused_30 : Integer range 0 .. 2**2 - 1;
	end record with Size => 32;

	for Clock_Control_Register use record
		HSION       at 0 range 0 .. 0;
		HSIRDY      at 0 range 1 .. 1;
		Unused_2  at 0 range 2 .. 2;
		HSITRIM     at 0 range 3 .. 7;
		HSICAL      at 0 range 8 .. 15;
		HSEON       at 0 range 16 .. 16;
		HSERDY      at 0 range 17 .. 17;
		HSEBYP      at 0 range 18 .. 18;
		CSSON       at 0 range 19 .. 19;
		Unused_20 at 0 range 20 .. 23;
		PLLON       at 0 range 24 .. 24;
		PLLRDY      at 0 range 25 .. 25;
		PLLI2SON    at 0 range 26 .. 26;
		PLLI2SRDY   at 0 range 27 .. 27;
		PLLSAION    at 0 range 28 .. 28;
		PLLSAIRDY   at 0 range 29 .. 29;
		Unused_30 at 0 range 30 .. 31;
	end record;

	--- PLLCFGR
	type PLL_Division_Factor is (
		PLL_Div_2,
		PLL_Div_4,
		PLL_Div_6,
		PLL_Div_8
	);
	for PLL_Division_Factor use (
		PLL_Div_2 => 2#00#,
		PLL_Div_4 => 2#01#,
		PLL_Div_6 => 2#10#,
		PLL_Div_8 => 2#11#
	);
	type PLL_Division_Factor_Value_Array is array (PLL_Division_Factor) of Integer range 2 .. 8;
	PLLP_Value : constant PLL_Division_Factor_Value_Array := (
		PLL_Div_2   =>  2,
		PLL_Div_4   =>  4,
		PLL_Div_6   =>  6,
		PLL_Div_8   =>  8
	);

	type PLL_Clock_Source is (
		PLLSRC_HSI,
		PLLSRC_HSE
	);

	for PLL_Clock_Source use (
		PLLSRC_HSI => 0,
		PLLSRC_HSE => 1
	);

	type PLL_Configuration_Register is record
		M         : Integer range 2 .. 63;
		N         : Integer range 50 .. 432;
		Unused_15 : Integer range 0 .. 1;
		P         : PLL_Division_Factor;
		Unused_18 : Integer range 0 .. 2**4 - 1;
		SRC       : PLL_Clock_Source;
		Unused_23 : Integer range 0 .. 1;
		Q         : Integer range 2 .. 15;
		Unused_28 : Integer range 0 .. 2 ** 4 - 1;
	end record with Size => 32;

	for PLL_Configuration_Register use record
		M         at 0 range 0 .. 5;
		N         at 0 range 6 .. 14;
		Unused_15 at 0 range 15 .. 15;
		P         at 0 range 16 .. 17;
		Unused_18 at 0 range 18 .. 21;
		SRC       at 0 range 22 .. 22;
		Unused_23 at 0 range 23 .. 23;
		Q         at 0 range 24 .. 27;
		Unused_28 at 0 range 28 .. 31;
	end record;

	-- CFGR
	type System_Clock_Switch is (
		System_Clock_HSI,
		System_Clock_HSE,
		System_Clock_PLL
	) with Size => 2;

	for System_Clock_Switch use (
		System_Clock_HSI => 2#00#,
		System_Clock_HSE => 2#01#,
		System_Clock_PLL => 2#10#
	);

	type AHB_Prescaler_Factor is (
		AHB_Prescaler_1,
		AHB_Prescaler_1_A,
		AHB_Prescaler_1_B,
		AHB_Prescaler_1_C,
		AHB_Prescaler_1_D,
		AHB_Prescaler_1_E,
		AHB_Prescaler_1_F,
		AHB_Prescaler_1_G,
		AHB_Prescaler_2,
		AHB_Prescaler_4,
		AHB_Prescaler_8,
		AHB_Prescaler_16,
		AHB_Prescaler_64,
		AHB_Prescaler_128,
		AHB_Prescaler_256,
		AHB_Prescaler_512
	) with Size => 4;
	for AHB_Prescaler_Factor use (
		AHB_Prescaler_1   => 2#0000#,
		AHB_Prescaler_1_A => 2#0001#,
		AHB_Prescaler_1_B => 2#0010#,
		AHB_Prescaler_1_C => 2#0011#,
		AHB_Prescaler_1_D => 2#0100#,
		AHB_Prescaler_1_E => 2#0101#,
		AHB_Prescaler_1_F => 2#0110#,
		AHB_Prescaler_1_G => 2#0111#,
		AHB_Prescaler_2   => 2#1000#,
		AHB_Prescaler_4   => 2#1001#,
		AHB_Prescaler_8   => 2#1010#,
		AHB_Prescaler_16  => 2#1011#,
		AHB_Prescaler_64  => 2#1100#,
		AHB_Prescaler_128 => 2#1101#,
		AHB_Prescaler_256 => 2#1110#,
		AHB_Prescaler_512 => 2#1111#
	);
	type AHB_Prescaler_Factor_Value_Array is array (AHB_Prescaler_Factor) of Integer range 1 .. 512;
	HPRE_Value : constant AHB_Prescaler_Factor_Value_Array := (
		AHB_Prescaler_1    =>   1,
		AHB_Prescaler_1_A =>   1,
		AHB_Prescaler_1_B =>   1,
		AHB_Prescaler_1_C =>   1,
		AHB_Prescaler_1_D =>   1,
		AHB_Prescaler_1_E =>   1,
		AHB_Prescaler_1_F =>   1,
		AHB_Prescaler_1_G =>   1,
		AHB_Prescaler_2   =>   2,
		AHB_Prescaler_4   =>   4,
		AHB_Prescaler_8   =>   8,
		AHB_Prescaler_16  =>  16,
		AHB_Prescaler_64  =>  64,
		AHB_Prescaler_128 => 128,
		AHB_Prescaler_256 => 256,
		AHB_Prescaler_512 => 512
	);

	type APB_Prescaler_Factor is (
		APB_Prescaler_1,
		APB_Prescaler_1_A,
		APB_Prescaler_1_B,
		APB_Prescaler_1_C,
		APB_Prescaler_2,
		APB_Prescaler_4,
		APB_Prescaler_8,
		APB_Prescaler_16
	) with Size => 3;

	for APB_Prescaler_Factor use (
		APB_Prescaler_1   => 2#000#,
		APB_Prescaler_1_A => 2#001#,
		APB_Prescaler_1_B => 2#010#,
		APB_Prescaler_1_C => 2#011#,
		APB_Prescaler_2   => 2#100#,
		APB_Prescaler_4   => 2#101#,
		APB_Prescaler_8   => 2#110#,
		APB_Prescaler_16  => 2#111#
	);
	type APB_Prescaler_Factor_Value_Array is array (APB_Prescaler_Factor) of Integer range 1 .. 16;
	PPRE_Value : constant APB_Prescaler_Factor_Value_Array := (
		APB_Prescaler_1   =>  1,
		APB_Prescaler_1_A =>  1,
		APB_Prescaler_1_B =>  1,
		APB_Prescaler_1_C =>  1,
		APB_Prescaler_2   =>  2,
		APB_Prescaler_4   =>  4,
		APB_Prescaler_8   =>  8,
		APB_Prescaler_16  => 16
	);

	type Clock_Output_1 is (
		Clock_Output_1_HSI,
		Clock_Output_1_LSE,
		Clock_Output_1_HSE,
		Clock_Output_1_PLL
	) with Size => 2;

	for Clock_Output_1 use (
		Clock_Output_1_HSI => 2#00#,
		Clock_Output_1_LSE => 2#01#,
		Clock_Output_1_HSE => 2#10#,
		Clock_Output_1_PLL => 2#11#
	);

	type I2S_Clock_Source is (
		I2S_Clock_PLLI2S,
		I2S_Clock_CLKIN
	) with Size => 1;

	for I2S_Clock_Source use (
		I2S_Clock_PLLI2S => 0,
		I2S_Clock_CLKIN => 1
	);

	type Clock_Output_Prescaler_Factor is (
		MCO_Prescaler_1,
		MCO_Prescaler_1_A,
		MCO_Prescaler_1_B,
		MCO_Prescaler_1_C,
		MCO_Prescaler_2,
		MCO_Prescaler_3,
		MCO_Prescaler_4,
		MCO_Prescaler_5
	) with Size => 3;
	for Clock_Output_Prescaler_Factor use (
		MCO_Prescaler_1   => 2#000#,
		MCO_Prescaler_1_A => 2#001#,
		MCO_Prescaler_1_B => 2#010#,
		MCO_Prescaler_1_C => 2#011#,
		MCO_Prescaler_2   => 2#100#,
		MCO_Prescaler_3   => 2#101#,
		MCO_Prescaler_4   => 2#110#,
		MCO_Prescaler_5   => 2#111#
	);
	type Clock_Output_Prescaler_Factor_Value_Array is array (Clock_Output_Prescaler_Factor) of Integer range 1 .. 5;
	MCOPRE_Value : constant Clock_Output_Prescaler_Factor_Value_Array := (
		MCO_Prescaler_1   => 1,
		MCO_Prescaler_1_A => 1,
		MCO_Prescaler_1_B => 1,
		MCO_Prescaler_1_C => 1,
		MCO_Prescaler_2   => 2,
		MCO_Prescaler_3   => 3,
		MCO_Prescaler_4   => 4,
		MCO_Prescaler_5   => 5
	);

	type Clock_Output_2 is (
		Clock_Output_2_SYSCLK,
		Clock_Output_2_PLLI2S,
		Clock_Output_2_HSE,
		Clock_Output_2_PLL
	) with Size => 2;

	for Clock_Output_2 use (
		Clock_Output_2_SYSCLK => 2#00#,
		Clock_Output_2_PLLI2S => 2#01#,
		Clock_Output_2_HSE    => 2#10#,
		Clock_Output_2_PLL    => 2#11#
	);

	type Clock_Configuration_Register is record
		SW        : System_Clock_Switch;
		SWS       : System_Clock_Switch;
		HPRE      : AHB_Prescaler_Factor;
		Unused_8  : Integer range 0 .. 3;
		PPRE1     : APB_Prescaler_Factor;
		PPRE2     : APB_Prescaler_Factor;
		RTCPRE    : Integer range 0 .. 31;
		MCO1      : Clock_Output_1;
		I2SSRC    : I2S_Clock_Source;
		MCO1PRE   : Clock_Output_Prescaler_Factor;
		MCO2PRE   : Clock_Output_Prescaler_Factor;
		MCO2      : Clock_Output_2;
	end record with Size => 32;

	for Clock_Configuration_Register use record
		SW        at 0 range  0 .. 1;
		SWS       at 0 range  2 .. 3;
		HPRE      at 0 range  4 .. 7;
		Unused_8  at 0 range  8 .. 9;
		PPRE1     at 0 range 10 .. 12;
		PPRE2     at 0 range 13 .. 15;
		RTCPRE    at 0 range 16 .. 20;
		MCO1      at 0 range 21 .. 22;
		I2SSRC    at 0 range 23 .. 23;
		MCO1PRE   at 0 range 24 .. 26;
		MCO2PRE   at 0 range 27 .. 29;
		MCO2      at 0 range 30 .. 31;
	end record;

	-- CIR
	type Clock_Interrupt_Register is record
		LSIRDYF     : Boolean;
		LSERDYF     : Boolean;
		HSIRDYF     : Boolean;
		HSERDYF     : Boolean;
		PLLRDYF     : Boolean;
		PLLI2SRDYF  : Boolean;
		PLLSAIRDYF  : Boolean; -- Only in F42xxx, F43xxx
		CSSF        : Boolean;
		LSIRDYIE    : Boolean;
		LSERDYIE    : Boolean;
		HSIRDYIE    : Boolean;
		HSERDYIE    : Boolean;
		PLLRDYIE    : Boolean;
		PLLI2SRDYIE : Boolean;
		PLLSAIRDYIE : Boolean; -- Only in F42xxx, F43xxx
		Unused_15   : Integer range 0 .. 1;
		LSIRDYC     : Boolean;
		LSERDYC     : Boolean;
		HSIRDYC     : Boolean;
		HSERDYC     : Boolean;
		PLLRDYC     : Boolean;
		PLLI2SRDYC  : Boolean;
		PLLSAIRDYC  : Boolean; -- Only in F42xxx, F43xxx
		CSSC        : Boolean;
		Unused_24   : Integer range 0 .. 2**8 - 1;
	end record with Size => 32;

	for Clock_Interrupt_Register use record
		LSIRDYF     at 0 range  0 .. 0;
		LSERDYF     at 0 range  1 .. 1;
		HSIRDYF     at 0 range  2 .. 2;
		HSERDYF     at 0 range  3 .. 3;
		PLLRDYF     at 0 range  4 .. 4;
		PLLI2SRDYF  at 0 range  5 .. 5;
		PLLSAIRDYF  at 0 range  6 .. 6;
		CSSF        at 0 range  7 .. 7;
		LSIRDYIE    at 0 range  8 .. 8;
		LSERDYIE    at 0 range  9 .. 9;
		HSIRDYIE    at 0 range 10 .. 10;
		HSERDYIE    at 0 range 11 .. 11;
		PLLRDYIE    at 0 range 12 .. 12;
		PLLI2SRDYIE at 0 range 13 .. 13;
		PLLSAIRDYIE at 0 range 14 .. 14;
		Unused_15   at 0 range 15 .. 15;
		LSIRDYC     at 0 range 16 .. 16;
		LSERDYC     at 0 range 17 .. 17;
		HSIRDYC     at 0 range 18 .. 18;
		HSERDYC     at 0 range 19 .. 19;
		PLLRDYC     at 0 range 20 .. 20;
		PLLI2SRDYC  at 0 range 21 .. 21;
		PLLSAIRDYC  at 0 range 22 .. 22;
		CSSC        at 0 range 23 .. 23;
		Unused_24   at 0 range 24 .. 31;
	end record;

	package Index is
		-- AHB1RSTR, AHB1ENR, AHB1LPENR
		type AHB1_Index is (
			GPIOA,      GPIOB,      GPIOC,      GPIOD,
			GPIOE,      GPIOF,      GPIOG,      GPIOH,
			GPIOI,      GPIOJ,      GPIOK,      AHB1_11, -- GPIOJ, GPIOK only in F42xxx, F43xxx
			CRC,        AHB1_13,    AHB1_14,    FLITF,
			SRAM1,      SRAM2,      BKPSRAM,    AHB1_19,
			CCMDATARAM, DMA1,       DMA2,       DMA2D, -- DMA2D only in F42xxx, F42xxx
			AHB1_24,    ETHMAC,     ETHMACTX,   ETHMACRX,
			ETHMACPTP,  OTGHS,      OTGHSULPI,  AHB1_31
		);

		-- AHB2RSTR, AHB2ENR
		type AHB2_Index is (
			DCMI,    AHB2_1,  AHB2_2,  AHB2_3,
			CRYP,    HASH,    RNG,     OTGFS,
			AHB2_8,  AHB2_9,  AHB2_10, AHB2_11,
			AHB2_12, AHB2_13, AHB2_14, AHB2_15,
			AHB2_16, AHB2_17, AHB2_18, AHB2_19,
			AHB2_20, AHB2_21, AHB2_22, AHB2_23,
			AHB2_24, AHB2_25, AHB2_26, AHB2_27,
			AHB2_28, AHB2_29, AHB2_30, AHB2_31
		);

		-- AHB3RSTR, AHB3ENR
		type AHB3_Index is (
			FMC,     AHB3_1,  AHB3_2,  AHB3_3,
			AHB3_4,  AHB3_5,  AHB3_6,  AHB3_7,
			AHB3_8,  AHB3_9,  AHB3_10, AHB3_11,
			AHB3_12, AHB3_13, AHB3_14, AHB3_15,
			AHB3_16, AHB3_17, AHB3_18, AHB3_19,
			AHB3_20, AHB3_21, AHB3_22, AHB3_23,
			AHB3_24, AHB3_25, AHB3_26, AHB3_27,
			AHB3_28, AHB3_29, AHB3_30, AHB3_31
		);
		FSMC : constant AHB3_Index := FMC;

		-- APB1RSTR, APB1ENR
		type APB1_Index is (
			TIM2,    TIM3,    TIM4,    TIM5,
			TIM6,    TIM7,    TIM12,   TIM13,
			TIM14,   APB1_9,  APB1_10, WWDG,
			APB1_12, APB1_13, SPI2,    SPI3,
			APB1_16, UART2,   UART3,   UART4,
			UART5,   I2C1,    I2C2,    I2C3,
			APB1_24, CAN1,    CAN2,    APB1_27,
			PWR,     DAC,     UART7,   UART8  -- UART7, UART8 only in F42xxx, F43xxx
		);

		-- APB2RSTR, APB2ENR
		type APB2_Index is (
			TIM1,    TIM2,    APB2_2,  APB2_3,
			USART1,  USART6,  APB2_6,  APB2_7,
			ADC1,    ADC2,    ADC3,    SDIO,
			SPI1,    SPI4,    SYSCFG,  APB2_15, -- SPI4 only in F42xxx, F43xxx
			TIM9,    TIM10,   TIM11,   APB2_19,
			SPI5,    SPI6,    SAI1,    APB2_23, -- SPI5, SPI6, SAI1 only in F42xxx, F43xxx
			APB2_24, APB2_25, LTDC,    APB2_27, -- LTDC only in F42xxx, F43xxx
			APB2_28, APB2_29, APB2_30, APB2_31
		);
		ADC : constant APB2_Index := ADC1; -- APB2RSTR(ADC) resets all ADCs
	end Index;

	type AHB1_Register is array (Index.AHB1_Index) of Boolean
	 with Pack, Size => 32;

	type AHB2_Register is array (Index.AHB2_Index) of Boolean
	 with Pack, Size => 32;

	type AHB3_Register is array (Index.AHB3_Index) of Boolean
	 with Pack, Size => 32;

	type APB1_Register is array (Index.APB1_Index) of Boolean
	 with Pack, Size => 32;

	type APB2_Register is array (Index.APB2_Index) of Boolean
	 with Pack, Size => 32;

	-- BCDR
	type RTC_Clock_Source is (
		RTC_Not_Clocked,
		RTC_Clock_LSE,
		RTC_Clock_LSI,
		RTC_Clock_HSE
	) with Size => 2;

	for RTC_Clock_Source use (
		RTC_Not_Clocked => 2#00#,
		RTC_Clock_LSE   => 2#01#,
		RTC_Clock_LSI   => 2#10#,
		RTC_Clock_HSE   => 2#11#
	);

	type Backup_Domain_Control_Register is record
		LSEON     : Boolean;
		LSERDY    : Boolean;
		LSEBYP    : Boolean;
		Unused_3  : Integer range 0 .. 2**5 - 1;
		RTCSEL    : RTC_Clock_Source;
		Unused_10 : Integer range 0 .. 2**5 - 1;
		RTCEN     : Boolean;
		BDRST     : Boolean;
		Unused_17 : Integer range 0 .. 2**15 - 1;
	end record with Size => 32;
	for Backup_Domain_Control_Register use record
		LSEON     at 0 range  0 .. 0;
		LSERDY    at 0 range  1 .. 1;
		LSEBYP    at 0 range  2 .. 2;
		Unused_3  at 0 range  3 .. 7;
		RTCSEL    at 0 range  8 .. 9;
		Unused_10 at 0 range 10 .. 14;
		RTCEN     at 0 range 15 .. 15;
		BDRST     at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 31;
	end record;

	-- CSR
	type Clock_Control_and_Status_Register is record
		LSION     : Boolean;
		LSIRDY    : Boolean;
		Unused_2  : Integer range 0 .. 2**22 - 1;
		RMVF      : Boolean;
		BORRSTF   : Boolean;
		PINRSTF   : Boolean;
		PORRSTF   : Boolean;
		SFTRSTF   : Boolean;
		IWDGRSTF  : Boolean;
		WWDGRSTF  : Boolean;
		LPWRRSTF  : Boolean;
	end record with Size => 32;
	for Clock_Control_and_Status_Register use record
		LSION     at 0 range  0 .. 0;
		LSIRDY    at 0 range  1 .. 1;
		Unused_2  at 0 range  2 .. 23;
		RMVF      at 0 range 24 .. 24;
		BORRSTF   at 0 range 25 .. 25;
		PINRSTF   at 0 range 26 .. 26;
		PORRSTF   at 0 range 27 .. 27;
		SFTRSTF   at 0 range 28 .. 28;
		IWDGRSTF  at 0 range 29 .. 29;
		WWDGRSTF  at 0 range 30 .. 30;
		LPWRRSTF  at 0 range 31 .. 31;
	end record;

	-- SSCGR
	type Spread_Select is (
		Center_Spread,
		Down_Spread
	) with Size => 1;
	for Spread_Select use (
		Center_Spread => 0,
		Down_Spread   => 1
	);

	type Spread_Spectrum_Clock_Generation_Register is record
		MODPER    : Integer range 0 .. 2**13 - 1;
		INCSTEP   : Integer range 0 .. 2**15 - 1;
		Unused_28 : Integer range 0 .. 3;
		SPREADSEL : Spread_Select;
		SSCGEN    : Boolean;
	end record with Size => 32;

	for Spread_Spectrum_Clock_Generation_Register use record
		MODPER    at 0 range 0 .. 12;
		INCSTEP   at 0 range 13 .. 27;
		Unused_28 at 0 range 28 .. 29;
		SPREADSEL at 0 range 30 .. 30;
		SSCGEN    at 0 range 31 .. 31;
	end record;

	-- PLLI2SCFGR
	type PLLI2S_Configuration_Register is record
		Unused_0  : Integer range 0 .. 2**6 - 1;
		N         : Integer range 2 .. 432;
		Unused_15 : Integer range 0 .. 2**9 - 1;
		Q         : Integer range 2 .. 15; -- only in F42xxx, F43xxx
		R         : Integer range 2 .. 7;
		Unused_31 : Integer range 0 .. 1;
	end record with Size => 32;

	for PLLI2S_Configuration_Register use record
		Unused_0  at 0 range 0 .. 5;
		N         at 0 range 6 .. 14;
		Unused_15 at 0 range 15 .. 23;
		Q         at 0 range 24 .. 27;
		R         at 0 range 28 .. 30;
		Unused_31 at 0 range 31 .. 31;
	end record;

	-- CLKCFGR
	type PLLSAI_Division_Factor is (
		PLLSAI_Div_2,
		PLLSAI_Div_4,
		PLLSAI_Div_8,
		PLLSAI_Div_16
	) with Size => 2;
	for PLLSAI_Division_Factor use (
		PLLSAI_Div_2  => 2#00#,
		PLLSAI_Div_4  => 2#01#,
		PLLSAI_Div_8  => 2#10#,
		PLLSAI_Div_16 => 2#11#
	);
	type PLLSAI_Division_Factor_Value_Array is array (PLLSAI_Division_Factor) of Integer range 2 .. 16;
	PLLSAIDIVR_Value : constant PLLSAI_Division_Factor_Value_Array := (
		PLLSAI_Div_2  =>  2,
		PLLSAI_Div_4  =>  4,
		PLLSAI_Div_8  =>  8,
		PLLSAI_Div_16 => 16
	);

	type SAI_Clock_Source is (
		SAI_Clock_PLLSAI,  -- SAI1-x frequency = fPLLSAI / PLLSAIDIVQ
		SAI_Clock_PLLI2S,  -- SAI1-x frequency = fPLLI2S / PLLI2SDIVQ
		SAI_Clock_AF_Input -- SAI1-x frequency = Aletrnate function input frequency
	) with Size => 2;
	for SAI_Clock_Source use (
		SAI_Clock_PLLSAI   => 2#00#,
		SAI_Clock_PLLI2S   => 2#01#,
		SAI_Clock_AF_Input => 2#10#
	);

	type Timer_Prescaler is (
		Timer_2PCLK, -- If the APB prescaler is 1, TIMxCLK = PCLKx, otherwise 2*PCLKx
		Timer_4PCLK  -- If the APB prescaler is 1, 2 or 4, TIMxCLK = HCLK, otherwise 4*PCLKx
	) with Size => 1;
	for Timer_Prescaler use (
		Timer_2PCLK => 0,
		Timer_4PCLK => 1
	);

	type Dedicated_Clock_Configuration_Register is record
		PLLI2SDIVQ  : Integer range 0 .. 31  := 0;
		-- PLLI2S division factor for SAI1 clock minus 1
		Unused_5    : Integer range 0 .. 7   := 0;
		PLLSAIDIVQ  : Integer range 0 .. 31  := 0;
		-- PLLSAI sivision factor for SAI1 clock minus 1
		Unused_13   : Integer range 0 .. 7   := 0;
		PLLSAIDIVR  : PLLSAI_Division_Factor := PLLSAI_Div_2;
		-- Division factor for LCD_CLK
		Unused_18   : Integer range 0 .. 3   := 0;
		SAI1ASRC    : SAI_Clock_Source       := SAI_Clock_PLLSAI;
		-- SAI1-A clock source selection
		SAI1BSRC    : SAI_Clock_Source       := SAI_Clock_PLLSAI;
		-- SAI1-B clock source selection
		TIMPRE      : Timer_Prescaler        := Timer_2PCLK;
		-- Timers clock prescalers selection
		Unused_25 : Integer range 0 .. 2**7 - 1;
	end record with Size => 32;
	for Dedicated_Clock_Configuration_Register use record
		PLLI2SDIVQ  at 0 range  0 ..  4;
		Unused_5    at 0 range  5 ..  7;
		PLLSAIDIVQ  at 0 range  8 .. 12;
		Unused_13   at 0 range 13 .. 15;
		PLLSAIDIVR  at 0 range 16 .. 17;
		Unused_18   at 0 range 18 .. 19;
		SAI1ASRC    at 0 range 20 .. 21;
		SAI1BSRC    at 0 range 22 .. 23;
		TIMPRE      at 0 range 24 .. 24;
		Unused_25   at 0 range 25 .. 31;
	end record;

	type RCC_Registers is record
		CR         : Clock_Control_Register;
		PLLCFGR    : PLL_Configuration_Register;
		CFGR       : Clock_Configuration_Register;
		CIR        : Clock_Interrupt_Register;
		AHB1RSTR   : AHB1_Register;
		AHB2RSTR   : AHB2_Register;
		AHB3RSTR   : AHB3_Register;
		APB1RSTR   : APB1_Register;
		APB2RSTR   : APB2_Register;
		AHB1ENR    : AHB1_Register;
		AHB2ENR    : AHB2_Register;
		AHB3ENR    : AHB3_Register;
		APB1ENR    : APB1_Register;
		APB2ENR    : APB2_Register;
		AHB1LPENR  : AHB1_Register;
		AHB2LPENR  : AHB2_Register;
		AHB3LPENR  : AHB3_Register;
		APB1LPENR  : APB1_Register;
		APB2LPENR  : APB2_Register;
		BDCR       : Backup_Domain_Control_Register;
		CSR        : Clock_Control_and_Status_Register;
		SSCGR      : Spread_Spectrum_Clock_Generation_Register;
		PLLI2SCFGR : PLLI2S_Configuration_Register;
		PLLSAICFGR : PLLI2S_Configuration_Register;
		DCKCFGR    : Dedicated_Clock_Configuration_Register;
	end record with Volatile;

	for RCC_Registers use record
		CR         at 16#00# range 0 .. 31;
		PLLCFGR    at 16#04# range 0 .. 31;
		CFGR       at 16#08# range 0 .. 31;
		CIR        at 16#0C# range 0 .. 31;
		AHB1RSTR   at 16#10# range 0 .. 31;
		AHB2RSTR   at 16#14# range 0 .. 31;
		AHB3RSTR   at 16#18# range 0 .. 31;
		APB1RSTR   at 16#20# range 0 .. 31;
		APB2RSTR   at 16#24# range 0 .. 31;
		AHB1ENR    at 16#30# range 0 .. 31;
		AHB2ENR    at 16#34# range 0 .. 31;
		AHB3ENR    at 16#38# range 0 .. 31;
		APB1ENR    at 16#40# range 0 .. 31;
		APB2ENR    at 16#44# range 0 .. 31;
		AHB1LPENR  at 16#50# range 0 .. 31;
		AHB2LPENR  at 16#54# range 0 .. 31;
		AHB3LPENR  at 16#58# range 0 .. 31;
		APB1LPENR  at 16#60# range 0 .. 31;
		APB2LPENR  at 16#64# range 0 .. 31;
		BDCR       at 16#70# range 0 .. 31;
		CSR        at 16#74# range 0 .. 31;
		SSCGR      at 16#80# range 0 .. 31;
		PLLI2SCFGR at 16#84# range 0 .. 31;
		PLLSAICFGR at 16#88# range 0 .. 31;
		DCKCFGR    at 16#8C# range 0 .. 31;
	end record;

	RCC : RCC_Registers with Volatile, Import, Address => Address_Map.RCC;

end STM32.Reset_and_Clock;
