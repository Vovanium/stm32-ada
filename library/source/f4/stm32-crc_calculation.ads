with STM32.Address_Map;

-- CRC Calculation Unit

package STM32.CRC_Calculation is
	-- Applies to whole STM32F4xx family

	-- IDR
	type Independent_Data_Register is record
		IDR        : Interfaces.Unsigned_8;
		Reserved_8 : Integer range 0 .. 2**24 - 1;
	end record with Size => 32;

	for Independent_Data_Register use record
		IDR        at 0 range  0 ..  7;
		Reserved_8 at 0 range  8 .. 31;
	end record;

	-- CR
	type Control_Register is record
		RESET      : Boolean;
		Reserved_1 : Integer range 0 .. 2**31 - 1;
	end record with Size => 32;

	for Control_Register use record
		RESET      at 0 range  0 ..  0;
		Reserved_1 at 0 range  1 .. 31;
	end record;

	--
	type CRC_Registers is record
		DR  : Unsigned_32;
		pragma Volatile_Full_Access (DR);
		IDR : Independent_Data_Register;
		pragma Volatile_Full_Access (IDR);
		CR  : Control_Register;
		pragma Volatile_Full_Access (CR);
	end record with Volatile;

	for CRC_Registers use record
		DR  at 16#00# range 0 .. 31;
		IDR at 16#04# range 0 .. 31;
		CR  at 16#08# range 0 .. 31;
	end record;

	CRC : CRC_Registers with Volatile, Import, Address => Address_Map.CRC;

end STM32.CRC_Calculation;