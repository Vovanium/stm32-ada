with STM32.Address_Map;

-- SD / SDIO / MMC interface
--
-- This unit is in all STM32F4xx series models except F410

package STM32.SDIO_Interface is

	-- POWER

	type Power_Control is (
		Power_Off,
		Reserved,
		Reserved_Power_Up,
		Power_On
	) with Size => 2;
	for Power_Control use (
		Power_Off         => 2#00#,
		Reserved          => 2#01#,
		Reserved_Power_Up => 2#10#,
		Power_On          => 2#11#
	);

	type Power_Control_Register is record
		PWRCTRL     : Power_Control := Power_Off; -- Power supply control bits
		Unused_2    : Integer range 0 .. 2**30 - 1;
	end record with Size => 32;
	for Power_Control_Register use record
		PWRCTRL     at 0 range  0 ..  1;
		Unused_2    at 0 range  2 .. 31;
	end record;

	-- CLKCR

	type Bus_Width is (
		Bus_1_Bit,
		Bus_4_Bit,
		Bus_8_Bit
	) with Size => 2;
	for Bus_Width use (
		Bus_1_Bit => 2#00#,
		Bus_4_Bit => 2#01#,
		Bus_8_Bit => 2#10#
	);

	type Clock_Control_Register is record
		CLKDIV      : Integer range 0 .. 2**8 - 1; -- Clock divide factor minus 2
		CLKEN       : Boolean := False; -- Clock enable bit
		PWRSAV      : Boolean := False; -- Power saving configuration
		BYPASS      : Boolean := False; -- Clock divider bypass enable
		WIDBUS      : Bus_Width := Bus_1_Bit; -- Wide bus mode enable bit
		NEGEDGE     : Boolean := False; -- SDIO_CK dephasing selection
		HWFC_EN     : Boolean := False; -- HW Flow Control enable
		Unused_15   : Integer range 0 .. 2**17 - 1 := 0;
	end record with Size => 32;
	for Clock_Control_Register use record
		CLKDIV      at 0 range  0 ..  7;
		CLKEN       at 0 range  8 ..  8;
		PWRSAV      at 0 range  9 ..  9;
		BYPASS      at 0 range 10 .. 10;
		WIDBUS      at 0 range 11 .. 12;
		NEGEDGE     at 0 range 13 .. 13;
		HWFC_EN     at 0 range 14 .. 14;
		Unused_15   at 0 range 15 .. 31;
	end record;

	-- CMD

	type Wait_for_Response is (
		No_Response,
		Short_Response,
		No_Response_A,
		Long_Response
	) with Size => 2;
	for Wait_for_Response use (
		No_Response    => 2#00#,
		Short_Response => 2#01#,
		No_Response_A  => 2#10#,
		Long_Response  => 2#11#
	);

	type Command_Register is record
		CMDINDEX    : Integer range 0 .. 63 := 0;           -- Command index
		WAITRESP    : Wait_for_Response     := No_Response; -- Wait for response bits
		WAITINT     : Boolean               := False;       -- CPSM waits for interrupt request
		WAITPEND    : Boolean               := False;       -- CPSM waits for ends of data transfer
		CPSMEN      : Boolean               := False;       -- Command path state machine (CPSM) Enable bit
		SDIOSuspend : Boolean               := False;       -- SD I/O suspend command
		ENCMDcompl  : Boolean               := False;       -- Enable CMD completion
		nIEN        : Boolean               := False;       -- Not Interrupt Enable
		CE_ATACMD   : Boolean               := False;       -- CE-ATA command (CPSM transfers CMD61)
		Unused_15   : Integer range 0 .. 2**17 - 1 := 0;
	end record with Size => 32;
	for Command_Register use record
		CMDINDEX    at 0 range  0 ..  5;
		WAITRESP    at 0 range  6 ..  7;
		WAITINT     at 0 range  8 ..  8;
		WAITPEND    at 0 range  9 ..  9;
		CPSMEN      at 0 range 10 .. 10;
		SDIOSuspend at 0 range 11 .. 11;
		ENCMDcompl  at 0 range 12 .. 12;
		nIEN        at 0 range 13 .. 13;
		CE_ATACMD   at 0 range 14 .. 14;
		Unused_15   at 0 range 15 .. 31;
	end record;

	-- RESPx

	type Response_Registers is array (1 .. 4) of Unsigned_32 with Component_Size => 32, Size => 32 * 4;

	-- RESPCMD

	type Command_Response_Register is record
		RESPCMD     : Integer range 0 .. 63; -- Response command index
		Unused_6    : Integer range 0 .. 2**26 - 1;
	end record with Size => 32;
	for Command_Response_Register use record
		RESPCMD     at 0 range  0 ..  5;
		Unused_6    at 0 range  6 .. 31;
	end record;

	-- DLEN, DCOUNT

	type Data_Length_Register is record
		DATALENGTH  : Integer range 0 .. 2**25 - 1 := 0; -- Data length value
		Unused_25   : Integer range 0 .. 2**7 - 1  := 0;
	end record with Size => 32;
	for Data_Length_Register use record
		DATALENGTH  at 0 range  0 .. 24;
		Unused_25   at 0 range 25 .. 31;
	end record;

	-- DCTRL

	type Data_Transfer_Direction is (
		Controller_to_Card,
		Card_to_Controller
	) with Size => 1;
	for Data_Transfer_Direction use (
		Controller_to_Card => 0,
		Card_to_Controller => 1
	);

	type Data_Transfer_Mode is (
		Block_Transfer,
		Stream_Transfer
	) with Size => 1;
	for Data_Transfer_Mode use (
		Block_Transfer  => 0,
		Stream_Transfer => 1
	);

	type Read_Wait_Mode is (
		Wait_D2,
		Wait_CK
	) with Size => 1;
	for Read_Wait_Mode use (
		Wait_D2 => 0,
		Wait_CK => 1
	);

	type Data_Control_Register is record
		DTEN        : Boolean                 := False;              -- DTEN
		DTDIR       : Data_Transfer_Direction := Controller_to_Card; -- Data transfer direction
		DTMODE      : Data_Transfer_Mode      := Block_Transfer;     -- Data transfer mode selection
		DMAEN       : Boolean                 := False;              -- DMA enable bit
		DBLOCKSIZE  : Integer range 0 .. 14   := 0;                  -- Log base 2 of data block size in bytes (0 - 1 byte, 1 - 2 bytes etc)
		RWSTART     : Boolean                 := False;              -- Read wait start
		RWSTOP      : Boolean                 := False;              -- Read wait stop if RWSTART is set
		RWMOD       : Read_Wait_Mode          := Wait_D2;            -- Read wait mode
		SDIOEN      : Boolean                 := False;              -- SD I/O enable functions
		Unused_12   : Integer range 0 .. 2**20 - 1 := 0;
	end record with Size => 32;
	for Data_Control_Register use record
		DTEN        at 0 range  0 ..  0;
		DTDIR       at 0 range  1 ..  1;
		DTMODE      at 0 range  2 ..  2;
		DMAEN       at 0 range  3 ..  3;
		DBLOCKSIZE  at 0 range  4 ..  7;
		RWSTART     at 0 range  8 ..  8;
		RWSTOP      at 0 range  9 ..  9;
		RWMOD       at 0 range 10 .. 10;
		SDIOEN      at 0 range 11 .. 11;
		Unused_12   at 0 range 12 .. 31;
	end record;

	-- STA, ICR (not all bits are used), MASK

	type Status_Register is record
		CCRCFAIL    : Boolean := False; -- Command response received (CRC check failed)
		DCRCFAIL    : Boolean := False; -- Data block sent or received (CRC check failed)
		CTIMEOUT    : Boolean := False; -- Command response timeout
		DTIMEOUT    : Boolean := False; -- Data timeout
		TXUNDERR    : Boolean := False; -- Transmit FIFO underrun error
		RXOVERR     : Boolean := False; -- Receive FIFO overrun error
		CMDREND     : Boolean := False; -- Command response received (CRC check passed)
		CMDSENT     : Boolean := False; -- Command sent (no response required)
		DATAEND     : Boolean := False; -- Data end (SDIDCOUNT is 0)
		STBITERR    : Boolean := False; -- Start bit not detected on all data signals in wide bus mode
		DBCKEND     : Boolean := False; -- Data block sent or received (CRC check passed)
		CMDACT      : Boolean := False; -- Command transfer in
		TXACT       : Boolean := False; -- Data transmit in progress
		RXACT       : Boolean := False; -- Data receive in progress
		TXFIFOHE    : Boolean := False; -- Transmit FIFO half empty: at least 8 words can be written into the FIFO
		RXFIFOHF    : Boolean := False; -- Receive FIFO half full: there are at least 8 words in the FIFO
		TXFIFOF     : Boolean := False; -- Transmit FIFO full
		RXFIFOF     : Boolean := False; -- Receive FIFO full
		TXFIFOE     : Boolean := False; -- Transmit FIFO empty
		RXFIFOE     : Boolean := False; -- Receive FIFO empty
		TXDAVL      : Boolean := False; -- Data available in transmit FIFO
		RXDAVL      : Boolean := False; -- Data available in receive FIFO
		SDIOIT      : Boolean := False; -- SDIO interrupt received
		CEATAEND    : Boolean := False; -- CE-ATA command completion signal receiver for CMD61
		Unused_24   : Integer range 0 .. 2**8 - 1 := 0;
	end record with Size => 32;
	for Status_Register use record
		CCRCFAIL    at 0 range  0 ..  0;
		DCRCFAIL    at 0 range  1 ..  1;
		CTIMEOUT    at 0 range  2 ..  2;
		DTIMEOUT    at 0 range  3 ..  3;
		TXUNDERR    at 0 range  4 ..  4;
		RXOVERR     at 0 range  5 ..  5;
		CMDREND     at 0 range  6 ..  6;
		CMDSENT     at 0 range  7 ..  7;
		DATAEND     at 0 range  8 ..  8;
		STBITERR    at 0 range  9 ..  9;
		DBCKEND     at 0 range 10 .. 10;
		CMDACT      at 0 range 11 .. 11;
		TXACT       at 0 range 12 .. 12;
		RXACT       at 0 range 13 .. 13;
		TXFIFOHE    at 0 range 14 .. 14;
		RXFIFOHF    at 0 range 15 .. 15;
		TXFIFOF     at 0 range 16 .. 16;
		RXFIFOF     at 0 range 17 .. 17;
		TXFIFOE     at 0 range 18 .. 18;
		RXFIFOE     at 0 range 19 .. 19;
		TXDAVL      at 0 range 20 .. 20;
		RXDAVL      at 0 range 21 .. 21;
		SDIOIT      at 0 range 22 .. 22;
		CEATAEND    at 0 range 23 .. 23;
		Unused_24   at 0 range 24 .. 31;
	end record;

	-- FIFOCNT

	type FIFO_Counter_Register is record
		FIFOCOUNT   : Integer range 0 .. 2**24 - 1; -- Remaining number of words to b
		Unused_24   : Integer range 0 .. 2**8 - 1;

	end record with Size => 32;
	for FIFO_Counter_Register use record
		FIFOCOUNT   at 0 range  0 .. 23;
		Unused_24   at 0 range 24 .. 31;

	end record;

	--

	type SDIO_Registers is record
		POWER       : Power_Control_Register;
		pragma Volatile_Full_Access (POWER);
		CLKCR       : Clock_Control_Register;
		pragma Volatile_Full_Access (CLKCR);
		ARG         : Unsigned_32;
		pragma Volatile_Full_Access (ARG);
		CMD         : Command_Register;
		pragma Volatile_Full_Access (CMD);
		RESPCMD     : Command_Response_Register;
		pragma Volatile_Full_Access (RESPCMD);
		RESP        : Response_Registers;
		DTIMER      : Unsigned_32;
		pragma Volatile_Full_Access (DTIMER);
		DLEN        : Data_Length_Register;
		pragma Volatile_Full_Access (DLEN);
		DCTRL       : Data_Control_Register;
		pragma Volatile_Full_Access (DCTRL);
		DCOUNT      : Data_Length_Register;
		pragma Volatile_Full_Access (DCOUNT);
		STA         : Status_Register;
		pragma Volatile_Full_Access (STA);
		ICR         : Status_Register;
		pragma Volatile_Full_Access (ICR);
		MASK        : Status_Register;
		pragma Volatile_Full_Access (MASK);
		FIFOCNT     : FIFO_Counter_Register;
		pragma Volatile_Full_Access (FIFOCNT);
		FIFO        : Unsigned_32;
		pragma Volatile_Full_Access (FIFO);
	end record;
	for SDIO_Registers use record
		POWER       at 16#00# range 0 .. 31;
		CLKCR       at 16#04# range 0 .. 31;
		ARG         at 16#08# range 0 .. 31;
		CMD         at 16#0C# range 0 .. 31;
		RESPCMD     at 16#10# range 0 .. 31;
		RESP        at 16#14# range 0 .. 32 * 4 - 1;
		DTIMER      at 16#24# range 0 .. 31;
		DLEN        at 16#28# range 0 .. 31;
		DCTRL       at 16#2C# range 0 .. 31;
		DCOUNT      at 16#30# range 0 .. 31;
		STA         at 16#34# range 0 .. 31;
		ICR         at 16#38# range 0 .. 31;
		MASK        at 16#3C# range 0 .. 31;
		FIFOCNT     at 16#48# range 0 .. 31;
		FIFO        at 16#80# range 0 .. 31;
	end record;

	SDIO   : aliased SDIO_Registers with Volatile, Import, Address => Address_Map.SDIO;

end STM32.SDIO_Interface;
