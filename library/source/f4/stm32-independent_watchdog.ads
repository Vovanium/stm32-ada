with STM32.Address_Map;

-- Independent Watchdog

package STM32.Independent_Watchdog is

	-- KR

	type Key_Code is new Integer range 0 .. 2**16 - 1;

	Key_Reset         : constant Key_Code := 16#AAAA#; -- Resets the watchdog
	Key_Enable_Access : constant Key_Code := 16#5555#; -- Enables access to PR and RLR
	Key_Start         : constant Key_Code := 16#CCCC#; -- Starts the watchdog

	type Key_Register is record
		KEY         : Key_Code := 0; -- Key value (write only, read 0000h)
		Unused_16   : Integer range 0 .. 2**16 - 1 := 0;
	end record with Size => 32;
	for Key_Register use record
		KEY         at 0 range  0 .. 15;
		Unused_16   at 0 range 16 .. 31;
	end record;

	-- PR

	type Prescaler_Factor is (
		Div_4,
		Div_8,
		Div_16,
		Div_32,
		Div_64,
		Div_128,
		Div_256,
		Div_256_A
	) with Size => 3;
	for Prescaler_Factor use (
		Div_4     => 2#000#,
		Div_8     => 2#001#,
		Div_16    => 2#010#,
		Div_32    => 2#011#,
		Div_64    => 2#100#,
		Div_128   => 2#101#,
		Div_256   => 2#110#,
		Div_256_A => 2#111#
	);

	type Prescaler_Register is record
		PR          : Prescaler_Factor := Div_4; -- Prescaler divider
		Unused_3    : Integer range 0 .. 2**29 - 1 := 0;
	end record with Size => 32;
	for Prescaler_Register use record
		PR          at 0 range  0 ..  2;
		Unused_3    at 0 range  3 .. 31;
	end record;

	-- RLR

	type Reload_Register is record
		RL          : Integer range 0 .. 2**12 - 1 := 16#FFF#; -- Watchdog counter reload
		Unused_12   : Integer range 0 .. 2**20 - 1 := 0;
	end record with Size => 32;
	for Reload_Register use record
		RL          at 0 range  0 .. 11;
		Unused_12   at 0 range 12 .. 31;
	end record;

	-- SR

	type Status_Register is record
		PVU         : Boolean; -- Watchdog prescaler value update
		RVU         : Boolean; -- Watchdog counter reload value update
		Unused_2    : Integer range 0 .. 2**30 - 1;
	end record with Size => 32;
	for Status_Register use record
		PVU         at 0 range  0 ..  0;
		RVU         at 0 range  1 ..  1;
		Unused_2    at 0 range  2 .. 31;
	end record;

	--

	type IWDG_Registers is record
		KR          : Key_Register;
		pragma Volatile_Full_Access (KR);
		PR          : Prescaler_Register;
		pragma Volatile_Full_Access (PR);
		RLR         : Reload_Register;
		pragma Volatile_Full_Access (RLR);
		SR          : Status_Register;
		pragma Volatile_Full_Access (SR);

	end record;
	for IWDG_Registers use record
		KR          at 16#00# range 0 .. 31;
		PR          at 16#04# range 0 .. 31;
		RLR         at 16#08# range 0 .. 31;
		SR          at 16#0C# range 0 .. 31;

	end record;

	IWDG   : aliased IWDG_Registers with Volatile, Import, Address => Address_Map.IWDG;

end STM32.Independent_Watchdog;
