package body STM32.USARTs is

	function Baud_Rate (
		Speed         : Positive;
		OVER8         : Boolean;
		Bus_Frequency : Positive)
		return          Baud_Rate_Register
	is
		Ratio         : Natural := (Bus_Frequency + Speed / 2) / Speed;
		Oversampling  : Positive := (if OVER8 then 8 else 16);
	begin
		return (DIV_Mantissa => Ratio / Oversampling,
			DIV_Fraction => Ratio mod Oversampling,
			others => 0);
	end Baud_Rate;

end STM32.USARTs;
