with STM32.Address_Map;

-- Serial Peripheral Interface Controllers
--
-- Units exist an all STM32F4 devices in various combinations
--
-- Availability chart:
-- SPI#  1 2 3 4 5 6
-- F401  + + + + - -
-- F405  + + + - - -
-- F407  + + + - - -
-- F410  + + - - - -
-- F411  + + + + + -
-- F412  + + + + + -
-- F413  + + + + + -
-- F415  + + + - - -
-- F417  + + + - - -
-- F423  + + + + + -
-- F427  + + + + + +
-- F429  + + + + + +
-- F437  + + + + + +
-- F446  + + + + - -
-- F469  + + + + + +
-- F479  + + + + + +
-- They all are identical

package STM32.SPIs is

	type Clock_Phase is (
		Late_Clock, -- First clock transition is the first data capture edge
		Early_Clock -- Second clock transition is the first data capture edge
	) with Size => 1;
	for Clock_Phase use (
		Late_Clock  => 0,
		Early_Clock => 1
	);

	type Clock_Polarity is (
		Positive_Clock, -- CK to 0 when idle, first transition is rising
		Negative_Clock  -- CK to 1 when idle, first transition is falling
	) with Size => 1;
	for Clock_Polarity use (
		Positive_Clock => 0,
		Negative_Clock => 1
	);

	type Master_Selection is (
		Slave,
		Master
	) with Size => 1;
	for Master_Selection use (
		Slave  => 0,
		Master => 1
	);

	type Baud_Rate is (
		PCLK_DIV_2,
		PCLK_DIV_4,
		PCLK_DIV_8,
		PCLK_DIV_16,
		PCLK_DIV_32,
		PCLK_DIV_64,
		PCLK_DIV_128,
		PCLK_DIV_256
	) with Size => 3;
	for Baud_Rate use (
		PCLK_DIV_2   => 2#000#,
		PCLK_DIV_4   => 2#001#,
		PCLK_DIV_8   => 2#010#,
		PCLK_DIV_16  => 2#011#,
		PCLK_DIV_32  => 2#100#,
		PCLK_DIV_64  => 2#101#,
		PCLK_DIV_128 => 2#110#,
		PCLK_DIV_256 => 2#111#
	);

	type Data_Frame_Format is (
		Frame_8_Bit,
		Frame_16_Bit
	) with Size => 1;
	for Data_Frame_Format use (
		Frame_8_bit  => 0,
		Frame_16_bit => 1
	);

	type Bidirectional_Mode is (
		Unidirectional,
		Bidirectional
	) with Size => 1;
	for Bidirectional_Mode use (
		Unidirectional => 0,
		Bidirectional  => 1
	);

	type Control_Register_1 is record
		CPHA        : Clock_Phase;
		CPOL        : Clock_Polarity;
		MSTR        : Master_Selection;
		BR          : Baud_Rate;
		SPE         : Boolean;
		LSBFIRST    : Boolean;
		SSI         : Boolean;
		SSM         : Boolean;
		RXONLY      : Boolean;
		DFF         : Data_Frame_Format;
		CRCNEXT     : Boolean;
		CRCEN       : Boolean;
		BIDIOE      : Boolean;
		BIDIMODE    : Bidirectional_Mode;
	end record with Size => 16;
	for Control_Register_1 use record
		CPHA        at 0 range  0 ..  0;
		CPOL        at 0 range  1 ..  1;
		MSTR        at 0 range  2 ..  2;
		BR          at 0 range  3 ..  5;
		SPE         at 0 range  6 ..  6;
		LSBFIRST    at 0 range  7 ..  7;
		SSI         at 0 range  8 ..  8;
		SSM         at 0 range  9 ..  9;
		RXONLY      at 0 range 10 .. 10;
		DFF         at 0 range 11 .. 11;
		CRCNEXT     at 0 range 12 .. 12;
		CRCEN       at 0 range 13 .. 13;
		BIDIOE      at 0 range 14 .. 14;
		BIDIMODE    at 0 range 15 .. 15;
	end record;

	--

	type Frame_Format is (
		Motorola_Mode,
		TI_Mode
	) with Size => 1;
	for Frame_Format use (
		Motorola_Mode => 0,
		TI_Mode       => 1
	);

	type Control_Register_2 is record
		RXDMAEN     : Boolean;
		TXDMAEN     : Boolean;
		SSOE        : Boolean;
		Reserved_3  : Integer range 0 .. 1;
		FRF         : Frame_Format;
		ERRIE       : Boolean;
		RXNEIE      : Boolean;
		TXEIE       : Boolean;
		Reserved_8  : Integer range 0 .. 2**8 - 1;
	end record with Size => 16;
	for Control_Register_2 use record
		RXDMAEN     at 0 range  0 ..  0;
		TXDMAEN     at 0 range  1 ..  1;
		SSOE        at 0 range  2 ..  2;
		Reserved_3  at 0 range  3 ..  3;
		FRF         at 0 range  4 ..  4;
		ERRIE       at 0 range  5 ..  5;
		RXNEIE      at 0 range  6 ..  6;
		TXEIE       at 0 range  7 ..  7;
		Reserved_8  at 0 range  8 .. 15;
	end record;

	--

	type Channel_Side is (
		Channel_Left,
		Channel_Right
	) with Size => 1;
	for Channel_Side use (
		Channel_Left  => 0,
		Channel_Right => 1
	);

	type Status_Register is record
		RXNE        : Boolean;
		TXE         : Boolean;
		CHSIDE      : Channel_Side;
		UDR         : Boolean;
		CRCERR      : Boolean;
		MODF        : Boolean;
		OVR         : Boolean;
		BSY         : Boolean;
		FRE         : Boolean;
		Reserved    : Integer range 0 .. 2**7 - 1;
	end record with Size => 16;
	for Status_Register use record
		RXNE        at 0 range  0 ..  0;
		TXE         at 0 range  1 ..  1;
		CHSIDE      at 0 range  2 ..  2;
		UDR         at 0 range  3 ..  3;
		CRCERR      at 0 range  4 ..  4;
		MODF        at 0 range  5 ..  5;
		OVR         at 0 range  6 ..  6;
		BSY         at 0 range  7 ..  7;
		FRE         at 0 range  8 ..  8;
		Reserved    at 0 range  9 .. 15;
	end record;

	--

	type Channel_Length is (
		Channel_16_Bit,
		Channel_32_Bit
	) with Size => 1;
	for Channel_Length use (
		Channel_16_Bit => 0,
		Channel_32_Bit => 1
	);

	type Data_Length is (
		Data_16_Bit,
		Data_24_Bit,
		Data_32_Bit
	) with Size => 2;
	for Data_Length use (
		Data_16_Bit => 2#00#,
		Data_24_Bit => 2#01#,
		Data_32_Bit => 2#10#
	);

	type I2S_Standard is (
		Philips_Standard,
		MSB_Justified_Standard,
		LSB_Justified_Standard,
		PCM_Standard
	) with Size => 2;
	for I2S_Standard use (
		Philips_Standard       => 2#00#,
		MSB_Justified_Standard => 2#01#,
		LSB_Justified_Standard => 2#10#,
		PCM_Standard           => 2#11#
	);

	type PCM_Frame_Synchronization is (
		Short_Frame,
		Long_Frame
	) with Size => 1;
	for PCM_Frame_Synchronization use (
		Short_Frame => 0,
		Long_Frame  => 1
	);

	type I2S_Configuration_Mode is (
		Slave_Transmit,
		Slave_Receive,
		Master_Transmit,
		Master_Receive
	) with Size => 22;
	for I2S_Configuration_Mode use (
		Slave_Transmit  => 2#00#,
		Slave_Receive   => 2#01#,
		Master_Transmit => 2#10#,
		Master_Receive  => 2#11#
	);

	type I2S_Configuration_Register is record
		CHLEN       : Channel_Length;
		DATLEN      : Data_Length;
		CKPOL       : Clock_Polarity;
		I2SSTD      : I2S_Standard;
		Reserved_6  : Integer range 0 .. 1;
		PCMSYNC     : PCM_Frame_Synchronization;
		I2SCFG      : I2S_Configuration_Mode;
		I2SE        : Boolean;
		I2SMOD      : Boolean;
		Reserved_12 : Integer range 0 .. 2**4 - 1;
	end record with Size => 16;
	for I2S_Configuration_Register use record
		CHLEN       at 0 range  0 ..  0;
		DATLEN      at 0 range  1 ..  2;
		CKPOL       at 0 range  3 ..  3;
		I2SSTD      at 0 range  4 ..  5;
		Reserved_6  at 0 range  6 ..  6;
		PCMSYNC     at 0 range  7 ..  7;
		I2SCFG      at 0 range  8 ..  9;
		I2SE        at 0 range 10 .. 10;
		I2SMOD      at 0 range 11 .. 11;
		Reserved_12 at 0 range 12 .. 15;
	end record;

	--

	type I2S_Prescaler_Register is record
		I2SDIV      : Integer range 2 .. 2**8 - 1;
		ODD         : Boolean;
		MCKOE       : Boolean;
		Reserved    : Integer range 0 .. 2**6 - 1;
	end record with Size => 16;
	for I2S_Prescaler_Register use record
		I2SDIV      at 0 range  0 ..  7;
		ODD         at 0 range  8 ..  8;
		MCKOE       at 0 range  9 ..  9;
		Reserved    at 0 range 10 .. 15;
	end record;

	--

	type SPI_Registers is record
		CR1        : Control_Register_1;
		pragma Volatile_Full_Access (CR1);
		CR2        : Control_Register_2;
		pragma Volatile_Full_Access (CR2);
		SR         : Status_Register;
		pragma Volatile_Full_Access (SR);
		DR         : Unsigned_16;
		pragma Volatile_Full_Access (DR);
		CRCPR      : Unsigned_16;
		pragma Volatile_Full_Access (CRCPR);
		RXCRCR     : Unsigned_16;
		pragma Volatile_Full_Access (RXCRCR);
		TXCRCR     : Unsigned_16;
		pragma Volatile_Full_Access (TXCRCR);
		I2SCFGR    : I2S_Configuration_Register;
		pragma Volatile_Full_Access (I2SCFGR);
		I2SPR      : I2S_Prescaler_Register;
		pragma Volatile_Full_Access (I2SPR);
	end record with Volatile;
	for SPI_Registers use record
		CR1        at 16#00# range 0 .. 15;
		CR2        at 16#04# range 0 .. 15;
		SR         at 16#08# range 0 .. 15;
		DR         at 16#0C# range 0 .. 15;
		CRCPR      at 16#10# range 0 .. 15;
		RXCRCR     at 16#14# range 0 .. 15;
		TXCRCR     at 16#18# range 0 .. 15;
		I2SCFGR    at 16#1C# range 0 .. 15;
		I2SPR      at 16#20# range 0 .. 15;
	end record;

	SPI1 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI1;

	SPI2 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI2_I2S2;

	SPI3 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI3_I2S3;

	SPI4 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI4;

	SPI5 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI5;

	SPI6 : aliased SPI_Registers with Volatile, Import,
	Address => Address_Map.SPI6;

	I2S2 : SPI_Registers renames SPI2;
	I2S3 : SPI_Registers renames SPI3;

end STM32.SPIs;
