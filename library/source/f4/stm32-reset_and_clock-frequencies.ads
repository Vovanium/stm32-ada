generic
	HSE_Frequency : Natural;               -- This parameter is defined by circuit
	HSI_Frequency : Natural := 16_000_000;
	LSE_Frequency : Natural :=     32_768;
	LSI_Frequency : Natural :=     32_000;
	RCC : RCC_Registers := Reset_and_Clock.RCC;
package STM32.Reset_and_Clock.Frequencies is
	function VCO_Input_Frequency return Natural;
	function PLLCLK_Frequency return Natural;
	function USB_Frequency return Natural;
	function HCLK_Frequency return Natural;
	function System_Timer_Clock_Frequency return Natural;
	function PCLK1_Frequency return Natural;
	function PCLK2_Frequency return Natural;
	function APB1_Timer_Clock_Frequency return Natural;
	function APB2_Timer_Clock_Frequency return Natural;
	function I2S_Clock_Frequency (External_Frequency : Natural := 0) return Natural;
	function SAI1A_Clock_Frequency (External_Frequency : Natural := 0) return Natural;
	function SAI1B_Clock_Frequency (External_Frequency : Natural := 0) return Natural;
	function LCD_Clock_Frequency return natural;
	function PLLI2S_N (Frequency : Natural) return Natural;
	function PLLSAI_N (Frequency : Natural) return Natural;
	procedure LCD_R (Frequency : Natural; R : out Natural; DIVR : out PLLSAI_Division_Factor);
end STM32.Reset_and_Clock.Frequencies;