with STM32.Address_Map;

-- Digital to Analog Convertors
--
-- Units exist in all F4 devices except F4x1

package STM32.Digital_to_Analog_Convertors is
	-- Correspondence in register names
	-- DHR12Rx   -- DHR (x).DHR12R
	-- DHR12Lx   -- DHR (x).DHR12L
	-- DHR8Rx    -- DHR (x).DHR8R

	-- Correspondence in field names
	-- ENx       -- CR (x).EN
	-- BOFFx     -- CR (x).BOFF
	-- TENx      -- CR (x).TEN
	-- TSELx     -- CR (x).TSEL
	-- WAVEx     -- CR (x).WAVE
	-- MAMPx     -- CR (x).MAMP
	-- DMAENx    -- CR (x).DMAEN
	-- DMAUDRIEx -- CR (x).DMAUDRIE
	-- DACCxDHR  -- DHR12* (x)             (12-bit)
	-- DACCxHDR  -- DHR8RD.DACCHDR (x)     (8-bit)
	-- DMAUDRx   -- SR (x).DMAUDR

	-- CR

	type Trigger_Source is (
		Timer_6_TRGO,
		Timer_8_TRGO,
		Timer_7_TRGO,
		Timer_5_TRGO,
		Timer_2_TRGO,
		Timer_4_TRGO,
		EXTI_Line9,
		Software
	) with Size => 3;
	for Trigger_Source use (
		Timer_6_TRGO => 2#000#,
		Timer_8_TRGO => 2#001#,
		Timer_7_TRGO => 2#010#,
		Timer_5_TRGO => 2#011#,
		Timer_2_TRGO => 2#100#,
		Timer_4_TRGO => 2#101#,
		EXTI_Line9   => 2#110#,
		Software     => 2#111#
	);

	type Wave_Type is (
		No_Wave,
		Noise,
		Triangle,
		Triangle_A
	) with Size => 2;
	for Wave_Type use (
		No_Wave    => 2#00#,
		Noise      => 2#01#,
		Triangle   => 2#10#,
		Triangle_A => 2#11#
	);

	type Channel_Control is record
		EN          : Boolean := False; -- Channel enable
		BOFF        : Boolean := False; -- Output buffer disable
		TEN         : Boolean := False; -- Trigger enable
		TSEL        : Trigger_Source := Timer_6_TRGO; -- Trigger source
		WAVE        : Wave_Type := No_Wave; -- Wave select
		MAMP        : Integer range 0 .. 15 := 0; -- Mask selector (number of bits minus 1)
		DMAEN       : Boolean := False; -- DMA enable
		DMAUDRIE    : Boolean := False; -- DMA underrun interrupt enable
		Reserved    : Integer range 0 .. 3 := 0;
	end record with Size => 16;
	for Channel_Control use record
		EN          at 0 range  0 ..  0;
		BOFF        at 0 range  1 ..  1;
		TEN         at 0 range  2 ..  2;
		TSEL        at 0 range  3 ..  5;
		WAVE        at 0 range  6 ..  7;
		MAMP        at 0 range  8 .. 11;
		DMAEN       at 0 range 12 .. 12;
		DMAUDRIE    at 0 range 13 .. 13;
		Reserved    at 0 range 14 .. 15;
	end record;

	subtype Channel_Index is Integer range 1 .. 2;

	type Control_Register is array (Channel_Index) of Channel_Control with Component_Size => 16, Size => 32;

	-- SWTRIGR

	type Channel_Trigger is array (Channel_Index) of Boolean with Component_Size => 1, Size => 2;

	type Software_Trigger_Register is record
		SWTRIG      : Channel_Trigger              := (others => false); -- Software channel trigger enable
		Reserved    : Integer range 0 .. 2**30 - 1 := 0;
	end record with Size => 32;
	for Software_Trigger_Register use record
		SWTRIG      at 0 range  0 ..  1;
		Reserved    at 0 range  2 .. 31;
	end record;

	-- DHR12Rx, DHR12Lx and DHR8Rx together

	type Data_Registers is record
		DHR12R      : Integer range 0 .. 2**12 - 1  := 0; -- Right aligned 12-bit data register for channel 1
		pragma Volatile_Full_Access (DHR12R);
		DHR12L      : Integer range 0 .. 2**16 - 16 := 0; -- left aligned data must have lowest 4 bits clear
		pragma Volatile_Full_Access (DHR12L);
		DHR8R       : Integer range 0 .. 2**8 - 1   := 0;
		pragma Volatile_Full_Access (DHR8R);
	end record with Size => 32 * 3;
	for Data_Registers use record
		DHR12R      at 16#0# range 0 .. 31;
		DHR12L      at 16#4# range 0 .. 31;
		DHR8R       at 16#8# range 0 .. 31;
	end record;

	type Data_Register_Array is array (Channel_Index) of Data_Registers with Size => 32 * 3 * 2, Component_Size => 32 * 3;

	-- DHR12RD, DHR12LD

	type Dual_Data_Register_16 is array (Channel_Index) of Integer range 0 .. 2**16 - 1
	with Pack, Component_Size => 16, Size => 32;

	-- DHR8RD

	type Dual_Data_8 is array (Channel_Index) of Integer range 0 .. 2**8 - 1
	with Pack, Component_Size => 8, Size => 16;

	type Dual_Data_Register_8 is record
		DACCDHR     : Dual_Data_8 := (others => 0); -- Channel data high 8 bits
		Reserved    : Integer range 0 .. 2**16 - 1 := 0;

	end record with Size => 32;
	for Dual_Data_Register_8 use record
		DACCDHR     at 0 range  0 .. 15;
		Reserved    at 0 range 16 .. 31;
	end record;

	-- DORx together

	type Data_Array_32 is array (Channel_Index) of Integer range 0 .. 2**12 - 1 with Component_Size => 32, Size => 64;

	-- SR

	type Channel_Status is record
		Reserved_0  : Integer range 0 .. 2**13 - 1 := 0;
		DMAUDR      : Boolean                      := False; -- DAC channel DMA underrun occured
		Reserved_14 : Integer range 0 .. 3         := 0;
	end record with Size => 16;
	for Channel_Status use record
		Reserved_0  at 0 range  0 .. 12;
		DMAUDR      at 0 range 13 .. 13;
		Reserved_14 at 0 range 14 .. 15;
	end record;

	type Status_Register is array (Channel_Index) of Channel_Status with Pack, Component_Size => 16, Size => 32;

	--

	type DAC_Registers is record
		CR          : Control_Register;
		SWTRIGR     : Software_Trigger_Register;
		DHR         : Data_Register_Array;
		DHR12RD     : Dual_Data_Register_16        := (others => 0); -- Channel data right aligned, packed in half-words
		DHR12LD     : Dual_Data_Register_16        := (others => 0); -- Channel data left aligned, packed in half-words
		DHR8RD      : Dual_Data_Register_8;  -- Channel data high 8 bits, packed in bytes
		DOR         : Data_Array_32                := (others => 0); -- Data output registers
		SR          : Status_Register;
	end record;
	for DAC_Registers use record
		CR          at 16#0# range 0 .. 31;
		SWTRIGR     at 16#4# range 0 .. 31;
		DHR         at 16#8# range 0 .. 32 * 3 * 2 - 1;
		DHR12RD     at 16#20# range 0 .. 31;
		DHR12LD     at 16#24# range 0 .. 31;
		DHR8RD      at 16#28# range 0 .. 31;
		DOR         at 16#2C# range 0 .. 32 * 2 - 1;
		SR          at 16#34# range 0 .. 31;
	end record;

	DAC : DAC_Registers with Volatile, Import, Address => Address_Map.DAC;

end STM32.Digital_to_Analog_Convertors;
