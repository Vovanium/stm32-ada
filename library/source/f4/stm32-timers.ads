with STM32.Address_Map;

-- Timers, Including Basic, General Purpose (both kids) and Advanced Control

package STM32.Timers is

	-- There's some differences in SVD files between devices,
	-- however according to reference manuals
	-- timers are seem to be identical across the whole STM32F4 family.
	-- Some SVD differences are noted here.

	type Channel_Index is range 1 .. 4;

	--
	-- CR1
	--

	type Count_Direction is (
		Count_Up,
		Count_Down) with Size => 1;

	type Align_Mode is (
		Edge_Aligned,            -- Counts only in direction specified in DIR
		Center_Aligned_OC_Down,  -- Counts in both directions alternatively,
		                         -- OC interrupts are set when counting down
		Center_Aligned_OC_Up,    -- Counts in both directions alternatively,
		                         -- OC interrupts are set when counting up
		Center_Aligned_OC_Both)  -- Counts in both directions alternatively,
		                         -- OC interrupts are set when counting in either direction
	with Size => 2;

	type Update_Request_Source is (
		Any_Source,
		Counter_Overflow) with Size => 1;

	type Sampling_Clock is new Logarithmic range Value_1 .. Value_8 with Size => 2;

	type Control_Register_1 is record
		CEN       : Boolean               := False;        -- Counter enable
		UDIS      : Boolean               := False;        -- Update disable
		URS       : Update_Request_Source := Any_source;   -- Update request source
		OPM       : Boolean               := False;        -- One-pulse mode
		DIR       : Count_Direction       := Count_Up;     -- Direction
		CMS       : Align_Mode            := Edge_Aligned; -- Center-aligned mode
		ARPE      : Boolean               := False;        -- Auto-reload preload enable
		CKD       : Sampling_Clock        := Value_1;      -- Clock division
		Unused_10 : Unused_1_Bit          := 0;
		UIFREMAP  : Boolean               := False;        -- UIF status bit remapping
		Unused_12 : Unused_4_Bits         := 0;
	end record with Size => 16;
	for Control_Register_1 use record      -- TIM: 1,2-5,8  9-14  6,7
		CEN       at 0 range  0 ..  0; --      +        +     +
		UDIS      at 0 range  1 ..  1; --      +        +     +
		URS       at 0 range  2 ..  2; --      +        +     +
		OPM       at 0 range  3 ..  3; --      +        +     +
		DIR       at 0 range  4 ..  4; --      +        -     -
		CMS       at 0 range  5 ..  6; --      +        -     -
		ARPE      at 0 range  7 ..  7; --      +        +     +
		CKD       at 0 range  8 ..  9; --      +        +     -
		Unused_10 at 0 range 10 .. 10;
		UIFREMAP  at 0 range 11 .. 11; --      -        -     ?
		Unused_12 at 0 range 12 .. 15;
	end record;

	-- Note: UIFREMAP bit is not documented but exist in SVD of STM32F412/413.
	--       This may be a bug in SVD or not yet documented feature.

	--
	-- CR2
	--

	type CCPC_Update is (
		COMG_Only,
		COMG_or_TRGI_Edge) with Size => 1;

	type CC_DMA_Request is (
		DMA_on_CC,
		DMA_on_Update) with Size => 1;

	type Master_Mode is (
		Master_Reset,
		Master_Enable,
		Master_Update,
		Master_Compare_Pulse,
		Master_Compare_OC1REF,
		Master_Compare_OC2REF,
		Master_Compare_OC3REF,
		Master_Compare_OC4REF) with Size => 3;
	-- In TIM6, TIM7 only Master_Reset, Master_Enable and Master_Enable values allowed.

	type TI1_Connection is (
		Channel_1_Only,
		Channels_1_2_3_Xored) with Size => 1;

	type Control_Register_2 is record
		CCPC      : Boolean        := False;          -- Capture/compare preloaded
		Unused_1  : Unused_1_Bit   := 0;
		CCUS      : CCPC_Update    := COMG_Only;      -- Capture/compare control update
		CCDS      : CC_DMA_Request := DMA_on_CC;      -- Capture/compare DMA
		MMS       : Master_Mode    := Master_Reset;   -- Master mode selection
		TI1S      : TI1_Connection := Channel_1_Only; -- TI1 selection
		OIS1      : Boolean        := False;          -- Output Idle state 1
		OIS1N     : Boolean        := False;          -- Output Idle state 1
		OIS2      : Boolean        := False;          -- Output Idle state 2
		OIS2N     : Boolean        := False;          -- Output Idle state 2
		OIS3      : Boolean        := False;          -- Output Idle state 3
		OIS3N     : Boolean        := False;          -- Output Idle state 3
		OIS4      : Boolean        := False;          -- Output Idle state 4
		Unused_15 : Unused_1_Bit   := 0;
	end record with Size => 16;
	for Control_Register_2 use record      -- TIM: 1,8  2-5  6,7  9
		CCPC      at 0 range  0 ..  0; --      +    -    -
		Unused_1  at 0 range  1 ..  1;
		CCUS      at 0 range  2 ..  2; --      +    -    -
		CCDS      at 0 range  3 ..  3; --      +    +    -
		MMS       at 0 range  4 ..  6; --      +    +    +    ?
		TI1S      at 0 range  7 ..  7; --      +    +    -
		OIS1      at 0 range  8 ..  8; --      +    -    -
		OIS1N     at 0 range  9 ..  9; --      +    -    -
		OIS2      at 0 range 10 .. 10; --      +    -    -
		OIS2N     at 0 range 11 .. 11; --      +    -    -
		OIS3      at 0 range 12 .. 12; --      +    -    -
		OIS3N     at 0 range 13 .. 13; --      +    -    -
		OIS4      at 0 range 14 .. 14; --      +    -    -
		Unused_15 at 0 range 15 .. 15;
	end record;

	--
	-- SMCR
	--

	type Slave_Mode is (
		Slave_Mode_Disabled,  --
		Slave_Encoder_Mode_1, --
		Slave_Encoder_Mode_2, --
		Slave_Encoder_Mode_3, --
		Slave_Reset,          -- Rising edge of TRGI reinitializes the counter and update the registers
		Slave_Gate,           -- Counting runs when TRGI is high else stops.
		Slave_Trigger,        -- Counting starts on a rising edge of TRGI (but not reset).
		Slave_Clock)          -- Counting rising edges of TRGI.
	with Size => 3;
	-- Slave_Encoder_Mode_(1,2,3) not present in GPT9

	type Trigger_Select is (
		Internal_Trigger_0,
		Internal_Trigger_1,
		Internal_Trigger_2,
		Internal_Trigger_3,
		TI1_Edge_Detector,
		Filtered_Input_1,
		Filtered_Input_2,
		External_Trigger)
	with Size => 3;
	-- External trigger not present in TIM9, TIM 12

	type Trigger_Filter is (
		DTS_Not_Filtered,
		CK_INT_Filter_2,
		CK_INT_Filter_4,
		CK_INT_Filter_8,
		DTS_DIV_2_Filter_6,
		DTS_DIV_2_Filter_8,
		DTS_DIV_4_Filter_6,
		DTS_DIV_4_Filter_8,
		DTS_DIV_8_Filter_6,
		DTS_DIV_8_Filter_8,
		DTS_DIV_16_Filter_5,
		DTS_DIV_16_Filter_6,
		DTS_DIV_16_Filter_8,
		DTS_DIV_32_Filter_5,
		DTS_DIV_32_Filter_6,
		DTS_DIV_32_Filter_8) with Size => 4;

	type Trigger_Prescaler is new Logarithmic range Value_1 .. Value_8 with Size => 2;

	type External_Trigger_Polarity is (Non_Inverted, Inverted) with Size => 1;

	type Slave_Mode_Control_Register is record
		SMS       : Slave_Mode                 := Slave_Mode_Disabled; -- Slave mode selection
		Unused_3  : Unused_1_Bit               := 0;
		TS        : Trigger_Select             := Internal_Trigger_0; -- Trigger selection
		MSM       : Boolean                    := False; -- Master/Slave mode
		ETF       : Trigger_Filter             := DTS_Not_Filtered; -- External trigger filter
		ETPS      : Trigger_Prescaler          := Value_1; -- External trigger prescaler
		ECE       : Boolean                    := False; -- External clock enable
		ETP       : External_Trigger_Polarity  := Non_Inverted; -- External trigger polarity
	end record with Size => 16;
	for Slave_Mode_Control_Register use record -- TIM: 1,2-5,8  9,12
		SMS       at 0 range  0 ..  2;     --      +        +
		Unused_3  at 0 range  3 ..  3;
		TS        at 0 range  4 ..  6;     --      +        +
		MSM       at 0 range  7 ..  7;     --      +        +
		ETF       at 0 range  8 .. 11;     --      +        -
		ETPS      at 0 range 12 .. 13;     --      +        -
		ECE       at 0 range 14 .. 14;     --      +        -
		ETP       at 0 range 15 .. 15;     --      +        -
	end record;

	--
	-- DIER
	--

	type DMA_Interrupt_Enable_Register is record
		UIE       : Boolean      := False; -- Update interrupt enable
		CC1IE     : Boolean      := False; -- Capture/Compare 1 interrupt
		CC2IE     : Boolean      := False; -- Capture/Compare 2 interrupt
		CC3IE     : Boolean      := False; -- Capture/Compare 3 interrupt
		CC4IE     : Boolean      := False; -- Capture/Compare 4 interrupt
		COMIE     : Boolean      := False; -- COM interrupt enable
		TIE       : Boolean      := False; -- Trigger interrupt enable
		BIE       : Boolean      := False; -- Break interrupt enable
		UDE       : Boolean      := False; -- Update DMA request enable
		CC1DE     : Boolean      := False; -- Capture/Compare 1 DMA request
		CC2DE     : Boolean      := False; -- Capture/Compare 2 DMA request
		CC3DE     : Boolean      := False; -- Capture/Compare 3 DMA request
		CC4DE     : Boolean      := False; -- Capture/Compare 4 DMA request
		COMDE     : Boolean      := False; -- COM DMA request enable
		TDE       : Boolean      := False; -- Trigger DMA request enable
		Unused_15 : Unused_1_Bit := 0;
	end record with Size => 16;
	for DMA_Interrupt_Enable_Register use record -- TIM: 1,8  2-5 9,12 10,11,13,14  6,7
		UIE       at 0 range  0 ..  0;       --      +    +   +    +            +
		CC1IE     at 0 range  1 ..  1;       --      +    +   +    +            -
		CC2IE     at 0 range  2 ..  2;       --      +    +   +    -            -
		CC3IE     at 0 range  3 ..  3;       --      +    +   -    -            -
		CC4IE     at 0 range  4 ..  4;       --      +    +   -    -            -
		COMIE     at 0 range  5 ..  5;       --      +    -   -    -            -
		TIE       at 0 range  6 ..  6;       --      +    +   +    -            -
		BIE       at 0 range  7 ..  7;       --      +    -   -    -            -
		UDE       at 0 range  8 ..  8;       --      +    +   -    -            +
		CC1DE     at 0 range  9 ..  9;       --      +    +   -    -            -
		CC2DE     at 0 range 10 .. 10;       --      +    +   -    -            -
		CC3DE     at 0 range 11 .. 11;       --      +    +   -    -            -
		CC4DE     at 0 range 12 .. 12;       --      +    +   -    -            -
		COMDE     at 0 range 13 .. 13;       --      +    -   -    -            -
		TDE       at 0 range 14 .. 14;       --      +    +   -    -            -
		Unused_15 at 0 range 15 .. 15;
	end record;

	--
	-- SR
	--

	type Status_Register is record
		UIF       : Boolean       := True; -- Update interrupt flag
		CC1IF     : Boolean       := True; -- Capture/compare 1 interrupt
		CC2IF     : Boolean       := True; -- Capture/Compare 2 interrupt
		CC3IF     : Boolean       := True; -- Capture/Compare 3 interrupt
		CC4IF     : Boolean       := True; -- Capture/Compare 4 interrupt
		COMIF     : Boolean       := True; -- COM interrupt flag
		TIF       : Boolean       := True; -- Trigger interrupt flag
		BIF       : Boolean       := True; -- Break interrupt flag
		Unused_8  : Unused_1_Bit  := 0;
		CC1OF     : Boolean       := True; -- Capture/Compare 1 overcapture
		CC2OF     : Boolean       := True; -- Capture/compare 2 overcapture
		CC3OF     : Boolean       := True; -- Capture/Compare 3 overcapture
		CC4OF     : Boolean       := True; -- Capture/Compare 4 overcapture
		Unused_13 : Unused_3_Bits := 0;
	end record with Size => 16;
	for Status_Register use record         -- TIM: 1,8  2-5  9,12  10,11,13,14  6,7
		UIF       at 0 range  0 ..  0; --      +    +    +     +            +
		CC1IF     at 0 range  1 ..  1; --      +    +    +     +            -
		CC2IF     at 0 range  2 ..  2; --      +    +    +     -            -
		CC3IF     at 0 range  3 ..  3; --      +    +    -     -            -
		CC4IF     at 0 range  4 ..  4; --      +    +    -     -            -
		COMIF     at 0 range  5 ..  5; --      +    -    -     -            -
		TIF       at 0 range  6 ..  6; --      +    +    +     -            -
		BIF       at 0 range  7 ..  7; --      +    -    -     -            -
		Unused_8  at 0 range  8 ..  8;
		CC1OF     at 0 range  9 ..  9; --      +    +    +     +            -
		CC2OF     at 0 range 10 .. 10; --      +    +    +     -            -
		CC3OF     at 0 range 11 .. 11; --      +    +    -     -            -
		CC4OF     at 0 range 12 .. 12; --      +    +    -     -            -
		Unused_13 at 0 range 13 .. 15;
	end record;

	--
	-- EGR
	--

	type Event_Generation_Register is record
		UG        : Boolean := False; -- Update generation
		CC1G      : Boolean := False; -- Capture/compare 1
		CC2G      : Boolean := False; -- Capture/compare 2
		CC3G      : Boolean := False; -- Capture/compare 3
		CC4G      : Boolean := False; -- Capture/compare 4
		COMG      : Boolean := False; -- Capture/Compare control update
		TG        : Boolean := False; -- Trigger generation
		BG        : Boolean := False; -- Break generation
		Unused_8  : Unused_8_Bits := 0;
	end record with Size => 16;
	for Event_Generation_Register use record -- TIM: 1,8  2-5  9,12  10,11,13,14  6,7
		UG        at 0 range  0 ..  0;   --      +    +    +     +            +
		CC1G      at 0 range  1 ..  1;   --      +    +    +     +            -
		CC2G      at 0 range  2 ..  2;   --      +    +    +     -            -
		CC3G      at 0 range  3 ..  3;   --      +    +    -     -            -
		CC4G      at 0 range  4 ..  4;   --      +    +    -     -            -
		COMG      at 0 range  5 ..  5;   --      +    -    -     -            -
		TG        at 0 range  6 ..  6;   --      +    +    +     -            -
		BG        at 0 range  7 ..  7;   --      +    -    -     -            -
		Unused_8  at 0 range  8 .. 15;
	end record;

	--
	-- CCMRx
	--

	type CC_Direction is (
		CC_Output,
		CC_Input_Primary,
		CC_Input_Alternate,
		CC_Input_TRC) with Size => 2;
	-- CC_Input_Alternate and CC_Input_TRC is not present in TIM10, TIM11, TIM13, TIM4.

	type Compare_Mode is (
		Frozen,
		Match_Set_to_Active,
		Match_Set_to_Inactive,
		Match_Toggle,
		Force_Inactive,
		Force_Active,
		PWM_True,
		PWM_Inverted) with Size => 3;

	type CC_Mode (S : CC_Direction := CC_Output) is record
		case S is
		when CC_Output =>
			FE  : Boolean      := False;  -- Output Compare fast
			PE  : Boolean      := False;  -- Output Compare preload
			M   : Compare_Mode := Frozen; -- Output Compare mode
			CE  : Boolean      := False;  -- Output Compare clear
		when others =>
			PCS : Trigger_Prescaler := Value_1; -- Input capture prescaler
			F   : Trigger_Filter    := DTS_Not_Filtered; -- Input capture filter
		end case;
	end record with Size => 8;
	for CC_Mode use record         -- TIM: 1-5,8  9,12  10,11,13,14
		S   at 0 range 0 .. 1; --      ++++   --++  ---+
		FE  at 0 range 2 .. 2; --      ++++   --++  ---+
		PE  at 0 range 3 .. 3; --      ++++   --++  ---+
		M   at 0 range 4 .. 6; --      ++++   --++  ---+
		CE  at 0 range 7 .. 7; --      ++++   ----  ---+
		PCS at 0 range 2 .. 3; --      ++++   --++  ---+
		F   at 0 range 4 .. 7; --      ++++   --++  ---+
	end record;

	type CC_Mode_Register is array (Positive range <>) of CC_Mode with Pack;

	type CC_Mode_Register_1 is new CC_Mode_Register (1 .. 2) with Size => 16;
	type CC_Mode_Register_2 is new CC_Mode_Register (3 .. 4) with Size => 16;

	--
	-- CCER
	--

	type Output_Polarity is (Active_High, Active_Low) with Size => 1;

	type CC_Enable_Channel is record
		E  : Boolean := False;
		P  : Output_Polarity := Active_High;
		NE : Boolean := False;
		NP : Output_Polarity := Active_High;
	end record with Size => 4;
	for CC_Enable_Channel use record
		E at 0 range 0 .. 0;
		P at 0 range 1 .. 1;
		NE at 0 range 2 .. 2;
		NP at 0 range 3 .. 3;
	end record;

	type CC_Enable_Register is array (1 .. 4) of CC_Enable_Channel with Pack, Size => 16;

	-- TIM: 1,8   2-5   9,12  10,11,13,14
	--      NN    NN    NN    NN
	--      PEPE  PEPE  PEPE  PEPE
	-- 1    ++++  +-++  +-++  +-++
	-- 2    ++++  +-++  +-++  ----
	-- 3    ++++  +-++  ----  ----
	-- 4    --++  +-++  ----  ----

	--
	-- CNT, ARR, CCRx
	--

	type Count_Register is range 0 .. 2**32 - 1 with Size => 32;

	type Count_Register_Array is array (Channel_Index) of Count_Register;

	-- TIM: 1,3,4,8-14  2,5
	-- bits 16          32

	-- Note: Bit 31 of CNT is declared in STM32F412/413's SVD as UIFCPY but not documented in RM.
	--       See note on CR1.

	--
	-- PSC
	--

	type Prescaler_Register is range 0 .. 2**16 - 1 with Size => 16;

	--
	-- RCR
	--

	type Repetition_Counter_Register is record
		REP       : Integer range 0 .. 2**8 - 1 := 0; -- Repetition counter value
		Unused_8  : Unused_8_Bits               := 0;
	end record with Size => 16;
	for Repetition_Counter_Register use record -- TIM: 1,8  2-5
		REP       at 0 range 0 .. 7;       --      +    -
		Unused_8  at 0 range 8 .. 15;
	end record;

	--
	-- BDTR
	--
	-- Register is specific to advanced control timer

	type Break_Polarity is (
		Active_Low,
		Active_High) with Size => 1;

	type Lock_Level is new Integer range 0 .. 3 with Size => 2;

	type Dead_Time is new Integer range 0 .. 2**8 - 1;
	-- Dead time    value
	-- 0nnn_nnnn   =>     nnn_nnnn * t_DTS
	-- 10nn_nnnn   =>    1nnn_nnn0 * t_DTS
	-- 110n_nnnn   =>  1_nnnn_n000 * t_DTS
	-- 111n_nnnn   => 1n_nnnn_0000 * t_DTS

	type Break_and_Dead_Time_Register is record
		DTG       : Dead_Time      := 0; -- Dead-time generator setup
		LOCK      : Lock_Level     := 0; -- Lock configuration
		OSSI      : Boolean        := False; -- Off-state selection for Idle
		OSSR      : Boolean        := False; -- Off-state selection for Run
		BKE       : Boolean        := False; -- Break enable
		BKP       : Break_Polarity := Active_Low; -- Break polarity
		AOE       : Boolean        := False; -- Automatic output enable
		MOE       : Boolean        := False; -- Main output enable
	end record with Size => 16;
	for Break_and_Dead_Time_Register use record
		DTG       at 0 range 0 .. 7;
		LOCK      at 0 range 8 .. 9;
		OSSI      at 0 range 10 .. 10;
		OSSR      at 0 range 11 .. 11;
		BKE       at 0 range 12 .. 12;
		BKP       at 0 range 13 .. 13;
		AOE       at 0 range 14 .. 14;
		MOE       at 0 range 15 .. 15;
	end record;

	--
	-- DCR
	--

	type DMA_Address is (
		DMA_CR1,
		DMA_CR2,
		DMA_SMCR,
		DMA_DIER,
		DMA_SR,
		DMA_EGR,
		DMA_CCMR1,
		DMA_CCMR2,
		DMA_CCER,
		DMA_CNT,
		DMA_PSC,
		DMA_ARR,
		DMA_RCR,
		DMA_CCR1,
		DMA_CCR2,
		DMA_CCR3,
		DMA_CCR4,
		DMA_BDTR,
		DMA_DCR,
		DMA_DMAR,
		DMA_OR) with Size => 5;

	type DMA_Control_Register is record
		DBA       : DMA_Address           := DMA_CR1; -- DMA base address
		Unused_5  : Unused_3_Bits         := 0;
		DBL       : Integer range 0 .. 31 := 0; -- DMA burst length
		Unused_13 : Unused_3_Bits         := 0;
	end record with Size => 16;
	for DMA_Control_Register use record
		DBA       at 0 range 0 .. 4;
		Unused_5  at 0 range 5 .. 7;
		DBL       at 0 range 8 .. 12;
		Unused_13 at 0 range 13 .. 15;
	end record;

	--
	-- DMAR
	--
	-- TIM: 1,8  2-5  9,12
	--      +    +    -

	--
	-- OR
	--

	type TI_Remap is (
		TI_GPIO,  -- Connect to GPIO
		TI_LSI,   -- Connect to LSI for calibration purposes
		TI_LSE,   -- Connext to LSE for calibration purposes
		TI_RTC)   -- Connect to RTC wakeup for calibration purposes
	with Size => 2;
	-- On TIM11, TI4_LSI and TI4_RTC values actually connect to GPIO.

	type ITR1_Remap is (
		ITR1_TIM8_TRGOUT,        -- Connect to TIM8 TRGOUT
		ITR1_PTP_Trigger_Output, -- Connect to PTP trigger output
		ITR1_OTG_FS_SOF,         -- Connect to OTG FS SOF
		ITR1_OTG_HS_SOF)         -- Connect to OTG HS SOF
	with Size => 2;

	type Option_Register is record
		TI1_RMP   : TI_Remap      := TI_GPIO; -- TIM11 Input 1 remap
		Unused_2  : Unused_4_Bits := 0;
		TI4_RMP   : TI_Remap      := TI_GPIO; -- TIM5 Input 4 remap
		Unused_8  : Unused_2_Bits := 0;
		ITR1_RMP  : ITR1_Remap    := ITR1_TIM8_TRGOUT; -- TIM2 Internal trigger 1 remap
		Unused_12 : Unused_4_Bits := 0;
	end record with Size => 16;
	for Option_Register use record         -- TIM: 1,3,4,8-10,12-14  2  5  11
		TI1_RMP   at 0 range  0 ..  1; --      -                 -  -  +
		Unused_2  at 0 range  2 ..  5;
		TI4_RMP   at 0 range  6 ..  7; --      -                 -  +  -
		Unused_8  at 0 range  8 ..  9;
		ITR1_RMP  at 0 range 10 .. 11; --      -                 +  -  -
		Unused_12 at 0 range 12 .. 15;
	end record;

	-- note: Register OR is named OPTR here to not clash with Ada's reserved word.

	--

	-- The following declaration covers all timer configurations
	-- Different timers may have missing several registers, register fields, or field values,
	-- and also have different register widths
	-- It is up to driver software not to write them.

	type Timer_Registers is record
		CR1   : Control_Register_1;
		pragma Volatile_Full_Access (CR1);
		CR2   : Control_Register_2;
		pragma Volatile_Full_Access (CR2);
		SMCR  : Slave_Mode_Control_Register;
		pragma Volatile_Full_Access (SMCR);
		DIER  : DMA_Interrupt_Enable_Register;
		pragma Volatile_Full_Access (DIER);
		SR    : Status_Register;
		pragma Volatile_Full_Access (SR);
		EGR   : Event_Generation_Register;
		pragma Volatile_Full_Access (EGR);
		CCMR1 : CC_Mode_Register_1;
		pragma Volatile_Full_Access (CCMR1);
		CCMR2 : CC_Mode_Register_2;
		pragma Volatile_Full_Access (CCMR2);
		CCER  : CC_Enable_Register;
		pragma Volatile_Full_Access (CCER);
		CNT   : Count_Register;
		pragma Volatile_Full_Access (CNT);
		PSC   : Prescaler_Register;
		pragma Volatile_Full_Access (PSC);
		ARR   : Count_Register;
		pragma Volatile_Full_Access (ARR);
		RCR   : Repetition_Counter_Register;
		pragma Volatile_Full_Access (RCR);
		CCR   : Count_Register_Array;
		BDTR  : Break_and_Dead_Time_Register;
		pragma Volatile_Full_Access (BDTR);
		DCR   : DMA_Control_Register;
		pragma Volatile_Full_Access (DCR);
		DMAR  : Unsigned_32;
		pragma Volatile_Full_Access (DMAR);
		OPTR  : Option_Register;              -- Option register (OR in original documentation)
		pragma Volatile_Full_Access (OPTR);
	end record with Volatile;
	for Timer_Registers use record         --  TIM: 1,8  2,5  3,4  9,12 10,13,14  11  6,7
		CR1   at 16#00# range 0 .. 15; --       +    +    +    +    +         +   +
		CR2   at 16#04# range 0 .. 15; --       +    +    +    -    -         -   +
		SMCR  at 16#08# range 0 .. 15; --       +    +    +    +    -         -   -
		DIER  at 16#0C# range 0 .. 15; --       +    +    +    +    +         +   +
		SR    at 16#10# range 0 .. 15; --       +    +    +    +    +         +   +
		EGR   at 16#14# range 0 .. 15; --       +    +    +    +    +         +   +
		CCMR1 at 16#18# range 0 .. 15; --       +    +    +    +    +-        +-  -
		CCMR2 at 16#1C# range 0 .. 15; --       +    +    +    -    -         -   -
		CCER  at 16#20# range 0 .. 15; --       +    +    +    +    +         +   -
		CNT   at 16#24# range 0 .. 31; --       16   32   16   16   16        16  16
		PSC   at 16#28# range 0 .. 15; --       +    +    +    +    +         +   +
		ARR   at 16#2C# range 0 .. 31; --       16   32   16   16   16        16  16
		RCR   at 16#30# range 0 .. 15; --       +    -    -    -    -         -   -
		CCR   at 16#34# range 0 .. 32 * 4 - 1; -- +  4*32 4*16 2*16 1*16     1*16 -
		BDTR  at 16#44# range 0 .. 15; --       +    -    -    -    -         -   -
		DCR   at 16#48# range 0 .. 15; --       +    +    +    -    -         -   -
		DMAR  at 16#4C# range 0 .. 31; --       +    +    +    -    -         -   -
		OPTR  at 16#50# range 0 .. 15; --       -    *    -    -    -         *   -
	end record;

	TIM1  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM1;
	TIM2  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM2;
	TIM3  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM3;
	TIM4  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM4;
	TIM5  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM5;
	TIM6  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM6;
	TIM7  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM7;
	TIM8  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM8;
	TIM9  : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM9;
	TIM10 : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM10;
	TIM11 : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM11;
	TIM12 : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM12;
	TIM13 : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM13;
	TIM14 : aliased Timer_Registers with Volatile, Import, Address => Address_Map.TIM14;

	-- In F469 TIM9,TIM12 no CR2

	-- There are register fields in SVD, but Docs know nothi about:
	-- in F412,F413 TIM6,7 CR1 have field UIFREMAP
	-- in F412,F413 TIM6,7 CNT have field UIFCPY

	-- Differences between timers
	--                    ACT   GPT
	--                    1,8   2-5
	-- Count direction    Any   Any
	-- Slave encoder mode  +     +
	-- External trigger    +     +
	-- COM interrupt/DMA   +     -
	-- Break interrupt     +     -

end STM32.Timers;
