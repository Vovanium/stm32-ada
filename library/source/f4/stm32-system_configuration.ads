with STM32.Address_Map;

-- System Configuration Controller

package STM32.System_Configuration is

	type Memory_Mapping is (
		Main_Flash,
		System_Flash,
		FSMC_Bank_1,
		Embedded_SRAM
	);

	for Memory_Mapping use (
		Main_Flash    => 2#00#,
		System_Flash  => 2#01#,
		FSMC_Bank_1   => 2#10#,
		Embedded_SRAM => 2#11#
	);

	type Memory_Remap_Register is record
		MEM_MODE : Memory_Mapping;
		Unused_2 : Unused_30_Bits;
	end record with Size => 32;

	for Memory_Remap_Register use record
		MEM_MODE at 0 range 0 ..  1;
		Unused_2 at 0 range 2 .. 31;
	end record;

	type Ethernet_Phy_Interface is (
		MII,
		RMII
	);

	for Ethernet_Phy_Interface use (
		MII => 0,
		RMII => 1
	);
	type Peripheral_Mode_Register is record
		Unused_0     : Unused_23_Bits;
		MII_RMII_SEL : Ethernet_Phy_Interface;
		Unused_24    : Unused_8_Bits;
	end record with Size => 32;

	for Peripheral_Mode_Register use record
		Unused_0     at 0 range  0 .. 22;
		MII_RMII_SEL at 0 range 23 .. 23;
		Unused_24    at 0 range 24 .. 31;
	end record;

	type Peripheral_Interrupt_Pin is (
		PA_Pin, PB_Pin, PC_Pin, PD_Pin,
		PE_Pin, PF_Pin, PG_Pin, PH_Pin,
		PI_Pin, PJ_Pin, PK_Pin
	);

	for Peripheral_Interrupt_Pin use (
		PA_Pin => 2#0000#, PB_Pin => 2#0001#,
		PC_Pin => 2#0010#, PD_Pin => 2#0011#,
		PE_Pin => 2#0100#, PF_Pin => 2#0101#,
		PG_Pin => 2#0110#, PH_Pin => 2#0111#,
		PI_Pin => 2#1000#, PJ_Pin => 2#1001#,
		PK_Pin => 2#1010#
	);

	generic
		Base : Integer;
	package External_Interrupt_Configuration is
		type Configuration_Array is
		array (Base .. Base + 3) of Peripheral_Interrupt_Pin with Pack, Size => 16;

		type Register is record
			EXTI      : Configuration_Array;
			Unused_16 : Integer range 0 .. 2**16 - 1;
		end record with Size => 32;

		for Register use record
			EXTI      at 0 range  0 .. 15;
			Unused_16 at 0 range 16 .. 31;
		end record;
	end External_Interrupt_Configuration;

	package External_Interrupt_Configuration_1 is
	new External_Interrupt_Configuration (0);

	package External_Interrupt_Configuration_2 is
	new External_Interrupt_Configuration (4);

	package External_Interrupt_Configuration_3 is
	new External_Interrupt_Configuration (8);

	package External_Interrupt_Configuration_4 is
	new External_Interrupt_Configuration (12);

	type Compensation_Cell_Power_Down_Mode is (
		Compensation_Cell_Power_Down,
		Compensation_Cell_Enabled
	);

	for Compensation_Cell_Power_Down_Mode use (
		Compensation_Cell_Power_Down => 0,
		Compensation_Cell_Enabled    => 1
	);

	type Compensation_Cell_Control_Register is record
		CMP_PD    : Compensation_Cell_Power_Down_Mode;
		Unused_1  : Unused_7_Bits;
		READY     : Boolean;
		Unused_9  : Unused_23_Bits;
	end record with Size => 32;

	for Compensation_Cell_Control_Register use record
		CMP_PD    at 0 range  0 ..  0;
		Unused_1  at 0 range  1 ..  7;
		READY     at 0 range  8 ..  8;
		Unused_9  at 0 range  9 .. 31;
	end record;

	type SYSCFG_Registers is record
		MEMRMP  : Memory_Remap_Register;
		pragma Volatile_Full_Access (MEMRMP);
		PMC     : Peripheral_Mode_Register;
		pragma Volatile_Full_Access (PMC);
		EXTICR1 : External_Interrupt_Configuration_1.Register;
		pragma Volatile_Full_Access (EXTICR1);
		EXTICR2 : External_Interrupt_Configuration_2.Register;
		pragma Volatile_Full_Access (EXTICR2);
		EXTICR3 : External_Interrupt_Configuration_3.Register;
		pragma Volatile_Full_Access (EXTICR3);
		EXTICR4 : External_Interrupt_Configuration_4.Register;
		pragma Volatile_Full_Access (EXTICR4);
		CMPCR   : Compensation_Cell_Control_Register;
		pragma Volatile_Full_Access (CMPCR);
	end record with Volatile;

	for SYSCFG_Registers use record
		MEMRMP  at 16#00# range 0 .. 31;
		PMC     at 16#04# range 0 .. 31;
		EXTICR1 at 16#08# range 0 .. 31;
		EXTICR2 at 16#0C# range 0 .. 31;
		EXTICR3 at 16#10# range 0 .. 31;
		EXTICR4 at 16#14# range 0 .. 31;
		CMPCR   at 16#20# range 0 .. 31;
	end record;

	SYSCFG : SYSCFG_Registers with Volatile, Import,
	Address => Address_Map.SYSCFG;

end STM32.System_Configuration;
