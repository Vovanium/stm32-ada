with STM32.Address_Map;

-- Controller Area Network Interfaces
--
-- Units exist in all STM32F4 devices except F4x1 and F410.
-- F4x3 devices have 3 CANs all others have 2.

package STM32.CANs is
	-- bxCAN hardware

	-- Correspondence in register names:

	-- FiRx   -- FR (i, x)
	-- RIxR   -- R (x).IR
	-- RDTxR  -- R (x).DTR
	-- RDLxR  -- R (x).DLR
	-- RDHxR  -- R (x).DHR
	-- RFxR   -- RFR (x)
	-- TIxR   -- T (x).IR
	-- TDTxR  -- T (x).DTR
	-- TDLxR  -- T (x).DLR
	-- TDHxR  -- T (x).DHR

	-- Correspondence in field names

	-- FMPIEx -- Fx.MPIE
	-- FFIEx  -- Fx.FIE
	-- FOVIEx -- Fx.OVIE

	-- MCR

	type Transmit_FIFO_Priority is (
		Identifier,   -- Messages are prioritized based on their identifier
		Request_Order -- Messgaes are transmitted in chronological order
	) with Size => 1;
	for Transmit_FIFO_Priority use (
		Identifier    => 0,
		Request_Order => 1
	);

	type Master_Control_Register is record
		INRQ        : Boolean                      := False; -- Initialization mode request
		SLEEP       : Boolean                      := True; -- Sleep mode request
		TXFP        : Transmit_FIFO_Priority       := Identifier; -- Fransmit queue prioritization type
		RFLM        : Boolean                      := False; -- Lock receive FIFO against overrun
		NART        : Boolean                      := False; -- No automatic retransmission
		AWUM        : Boolean                      := False; -- Automatic wakeup enable
		ABOM        : Boolean                      := False; -- Automatic bus-off management enable
		TTCM        : Boolean                      := False; -- Time triggered comunication mode enable
		Reserved_8  : Integer range 0 .. 2**7 - 1  := 0;
		RESET       : Boolean                      := False; -- Master reset
		DBF         : Boolean                      := True;  -- Debug bus freeze enable
		Reserved_17 : Integer range 0 .. 2**15 - 1 := 0;
	end record with Size => 32;
	for Master_Control_Register use record
		INRQ        at 0 range  0 ..  0;
		SLEEP       at 0 range  1 ..  1;
		TXFP        at 0 range  2 ..  2;
		RFLM        at 0 range  3 ..  3;
		NART        at 0 range  4 ..  4;
		AWUM        at 0 range  5 ..  5;
		ABOM        at 0 range  6 ..  6;
		TTCM        at 0 range  7 ..  7;
		Reserved_8  at 0 range  8 .. 14;
		RESET       at 0 range 15 .. 15;
		DBF         at 0 range 16 .. 16;
		Reserved_17 at 0 range 17 .. 31;
	end record;

	-- MSR

	type Master_Status_Register is record
		INAK        : Boolean                      := False; -- Initialization acknowledge
		SLAK        : Boolean                      := True;  -- Sleep acknowledge
		ERRI        : Boolean                      := False; -- Error interrupt
		WKUI        : Boolean                      := False; -- Wakeup interrupt
		SLAKI       : Boolean                      := False; -- Sleep acknowledge interrupt
		Reserved_5  : Integer range 0 .. 7         := 0;
		TXM         : Boolean                      := False; -- CAN is currently transmitter
		RXM         : Boolean                      := False; -- CAN is currently receiver
		SAMP        : Integer range 0 .. 1         := 0; -- Last sample bit
		RX          : Integer range 0 .. 1         := 1; -- CAN_Rx pin state
		Reserved_12 : Integer range 0 .. 2**20 - 1 := 1;
	end record with Size => 32;
	for Master_Status_Register use record
		INAK        at 0 range  0 ..  0;
		SLAK        at 0 range  1 ..  1;
		ERRI        at 0 range  2 ..  2;
		WKUI        at 0 range  3 ..  3;
		SLAKI       at 0 range  4 ..  4;
		Reserved_5  at 0 range  5 ..  7;
		TXM         at 0 range  8 ..  8;
		RXM         at 0 range  9 ..  9;
		SAMP        at 0 range 10 .. 10;
		RX          at 0 range 11 .. 11;
		Reserved_12 at 0 range 12 .. 31;
	end record;

	-- TSR
	subtype Mailbox_Index is Integer range 0 .. 2;

	type Mailbox_Flag_Array is array (Mailbox_Index) of Boolean with Pack, Component_Size => 1;

	type Mailbox_Transmit_Status is record
		RQCP        : Boolean              := False; -- Request completed
		TXOK        : Boolean              := False; -- Transmission succeded
		ALST        : Boolean              := False; -- Arbitration lost
		TERR        : Boolean              := False; -- Transmission error occured
		Reserved    : Integer range 0 .. 7 := 0;
		ABRQ        : Boolean              := False; -- Abort request
	end record with Size => 8;
	for Mailbox_Transmit_Status use record
		RQCP        at 0 range 0 .. 0;
		TXOK        at 0 range 1 .. 1;
		ALST        at 0 range 2 .. 2;
		TERR        at 0 range 3 .. 3;
		Reserved    at 0 range 4 .. 6;
		ABRQ        at 0 range 7 .. 7;
	end record;

	type Mailbox_Transmit_Status_Array is array (Mailbox_Index) of Mailbox_Transmit_Status with Pack, Component_Size => 8;

	type Transmit_Status_Register is record
		S           : Mailbox_Transmit_Status_Array;             -- Transmit status per mailbox
		CODE        : Integer range 0 .. 3 := 0;                 -- Next transmit mailbox free
		TME         : Mailbox_Flag_Array   := (others => True);  -- Transmit mailbox is empty
		LOW         : Mailbox_Flag_Array   := (others => False); -- Mailbox has lowest priority
	end record with Size => 32;
	for Transmit_Status_Register use record
		S           at 0 range  0 .. 23;
		CODE        at 0 range 24 .. 25;
		TME         at 0 range 26 .. 28;
		LOW         at 0 range 29 .. 31;

	end record;

	-- RFxR

	subtype FIFO_Index is Integer range 0 .. 1;

	type Receive_FIFO_Register is record
		FMP         : Integer range 0 .. 3         := 0; -- Number of messages pending
		Reserved_2  : Integer range 0 .. 1         := 0;
		FULL        : Boolean                      := False; -- FIFO full
		FOVR        : Boolean                      := False; -- FIFO overrun
		RFOM        : Boolean                      := False; -- Release FIFO output mailbox
		Reserved_6  : Integer range 0 .. 2**26 - 1 := 0;
	end record with Size => 32;
	for Receive_FIFO_Register use record
		FMP         at 0 range  0 ..  1;
		Reserved_2  at 0 range  2 ..  2;
		FULL        at 0 range  3 ..  3;
		FOVR        at 0 range  4 ..  4;
		RFOM        at 0 range  5 ..  5;
		Reserved_6  at 0 range  6 .. 31;
	end record;

	pragma Volatile_Full_Access (Receive_FIFO_Register);

	type Receive_FIFO_Register_Array is array (FIFO_Index) of Receive_FIFO_Register with Size => 32 * 2;

	-- IER

	type FIFO_Interrupt_Enable is record
		MPIE        : Boolean := False; -- FIFO message pending interrupt enable
		FIE         : Boolean := False; -- FIFO full interrupt enable
		OVIE        : Boolean := False; -- FIFO overrun interrupt enable
	end record with Size => 3;
	for FIFO_Interrupt_Enable use record
		MPIE        at 0 range  0 ..  0;
		FIE         at 0 range  1 ..  1;
		OVIE        at 0 range  2 ..  2;
	end record;

	type Interrupt_Enable_Register is record
		TMEIE       : Boolean                      := False; -- Transmit mailbox empty interrupt enable
		F0          : FIFO_Interrupt_Enable;                 -- FIFO 0 interrupt enable
		F1          : FIFO_Interrupt_Enable;                 -- FIFO 1 interrupt enable
		Reserved_7  : Integer range 0 .. 1         := 0;
		EWGIE       : Boolean                      := False; -- Error warning interrupt enable
		EPVIE       : Boolean                      := False; -- Error passive interrupt enable
		BOFIE       : Boolean                      := False; -- Bus-off interrupt enable
		LECIE       : Boolean                      := False; -- Last error code interrupt enable
		Reserved_12 : Integer range 0 .. 7         := 0;
		ERRIE       : Boolean                      := False; -- Error interrupt enable
		WKUIE       : Boolean                      := False; -- Wakeup interrupt enable
		SLKIE       : Boolean                      := False; -- Sleep interrupt enable
		Reserved_18 : Integer range 0 .. 2**14 - 1 := 0;
	end record with Size => 32;
	for Interrupt_Enable_Register use record
		TMEIE       at 0 range  0 ..  0;
		F0          at 0 range  1 ..  3;
		F1          at 0 range  4 ..  6;
		Reserved_7  at 0 range  7 ..  7;
		EWGIE       at 0 range  8 ..  8;
		EPVIE       at 0 range  9 ..  9;
		BOFIE       at 0 range 10 .. 10;
		LECIE       at 0 range 11 .. 11;
		Reserved_12 at 0 range 12 .. 14;
		ERRIE       at 0 range 15 .. 15;
		WKUIE       at 0 range 16 .. 16;
		SLKIE       at 0 range 17 .. 17;
		Reserved_18 at 0 range 18 .. 31;
	end record;

	-- Note about F0 and F1: packing of 3 bit components is not supported by configuration
	-- compiler gives an error

	-- ESR

	type Error_Code is (
		No_Error,
		Stuff_Error,
		Form_Error,
		Acknowledgement_Error,
		Bit_Recessive_Error,
		Bit_Dominant_Error,
		CRC_Error,
		Software_Error
	) with Size => 3;
	for Error_Code use (
		No_Error              => 2#000#,
		Stuff_Error           => 2#001#,
		Form_Error            => 2#010#,
		Acknowledgement_Error => 2#011#,
		Bit_Recessive_Error   => 2#100#,
		Bit_Dominant_Error    => 2#101#,
		CRC_Error             => 2#110#,
		Software_Error        => 2#111#
	);

	type Error_Status_Register is record
		EWGF        : Boolean                     := False; -- Error warning flag
		EPVF        : Boolean                     := False; -- Error passive flag
		BOFF        : Boolean                     := False; -- Bus off flag
		Reserved_3  : Integer range 0 .. 1        := 0;
		LEC         : Error_Code                  := No_Error; -- Last error code
		Reserved_7  : Integer range 0 .. 2**9 - 1 := 0;
		TEC         : Integer range 0 .. 2**8 - 1 := 0; -- Least significant byte of the transmit error counter
		REC         : Integer range 0 .. 2**8 - 1 := 0; -- Receive error counter
	end record with Size => 32;
	for Error_Status_Register use record
		EWGF        at 0 range 0 .. 0;
		EPVF        at 0 range 1 .. 1;
		BOFF        at 0 range 2 .. 2;
		Reserved_3  at 0 range  3 ..  3;
		LEC         at 0 range  4 ..  6;
		Reserved_7  at 0 range  7 .. 15;
		TEC         at 0 range 16 .. 23;
		REC         at 0 range 24 .. 31;
	end record;

	-- BTR

	type Bit_Timing_Register is record
		BRP         : Integer range 0 .. 2**10 - 1 := 0; -- Baud rate prescaler minus 1
		Reserved_10 : Integer range 0 .. 2**6 - 1  := 0;
		TS1         : Integer range 0 .. 15        := 3; -- Time segment 1 minus 1
		TS2         : Integer range 0 .. 7         := 2; -- Time segment 2 minus 1
		Reserved_23 : Integer range 0 .. 1         := 0;
		SJW         : Integer range 0 .. 3         := 1; -- Resynchronization jump width minus 1
		Reserved_26 : Integer range 0 .. 15        := 0;
		LBKM        : Boolean                      := False; -- Loop back mode
		SILM        : Boolean                      := False; -- Silent mode
	end record with Size => 32;
	for Bit_Timing_Register use record
		BRP         at 0 range  0 ..  9;
		Reserved_10 at 0 range 10 .. 15;
		TS1         at 0 range 16 .. 19;
		TS2         at 0 range 20 .. 22;
		Reserved_23 at 0 range 23 .. 23;
		SJW         at 0 range 24 .. 25;
		Reserved_26 at 0 range 26 .. 29;
		LBKM        at 0 range 30 .. 30;
		SILM        at 0 range 31 .. 31;
	end record;

	-- TIxR, RIxR

	subtype STID_Type is Unsigned_32 range 0 .. 2**11 - 1;
	subtype EXID_Type is Unsigned_32 range 0 .. 2**29 - 1;

	function To_EXID (ID : STID_Type) return EXID_Type is (
		Shift_Left (ID, 18)
	) with Inline;
	function To_STID (ID : EXID_Type) return STID_Type is (
		Shift_Right (ID, 18)
	) with Inline;

	type Identifier_Register is record
		TXRQ        : Boolean;   -- Transmit mailbox request
		RTR         : Boolean;   -- Remote transmission request
		IDE         : Boolean;   -- Extended identifier enable
		EXID        : EXID_Type; -- Lower part of extended identifier
	end record with Size => 32;
	for Identifier_Register use record
		TXRQ        at 0 range  0 ..  0;
		RTR         at 0 range  1 ..  1;
		IDE         at 0 range  2 ..  2;
		EXID        at 0 range  3 .. 31;
	end record;


	-- TDTxR

	type Transmit_Length_and_Time_Register is record
		DLC         : Integer range 0 .. 8;         -- Data length code
		Reserved_4  : Integer range 0 .. 15;
		TGT         : Boolean;                      -- Transmit global time
		Reserved_9  : Integer range 0 .. 2**7 - 1;
		TIME        : Integer range 0 .. 2**16 - 1; -- Message time stamp
	end record with Size => 32;
	for Transmit_Length_and_Time_Register use record
		DLC         at 0 range  0 ..  3;
		Reserved_4  at 0 range  4 ..  7;
		TGT         at 0 range  8 ..  8;
		Reserved_9  at 0 range  9 .. 15;
		TIME        at 0 range 16 .. 31;
	end record;

	-- TDLxR, TDHxR

	type Data_Array is array (Natural range <>) of Unsigned_8 with Pack, Component_Size => 8;

	subtype Data_Low_Register  is Data_Array (0 .. 3);
	subtype Data_High_Register is Data_Array (4 .. 7);

	-- Transmitter mailbox register groups

	type Transmit_Registers is record
		IR      : Identifier_Register;
		pragma Volatile_Full_Access (IR);
		DTR     : Transmit_Length_and_Time_Register;
		pragma Volatile_Full_Access (DTR);
		DLR     : Data_Low_Register;
		pragma Volatile_Full_Access (DLR);
		DHR     : Data_High_Register;
		pragma Volatile_Full_Access (DHR);
	end record with Size => 32 * 4;
	for Transmit_Registers use record
		IR      at 16#00# range 0 .. 31;
		DTR     at 16#04# range 0 .. 31;
		DLR     at 16#08# range 0 .. 31;
		DHR     at 16#0C# range 0 .. 31;
	end record;

	type Transmit_Register_Array is array (Mailbox_Index) of Transmit_Registers
	with Component_Size => 32 * 4, Size => 32 * 4 * 3;

	-- RDTxR

	type Receive_Length_and_Time_Register is record
		DLC         : Integer range 0 .. 15;        -- Data length code
		Reserved_4  : Integer range 0 .. 15;
		FMI         : Integer range 0 .. 2**8 - 1;  -- Filter match index
		TIME        : Integer range 0 .. 2**16 - 1; -- Message time stamp
	end record with Size => 32;
	for Receive_Length_and_Time_Register use record
		DLC         at 0 range  0 ..  3;
		Reserved_4  at 0 range  4 ..  7;
		FMI         at 0 range  8 .. 15;
		TIME        at 0 range 16 .. 31;
	end record;
	-- Note: uninitialized register could have any value so no restriction on DLC field

	-- Receiver FIFO register groups

	type Receive_Registers is record
		IR      : Identifier_Register;
		pragma Volatile_Full_Access (IR);
		DTR     : Receive_Length_and_Time_Register;
		pragma Volatile_Full_Access (DTR);
		DLR     : Data_Low_Register;
		pragma Volatile_Full_Access (DLR);
		DHR     : Data_High_Register;
		pragma Volatile_Full_Access (DHR);
	end record with Size => 32 * 4;
	for Receive_Registers use record
		IR      at 16#00# range 0 .. 31;
		DTR     at 16#04# range 0 .. 31;
		DLR     at 16#08# range 0 .. 31;
		DHR     at 16#0C# range 0 .. 31;
	end record;

	type Receive_Register_Array is array (FIFO_Index) of Receive_Registers
	with Component_Size => 32 * 4, Size => 32 * 4 * 2;

	-- FMR

	type Filter_Master_Register is record
		FINIT       : Boolean                      := True; --Filter init mode
		Reserved_1  : Integer range 0 .. 2**7 - 1  := 0;
		CAN2SB      : Integer range 0 .. 2**6 - 1  := 16#0E#; -- CAN2 start bank
		Reserved_14 : Integer range 0 .. 2**18 - 1 := 2#0010_1010_0001_1100_00#;
	end record with Size => 32;
	for Filter_Master_Register use record
		FINIT       at 0 range 0 .. 0;
		Reserved_1  at 0 range  1 ..  7;
		CAN2SB      at 0 range  8 .. 13;
		Reserved_14 at 0 range 14 .. 31;
	end record;

	-- FM1R

	subtype Filter_Bank_Index is Integer range 0 .. 27;

	type Filter_Mode is (
		Mask, -- Registers are in Mask mode
		List  -- Registers are in List mode
	) with Size => 1;
	for Filter_Mode use (
		Mask => 0,
		List => 1
	);

	type Filter_Mode_Array is array (Filter_Bank_Index) of Filter_Mode with Pack, Component_Size => 1;

	type Filter_Mode_Register is record
		FBM         : Filter_Mode_Array     := (others => Mask); -- Mode of the registers of bank
		Reserved    : Integer range 0 .. 15 := 0;
	end record with Size => 32;

	for Filter_Mode_Register use record
		FBM         at 0 range  0 .. 27;
		Reserved    at 0 range 28 .. 31;
	end record;

	-- FS1R

	type Filter_Scale is (
		Dual_16_Bit,   -- Dual 16-bit scale configuration
		Single_32_Bit  -- Single 32-bit scale configuration
	) with Size => 1;
	for Filter_Scale use (
		Dual_16_Bit   => 0,
		Single_32_Bit => 1
	);

	type Filter_Scale_Array is array (Filter_Bank_Index) of Filter_Scale with Pack, Component_Size => 1;

	type Filter_Scale_Register is record
		FSC         : Filter_Scale_Array    := (others => Dual_16_Bit); -- Filter scale configuration
		Reserved    : Integer range 0 .. 15 := 0;
	end record with Size => 32;
	for Filter_Scale_Register use record
		FSC         at 0 range  0 .. 27;
		Reserved    at 0 range 28 .. 31;
	end record;

	-- FFA1R

	type Filter_FIFO_Assignment_Array is array (Filter_Bank_Index) of FIFO_Index with Pack, Component_Size => 1;

	type Filter_FIFO_Assignment_Register is record
		FFA         : Filter_FIFO_Assignment_Array := (others => 0); -- Filter FIFO assignment for each filter
		Reserved    : Integer range 0 .. 15        := 0;
	end record with Size => 32;
	for Filter_FIFO_Assignment_Register use record
		FFA         at 0 range  0 .. 27;
		Reserved    at 0 range 28 .. 31;
	end record;

	-- FA1R

	type Filter_Activation_Array is array (Filter_Bank_Index) of Boolean with Pack, Component_Size => 1;

	type Filter_Activation_Register is record
		FACT        : Filter_Activation_Array := (others => False); -- Filter active
		Reserved    : Integer range 0 .. 15;
	end record with Size => 32;
	for Filter_Activation_Register use record
		FACT        at 0 range  0 .. 27;
		Reserved    at 0 range 28 .. 31;
	end record;


	-- FiRx

	subtype Filter_Bank_Register is Unsigned_32;

	-- pragma Volatile_Full_Access (Filter_Bank_Register);
	-- GNAT does not allow this, you should keep it in mind

	type Filter_Bank_Register_Array is array (Filter_Bank_Index, Integer range 1 .. 2) of Filter_Bank_Register
	with Component_Size => 32, Size => 32 * 2 * 28;

	--

	type CAN_Registers is record
		MCR     : Master_Control_Register;
		pragma Volatile_Full_Access (MCR);
		MSR     : Master_Status_Register;
		pragma Volatile_Full_Access (MSR);
		TSR     : Transmit_Status_Register;
		pragma Volatile_Full_Access (TSR);
		RFR     : Receive_FIFO_Register_Array;
		IER     : Interrupt_Enable_Register;
		pragma Volatile_Full_Access (IER);
		ESR     : Error_Status_Register;
		pragma Volatile_Full_Access (ESR);
		BTR     : Bit_Timing_Register;
		pragma Volatile_Full_Access (BTR);
		T       : Transmit_Register_Array;
		R       : Receive_Register_Array;
		FMR     : Filter_Master_Register;
		pragma Volatile_Full_Access (FMR);
		FM1R    : Filter_Mode_Register;
		pragma Volatile_Full_Access (FM1R);
		FS1R    : Filter_Scale_Register;
		pragma Volatile_Full_Access (FS1R);
		FFA1R   : Filter_FIFO_Assignment_Register;
		pragma Volatile_Full_Access (FFA1R);
		FA1R    : Filter_Activation_Register;
		pragma Volatile_Full_Access (FA1R);
		FR      : Filter_Bank_Register_Array;
	end record with Volatile;
	for CAN_Registers use record
		MCR     at 16#0000# range 0 .. 31;
		MSR     at 16#0004# range 0 .. 31;
		TSR     at 16#0008# range 0 .. 31;
		RFR     at 16#000C# range 0 .. 32 * 2 - 1;
		IER     at 16#0014# range 0 .. 31;
		ESR     at 16#0018# range 0 .. 31;
		BTR     at 16#001C# range 0 .. 31;
		T       at 16#0180# range 0 .. 32 * 4 * 3 - 1;
		R       at 16#01B0# range 0 .. 32 * 4 * 2 - 1;
		FMR     at 16#0200# range 0 .. 31;
		FM1R    at 16#0204# range 0 .. 31;
		FS1R    at 16#020C# range 0 .. 31;
		FFA1R   at 16#0214# range 0 .. 31;
		FA1R    at 16#021C# range 0 .. 31;
		FR      at 16#0240# range 0 .. 32 * 2 * 28 - 1;
	end record;

	CAN1 : aliased CAN_Registers with Volatile, Import, Address => Address_Map.CAN1;
	CAN2 : aliased CAN_Registers with Volatile, Import, Address => Address_Map.CAN2;
	CAN3 : aliased CAN_Registers with Volatile, Import, Address => Address_Map.CAN3;

end STM32.CANs;
