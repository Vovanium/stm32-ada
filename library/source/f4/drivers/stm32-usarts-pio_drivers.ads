with Ada.Streams;
use  Ada.Streams;
with Ada.Interrupts;
use  Ada.Interrupts;
with Interfaces;
use  Interfaces;
with STM32.Circular_Buffers;
with STM32.Stream_Drivers;

package STM32.USARTs.PIO_Drivers is

	subtype Data_Type is Unsigned_16; -- 8 or 9 bits are used depending on mode
	-- (also special value for BREAK may be used (in future))

	package CB is new Circular_Buffers (Data_Type);

	type USART_Mode is (
		UART_Full_Duplex_Mode,
		UART_Half_Duplex_Mode
	);

	protected type USART_Driver (
		Interrupt      : Ada.Interrupts.Interrupt_Id;
		Registers      : access STM32.USARTs.USART_Registers;
		Rx_Buffer_Size,
		Tx_Buffer_Size : Positive;
		Bus_Frequency  : access function return Positive
	) is new Stream_Drivers.Stream_Driver with
		--
		-- Common
		--

		procedure Enable (A : Boolean);
		-- Enable or disable the port

		-- Note: there's no method to detect if transmission or receiving
		-- is active, thus user should stop operations manually
		-- before chainging any parameters

		procedure Interrupt_Handler with Attach_Handler => Interrupt;
		-- An ISR

		--
		-- Receiver
		--

		procedure Receiver_Time_Out;
		-- Call it to stop waiting
		-- (There's no internal timeout timer but this can be called from external one)

		procedure Receiver_Time_Out_Reset;
		-- Reset time out status (to allow waiting again)

		function Receiver_Timed_Out
			return Boolean;
		-- Get time out status

		overriding procedure Read_Immediate (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Get the already available data from the receiver

		overriding entry Read (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Get the data from the receiver

		--
		-- Transmitter
		--

		overriding entry Write (
			Data : in     Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Put the data to the transmitter

	private
		Rx_Ready     : Boolean := False; -- Set when data is arrived or time out or some error occured
		Rx_Overflow  : Boolean := False;
		Rx_Time_Out  : Boolean := False;
		Tx_Ready     : Boolean := True;

		Rx_Buffer    : CB.Circular_Buffer (Rx_Buffer_Size);
		Tx_Buffer    : CB.Circular_Buffer (Tx_Buffer_Size);
	end USART_Driver;

end STM32.USARTs.PIO_Drivers;
