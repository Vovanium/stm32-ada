package body STM32.SPIs.PIO_Drivers is

	-- Buffer description
	--    Get           .-DR<-.                  Put
	--     ^            v     |                   v
	--    +------------+     +-------------------+
	--    |            |     |                   |
	--    +------------+     +-------------------+
	--     ^            ^     ^                   ^
	--  Rx_Tail     Rx_Head Tx_Tail             Tx_Head

	protected body Driver is

		procedure Interrupt_Handler is
		begin
			if Registers.SR.TXE then
				if Tx_Head /= Tx_Tail then
					Registers.DR := Unsigned_16 (Buffer (Tx_Tail));
					Tx_Tail := (Tx_Tail + 1) mod (Buffer_Size + 1);
				else
					declare
						R : Control_Register_2 := Registers.CR2;
					begin
						R.TXEIE := False;
						Registers.CR2 := R;
					end;
				end if;
			end if;

			if Registers.SR.RXNE then
				Buffer (Rx_Head) := Data_Type (Registers.DR);
				Rx_Head := (Rx_Head + 1) mod (Buffer_Size + 1);
			end if;
			-- will not overflow as amount of received data will not exceed transmitted.
		end Interrupt_Handler;

		function Next (A : Natural) return Natural is ((A + 1) mod (Buffer_Size + 1));

		entry Get (Data : out Data_Type; Got : out Boolean) when Rx_Tail /= Rx_Head or Rx_Tail = Tx_Head
		-- second condition is to return when no transmission active
		is
		begin
			Got := Rx_Tail /= Tx_Head;
			if Got then
				Data := Buffer (Rx_Tail);
				Rx_Tail := (Rx_Tail + 1) mod (Buffer_Size + 1);
				Full := False;
			end if;
		end Get;

		function Is_Full return Boolean is (Next (Tx_Head) = Rx_Tail);

		entry Put (Data : in Data_Type) when not Full
		is
		begin
			Buffer (Tx_Head) := Data;
			Tx_Head := (Tx_Head + 1) mod (Buffer_Size + 1);
			declare
				R : Control_Register_2 := Registers.CR2;
			begin
					R.TXEIE := True;
					R.RXNEIE := True;
					Registers.CR2 := R;
			end;
			Full := Is_Full;
		end Put;

		procedure Read_Immediate (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset)
		is
		begin
			Last := Data'First - 1;
			while Last < Data'Last and then Rx_Tail /= Tx_Head loop
				Last := Last + 1;
				Data (Last) := Stream_Element (Buffer (Rx_Tail));
				Rx_Tail := Next (Rx_Tail);
			end loop;
			if Last /= Data'First - 1 then
				Full := False;
			end if;
		end Read_Immediate;

		entry Read (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset)
		when Rx_Tail /= Rx_Head or Rx_Tail = Tx_Head is
		begin
			Read_Immediate (Data, Last);
		end Read;

		entry Write (
			Data : in     Stream_Element_Array;
			Last :    out Stream_Element_Offset)
		when not Full is
		begin
			Last := Data'First - 1;
			while Last < Data'Last and then not Is_Full loop
				Last := Last + 1;
				Buffer (Tx_Head) := Data_Type (Data (Last));
				Tx_Head := Next (Tx_Head);
			end loop;
			Full := Is_Full;
			declare
				R : Control_Register_2 := Registers.CR2;
			begin
					R.TXEIE := True;
					R.RXNEIE := True;
					Registers.CR2 := R;
			end;
		end Write;

	end Driver;

end STM32.SPIs.PIO_Drivers;
