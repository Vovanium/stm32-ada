with STM32.USARTs;
use  STM32.USARTs;

package body STM32.USARTs.PIO_Drivers is

	protected body USART_Driver is
		procedure Interrupt_Handler is
			D : Interfaces.Unsigned_32;
			E : Interfaces.Unsigned_16;
		begin
			if Registers.SR.RXNE then
				D := Registers.DR;
				if CB.Is_Full (Rx_Buffer) then
					Rx_Overflow := True;
				else
					CB.Enqueue (Rx_Buffer, Unsigned_16 (D));
				end if;
				Rx_Ready := True;
			end if;

			if Registers.SR.TXE then
				if CB.Is_Empty (Tx_Buffer) then -- no more to send
					declare
						R : Control_Register_1 := Registers.CR1;
					begin
						R.TXEIE := False;
						Registers.CR1 := R;
					end; -- Disable interrupt, to avoid entring ISR again
				else
					CB.Dequeue (Tx_Buffer, E);
					Registers.DR := Unsigned_32 (E);
				end if;
				Tx_Ready := True;
			end if;
		end Interrupt_Handler;

		procedure Enable (A : Boolean) is
			CR1 : Control_Register_1 := Registers.CR1;
		begin
			if A then
				CR1.TE := True;
				CR1.RE := True;
				CR1.RXNEIE := True;
			end if;

			Registers.CR1 := CR1;

			CR1.UE := A;

			Registers.CR1 := CR1;
		end Enable;

		--
		-- Receiver
		--

		procedure Update_Rx_Ready is
		begin
			Rx_Ready := (not CB.Is_Empty (Rx_Buffer)) or Rx_Time_Out;
		end Update_Rx_Ready;

		procedure Receiver_Time_Out is
		begin
			Rx_Time_Out := True;
			Update_Rx_Ready;
		end Receiver_Time_Out;

		procedure Receiver_Time_Out_Reset is
		begin
			Rx_Time_Out := False;
			Update_Rx_Ready;
		end Receiver_Time_Out_Reset;

		function Receiver_Timed_Out return Boolean is (Rx_Time_Out);

		procedure Read_Immediate (Data : out Stream_Element_Array; Last : out Stream_Element_Offset) is
			D : Data_Type;
		begin
			Last := Data'First - 1;
			while Last < Data'Last and then (not CB.Is_Empty (Rx_Buffer)) loop
				Last := Last + 1;
				CB.Dequeue (Rx_Buffer, D);
				Data (Last) := Stream_Element (D);
			end loop;
			Update_Rx_Ready;
		end;

		entry Read (Data : out Stream_Element_Array; Last : out Stream_Element_Offset) when Rx_Ready is
		begin
			Read_Immediate (Data, Last);
		end;

		--
		-- Transmitter
		--

		entry Write (Data : in Stream_Element_Array; Last : out Stream_Element_Offset) when Tx_Ready is
		begin
			Last := Data'First - 1;
			while Last < Data'Last and then (not CB.Is_Full (Tx_Buffer)) loop
				Last := Last + 1;
				CB.Enqueue (Tx_Buffer, Data_Type (Data (Last)));
			end loop;
			Tx_Ready := not CB.Is_Full (Tx_Buffer);
			declare
				R : Control_Register_1 := Registers.CR1;
			begin
				R.TXEIE := True;
				Registers.CR1 := R;
			end; -- Enable interrupt (it will occur immediately if UART is not busy)
		end Write;

	end USART_Driver;

end STM32.USARTs.PIO_Drivers;
