with Ada.Streams;
use  Ada.Streams;
with Ada.Interrupts;
use  Ada.Interrupts;
with Interfaces;
use  Interfaces;
with STM32.Stream_Drivers;

package STM32.SPIs.PIO_Drivers is

	subtype Data_Type is Unsigned_8; -- 8 or 16 bits are used depending on mode

	type Data_Array is array (Natural range <>) of Data_Type;

	protected type Driver (
		Interrupt      : Ada.Interrupts.Interrupt_Id;
		Registers      : access STM32.SPIs.SPI_Registers;
		Buffer_Size    : Positive;
		Bus_Frequency  : access function return Positive)
	is new Stream_Drivers.Stream_Driver with

		procedure Interrupt_Handler with Attach_Handler => Interrupt;
		-- An ISR

		entry Get (
			Data :    out Data_Type;
			Got  :    out Boolean);
		-- Get the data from the receiver

		entry Put (
			Data : in     Data_Type);
		-- Put the data to the transmitter

		overriding procedure Read_Immediate (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Get the already available data from the receiver

		overriding entry Read (
			Data :    out Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Get the data from the receiver

		overriding entry Write (
			Data : in     Stream_Element_Array;
			Last :    out Stream_Element_Offset);
		-- Put the data to the transmitter

	private
		Tx_Head,
		Tx_Tail,
		Rx_Head,
		Rx_Tail : Natural := 0;
		Full    : Boolean := False;
		Buffer  : Data_Array (0 .. Buffer_Size);
	end Driver;

end STM32.SPIs.PIO_Drivers;
