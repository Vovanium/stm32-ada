--
-- Host-mode-related registers of USB OTG (HS / FS) controller
--

package STM32.USB_OTG.Host is

	-- Registers renamed
	-- Orig. register  Register name here
	--
	-- HCCHARx         HC (x).CHAR
	-- HCSPLTx         HC (x).SPLT
	-- HCINTx          HC (x).INT
	-- HCINTMSKx       HC (x).INTMSK
	-- HCTSIZx         HC (x).TSIZ
	-- HCDMA           HC (x).DMA

	-- Fields renamed
	-- Register      Orig. field  Field name here
	--
	-- HAINTMSK      HAINTM       HAINT (array)
	-- HCINTMSK      *            corresponding fields in HCINT

	-- HCFG

	type PHY_Clock_Select is (
		PHY_Clock_Reserved_0,
		PHY_Clock_48_MHz,
		PHY_Clock_6_MHz,
		PHY_Clock_Reserved_3) with Size => 2;

	type Configuration_Register is record
		FSLSPCS   : PHY_Clock_Select;          -- FS/LS PHY clock select
		FSLSS     : Boolean          := False; -- FS- and LS-only support
		Unused_3  : Unused_29_Bits   := 0;
	end record with Size => 32;
	for Configuration_Register use record
		FSLSPCS   at 0 range  0 ..  1;
		FSLSS     at 0 range  2 ..  2;
		Unused_3  at 0 range  3 .. 31;
	end record;

	-- HFIR

	type Frame_Interval_Register is record
		FRIVL     : Integer range 0 .. 2**16 - 1 := 16#EA60#; -- Frame interval
		RLDCTRL   : Boolean                      := False;
		Unused_17 : Unused_15_Bits               := 0;
	end record with Size => 32;
	for Frame_Interval_Register use record
		FRIVL     at 0 range  0 .. 15;
		RLDCTRL   at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 31;
	end record;

	-- HFNUM

	type Frame_Number_Register is record
		FRNUM     : Integer range 0 .. 2**16 - 1 := 16#3FFF#; -- Frame number
		FTREM     : Integer range 0 .. 2**16 - 1 := 0; -- Frame time remaining
	end record with Size => 32;
	for Frame_Number_Register use record
		FRNUM     at 0 range  0 .. 15;
		FTREM     at 0 range 16 .. 31;
	end record;

	-- HPTXSTS

	type Periodic_Transmit_Status_Register is record
		PTXFSAVL  : Integer range 0 .. 2**16 - 1 := 16#100#; -- Periodic tx data FIFO space available
		PTXQSAV   : Integer range 0 .. 2**8 - 1  := 8; -- Periodic tx request queue space available
		PTXQTOP   : Integer range 0 .. 2**8 - 1  := 0; -- Top of the periodic transmit request queue
	end record with Size => 32;
	for Periodic_Transmit_Status_Register use record
		PTXFSAVL  at 0 range 0 .. 15;
		PTXQSAV   at 0 range 16 .. 23;
		PTXQTOP   at 0 range 24 .. 31;
	end record;

	-- HAINT, HAINTMSK

	type All_Channels_Interrupt_Register is record
		HAINT     : Channel_Set;    -- Channel interrupts
		Unused_16 : Unused_16_Bits;
	end record with Size => 32;
	for All_Channels_Interrupt_Register use record
		HAINT     at 0 range  0 .. 15;
		Unused_16 at 0 range 16 .. 31;
	end record;

	-- HPRT

	type Port_Speed is (
		High_Speed,
		Full_Speed,
		Low_Speed)
	with Size => 2;
	for Port_Speed use (
		High_Speed => 2#00#,
		Full_Speed => 2#01#,
		Low_Speed  => 2#10#);

	type Port_Control_Status_Register is record
		PCSTS     : Boolean              := False;      -- Port connect status
		PCDET     : Boolean              := False;      -- Port connect detected
		PENA      : Boolean              := False;      -- Port enable
		PENCHNG   : Boolean              := False;      -- Port enable/disable change
		POCA      : Boolean              := False;      -- Port overcurrent active
		POCCHNG   : Boolean              := False;      -- Port overcurrent change
		PRES      : Boolean              := False;      -- Port resume
		PSUSP     : Boolean              := False;      -- Port suspend
		PRST      : Boolean              := False;      -- Port reset
		Unused_9  : Unused_1_Bit         := 0;
		PLSTS_DP  : Integer range 0 .. 1 := 0;          -- Port line DP status
		PLSTS_DM  : Integer range 0 .. 1 := 0;          -- Port line DM status
		PPWR      : Boolean              := False;      -- Port power
		PTCTL     : Test_Mode            := No_Test;    -- Port test control
		PSPD      : Port_Speed           := High_Speed; -- Port speed
		Unused_19 : Unused_13_Bits       := 0;
	end record with Size => 32;
	for Port_Control_Status_Register use record
		PCSTS at 0 range 0 .. 0;
		PCDET at 0 range 1 .. 1;
		PENA at 0 range 2 .. 2;
		PENCHNG at 0 range 3 .. 3;
		POCA at 0 range 4 .. 4;
		POCCHNG at 0 range 5 .. 5;
		PRES at 0 range 6 .. 6;
		PSUSP at 0 range 7 .. 7;
		PRST at 0 range 8 .. 8;
		Unused_9  at 0 range  9 ..  9;
		PLSTS_DP  at 0 range 10 .. 10;
		PLSTS_DM  at 0 range 11 .. 11;
		PPWR      at 0 range 12 .. 12;
		PTCTL     at 0 range 13 .. 16;
		PSPD      at 0 range 17 .. 18;
		Unused_19 at 0 range 19 .. 31;

	end record;

	-- HCCHARx

	type Endpoint_Direction is (
		Out_Endpoint,
		In_Endpoint)
	with Size => 1;

	type Channel_Characteristics_Register is record
		MPSIZ     : Packet_Byte_Count     := 0;                -- Maximum packet size
		EPNUM     : Integer range 0 .. 15 := 0;                -- Endpoint number
		EPDIR     : Endpoint_Direction    := Out_Endpoint;     -- Endpoint direction
		Unused_16 : Unused_1_Bit          := 0;
		LSDEV     : Boolean               := False;            -- Low-speed device
		EPTYP     : Endpoint_Type         := Control_Endpoint; -- Endpoint type
		MCNT      : Integer range 0 .. 3  := 0;                -- Multicount
		DAD       : Device_Address        := 0;                -- Device address
		ODDFRM    : Boolean               := False;            -- Odd frame
		CHDIS     : Boolean               := False;            -- Channel disable
		CHENA     : Boolean               := False;            -- Channel enable
	end record with Size => 32;
	for Channel_Characteristics_Register use record
		MPSIZ     at 0 range  0 .. 10;
		EPNUM     at 0 range 11 .. 14;
		EPDIR     at 0 range 15 .. 15;
		Unused_16 at 0 range 16 .. 16;
		LSDEV     at 0 range 17 .. 17;
		EPTYP     at 0 range 18 .. 19;
		MCNT      at 0 range 20 .. 21;
		DAD       at 0 range 22 .. 28;
		ODDFRM    at 0 range 29 .. 29;
		CHDIS     at 0 range 30 .. 30;
		CHENA     at 0 range 31 .. 31;
	end record;

	-- HCSPLTx

	type Transaction_Part is (
		Payload_Middle,
		Payload_End,
		Payload_Begin,
		Payload_All)
	with Size => 2;

	type Channel_Split_Control_Register is record
		PRTADDR   : Device_Address   := 0;              -- Port address
		HUBADDR   : Device_Address   := 0;              -- Hub address
		XACTPOS   : Transaction_Part := Payload_Middle; -- Transaction position
		COMPLSPLT : Boolean          := False;          -- Do complete split
		Unused_17 : Unused_14_Bits   := 0;
		SPLITEN   : Boolean          := False;          -- Split enable
	end record with Size => 32;
	for Channel_Split_Control_Register use record
		PRTADDR   at 0 range  0 ..  6;
		HUBADDR   at 0 range  7 .. 13;
		XACTPOS   at 0 range 14 .. 15;
		COMPLSPLT at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 30;
		SPLITEN   at 0 range 31 .. 31;
	end record;

	-- HCINTx, HCINTMSKx

	type Channel_Interrupt_Register is record
		XFRC      : Boolean        := False; -- Transfer completed
		CHH       : Boolean        := False; -- Channel halted
		AHBERR    : Boolean        := False; -- AHB error (HS only)
		STALL     : Boolean        := False; -- STALL response received
		NAK       : Boolean        := False; -- NAK response received
		ACK       : Boolean        := False; -- ACK response received/transmitted
		NYET      : Boolean        := False; -- Not yet ready response received (HS only)
		TXERR     : Boolean        := False; -- Transaction error
		BBERR     : Boolean        := False; -- Babble error
		FRMOR     : Boolean        := False; -- Frame overrun
		DTERR     : Boolean        := False; -- Data toggle error
		Unused_11 : Unused_21_Bits := 0;
	end record with Size => 32;
	for Channel_Interrupt_Register use record
		XFRC      at 0 range  0 ..  0;
		CHH       at 0 range  1 ..  1;
		AHBERR    at 0 range  2 ..  2;
		STALL     at 0 range  3 ..  3;
		NAK       at 0 range  4 ..  4;
		ACK       at 0 range  5 ..  5;
		NYET      at 0 range  6 ..  6;
		TXERR     at 0 range  7 ..  7;
		BBERR     at 0 range  8 ..  8;
		FRMOR     at 0 range  9 ..  9;
		DTERR     at 0 range 10 .. 10;
		Unused_11 at 0 range 11 .. 31;
	end record;

	-- HCTSIZx

	subtype Transfer_Byte_Count is Storage_Count range 0 .. 2**19 - 1;
	subtype Packet_Count is Integer range 0 .. 2**10 - 1;

	type Channel_Transfer_Size_Register is record
		XFRSIZ    : Transfer_Byte_Count := 0; -- Transfer size
		PKTCNT    : Packet_Count        := 0; -- Packet count
		DPID      : Data_PID            := DATA0; -- Data PID
		Unused_31 : Unused_1_Bit        := 0;
	end record with Size => 32;
	for Channel_Transfer_Size_Register use record
		XFRSIZ    at 0 range  0 .. 18;
		PKTCNT    at 0 range 19 .. 28;
		DPID      at 0 range 29 .. 30;
		Unused_31 at 0 range 31 .. 31;
	end record;

	type Channel_Registers is record
		CHAR   : Channel_Characteristics_Register;
		SPLT   : Channel_Split_Control_Register;
		INT    : Channel_Interrupt_Register;
		INTMSK : Channel_Interrupt_Register;
		TSIZ   : Channel_Transfer_Size_Register;
		DMA    : Address_Register;
	end record with Size => 8 * 32;
	for Channel_Registers use record
		CHAR   at 16#00# range 0 .. 31;
		SPLT   at 16#04# range 0 .. 31;
		INT    at 16#08# range 0 .. 31;
		INTMSK at 16#0C# range 0 .. 31;
		TSIZ   at 16#10# range 0 .. 31;
		DMA    at 16#14# range 0 .. 31;
	end record;

	type Channel_Register_Array is array (Channel_Number) of Channel_Registers;

	--

	type USB_OTG_Host_Registers is record
		HCFG     : Configuration_Register;
		HPTXSTS  : Periodic_Transmit_Status_Register;
		HAINT    : All_Channels_Interrupt_Register;
		HAINTMSK : All_Channels_Interrupt_Register;
		HFIR     : Frame_Interval_Register;
		HPRT     : Port_Control_Status_Register;
		HFNUM    : Frame_Number_Register;
		HC       : Channel_Register_Array;
	end record;
	for USB_OTG_Host_Registers use record
		HCFG     at 16#0000# range 0 .. 31;
		HFIR     at 16#0004# range 0 .. 31;
		HFNUM    at 16#0008# range 0 .. 31;
		HPTXSTS  at 16#0010# range 0 .. 31;
		HAINT    at 16#0014# range 0 .. 31;
		HAINTMSK at 16#0018# range 0 .. 31;
		HPRT     at 16#0040# range 0 .. 31;
		HC       at 16#0100# range 0 .. 16 * 8 * 32 - 1;
	end record;

end STM32.USB_OTG.Host;
