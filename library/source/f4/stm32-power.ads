with STM32.Address_Map;

-- Power Controller

package STM32.Power is

	type PVD_Level is (
		PVD_2_0_V,
		PVD_2_1_V,
		PVD_2_3_V,
		PVD_2_5_V,
		PVD_2_6_V,
		PVD_2_7_V,
		PVD_2_8_V,
		PVD_2_9_V
	) with Size => 3;
	for PVD_Level use (
		PVD_2_0_V => 2#000#,
		PVD_2_1_V => 2#001#,
		PVD_2_3_V => 2#010#,
		PVD_2_5_V => 2#011#,
		PVD_2_6_V => 2#100#,
		PVD_2_7_V => 2#101#,
		PVD_2_8_V => 2#110#,
		PVD_2_9_V => 2#111#
	);

	type Voltage_Scaling is (
		Scale_3,
		Scale_2,
		Scale_1
	) with Size => 2;
	for Voltage_Scaling use (
		Scale_3 => 2#00#,
		Scale_2 => 2#01#,
		Scale_1 => 2#10#
	);

	type Underdrive_Mode is (
		Underdrive_Off,
		Underdrive_On
	) with Size => 2;
	for Underdrive_Mode use (
		Underdrive_Off => 2#00#,
		Underdrive_On  => 2#11#
	);

	type Power_Control_Register is record
		LPDS      : Boolean;
		PDDS      : Boolean;
		CWUF      : Boolean;
		CSBF      : Boolean;
		PVDE      : Boolean;
		PLS       : PVD_Level;
		DBP       : Boolean;
		FPDS      : Boolean;
		LPUDS     : Boolean;
		MRUDS     : Boolean;
		Unused_12 : Integer range 0 .. 1;
		ADCDC1    : Boolean;
		VOS       : Voltage_Scaling;
		ODEN      : Boolean;
		ODSWEN    : Boolean;
		UDEN      : Underdrive_Mode;
		Unused_20 : Integer range 0 .. 2**12 - 1;
	end record with Size => 32;

	for Power_Control_Register use record
		LPDS      at 0 range 0 .. 0;
		PDDS      at 0 range 1 .. 1;
		CWUF      at 0 range 2 .. 2;
		CSBF      at 0 range 3 .. 3;
		PVDE      at 0 range 4 .. 4;
		PLS       at 0 range 5 .. 7;
		DBP       at 0 range 8 .. 8;
		FPDS      at 0 range 9 .. 9;
		LPUDS     at 0 range 10 .. 10;
		MRUDS     at 0 range 11 .. 11;
		Unused_12 at 0 range 12 .. 12;
		ADCDC1    at 0 range 13 .. 13;
		VOS       at 0 range 14 .. 15;
		ODEN      at 0 range 16 .. 16;
		ODSWEN    at 0 range 17 .. 17;
		UDEN      at 0 range 18 .. 19;
		Unused_20 at 0 range 20 .. 31;
	end record;

	type Power_Control_Status_Register is record
		WUF     : Boolean;
		SBF     : Boolean;
		PVDO    : Boolean;
		BRR     : Boolean;
		Reserved_4 : Integer range 0 .. 2**4 - 1;
		EWUP    : Boolean;
		BRE     : Boolean;
		Reserved_10 : Integer range 0 .. 2**4 - 1;
		VOSRDY  : Boolean;
		Reserved_15 : Integer range 0 .. 1;
		ODRDY   : Boolean;
		ODSWRDY : Boolean;
		UDRDY   : Underdrive_Mode;
		Reserved_20 : Integer range 0 .. 2**12 - 1;
	end record with Size => 32;

	for Power_Control_Status_Register use record
		WUF     at 0 range  0 .. 0;
		SBF     at 0 range  1 .. 1;
		PVDO    at 0 range  2 .. 2;
		BRR     at 0 range  3 .. 3;
		Reserved_4 at 0 range 4 .. 7;
		EWUP    at 0 range  8 .. 8;
		BRE     at 0 range  9 .. 9;
		Reserved_10 at 0 range 10 .. 13;
		VOSRDY  at 0 range 14 .. 14;
		Reserved_15 at 0 range 15 .. 15;
		ODRDY   at 0 range 16 .. 16;
		ODSWRDY at 0 range 17 .. 17;
		UDRDY   at 0 range 18 .. 19;
		Reserved_20 at 0 range 20 .. 31;
	end record;

	type PWR_Registers is record
		CR  : Power_Control_Register;
		pragma Volatile_Full_Access (CR);
		CSR : Power_Control_Status_Register;
		pragma Volatile_Full_Access (CSR);
	end record with Volatile;

	for PWR_Registers use record
		CR  at 16#00# range 0 .. 31;
		CSR at 16#04# range 0 .. 31;
	end record;

	PWR : PWR_Registers with Volatile, Import, Address => Address_Map.PWR;

end STM32.Power;
