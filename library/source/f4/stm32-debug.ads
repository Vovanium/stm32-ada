with STM32.Device_IDs;
with STM32.Address_Map;

-- Debug support

package STM32.Debug is

	type Revision_ID is range 0 .. 2**16 - 1;

	type Device_ID_Register is record
		DEV_ID      : Device_IDs.Device_ID;     -- Device identifier
		Unused_12   : Unused_4_Bits;
		REV_ID      : Revision_ID;   -- Revision identifier
	end record with Size => 32;
	for Device_ID_Register use record
		DEV_ID      at 0 range  0 .. 11;
		Unused_12   at 0 range 12 .. 15;
		REV_ID      at 0 range 16 .. 31;
	end record;

	-- CR

	type Trace_Mode_Select is (
		Asynchronous,
		Synchronous_Data_Size_1,
		Synchronous_Data_Size_2,
		Synchronous_Data_Size_4
	) with Size => 2;
	for Trace_Mode_Select use (
		Asynchronous            => 2#00#, -- Asynchronous mode
		Synchronous_Data_Size_1 => 2#01#, -- Synchronous mode with TRACEDATA size of 1
		Synchronous_Data_Size_2 => 2#10#, -- Synchronous mode with TRACEDATA size of 2
		Synchronous_Data_Size_4 => 2#11#  -- Synchronous mode with TRACEDATA size of 4
	);

	type Configuration_Register is record
		DBG_SLEEP              : Boolean           := False; -- Debug sleep mode
		DBG_STOP               : Boolean           := False; -- Debug stop mode
		DBG_STANDBY            : Boolean           := False; -- Debug standby mode
		Unused_3               : Integer range 0 .. 3 := 0;
		TRACE_IOEN             : Boolean           := False; -- Trace pins assignment
		TRACE_MODE             : Trace_Mode_Select := Asynchronous; -- Trace mode select
		Unused_8               : Integer range 0 .. 2**8 - 1 := 0;
		DBG_I2C2_SMBUS_TIMEOUT : Boolean           := False; -- DBG_I2C2_SMBUS_TIMEOUT
		DBG_TIM8_STOP          : Boolean           := False; -- DBG_TIM8_STOP
		DBG_TIM5_STOP          : Boolean           := False; -- DBG_TIM5_STOP
		DBG_TIM6_STOP          : Boolean           := False; -- DBG_TIM6_STOP
		DBG_TIM7_STOP          : Boolean           := False; -- DBG_TIM7_STOP
		Unused_21              : Integer range 0 .. 2**11 - 1 := 0;
	end record with Size => 32;
	for Configuration_Register use record
		DBG_SLEEP              at 0 range  0 ..  0;
		DBG_STOP               at 0 range  1 ..  1;
		DBG_STANDBY            at 0 range  2 ..  2;
		Unused_3               at 0 range  3 ..  4;
		TRACE_IOEN             at 0 range  5 ..  5;
		TRACE_MODE             at 0 range  6 ..  7;
		Unused_8               at 0 range  8 .. 15;
		DBG_I2C2_SMBUS_TIMEOUT at 0 range 16 .. 16;
		DBG_TIM8_STOP          at 0 range 17 .. 17;
		DBG_TIM5_STOP          at 0 range 18 .. 18;
		DBG_TIM6_STOP          at 0 range 19 .. 19;
		DBG_TIM7_STOP          at 0 range 20 .. 20;
		Unused_21              at 0 range 21 .. 31;
	end record;

	-- PB1_FZ

	type APB1_Freeze_Register is record
		DBG_TIM2_STOP          : Boolean := False; -- TIM2 counter is stopped when core is halted
		DBG_TIM3_STOP          : Boolean := False; -- TIM3 counter is stopped when core is halted
		DBG_TIM4_STOP          : Boolean := False; -- TIM4 counter is stopped when core is halted
		DBG_TIM5_STOP          : Boolean := False; -- TIM5 counter is stopped when core is halted
		DBG_TIM6_STOP          : Boolean := False; -- TIM6 counter is stopped when core is halted
		DBG_TIM7_STOP          : Boolean := False; -- TIM7 counter is stopped when core is halted
		DBG_TIM12_STOP         : Boolean := False; -- TIM12 counter is stopped when core is halted
		DBG_TIM13_STOP         : Boolean := False; -- TIM13 counter is stopped when core is halted
		DBG_TIM14_STOP         : Boolean := False; -- TIM14 counter is stopped when core is halted
		Unused_9               : Integer range 0 .. 1 := 0;
		DBG_RTC_Stop           : Boolean := False; -- RTC stopped when core is halted
		DBG_WWDG_STOP          : Boolean := False; -- Window watchdog is halted when core is halted
		DBG_IWDEG_STOP         : Boolean := False; -- Independent watchdog is halted when core is halted
		Unused_13              : Integer range 0 .. 2**8 - 1 := 0;
		DBG_I2C1_SMBUS_TIMEOUT : Boolean := False; -- I2C1 SMBUS timeout mode stopped whrn core is halted
		DBG_I2C2_SMBUS_TIMEOUT : Boolean := False; -- I2C2 SMBUS timeout mode stopped when core is halted
		DBG_I2C3_SMBUS_TIMEOUT : Boolean := False; -- I2C3 SMBUS timeout mode stopped when core is halted
		Unused_24              : Integer range 0 .. 1 := 0;
		DBG_CAN1_STOP          : Boolean := False; -- CAN1 stopped when core is halted
		DBG_CAN2_STOP          : Boolean := False; -- CAN2 stopped when core is halted
		Unused_27              : Integer range 0 .. 2**5 - 1 := 0;
	end record with Size => 32;
	for APB1_Freeze_Register use record
		DBG_TIM2_STOP          at 0 range  0 ..  0;
		DBG_TIM3_STOP          at 0 range  1 ..  1;
		DBG_TIM4_STOP          at 0 range  2 ..  2;
		DBG_TIM5_STOP          at 0 range  3 ..  3;
		DBG_TIM6_STOP          at 0 range  4 ..  4;
		DBG_TIM7_STOP          at 0 range  5 ..  5;
		DBG_TIM12_STOP         at 0 range  6 ..  6;
		DBG_TIM13_STOP         at 0 range  7 ..  7;
		DBG_TIM14_STOP         at 0 range  8 ..  8;
		Unused_9               at 0 range  9 ..  9;
		DBG_RTC_Stop           at 0 range 10 .. 10;
		DBG_WWDG_STOP          at 0 range 11 .. 11;
		DBG_IWDEG_STOP         at 0 range 12 .. 12;
		Unused_13              at 0 range 13 .. 20;
		DBG_I2C1_SMBUS_TIMEOUT at 0 range 21 .. 21;
		DBG_I2C2_SMBUS_TIMEOUT at 0 range 22 .. 22;
		DBG_I2C3_SMBUS_TIMEOUT at 0 range 23 .. 23;
		Unused_24              at 0 range 24 .. 24;
		DBG_CAN1_STOP          at 0 range 25 .. 25;
		DBG_CAN2_STOP          at 0 range 26 .. 26;
		Unused_27              at 0 range 27 .. 31;
	end record;

	-- DBGMCU_APB2_FZ

	type APB2_Freeze_Register is record
		DBG_TIM1_STOP  : Boolean; -- TIM1 counter stopped when core is halted
		DBG_TIM8_STOP  : Boolean; -- TIM8 counter stopped when core is halted
		Unused_2       : Integer range 0 .. 2**14 - 1;
		DBG_TIM9_STOP  : Boolean; -- TIM9 counter stopped when core is halted
		DBG_TIM10_STOP : Boolean; -- TIM10 counter stopped when core is halted
		DBG_TIM11_STOP : Boolean; -- TIM11 counter stopped when core is halted
		Unused_19      : Integer range 0 .. 2**13 - 1;
	end record with Size => 32;
	for APB2_Freeze_Register use record
		DBG_TIM1_STOP  at 0 range  0 ..  0;
		DBG_TIM8_STOP  at 0 range  1 ..  1;
		Unused_2       at 0 range  2 .. 15;
		DBG_TIM9_STOP  at 0 range 16 .. 16;
		DBG_TIM10_STOP at 0 range 17 .. 17;
		DBG_TIM11_STOP at 0 range 18 .. 18;
		Unused_19      at 0 range 19 .. 31;
	end record;

	type DBG_Registers is record
		IDCODE  : Device_ID_Register;
		pragma Volatile_Full_Access (IDCODE);
		CR      : Configuration_Register;
		pragma Volatile_Full_Access (CR);
		APB1_FZ : APB1_Freeze_Register;
		pragma Volatile_Full_Access (APB1_FZ);
		APB2_FZ : APB2_Freeze_Register;
		pragma Volatile_Full_Access (APB2_FZ);
	end record;
	for DBG_Registers use record
		IDCODE  at 16#00# range  0 .. 31;
		CR      at 16#04# range  0 .. 31;
		APB1_FZ at 16#08# range  0 .. 31;
		APB2_FZ at 16#0C# range  0 .. 31;
	end record;

	DBG : DBG_Registers with Volatile, Import, Address => Address_Map.DBG;

end STM32.Debug;
