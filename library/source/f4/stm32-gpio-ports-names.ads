package STM32.GPIO.Ports.Names is

	package Port_A is new GPIO_Port (GPIOA, Index.GPIOA, PA_Pin);
	package Port_B is new GPIO_Port (GPIOB, Index.GPIOB, PB_Pin);
	package Port_C is new GPIO_Port (GPIOC, Index.GPIOC, PC_Pin);
	package Port_D is new GPIO_Port (GPIOD, Index.GPIOD, PD_Pin);
	package Port_E is new GPIO_Port (GPIOE, Index.GPIOE, PE_Pin);
	package Port_F is new GPIO_Port (GPIOF, Index.GPIOF, PF_Pin);
	package Port_G is new GPIO_Port (GPIOG, Index.GPIOG, PG_Pin);
	package Port_H is new GPIO_Port (GPIOH, Index.GPIOH, PH_Pin);
	package Port_I is new GPIO_Port (GPIOI, Index.GPIOI, PI_Pin);
	package Port_J is new GPIO_Port (GPIOJ, Index.GPIOJ, PJ_Pin);
	package Port_K is new GPIO_Port (GPIOK, Index.GPIOK, PK_Pin);

end STM32.GPIO.Ports.Names;
