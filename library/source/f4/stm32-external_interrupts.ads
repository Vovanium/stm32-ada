with STM32.Address_Map;

-- External Interrupts and Events Controller

package STM32.External_Interrupts is

	subtype Line_Number is Integer range 0 .. 31;
	package Lines is
		-- line 0 to line 15 are external
		PVD               : constant Line_Number := 16;
		RTC_Alarm         : constant Line_Number := 17;
		USB_OTG_FS_Wakeup : constant Line_Number := 18;
		Ethernet_Wakeup   : constant Line_Number := 19;
		USB_OTG_HS_Wakeup : constant Line_Number := 20;
		RTC_Tamper        : constant Line_Number := 21;
		RTC_Timestamp     : constant Line_Number := 21; -- alias for RTC_Tamper
		RTC_Wakeup        : constant Line_Number := 22;
	end Lines;

	type Line_Set_Register is array (Line_Number) of Boolean
	with Pack, Size => 32;

	type EXTI_Registers is record
		IMR   : Line_Set_Register;
		pragma Volatile_Full_Access (IMR);
		EMR   : Line_Set_Register;
		pragma Volatile_Full_Access (EMR);
		RTSR  : Line_Set_Register;
		pragma Volatile_Full_Access (RTSR);
		FTSR  : Line_Set_Register;
		pragma Volatile_Full_Access (FTSR);
		SWIER : Line_Set_Register;
		pragma Volatile_Full_Access (SWIER);
		PR    : Line_Set_Register;
		pragma Volatile_Full_Access (PR);
	end record with Volatile;

	for EXTI_Registers use record
		IMR   at 16#00# range 0 .. 31;
		EMR   at 16#04# range 0 .. 31;
		RTSR  at 16#08# range 0 .. 31;
		FTSR  at 16#0C# range 0 .. 31;
		SWIER at 16#10# range 0 .. 31;
		PR    at 16#14# range 0 .. 31;
	end record;

	EXTI : EXTI_Registers with Volatile, Import, Address => Address_Map.EXTI;

end STM32.External_Interrupts;
