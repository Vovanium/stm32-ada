with STM32.Address_Map;

-- Fast Mode+ I2C controllers
--
-- These units exist in models F410, F412, F413, F423 and F446.
-- They're identical through all listed models.

package STM32.Fast_Mode_Plus_I2Cs is

	-- CR1

	type Control_Register_1 is record
		PE        : Boolean               := False; -- Peripheral inable
		TXIE      : Boolean               := False; -- Transmitter interrupt enable
		RXIE      : Boolean               := False; -- Receiver interrupt enable
		ADDRE     : Boolean               := False; -- Address match interrupt enable
		NACKIE    : Boolean               := False; -- Not acknowledge interrupt enable
		STOPIE    : Boolean               := False; -- Stop detection interrupt enable
		TCIE      : Boolean               := False; -- Transfer complete interrupt enable
		ERRIE     : Boolean               := False; -- Error detection interrupts enable
		DNF       : Integer range 0 .. 15 := 0; -- Digital filter period
		ANFOFF    : Boolean               := False; -- Disable analog noise filter
		Unused_13 : Unused_1_Bit          := 0;
		TXDMAEN   : Boolean               := False; -- Transmit DMA requests enable
		RXDMAEN   : Boolean               := False; -- Receive DMA resuqests enable
		SBC       : Boolean               := False; -- Slave byte control
		NOSTRETCH : Boolean               := False; -- Clock stretching disable
		WUPEN     : Boolean               := False; -- Undocumented?
		GCEN      : Boolean               := False; -- General call ACK enable
		SMBHEN    : Boolean               := False; -- SMBus host address enable
		SMBDEN    : Boolean               := False; -- SMBus device default address enable
		ALERTEN   : Boolean               := False; -- SMBus alert enable
		PECEN     : Boolean               := False; -- PEC calculation enable
		Unused_24 : Unused_8_Bits         := 0;
	end record with Size => 32;
	for Control_Register_1 use record      -- F410,F412  F4x3,F446
		PE        at 0 range  0 ..  0; -- +          +
		TXIE      at 0 range  1 ..  1; -- +          +
		RXIE      at 0 range  2 ..  2; -- +          +
		ADDRE     at 0 range  3 ..  3; -- +          +
		NACKIE    at 0 range  4 ..  4; -- +          +
		STOPIE    at 0 range  5 ..  5; -- +          +
		TCIE      at 0 range  6 ..  6; -- +          +
		ERRIE     at 0 range  7 ..  7; -- +          +
		DNF       at 0 range  8 .. 11; -- +          +
		ANFOFF    at 0 range 12 .. 12; -- +          +
		Unused_13 at 0 range 13 .. 13;
		TXDMAEN   at 0 range 14 .. 14; -- +          +
		RXDMAEN   at 0 range 15 .. 15; -- +          +
		SBC       at 0 range 16 .. 16; -- +          +
		NOSTRETCH at 0 range 17 .. 17; -- +          +
		WUPEN     at 0 range 18 .. 18; -- +          -
		GCEN      at 0 range 19 .. 19; -- +          +
		SMBHEN    at 0 range 20 .. 20; -- +          +
		SMBDEN    at 0 range 21 .. 21; -- +          +
		ALERTEN   at 0 range 22 .. 22; -- +          +
		PECEN     at 0 range 23 .. 23; -- +          +
		Unused_24 at 0 range 24 .. 31;
	end record;

	-- CR2

	type Direction is (
		Write,
		Read
	) with Size => 1;
	for Direction use (
		Write => 0,
		Read  => 1
	);

	type Control_Register_2 is record
		SADD        : Integer range 0 .. 2**10 - 1 := 0; -- Slave address
		RD_WRN      : Direction := Write; -- Transfer direction header
		ADD10       : Boolean := False; -- 10-bit addressing mode
		HEAD10R     : Boolean := False; -- Send only 7 bit part of address in read direction
		START       : Boolean := False; -- START generation
		STOP        : Boolean := False; -- STOP generation
		NACK        : Boolean := False; -- NACK generation
		NBYTES      : Integer range 0 .. 2**8 - 1 := 0; -- Number of bytes
		RELOAD      : Boolean := False; -- Reload NBYTES after transfer
		AUTOEND     : Boolean := False; -- Automatic end mode (send stop after NBYTES transferred)
		PECBYTE     : Boolean := False; -- PEC byte transfer
		Reserved_27 : Integer range 0 .. 2**5 - 1 := 0;
	end record with Size => 32;
	for Control_Register_2 use record
		SADD        at 0 range  0 ..  9;
		RD_WRN      at 0 range 10 .. 10;
		ADD10       at 0 range 11 .. 11;
		HEAD10R     at 0 range 12 .. 12;
		START       at 0 range 13 .. 13;
		STOP        at 0 range 14 .. 14;
		NACK        at 0 range 15 .. 15;
		NBYTES      at 0 range 16 .. 23;
		RELOAD      at 0 range 24 .. 24;
		AUTOEND     at 0 range 25 .. 25;
		PECBYTE     at 0 range 26 .. 26;
		Reserved_27 at 0 range 27 .. 31;
	end record;

	-- OAR1

	type Addressing_Mode is (
		Address_7_Bit,
		Address_10_Bit
	) with Size => 1;
	for Addressing_Mode use (
		Address_7_Bit  => 0,
		Address_10_Bit => 1
	);

	type Own_Address_Register_1 is record
		OA1         : Integer range 0 .. 2**10 - 1 := 0; -- Own address
		OA1MODE     : Addressing_Mode              := Address_7_Bit; -- Address size
		Reserved_11 : Integer range 0 .. 15        := 0;
		OA1EN       : Boolean                      := False; -- Own address enable
		Reserved_16 : Integer range 0 .. 2**16 - 1 := 0;
	end record with Size => 32;
	for Own_Address_Register_1 use record
		OA1         at 0 range  0 ..  9;
		OA1MODE     at 0 range 10 .. 10;
		Reserved_11 at 0 range 11 .. 14;
		OA1EN       at 0 range 15 .. 15;
		Reserved_16 at 0 range 16 .. 31;
	end record;

	-- OAR2

	type Own_Address_Register_2 is record
		Reserved_0  : Integer range 0 .. 1         := 0;
		OA2         : Integer range 0 .. 2**7 - 1  := 0; -- Address
		OA2MSK      : Integer range 0 .. 7         := 0; -- Masked bits
		Reserved_11 : Integer range 0 .. 15        := 0;
		OA2EN       : Boolean                      := False; -- Own address 2 enable
		Reserved_16 : Integer range 0 .. 2**16 - 1 := 0;
	end record with Size => 32;
	for Own_Address_Register_2 use record
		Reserved_0  at 0 range  0 ..  0;
		OA2         at 0 range  1 ..  7;
		OA2MSK      at 0 range  8 .. 10;
		Reserved_11 at 0 range 11 .. 14;
		OA2EN       at 0 range 15 .. 15;
		Reserved_16 at 0 range 16 .. 31;
	end record;

	-- TIMINGR

	type TIMINGR_Register is record
		SCLL        : Integer range 0 .. 2**8 - 1 := 0; -- SCL low period
		SCLH        : Integer range 0 .. 2**8 - 1 := 0; -- SCL high period
		SDADEL      : Integer range 0 .. 15       := 0; -- Data hold time
		SCLDEL      : Integer range 0 .. 15       := 0; -- Data setup time
		Reserved    : Integer range 0 .. 15       := 0;
		PRESC       : Integer range 0 .. 15       := 0; -- Prescaler factor minus 1
	end record with Size => 32;
	for TIMINGR_Register use record
		SCLL        at 0 range  0 ..  7;
		SCLH        at 0 range  8 .. 15;
		SDADEL      at 0 range 16 .. 19;
		SCLDEL      at 0 range 20 .. 23;
		Reserved    at 0 range 24 .. 27;
		PRESC       at 0 range 28 .. 31;
	end record;

	-- TIMEOUTR

	type Timeout_Register is record
		TIMEOUTA    : Integer range 0 .. 2**12 - 1 := 0; -- Bus timeout A
		TIDLE       : Boolean                      := False; -- Idle detection enable
		Reserved_13 : Integer range 0 .. 3         := 0;
		TIMOUTEN    : Boolean                      := False; -- Clock timeout enable
		TIMEOUTB    : Integer range 0 .. 2**12 - 1 := 0; -- Bus timeout B
		Reserved_28 : Integer range 0 .. 7         := 0;
		TEXTEN      : Boolean                      := False; -- Extended clock timeout enable
	end record with Size => 32;
	for Timeout_Register use record
		TIMEOUTA    at 0 range  0 .. 11;
		TIDLE       at 0 range 12 .. 12;
		Reserved_13 at 0 range 13 .. 14;
		TIMOUTEN    at 0 range 15 .. 15;
		TIMEOUTB    at 0 range 16 .. 27;
		Reserved_28 at 0 range 28 .. 30;
		TEXTEN      at 0 range 31 .. 31;
	end record;

	-- ISR

	type Interrupt_and_Status_Register is record
		TXE         : Boolean;                     -- Transmit data register empty
		TXIS        : Boolean;                     -- Transmit interrupt status
		RXNE        : Boolean;                     -- Receive data not empty
		ADDR        : Boolean;                     -- Address matched (slave mode)
		NACKF       : Boolean;                     -- Not acknowledge received flag
		STOPF       : Boolean;                     -- Stop detection flag
		TC          : Boolean;                     -- Transfer complete
		TCR         : Boolean;                     -- Transfer complete reload
		BERR        : Boolean;                     -- Bus error
		ARLO        : Boolean;                     -- Arbitration lost
		OVR         : Boolean;                     -- Overrun or underrun (slave mode)
		PECERR      : Boolean;                     -- PEC error in reception
		TIMEOUT     : Boolean;                     -- Timeout or Tlow detection flag
		ALERT       : Boolean;                     -- SMBus alert
		Reserved_14 : Integer range 0 .. 1;
		BUSY        : Boolean;                     -- Bus busy
		DIR         : Direction;                   -- Transfer direction (updated on address match event)
		ADDCODE     : Integer range 0 .. 2**7 - 1; -- Address match code (slave mode)
		Reserved_24 : Integer range 0 .. 2**8 - 1;
	end record with Size => 32;
	for Interrupt_and_Status_Register use record
		TXE         at 0 range  0 ..  0;
		TXIS        at 0 range  1 ..  1;
		RXNE        at 0 range  2 ..  2;
		ADDR        at 0 range  3 ..  3;
		NACKF       at 0 range  4 ..  4;
		STOPF       at 0 range  5 ..  5;
		TC          at 0 range  6 ..  6;
		TCR         at 0 range  7 ..  7;
		BERR        at 0 range  8 ..  8;
		ARLO        at 0 range  9 ..  9;
		OVR         at 0 range 10 .. 10;
		PECERR      at 0 range 11 .. 11;
		TIMEOUT     at 0 range 12 .. 12;
		ALERT       at 0 range 13 .. 13;
		Reserved_14 at 0 range 14 .. 14;
		BUSY        at 0 range 15 .. 15;
		DIR         at 0 range 16 .. 16;
		ADDCODE     at 0 range 17 .. 23;
		Reserved_24 at 0 range 24 .. 31;
	end record;

	-- ICR

	type Interrupt_Clear_Register is record
		Reserved_0  : Integer range 0 .. 7;
		ADDRCF      : Boolean := False; -- Clear address matched flag
		NACKCF      : Boolean := False; -- Clear not acknowledge flag
		STOPCF      : Boolean := False; -- Clear stop detection flag
		Reserved_6  : Integer range 0 .. 3;
		BERRCF      : Boolean := False; -- Clear bus error flag
		ARLOCF      : Boolean := False; -- Clear arbitration lost flag
		OVRCF       : Boolean := False; -- Clear overrun/underrun flag
		PECCF       : Boolean := False; -- Clear PEC error flag
		TIMOUTCF    : Boolean := False; -- Clear timeout detection flag
		ALERTC      : Boolean := False; -- Clear alert flag
		Reserved_14 : Integer range 0 .. 2**18 - 1;
	end record with Size => 32;
	for Interrupt_Clear_Register use record
		Reserved_0  at 0 range  0 ..  2;
		ADDRCF      at 0 range  3 ..  3;
		NACKCF      at 0 range  4 ..  4;
		STOPCF      at 0 range  5 ..  5;
		Reserved_6  at 0 range  6 ..  7;
		BERRCF      at 0 range  8 ..  8;
		ARLOCF      at 0 range  9 ..  9;
		OVRCF       at 0 range 10 .. 10;
		PECCF       at 0 range 11 .. 11;
		TIMOUTCF    at 0 range 12 .. 12;
		ALERTC      at 0 range 13 .. 13;
		Reserved_14 at 0 range 14 .. 31;
	end record;

	--

	type FMPI2C_Registers is record
		CR1         : Control_Register_1;
		pragma Volatile_Full_Access (CR1);
		CR2         : Control_Register_2;
		pragma Volatile_Full_Access (CR2);
		OAR1        : Own_Address_Register_1;
		pragma Volatile_Full_Access (OAR1);
		OAR2        : Own_Address_Register_2;
		pragma Volatile_Full_Access (OAR2);
		TIMINGR     : TIMINGR_Register;
		pragma Volatile_Full_Access (TIMINGR);
		TIMEOUTR    : Timeout_Register;
		pragma Volatile_Full_Access (TIMEOUTR);
		ISR         : Interrupt_and_Status_Register;
		pragma Volatile_Full_Access (ISR);
		ICR         : Interrupt_Clear_Register;
		pragma Volatile_Full_Access (ICR);
		PECR        : Integer range 0 .. 2**8 - 1;
		pragma Volatile_Full_Access (PECR);
		RXDR        : Integer range 0 .. 2**8 - 1;
		pragma Volatile_Full_Access (RXDR);
		TXDR        : Integer range 0 .. 2**8 - 1;
		pragma Volatile_Full_Access (TXDR);
	end record;
	for FMPI2C_Registers use record
		CR1         at 16#00# range  0 .. 31;
		CR2         at 16#04# range  0 .. 31;
		OAR1        at 16#08# range  0 .. 31;
		OAR2        at 16#0C# range  0 .. 31;
		TIMINGR     at 16#10# range  0 .. 31;
		TIMEOUTR    at 16#14# range  0 .. 31;
		ISR         at 16#18# range  0 .. 31;
		ICR         at 16#1C# range  0 .. 31;
		PECR        at 16#20# range  0 .. 31;
		RXDR        at 16#24# range  0 .. 31;
		TXDR        at 16#28# range  0 .. 31;
	end record;

	FMPI2C1 : aliased FMPI2C_Registers with Volatile, Import, Address => Address_Map.FMPI2C1;

	I2C4    : FMPI2C_Registers renames FMPI2C1;

	-- Device user manuals use different names for these units,
	-- but they're the same.

end STM32.Fast_Mode_Plus_I2Cs;
