with STM32.Address_Map;

-- Flexible (Static) Memory Controller

package STM32.Flexible_Memory_Controller is

	type Memory_Width is (
		Memory_8_Bits,
		Memory_16_Bits,
		Memory_32_Bits, -- note: not all devices allow this
		Memory_Width_Unused
	) with Size => 2;

	-- NOR Flash / PSRAM controller registers

	package SRAM is

	-- BCRx

		type Memory_Type is (
			SRAM,
			PSRAM,
			NOR_Flash
		) with Size => 2;
		for Memory_Type use (
			SRAM      => 2#00#,
			PSRAM     => 2#01#,
			NOR_Flash => 2#10#
		);

		type Signal_Polarity is (
			Active_Low,
			Active_High
		) with Size => 1;
		for Signal_Polarity use (
			Active_Low  => 0,
			Active_High => 1
		);

		type Wait_Timing is (
			One_Cycle_Before,
			During
		) with Size => 1;
		for Wait_Timing use (
			One_Cycle_Before => 0,
			During           => 1
		);

		type CRAM_Page_Size is (
			CRAM_No_Burst_Split,
			CRAM_Page_128_Bytes,
			CRAM_Page_256_Bytes,
			CRAM_Page_512_Bytes,
			CRAM_Page_1024_Bytes
		) with Size => 3;
		for CRAM_Page_Size use (
			CRAM_No_Burst_Split => 2#000#,
			CRAM_Page_128_Bytes => 2#001#,
			CRAM_Page_256_Bytes => 2#010#,
			CRAM_Page_512_Bytes => 2#011#,
			CRAM_Page_1024_Bytes => 2#100#
		);

		type Chip_Select_Control_Register is record
			MBKEN     : Boolean;
			MUXEN     : Boolean;
			MTYP      : Memory_Type;
			MWID      : Memory_Width;
			FACCEN    : Boolean;
			Unused_7  : Unused_1_bit     := 0;
			BURSTEN   : Boolean;
			WAITPOL   : Signal_Polarity;
			WRAPMOD   : Boolean;
			WAITCFG   : Wait_Timing;
			WREN      : Boolean;
			WAITEN    : Boolean;
			EXTMOD    : Boolean;
			ASYNCWAIT : Boolean;
			CPSIZE    : CRAM_Page_Size;
			CBURSTRW  : Boolean;
			CCLKEN    : Boolean; -- (not available in all versions)
			WFDIS     : Boolean;
			Unused_22 : Unused_10_Bits := 0;
		end record with Size => 32;
		for Chip_Select_Control_Register use record
			MBKEN     at 0 range  0 ..  0;
			MUXEN     at 0 range  1 ..  1;
			MTYP      at 0 range  2 ..  3;
			MWID      at 0 range  4 ..  5;
			FACCEN    at 0 range  6 ..  6;
			Unused_7  at 0 range  7 ..  7;
			BURSTEN   at 0 range  8 ..  8;
			WAITPOL   at 0 range  9 ..  9;
			WRAPMOD   at 0 range 10 .. 10;
			WAITCFG   at 0 range 11 .. 11;
			WREN      at 0 range 12 .. 12;
			WAITEN    at 0 range 13 .. 13;
			EXTMOD    at 0 range 14 .. 14;
			ASYNCWAIT at 0 range 15 .. 15;
			CPSIZE    at 0 range 16 .. 18;
			CBURSTRW  at 0 range 19 .. 19;
			CCLKEN    at 0 range 20 .. 20;
			WFDIS     at 0 range 21 .. 21;
			Unused_22 at 0 range 22 .. 31;
		end record;

		-- BTRx, also PWTRx

		type Access_Mode is (
			Access_Mode_A,
			Access_Mode_B,
			Access_Mode_C,
			Access_Mode_D
		) with Size => 2;
		for Access_Mode use (
			Access_Mode_A => 2#00#,
			Access_Mode_B => 2#01#,
			Access_Mode_C => 2#10#,
			Access_Mode_D => 2#11#
		);

		type Chip_Select_Timing_Register is record
			ADDSET      : Integer range 0 .. 15;  -- Address setup phase duration in HCLK cycles
			ADDHLD      : Integer range 1 .. 15;  -- Address hold phase duration in HCLK cycles
			DATAST      : Integer range 1 .. 255; -- Data phase duration in HCLK cycles
			BUSTURN     : Integer range 0 .. 15;  -- Bus turnaround phase duration in HCLK cycles
			CLKDIV      : Integer range 1 .. 15;  -- Clock divide ratio fo FMC_CLK signal minus 1 (HCLK periods) (not used in BWTR)
			DATLAT      : Integer range 0 .. 15;  -- Data latency for synchronous memory minus 2 in FMC_CLK cycles (not used in BWTR)
			ACCMOD      : Access_Mode; -- Access mode
			Reserved    : Integer range 0 .. 3;
		end record with Size => 32;
		for Chip_Select_timing_Register use record
			ADDSET      at 0 range  0 ..  3;
			ADDHLD      at 0 range  4 ..  7;
			DATAST      at 0 range  8 .. 15;
			BUSTURN     at 0 range 16 .. 19;
			CLKDIV      at 0 range 20 .. 23;
			DATLAT      at 0 range 24 .. 27;
			ACCMOD      at 0 range 28 .. 29;
			Reserved    at 0 range 30 .. 31;
		end record;

	end SRAM;

	-- NANO Flash / PC-Card controller registers

	package PC_Card is

	-- PCRx

		type Memory_Type is (
			PC_Card,
			NAND_Flash
		);
		for Memory_Type use (
			PC_Card    => 0,
			NAND_Flash => 1
		);

		subtype Memory_Width is Flexible_Memory_Controller.Memory_Width range Memory_8_Bits .. Memory_16_Bits;

		type ECC_Page_Size is (
			ECC_Page_256_Bytes,
			ECC_Page_512_Bytes,
			ECC_Page_1024_Bytes,
			ECC_Page_2048_Bytes,
			ECC_Page_4096_Bytes,
			ECC_Page_8192_Bytes
		);
		for ECC_Page_Size use (
			ECC_Page_256_Bytes  => 2#000#,
			ECC_Page_512_Bytes  => 2#001#,
			ECC_Page_1024_Bytes => 2#010#,
			ECC_Page_2048_Bytes => 2#011#,
			ECC_Page_4096_Bytes => 2#100#,
			ECC_Page_8192_Bytes => 2#101#
		);

		type Control_Register is record
			Reserved_0  : Integer range 0 .. 1  := 0;
			PWAITEN     : Boolean               := False;  -- Wait feature enable
			PBKEN       : Boolean               := False;  -- Memory bank enable
			PTYP        : Memory_Type           := NAND_Flash; -- Memory type
			PWID        : Memory_Width          := Memory_16_Bits; -- Memory width
			ECCEN       : Boolean               := False; -- ECC computation logic enable bit
			Reserved_7  : Integer range 0 .. 3  := 2#00#;
			TCLR        : Integer range 0 .. 15 := 0; -- CLE to RE delay minus 1
			TAR         : Integer range 0 .. 15 := 0; -- ALE to RE delay minus 1
			ECCPS       : ECC_Page_Size         := ECC_Page_256_Bytes; -- ECC page size for the extended ECC
			Reserved_20 : Integer range 0 .. 2**12 - 1 := 0;
		end record;
		for Control_Register use record
			Reserved_0  at 0 range  0 ..  0;
			PWAITEN     at 0 range  1 ..  1;
			PBKEN       at 0 range  2 ..  2;
			PTYP        at 0 range  3 ..  3;
			PWID        at 0 range  4 ..  5;
			ECCEN       at 0 range  6 ..  6;
			Reserved_7  at 0 range  7 ..  8;
			TCLR        at 0 range  9 .. 12;
			TAR         at 0 range 13 .. 16;
			ECCPS       at 0 range 17 .. 19;
			Reserved_20 at 0 range 20 .. 31;
		end record;

		-- PSR
		type FIFO_Status_and_Interrupt_Register is record
			IRS         : Boolean;
			ILS         : Boolean;
			IFS         : Boolean;
			IREN        : Boolean;
			ILEN        : Boolean;
			IFEN        : Boolean;
			FEMPT       : Boolean;
			Reserved    : Integer range 0 .. 2**25 - 1;
		end record with Size => 32;
		for FIFO_Status_and_Interrupt_Register use record
			IRS         at 0 range 0 .. 0;
			ILS         at 0 range 1 .. 1;
			IFS         at 0 range 2 .. 2;
			IREN        at 0 range 3 .. 3;
			ILEN        at 0 range 4 .. 4;
			IFEN        at 0 range 5 .. 5;
			FEMPT       at 0 range 6 .. 6;
			Reserved    at 0 range 7 .. 31;
		end record;

		type Timing_Register is record
			SET      : Integer range 0 .. 254; -- Setup time minus 1 in HCLK cycles
			WAIT     : Integer range 1 .. 254; -- Wait time minus 1 in HCLK cycles
			HOLD     : Integer range 1 .. 254; -- Hold time (minus 2 for read access) in HCLK cycles
			HIZ      : Integer range 0 .. 254; -- Data bus Hi-Z time minus 1 in HCLK cycles
		end record with Size => 32;
		for Timing_Register use record
			SET      at 0 range  0 ..  7;
			WAIT     at 0 range  8 .. 15;
			HOLD     at 0 range 16 .. 23;
			HIZ      at 0 range 24 .. 31;
		end record;
	end PC_Card;

	package SDRAM is

		-- SDCR

		type Column_Address_Bits is (
			Column_8_Bits,
			Column_9_Bits,
			Column_10_Bits,
			Column_11_Bits
		) with Size => 2;
		for Column_Address_Bits use (
			Column_8_Bits  => 2#00#,
			Column_9_Bits  => 2#01#,
			Column_10_Bits => 2#10#,
			Column_11_Bits => 2#11#
		);

		type Row_Address_Bits is (
			Row_11_Bits,
			Row_12_Bits,
			Row_13_Bits
		) with Size => 2;
		for Row_Address_Bits use (
			Row_11_Bits => 2#00#,
			Row_12_Bits => 2#01#,
			Row_13_Bits => 2#10#
		);

		type Internal_Banks is (
			Two_Banks,
			Four_Banks
		) with Size => 1;
		for Internal_Banks use (
			Two_Banks  => 0,
			Four_Banks => 1
		);

		type Clock_Configuration is (
			Clock_Disabled,
			Period_2_HCLK,
			Period_3_HCLK
		) with Size => 2;
		for Clock_Configuration use (
			Clock_Disabled => 2#00#,
			Period_2_HCLK  => 2#10#,
			Period_3_HCLK  => 2#11#
		);

		type Control_Register is record
			NC       : Column_Address_Bits;
			NR       : Row_Address_Bits;
			MWID     : Memory_Width;
			NB       : Internal_Banks;
			CAS      : Integer range 1 .. 3;
			WP       : Boolean;
			SDCLK    : Clock_Configuration;
			RBURST   : Boolean;
			RPIPE    : Integer range 0 .. 2;
			Reserved : Integer range 0 .. 2**17 - 1;
		end record with Size => 32;
		for Control_Register use record
			NC       at 0 range  0 ..  1;
			NR       at 0 range  2 ..  3;
			MWID     at 0 range  4 ..  5;
			NB       at 0 range  6 ..  6;
			CAS      at 0 range  7 ..  8;
			WP       at 0 range  9 ..  9;
			SDCLK    at 0 range 10 .. 11;
			RBURST   at 0 range 12 .. 12;
			RPIPE    at 0 range 13 .. 14;
			Reserved at 0 range 15 .. 31;
		end record;

		-- SDTR

		type Timing_Register is record
			TMRD     : Integer range 0 .. 15; -- Load mode register to active minus 1
			TXSR     : Integer range 0 .. 15; -- Exit self-refresh delay minus 1
			TRAS     : Integer range 0 .. 15; -- Self refresh time minus 1
			TRC      : Integer range 0 .. 15; -- Row cycle delay minus 1
			TWR      : Integer range 0 .. 15; -- Recovery delay minus 1
			TRP      : Integer range 0 .. 15; -- Row precharge delay minus 1
			TRCD     : Integer range 0 .. 15; -- Row to column delay minus 1
			Reserved : Integer range 0 .. 15;
		end record with Size => 32;
		for Timing_Register use record
			TMRD     at 0 range  0 ..  3;
			TXSR     at 0 range  4 ..  7;
			TRAS     at 0 range  8 .. 11;
			TRC      at 0 range 12 .. 15;
			TWR      at 0 range 16 .. 19;
			TRP      at 0 range 20 .. 23;
			TRCD     at 0 range 24 .. 27;
			Reserved at 0 range 28 .. 31;
		end record;

		-- CMR

		type Command_Mode is (
			Normal_Mode,
			Clock_Configuration_Enable,
			PALL,
			Auto_Refresh,
			Load_Mode_Register,
			Self_Refresh,
			Power_Down
		) with Size => 3;
		for Command_Mode use (
			Normal_Mode                => 2#000#,
			Clock_Configuration_Enable => 2#001#,
			PALL                       => 2#010#,
			Auto_Refresh               => 2#011#,
			Load_Mode_Register         => 2#100#,
			Self_Refresh               => 2#101#,
			Power_Down                 => 2#110#
		);

		type Command_Mode_Register is record
			MODE     : Command_Mode := Normal_Mode; -- Command mode
			CTB2     : Boolean := False; -- Command target bank 2
			CTB1     : Boolean := False; -- Command target bank 1
			NRFS     : Integer range 0 .. 14 := 0; -- Number of refresh cycles minus 1
			MRD      : Integer range 0 .. 2**12 - 1 := 0;
			Reserved : Integer range 0 .. 2**10 - 1 := 0;
		end record with Size => 32;
		for Command_Mode_Register use record
			MODE     at 0 range  0 ..  2;
			CTB2     at 0 range  3 ..  3;
			CTB1     at 0 range  4 ..  4;
			NRFS     at 0 range  5 ..  8;
			MRD      at 0 range  9 .. 21;
			Reserved at 0 range 22 .. 31;
		end record;

		-- SDRTR

		type Refresh_Timer_Register is record
			CRE      : Boolean;
			COUNT    : Integer range 41 .. 2**13 - 1;
			REIE     : Boolean;
			Reserved : Integer range 0 .. 2**17 - 1;
		end record with Size => 32;
		for Refresh_Timer_Register use record
			CRE      at 0 range  0 ..  0;
			COUNT    at 0 range  1 .. 13;
			REIE     at 0 range 14 .. 14;
			Reserved at 0 range 15 .. 31;
		end record;

		-- SDSR

		type Status_Mode is (
			Normal_Mode,
			Self_Refresh,
			Power_Down
		) with Size => 2;
		for Status_Mode use (
			Normal_Mode  => 2#00#,
			Self_Refresh => 2#01#,
			Power_Down   => 2#10#
		);

		type Status_Register is record
			RE       : Boolean;
			MODES1   : Status_Mode;
			MODES2   : Status_Mode;
			BUSY     : Boolean;
			Reserved : Integer range 0 .. 2**26 - 1;
		end record with Size => 32;
		for Status_Register use record
			RE       at 0 range  0 ..  0;
			MODES1   at 0 range  1 ..  2;
			MODES2   at 0 range  3 ..  4;
			BUSY     at 0 range  5 ..  5;
			Reserved at 0 range  6 .. 31;
		end record;

	end SDRAM;

	type FMC_Registers is record
		BCR1  : SRAM.Chip_Select_Control_Register;
		BCR2  : SRAM.Chip_Select_Control_Register;
		BCR3  : SRAM.Chip_Select_Control_Register;
		BCR4  : SRAM.Chip_Select_Control_Register;
		BTR1  : SRAM.Chip_Select_Timing_Register;
		BTR2  : SRAM.Chip_Select_Timing_Register;
		BTR3  : SRAM.Chip_Select_Timing_Register;
		BTR4  : SRAM.Chip_Select_Timing_Register;
		BWTR1 : SRAM.Chip_Select_Timing_Register;
		BWTR2 : SRAM.Chip_Select_Timing_Register;
		BWTR3 : SRAM.Chip_Select_Timing_Register;
		BWTR4 : SRAM.Chip_Select_Timing_Register;
		PCR2  : PC_Card.Control_Register;
		PCR3  : PC_Card.Control_Register;
		PCR4  : PC_Card.Control_Register;
		SR2   : PC_Card.FIFO_Status_and_Interrupt_Register;
		SR3   : PC_Card.FIFO_Status_and_Interrupt_Register;
		SR4   : PC_Card.FIFO_Status_and_Interrupt_Register;
		PMEM2 : PC_Card.Timing_Register;
		PMEM3 : PC_Card.Timing_Register;
		PMEM4 : PC_Card.Timing_Register;
		PATT2 : PC_Card.Timing_Register;
		PATT3 : PC_Card.Timing_Register;
		PATT4 : PC_Card.Timing_Register;
		PIO4  : PC_Card.Timing_Register;
		ECCR2 : Unsigned_32;
		ECCR3 : Unsigned_32;
		-- Registers below are not available in FSMC
		SDCR1 : SDRAM.Control_Register;
		SDCR2 : SDRAM.Control_Register;
		SDTR1 : SDRAM.Timing_Register;
		SDTR2 : SDRAM.Timing_Register;
		SDCMR : SDRAM.Command_Mode_Register;
		SDRTR : SDRAM.Refresh_Timer_Register;
		SDSR  : SDRAM.Status_Register;
	end record with Volatile;
	for FMC_Registers use record
		BCR1  at 16#0000# range 0 .. 31;
		BCR2  at 16#0008# range 0 .. 31;
		BCR3  at 16#0010# range 0 .. 31;
		BCR4  at 16#0018# range 0 .. 31;
		BTR1  at 16#0004# range 0 .. 31;
		BTR2  at 16#000C# range 0 .. 31;
		BTR3  at 16#0014# range 0 .. 31;
		BTR4  at 16#001C# range 0 .. 31;
		BWTR1 at 16#0104# range 0 .. 31;
		BWTR2 at 16#010C# range 0 .. 31;
		BWTR3 at 16#0114# range 0 .. 31;
		BWTR4 at 16#011C# range 0 .. 31;
		PCR2  at 16#0060# range 0 .. 31;
		PCR3  at 16#0080# range 0 .. 31;
		PCR4  at 16#00A0# range 0 .. 31;
		SR2   at 16#0064# range 0 .. 31;
		SR3   at 16#0084# range 0 .. 31;
		SR4   at 16#00A4# range 0 .. 31;
		PMEM2 at 16#0068# range 0 .. 31;
		PMEM3 at 16#0088# range 0 .. 31;
		PMEM4 at 16#00A8# range 0 .. 31;
		PATT2 at 16#006C# range 0 .. 31;
		PATT3 at 16#008C# range 0 .. 31;
		PATT4 at 16#00AC# range 0 .. 31;
		PIO4  at 16#00B0# range 0 .. 31;
		ECCR2 at 16#0074# range 0 .. 31;
		ECCR3 at 16#0094# range 0 .. 31;
		SDCR1 at 16#0140# range 0 .. 31;
		SDCR2 at 16#0144# range 0 .. 31;
		SDTR1 at 16#0148# range 0 .. 31;
		SDTR2 at 16#014C# range 0 .. 31;
		SDCMR at 16#0150# range 0 .. 31;
		SDRTR at 16#0154# range 0 .. 31;
		SDSR  at 16#0158# range 0 .. 31;
	end record;

	FMC : FMC_Registers with Volatile, Import, Address => Address_Map.FMC_FSMC;

	FSMC : FMC_Registers renames FMC;

end STM32.Flexible_Memory_Controller;
