-- OTG USB Device mode

package STM32.USB_OTG.Device is

	-- Registers renamed
	-- Orig. register  Register name here
	--
	-- DIEPCTLx        DIEP (x).CTL
	-- DIEPINTx        DIEP (x).INT
	-- DIEPTSIZx       DIEP (x).TSIZ
	-- DIEPDMAx        DIEP (x).DMA
	-- DTXFSTSx        DIEP (x).TXFSTS
	-- DOEPCTLx        DOEP (x).CTL
	-- DOEPINTx        DOEP (x).INT
	-- DOEPTSIZx       DOEP (x).TSIZ
	--
	-- Fields renamed
	-- Register      Orig. field  Field name here
	--
	-- DAINT         IEPINT       IEP (array)
	-- DAINT         OEPINT       OEP (array)
	-- DAINTMSK      IEPM         IEP (array)
	-- DAINTMSK      OEPM         OEP (array)
	-- DEACHINT      IEP1INT      IEP (1)
	-- DEACHINT      OEP1INT      OEP (1)
	-- DEACHINTMSK   IEP1INTM     IEP (1)
	-- DEACHINTMSK   OEP1INTM     OEP (1)
	-- DIEPMSK       *            corresponding fields in DIEPINT
	-- DIEPEACKMSKx  *            corresponding fields in DIEPINT
	-- DOEPMSK       *            corresponding fields in DOEPINT
	-- DOEPEACHMSKx  *            corresponding fields in DOEPINT
	-- DAINTMSK      IEPM         IEP
	-- DAINTMSK      OEPM         OEP
	-- DIEPEMPMSK    INEPTXFEM    IEP
	-- DSTS          DEVLNSTS     DEVLNSTS_DP, DEVLNSTS_DM

	-- DCFG

	type Device_Speed is (
		High_Speed,
		Full_Speed_using_HS,
		Full_Speed_using_FS)
	with Size => 2;
	for Device_Speed use (
		High_Speed          => 2#00#,
		Full_Speed_using_HS => 2#01#,
		Full_Speed_using_FS => 2#11#);

	type Periodic_Frame_Interval is (
		Frame_80_Percent,
		Frame_85_Percent,
		Frame_90_Percent,
		Frame_95_Percent)
	with Size => 2;

	type Periodic_Schedule_Interval is (
		Schedule_25_Percent,
		Schedule_50_Percent,
		Schedule_75_Percent)
	with Size => 2;
	for Periodic_Schedule_Interval use (
		Schedule_25_Percent => 2#00#,
		Schedule_50_Percent => 2#01#,
		Schedule_75_Percent => 3#10#);

	type Device_Configuration_Register is record
		DSPD      : Device_Speed  := High_Speed; -- Device speed
		NZLSOHSK  : Boolean       := False; -- Nonzero-length status OUT handshake send received OUT packet
		Unused_3  : Unused_1_Bit  := 0;
		DAD       : Device_Address          := 0; -- Device address
		PFIVL     : Periodic_Frame_Interval := Frame_80_Percent; -- Periodic (micro)frame interval
		Unused_13 : Unused_1_Bit  := 0;
		XCVRDLY   : Boolean       := False; -- Transceiver delay enable (HS only)
		ERRATIM   : Boolean       := False; -- Erratic error interrupt mask
		Unused_16 : Unused_8_Bits := 16#02#;
		PERSCHIVL : Periodic_Schedule_Interval := Schedule_75_Percent; -- Periodic schedule interval (HS only)
		Unused_26 : Unused_6_Bits := 0;
	end record with Size => 32;
	for Device_Configuration_Register use record
		DSPD      at 0 range  0 ..  1;
		NZLSOHSK  at 0 range  2 ..  2;
		Unused_3  at 0 range  3 ..  3;
		DAD       at 0 range  4 .. 10;
		PFIVL     at 0 range 11 .. 12;
		Unused_13 at 0 range 13 .. 13;
		XCVRDLY   at 0 range 14 .. 14;
		ERRATIM   at 0 range 15 .. 15;
		Unused_16 at 0 range 16 .. 23;
		PERSCHIVL at 0 range 24 .. 25;
		Unused_26 at 0 range 26 .. 31;
	end record;

	-- DCTL

	type Device_Control_Register is record
		RWUSIG      : Boolean        := False; -- Remote wakeup signaling
		SDIS        : Boolean        := False; -- Soft disconnect
		GINSTS      : Boolean        := False; -- Global IN NAK status
		GONSTS      : Boolean        := False; -- Global OUT NAK status
		TCTL        : Test_Mode      := No_Test; -- Test control
		SGINAK      : Boolean        := False; -- Set global IN NAK
		CGINAK      : Boolean        := False; -- Clear global IN NAK
		SGONAK      : Boolean        := False; -- Set global OUT NAK
		CGONAK      : Boolean        := False; -- Clear global OUT NAK
		POPRGDNE    : Boolean        := False; -- Power-on programming done
		Unused_12   : Unused_6_Bits  := 0;
		DSBESLRJCT  : Boolean        := False; -- Deep sleep BESL reject
		Unused_19   : Unused_13_Bits := 0;
	end record with Size => 32;
	for Device_Control_Register use record
		RWUSIG      at 0 range  0 ..  0;
		SDIS        at 0 range  1 ..  1;
		GINSTS      at 0 range  2 ..  2;
		GONSTS      at 0 range  3 ..  3;
		TCTL        at 0 range  4 ..  6;
		SGINAK      at 0 range  7 ..  7;
		CGINAK      at 0 range  8 ..  8;
		SGONAK      at 0 range  9 ..  9;
		CGONAK      at 0 range 10 .. 10;
		POPRGDNE    at 0 range 11 .. 11;
		Unused_12   at 0 range 12 .. 17;
		DSBESLRJCT  at 0 range 18 .. 18;
		Unused_19   at 0 range 19 .. 31;
	end record;

	-- DSTS

	type Device_Status_Register is record
		SUSPSTS : Boolean; -- Suspend status
		ENUMSPD     : Device_Speed;                 -- Enumerated speed
		EERR        : Boolean;                      -- Erratic error
		Unused_4    : Unused_4_Bits;
		FNSOF       : Integer range 0 .. 2**14 - 1; -- Frame number of the received SOF
		DEVLNSTS_DM : Integer range 0 .. 1;         -- Device line status - D-
		DEVLNSTS_DP : Integer range 0 .. 1;         -- Device line status - D+
		Unused_24   : Unused_8_Bits;
	end record with Size => 32;
	for Device_Status_Register use record
		SUSPSTS     at 0 range  0 ..  0;
		ENUMSPD     at 0 range  1 ..  2;
		EERR        at 0 range  3 ..  3;
		Unused_4    at 0 range  4 ..  7;
		FNSOF       at 0 range  8 .. 21;
		DEVLNSTS_DM at 0 range 22 .. 22;
		DEVLNSTS_DP at 0 range 23 .. 23;
		Unused_24   at 0 range 24 .. 31;
	end record;

	-- DIEPMSK, DIEPEACKMSKx, DIEPINTx, DIEPEACHMSK1

	type Device_In_Endpoint_Interrupt_Register is record
		XFRC        : Boolean        := False; -- Transfer completed interrupt
		EPDISD      : Boolean        := False; -- Endpoint disabled interrupt
		AHBERR      : Boolean        := False; -- AHB error (HS only)
		TOC         : Boolean        := False; -- Timeout condition (non-isochronous endpoints)
		ITTXFE      : Boolean        := False; -- IN token received when TxFIFO empty
		INEPNM      : Boolean        := False; -- IN token received with EP mismatch
		INEPNE      : Boolean        := False; -- IN endpoint NAK effective
		TXFE        : Boolean        := False; -- Transmit FIFO Empty (Half or completely, see TXFELVL)
		TXFIFOUDRN  : Boolean        := False; -- FIFO underrun
		BI          : Boolean        := False; -- BNA interrupt (?)
		Unused_10   : Unused_1_Bit   := 0;
		PKTDRPSTS   : Boolean        := False; -- Packet dropped status (not interrupt, not masked)
		Unused_12   : Unused_1_Bit   := 0;
		NAK         : Boolean        := False; -- NAK interrupt
		Unused_14   : Unused_18_Bits := 0;
	end record with Size => 32;
	for Device_In_Endpoint_Interrupt_Register use record
		XFRC        at 0 range  0 ..  0;
		EPDISD      at 0 range  1 ..  1;
		AHBERR      at 0 range  2 ..  2;
		TOC         at 0 range  3 ..  3;
		ITTXFE      at 0 range  4 ..  4;
		INEPNM      at 0 range  5 ..  5;
		INEPNE      at 0 range  6 ..  6;
		TXFE        at 0 range  7 ..  7;
		TXFIFOUDRN  at 0 range  8 ..  8;
		BI          at 0 range  9 ..  9;
		Unused_10   at 0 range 10 .. 10;
		PKTDRPSTS   at 0 range 11 .. 11;
		Unused_12   at 0 range 12 .. 12;
		NAK         at 0 range 13 .. 13;
		Unused_14   at 0 range 14 .. 31;
	end record;

	-- DOEPMSK, DOEPINT, DOEPEACHMSK1

	type Device_Out_Endpoint_Interrupt_Register is record
		XFRC        : Boolean        := False; -- Transfer completed interrupt
		EPDISD      : Boolean        := False; -- Endpoint disabled interrupt
		AHBERR      : Boolean        := False; -- AHB error (HS only)
		STUP        : Boolean        := False; -- SETUP phase done
		OTEPDIS     : Boolean        := False; -- OUT token received when endpoint disabled
		STSPHSRX    : Boolean        := False; -- Status phase received for control write
		B2BSTUP     : Boolean        := False; -- Back-to-back SETUP packets received (HS only)
		Unused_7    : Unused_1_Bit   := 0;
		OUTPKTERR   : Boolean        := False; -- OUT packet error mask
		BOI         : Boolean        := False; -- BNA interrupt (?)
		Unused_10   : Unused_2_Bits  := 0;
		BERR        : Boolean        := False; -- Babble error
		NAK         : Boolean        := False; -- NAK interrupt
		NYET        : Boolean        := False; -- NYET interrupt (HS only)
		STPKTRX     : Boolean        := False; -- Setup packet received (HS only, not maskable?)
		Unused_16   : Unused_16_Bits := 0;
	end record with Size => 32;
	for Device_Out_Endpoint_Interrupt_Register use record
		XFRC        at 0 range  0 ..  0;
		EPDISD      at 0 range  1 ..  1;
		AHBERR      at 0 range  2 ..  2;
		STUP        at 0 range  3 ..  3;
		OTEPDIS     at 0 range  4 ..  4;
		STSPHSRX    at 0 range  5 ..  5;
		B2BSTUP     at 0 range  6 ..  6;
		Unused_7    at 0 range  7 ..  7;
		OUTPKTERR   at 0 range  8 ..  8;
		BOI         at 0 range  9 ..  9;
		Unused_10   at 0 range 10 .. 11;
		BERR        at 0 range 12 .. 12;
		NAK         at 0 range 13 .. 13;
		NYET        at 0 range 14 .. 14;
		STPKTRX     at 0 range 15 .. 15;
		Unused_16   at 0 range 16 .. 31;
	end record;

	-- DAINT, DAINTMSK, DIEPEMPMSK

	type Device_Endpoint_Set_Register is record
		IEP  : Channel_Set; -- IN endpoints
		OEP  : Channel_Set; -- OUT endpoints
	end record with Size => 32;
	for Device_Endpoint_Set_Register use record
		IEP  at 0 range  0 .. 15;
		OEP  at 0 range 16 .. 31;
	end record;

	-- note 1: DIEPEMPMSK have only INEPTXFEM renamed to IEP

	-- DVBUSDIS, DVBPULSE

	type Device_SRP_VBus_Time_Register is new Integer range 0 .. 2**16 - 1;

	-- DTHRCTL

	type Device_Threshold_Control_Register is record
		NONISOTHREN : Boolean       := False; -- Nonisochronous IN endpoints th
		ISOTHREN    : Boolean       := False; -- ISO IN endpoint threshold
		TXTHRLEN    : Integer range 0 .. 2**9 - 1 := 0; -- Transmit threshold length
		Unused_11   : Unused_5_Bits := 0;
		RXTHREN     : Boolean       := False; -- Receive threshold enable
		RXTHRLEN    : Integer range 0 .. 2**9 - 1 := 0; -- Receive threshold length
		Unused_26   : Unused_1_Bit  := 0;
		ARPEN       : Boolean       := False; -- Arbiter parking enable
		Unused_28   : Unused_4_Bits := 0;
	end record with Size => 32;
	for Device_Threshold_Control_Register use record
		NONISOTHREN at 0 range  0 ..  0;
		ISOTHREN    at 0 range  1 ..  1;
		TXTHRLEN    at 0 range  2 .. 10;
		Unused_11   at 0 range 11 .. 15;
		RXTHREN     at 0 range 16 .. 16;
		RXTHRLEN    at 0 range 17 .. 25;
		Unused_26   at 0 range 26 .. 26;
		ARPEN       at 0 range 27 .. 27;
		Unused_28   at 0 range 28 .. 31;
	end record;

	-- DIEPCTLx

	subtype Endpoint_0_Maximum_Packet_Size is Packet_Byte_Count range 0 .. 3;
	Max_64_Bytes : constant Endpoint_0_Maximum_Packet_Size := 0;
	Max_32_Bytes : constant Endpoint_0_Maximum_Packet_Size := 1;
	Max_16_Bytes : constant Endpoint_0_Maximum_Packet_Size := 2;
	Max_8_Bytes  : constant Endpoint_0_Maximum_Packet_Size := 3;

	type Device_In_Endpoint_Control_Register is record
		MPSIZ          : Packet_Byte_Count := 0;     -- Maximum packet size (Special meaning in EP 0!)
		Unused_11      : Unused_4_Bits     := 0;
		USBAEP         : Boolean           := False; -- USB active endpoint
		EONUM_DPID     : Integer range 0 .. 1 := 0;  -- Even/odd frame / Endpoint data PID (not in EP 0)
		NAKSTS         : Boolean           := False; -- NAK status
		EPTYP          : Endpoint_Type     := Control_Endpoint; -- Endpoint type (should be Control in EP 0)
		Unused_20      : Unused_1_Bit      := 0;
		STALL          : Boolean           := False; -- STALL handshake
		TXFNUM         : Channel_Number    := 0;     -- TxFIFO number
		CNAK           : Boolean           := False; -- Clear NAK
		SNAK           : Boolean           := False; -- Set NAK
		SD0PID_SEVNFRM : Boolean           := False; -- Set DATA0 PID / set even frame (not in EP 0)
		SODDFRM        : Boolean           := False; -- Set odd frame (not in EP 0)
		EPDIS          : Boolean           := False; -- Endpoint disable
		EPENA          : Boolean           := False; -- Endpoint enable
	end record with Size => 32;
	for Device_In_Endpoint_Control_Register use record
		MPSIZ          at 0 range  0 .. 10;
		Unused_11      at 0 range 11 .. 14;
		USBAEP         at 0 range 15 .. 15;
		EONUM_DPID     at 0 range 16 .. 16;
		NAKSTS         at 0 range 17 .. 17;
		EPTYP          at 0 range 18 .. 19;
		Unused_20      at 0 range 20 .. 20;
		STALL          at 0 range 21 .. 21;
		TXFNUM         at 0 range 22 .. 25;
		CNAK           at 0 range 26 .. 26;
		SNAK           at 0 range 27 .. 27;
		SD0PID_SEVNFRM at 0 range 28 .. 28;
		SODDFRM        at 0 range 29 .. 29;
		EPDIS          at 0 range 30 .. 30;
		EPENA          at 0 range 31 .. 31;
	end record;

	-- DIEPTSIZx

	subtype Transfer_Byte_Count is Storage_Count range 0 .. 2**11 - 1;
	subtype Endpoint_0_Transfer_Byte_Count is Transfer_Byte_Count range 0 .. 2**7 - 1;

	subtype Packet_Count is Integer range 0 .. 2**10 - 1;
	subtype Endpoint_0_Packet_Count is Packet_Count range 0 .. 3;

	subtype Multi_Count is Integer range 0 .. 3;

	type Device_In_Endpoint_Transfer_Size_Register is record
		XFRSIZ    : Transfer_Byte_Count := 0; -- Transfer size (limited range for EP 0)
		PKTCNT    : Packet_Count        := 0; -- Packet count (limited range for EP 0)
		MCNT      : Multi_Count         := 0; -- Multi count (not in EP 0)
		Unused_31 : Unused_1_Bit        := 0;
	end record with Size => 32;
	for Device_In_Endpoint_Transfer_Size_Register use record
		XFRSIZ    at 0 range 0 .. 18;
		PKTCNT    at 0 range 19 .. 28;
		MCNT      at 0 range 29 .. 30;
		Unused_31 at 0 range 31 .. 31;
	end record;

	-- DTXFSTSx

	type Device_In_Endpoint_FIFO_Status_Register is record
		INEPTFSAV : Integer range 0 .. 2**16 - 1; -- IN endpoint TxFIFO space
		Unused_16 : Unused_16_Bits;
	end record with Size => 32;
	for Device_In_Endpoint_FIFO_Status_Register use record
		INEPTFSAV at 0 range 0 .. 15;
		Unused_16 at 0 range 16 .. 31;
	end record;

	--

	type Device_In_Endpoint_Registers is record
		CTL   : Device_In_Endpoint_Control_Register;
		INT   : Device_In_Endpoint_Interrupt_Register;
		TSIZ  : Device_In_Endpoint_Transfer_Size_Register;
		DMA   : Address_Register;
		TXFSTS  : Device_In_Endpoint_FIFO_Status_Register;
	end record with Size => 8 * 32;
	for Device_In_Endpoint_Registers use record
		CTL     at 16#00# range 0 .. 31;
		INT     at 16#08# range 0 .. 31;
		TSIZ    at 16#10# range 0 .. 31;
		DMA     at 16#14# range 0 .. 31;
		TXFSTS  at 16#18# range 0 .. 31;
	end record;

	type Device_In_Endpoint_Register_Array is array (0 .. 7) of Device_In_Endpoint_Registers;

	-- DOEPCTLx

	type Device_Out_Endpoint_Control_Register is record
		MPSIZ          : Packet_Byte_Count := 0;     -- Maximum packet size (Special meaning in EP 0!)
		Unused_11      : Unused_4_Bits     := 0;
		USBAEP         : Boolean           := False; -- USB active endpoint
		EONUM_DPID     : Integer range 0 .. 1 := 0;  -- Even/odd frame / Endpoint data PID (not in EP 0)
		NAKSTS         : Boolean           := False; -- NAK status
		EPTYP          : Endpoint_Type     := Control_Endpoint; -- Endpoint type (should be Control in EP 0)
		SNPM           : Boolean           := False; -- Snoop mode
		STALL          : Boolean           := False; -- STALL handshake
		Unused_22      : Unused_4_Bits     := 0;
		CNAK           : Boolean           := False; -- Clear NAK
		SNAK           : Boolean           := False; -- Set NAK
		SD0PID_SEVNFRM : Boolean           := False; -- Set DATA0 PID / set even frame (not in EP 0)
		SD1PID_SODDFRM : Boolean           := False; -- Set DATA1 PID / Set odd frame (not in EP 0)
		EPDIS          : Boolean           := False; -- Endpoint disable
		EPENA          : Boolean           := False; -- Endpoint enable
	end record with Size => 32;
	for Device_Out_Endpoint_Control_Register use record
		MPSIZ          at 0 range  0 .. 10;
		Unused_11      at 0 range 11 .. 14;
		USBAEP         at 0 range 15 .. 15;
		EONUM_DPID     at 0 range 16 .. 16;
		NAKSTS         at 0 range 17 .. 17;
		EPTYP          at 0 range 18 .. 19;
		SNPM           at 0 range 20 .. 20;
		STALL          at 0 range 21 .. 21;
		Unused_22      at 0 range 22 .. 25;
		CNAK           at 0 range 26 .. 26;
		SNAK           at 0 range 27 .. 27;
		SD0PID_SEVNFRM at 0 range 28 .. 28;
		SD1PID_SODDFRM at 0 range 29 .. 29;
		EPDIS          at 0 range 30 .. 30;
		EPENA          at 0 range 31 .. 31;
	end record;

	-- DOEPTSIZx

	type Device_Out_Endpoint_Transfer_Size_Register is record
		XFRSIZ         : Transfer_Byte_Count := 0; -- Transfer size (limited range for EP 0)
		PKTCNT         : Packet_Count        := 0; -- Packet count (limited range for EP 0)
		RXDPID_STUPCNT : Multi_Count         := 0; -- Received data PID (not in EP 0) / SETUP packet count
		Unused_31      : Unused_1_Bit        := 0;
	end record with Size => 32;
	for Device_Out_Endpoint_Transfer_Size_Register use record
		XFRSIZ         at 0 range  0 .. 18;
		PKTCNT         at 0 range 19 .. 28;
		RXDPID_STUPCNT at 0 range 29 .. 30;
		Unused_31      at 0 range 31 .. 31;
	end record;

	--

	type Device_Out_Endpoint_Registers is record
		CTL  : Device_Out_Endpoint_Control_Register;
		INT  : Device_Out_Endpoint_Interrupt_Register;
		TSIZ : Device_Out_Endpoint_Transfer_Size_Register;
	end record with Size => 8 * 32;
	for Device_Out_Endpoint_Registers use record
		CTL  at 16#00# range 0 .. 31;
		INT  at 16#08# range 0 .. 31;
		TSIZ at 16#10# range 0 .. 31;
	end record;

	type Device_Out_Endpoint_Register_Array is array (0 .. 7) of Device_Out_Endpoint_Registers;

	--

	type DEVICE_Registers is record
		DCFG         : Device_Configuration_Register;
		DCTL         : Device_Control_Register;
		DSTS         : Device_Status_Register;
		DIEPMSK      : Device_In_Endpoint_Interrupt_Register;
		DOEPMSK      : Device_Out_Endpoint_Interrupt_Register;
		DAINT        : Device_Endpoint_Set_Register;
		DAINTMSK     : Device_Endpoint_Set_Register;
		DVBUSDIS     : Device_SRP_VBus_Time_Register;
		DVBUSPULSE   : Device_SRP_VBus_Time_Register;
		DTHRCTL      : Device_Threshold_Control_Register;
		DIEPEMPMSK   : Device_Endpoint_Set_Register;
		DEACHINT     : Device_Endpoint_Set_Register; -- Only channel 1 is there.
		DEACHINTMSK  : Device_Endpoint_Set_Register; -- Only channel 1 is there.
		DIEPEACHMSK1 : Device_In_Endpoint_Interrupt_Register;
		DOEPEACHMSK1 : Device_Out_Endpoint_Interrupt_Register;
		DIEP         : Device_In_Endpoint_Register_Array;
		DOEP         : Device_Out_Endpoint_Register_Array;
	end record;
	for DEVICE_Registers use record
		DCFG         at 16#00# range 0 .. 31;
		DCTL         at 16#04# range 0 .. 31;
		DSTS         at 16#08# range 0 .. 31;
		DIEPMSK      at 16#10# range 0 .. 31;
		DOEPMSK      at 16#14# range 0 .. 31;
		DAINT        at 16#18# range 0 .. 31;
		DAINTMSK     at 16#1C# range 0 .. 31;
		DVBUSDIS     at 16#28# range 0 .. 31;
		DVBUSPULSE   at 16#2C# range 0 .. 31;
		DTHRCTL      at 16#30# range 0 .. 31;
		DIEPEMPMSK   at 16#34# range 0 .. 31;
		DEACHINT     at 16#38# range 0 .. 31;
		DEACHINTMSK  at 16#3C# range 0 .. 31;
		DIEPEACHMSK1 at 16#44# range 0 .. 31;
		DOEPEACHMSK1 at 16#84# range 0 .. 31;
		DIEP         at 16#100# range 0 .. 8 * 8 * 32 - 1;
		DOEP         at 16#300# range 0 .. 8 * 8 * 32 - 1;
	end record;


end STM32.USB_OTG.Device;
