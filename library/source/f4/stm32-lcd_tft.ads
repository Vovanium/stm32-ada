with STM32.Address_Map;

with STM32.Graphics;
use  STM32.Graphics;

-- LCD TFT Controller

package STM32.LCD_TFT is

	type Frame_Configuration_Register is record
		VH : Integer range 0 .. 2**11 - 1;
		-- Vertical Height minus 1
		-- in units of scan line
		HW : Integer range 0 .. 2**12 - 1;
		-- Horizontal Width minus 1
		-- in pixel clock periods
	end record with Size => 32;
	for Frame_Configuration_Register use record
		VH at 0 range 0 .. 15;
		HW at 0 range 16 .. 31;
	end record;
	-- Same record used fo sync, back porch, active width and total Width
	-- configuration registers

	type Clock_Polarity is (
		True_Polarity, -- Input pixel clock
		Inverted_Polarity -- Inverted pixel clock
	) with Size => 1;
	for Clock_Polarity use (
		True_Polarity     => 2#0#,
		Inverted_Polarity => 2#1#
	);

	type Signal_Polarity is (
		Active_Low,
		Active_High
	) with Size => 1;
	for Signal_Polarity use (
		Active_Low  => 2#0#,
		Active_High => 2#1#
	);

	type Global_Control_Register is record
		LTDCEN      : Boolean;
		Reserved_1  : Integer range 0 .. 7;
		DBW         : Integer range 0 .. 7;
		DGW         : Integer range 0 .. 7;
		DRW         : Integer range 0 .. 7;
		DEN         : Boolean;
		Reserved_17 : Integer range 0 .. 2**11 - 1;
		PCPOL       : Clock_Polarity;
		DEPOL       : Signal_Polarity;
		VSPOL       : Signal_Polarity;
		HSPOL       : Signal_Polarity;
	end record with Size => 32;
	for Global_Control_Register use record
		LTDCEN      at 0 range  0 ..  0;
		Reserved_1  at 0 range  1 ..  3;
		DBW         at 0 range  4 ..  7;
		DGW         at 0 range  8 .. 11;
		DRW         at 0 range 12 .. 15;
		DEN         at 0 range 16 .. 16;
		Reserved_17 at 0 range 17 .. 27;
		PCPOL       at 0 range 28 .. 28;
		DEPOL       at 0 range 29 .. 29;
		VSPOL       at 0 range 30 .. 30;
		HSPOL       at 0 range 31 .. 31;
	end record;

	type Shadow_Reload_Configuration_Register is record
		IMR         : Boolean := False;
		VBR         : Boolean := False;
		Reserved    : Integer range 0 .. 2**30 - 1 := 0;
	end record with Size => 32;
	for Shadow_Reload_Configuration_Register use record
		IMR         at 0 range  0 ..  0;
		VBR         at 0 range  1 ..  1;
		Reserved    at 0 range  2 .. 31;
	end record;

	subtype Color_Register is ARGB8888_Pixel;

	-- IER, ISR and ICR
	type Interrupt_Register is record
		LI          : Boolean;
		FUI         : Boolean;
		TERRI       : Boolean;
		RRI         : Boolean;
		Reserved    : Integer range 0 .. 2**28 - 1;
	end record with Size => 32;
	for Interrupt_Register use record
		LI          at 0 range  0 ..  0;
		FUI         at 0 range  1 ..  1;
		TERRI       at 0 range  2 ..  2;
		RRI         at 0 range  3 ..  3;
		Reserved    at 0 range  4 .. 31;
	end record;

	type Current_Position_Register is record
		CYPOS       : Integer range 0 .. 2**16 - 1;
		CXPOS       : Integer range 0 .. 2**16 - 1;
	end record with Size => 32;
	for Current_Position_Register use record
		CYPOS       at 0 range  0 .. 15;
		CXPOS       at 0 range 16 .. 31;
	end record;

	type Current_Display_Status_Register is record
		VDES        : Boolean;
		HDES        : Boolean;
		VSYNCS      : Boolean;
		HSYNCS      : Boolean;
		Reserved    : Integer range 0 .. 2**28 - 1;
	end record with Size => 32;
	for Current_Display_Status_Register use record
		VDES        at 0 range  0 ..  0;
		HDES        at 0 range  1 ..  1;
		VSYNCS      at 0 range  2 ..  2;
		HSYNCS      at 0 range  3 ..  3;
		Reserved    at 0 range  4 .. 31;
	end record;

	type Layer_Control_Register is record
		LEN         : Boolean;
		COLKEN      : Boolean;
		Reserved_2  : Integer range 0 .. 3;
		CLUTEN      : Boolean;
		Reserved_5  : Integer range 0 .. 2**27 - 1;
	end record with Size => 32;
	for Layer_Control_Register use record
		LEN         at 0 range  0 ..  0;
		COLKEN      at 0 range  1 ..  1;
		Reserved_2  at 0 range  2 ..  3;
		CLUTEN      at 0 range  4 ..  4;
		Reserved_5  at 0 range  5 .. 31;
	end record;

	type Layer_Horizontal_Position_Register is record
		WHSTPOS     : Integer range 0 .. 2**12 - 1;
		WHSPPOS     : Integer range 0 .. 2**12 - 1;
	end record with Size => 32;
	for Layer_Horizontal_Position_Register use record
		WHSTPOS     at 0 range  0 .. 15;
		WHSPPOS     at 0 range 16 .. 31;
	end record;

	type Layer_Vertical_Position_Register is record
		WVSTPOS     : Integer range 0 .. 2**11 - 1;
		WVSPPOS     : Integer range 0 .. 2**11 - 1;
	end record with Size => 32;
	for Layer_Vertical_Position_Register use record
		WVSTPOS     at 0 range  0 .. 15;
		WVSPPOS     at 0 range 16 .. 31;
	end record;

	type Pixel_Format_Configuration_Register is record
		PF          : Color_Mode range ARGB8888 .. AL88;
		Reserved    : Integer range 0 .. 2**29 - 1 := 0;
	end record with Size => 32;
	for Pixel_Format_Configuration_Register use record
		PF          at 0 range  0 ..  2;
		Reserved    at 0 range  3 .. 31;
	end record;

	type Blending_Factor_1 is (
		Constant_Alpha,
		Pixel_by_Constant
	);
	for Blending_Factor_1 use (
		Constant_Alpha    => 2#100#,
		Pixel_by_Constant => 2#110#
	);

	type Blending_Factor_2 is (
		One_Minus_Constant_Alpha,
		One_Minus_Pixel_by_Constant
	);
	for Blending_Factor_2 use (
		One_Minus_Constant_Alpha    => 2#101#,
		One_Minus_Pixel_by_Constant => 2#111#
	);

	type Layer_Blending_Factors_Config_Register is record
		BF2         : Blending_Factor_2;
		Reserved_3  : Integer range 0 .. 2**5 - 1 := 0;
		BF1         : Blending_Factor_1;
		Reserved_11 : Integer range 0 .. 2**21 - 1 := 0;
	end record with Size => 32;
	for Layer_Blending_Factors_Config_Register use record
		BF2         at 0 range  0 ..  2;
		Reserved_3  at 0 range  3 ..  7;
		BF1         at 0 range  8 .. 10;
		Reserved_11 at 0 range 11 .. 31;
	end record;

	type Layer_Buffer_Length_Register is record
		CFBLL       : Integer range 0 .. 2**13 - 1;
		CFBP        : Integer range 0 .. 2**13 - 1;
	end record with Size => 32;
	for Layer_Buffer_Length_Register use record
		CFBLL       at 0 range  0 .. 15;
		CFBP        at 0 range 16 .. 31;
	end record;

	type CLUT_Write_Register is record
		B           : Integer range 0 .. 2**8 - 1;
		R           : Integer range 0 .. 2**8 - 1;
		G           : Integer range 0 .. 2**8 - 1;
		CLUTADD     : Integer range 0 .. 2**8 - 1;
	end record with Size => 32;
	for CLUT_Write_Register use record
		B           at 0 range  0 ..  7;
		R           at 0 range  8 .. 15;
		G           at 0 range 16 .. 23;
		CLUTADD     at 0 range 24 .. 31;
	end record;

	pragma Warnings (Off, "448 bits of ""Layer_Registers"" unused");
	type Layer_Registers is record
		CR          : Layer_Control_Register;
		pragma Volatile_Full_Access (CR);
		WHPCR       : Layer_Horizontal_Position_Register;
		pragma Volatile_Full_Access (WHPCR);
		WVPCR       : Layer_Vertical_Position_Register;
		pragma Volatile_Full_Access (WVPCR);
		CKCR        : Color_Register;
		pragma Volatile_Full_Access (CKCR);
		PFCR        : Pixel_Format_Configuration_Register;
		pragma Volatile_Full_Access (PFCR);
		CACR        : Integer range 0 .. 2**8 - 1;
		pragma Volatile_Full_Access (CACR);
		DCCR        : Color_Register;
		pragma Volatile_Full_Access (DCCR);
		BFCR        : Layer_Blending_Factors_Config_Register;
		pragma Volatile_Full_Access (BFCR);
		CFBAR       : Address_Register;
		pragma Volatile_Full_Access (CFBAR);
		CFBLR       : Layer_Buffer_Length_Register;
		pragma Volatile_Full_Access (CFBLR);
		CFBLNR      : Integer range 0 .. 2**11 - 1;
		pragma Volatile_Full_Access (CFBLNR);
		CLUTWR      : CLUT_Write_Register;
		pragma Volatile_Full_Access (CLUTWR);
	end record with Size => 8 * 16#80#;
	pragma Warnings (On, "448 bits of ""Layer_Registers"" unused");
	for Layer_Registers use record
		CR          at 16#04# range 0 .. 31;
		WHPCR       at 16#08# range 0 .. 31;
		WVPCR       at 16#0C# range 0 .. 31;
		CKCR        at 16#10# range 0 .. 31;
		PFCR        at 16#14# range 0 .. 31;
		CACR        at 16#18# range 0 .. 31;
		DCCR        at 16#1C# range 0 .. 31;
		BFCR        at 16#20# range 0 .. 31;
		CFBAR       at 16#2C# range 0 .. 31;
		CFBLR       at 16#30# range 0 .. 31;
		CFBLNR      at 16#34# range 0 .. 31;
		CLUTWR      at 16#44# range 0 .. 31;
	end record;

	type Layer_Register_Array is array (1 .. 2) of Layer_Registers
	with Size => 2 * 8 * 16#80#;

	type LTDC_Registers is record
		SSCR        : Frame_Configuration_Register;
		pragma Volatile_Full_Access (SSCR);
		BPCR        : Frame_Configuration_Register;
		pragma Volatile_Full_Access (BPCR);
		AWCR        : Frame_Configuration_Register;
		pragma Volatile_Full_Access (AWCR);
		TWCR        : Frame_Configuration_Register;
		pragma Volatile_Full_Access (TWCR);
		GCR         : Global_Control_Register;
		pragma Volatile_Full_Access (GCR);
		SRCR        : Shadow_Reload_Configuration_Register;
		pragma Volatile_Full_Access (SRCR);
		BCCR        : Color_Register;
		pragma Volatile_Full_Access (BCCR);
		IER         : Interrupt_Register;
		pragma Volatile_Full_Access (IER);
		ISR         : Interrupt_Register;
		pragma Volatile_Full_Access (ISR);
		ICR         : Interrupt_Register;
		pragma Volatile_Full_Access (ICR);
		LIPCR       : Integer range 0 .. 2**11 - 1;
		pragma Volatile_Full_Access (LIPCR);
		CPSR        : Current_Position_Register;
		pragma Volatile_Full_Access (CPSR);
		CDSR        : Current_Display_Status_Register;
		pragma Volatile_Full_Access (CDSR);
		L           : Layer_Register_Array;
	end record with Volatile;
	for LTDC_Registers use record
		SSCR        at 16#0008# range 0 .. 31;
		BPCR        at 16#000C# range 0 .. 31;
		AWCR        at 16#0010# range 0 .. 31;
		TWCR        at 16#0014# range 0 .. 31;
		GCR         at 16#0018# range 0 .. 31;
		SRCR        at 16#0024# range 0 .. 31;
		BCCR        at 16#002C# range 0 .. 31;
		IER         at 16#0034# range 0 .. 31;
		ISR         at 16#0038# range 0 .. 31;
		ICR         at 16#003C# range 0 .. 31;
		LIPCR       at 16#0040# range 0 .. 31;
		CPSR        at 16#0044# range 0 .. 31;
		CDSR        at 16#0048# range 0 .. 31;
		L           at 16#0080# range 0 .. 2 * 8 * 16#80# - 1;
	end record;

	LTDC : LTDC_Registers with Volatile, Import, Address => Address_Map.LTDC;

end STM32.LCD_TFT;
