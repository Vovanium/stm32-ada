with STM32.Address_Map;
with STM32.Graphics;
use  STM32.Graphics;

-- DMA2D Graphic Accelerator

package STM32.Graphic_Accelerator is

	-- CR
	type DMA2D_Mode is (
		Memory_to_Memory,
		Memory_to_Memory_with_PFC,
		Memory_to_Memory_with_Blending,
		Register_to_Memory
	) with Size => 2;

	for DMA2D_Mode use (
		Memory_to_Memory               => 2#00#,
		Memory_to_Memory_with_PFC      => 2#01#,
		Memory_to_Memory_with_Blending => 2#10#,
		Register_to_Memory             => 2#11#
	);

	type Interrupt_Index is (
		TEI,  -- Transfer error interrupt
		TCI,  -- Transfer complete interrupt
		TWI,  -- Transfer watermark interrupt
		CAEI, -- CLUT access error interrupt
		CTCI, -- CLUT transfer complete interrupt
		CEI   -- Configuration error interrupt
	);
	type Interrupt_Array is array (Interrupt_Index) of Boolean with Pack, Component_Size => 1, Size => 6;

	type Control_Register is record
		START       : Boolean    := False; -- Start
		SUSP        : Boolean    := False; -- Suspend
		ABRT        : Boolean    := False; -- Abort (Note this field is renamed)
		Reserved_3  : Integer range 0 .. 2**5 - 1;
		E           : Interrupt_Array := (others => False); -- Interrupt enable
		Reserved_14 : Integer range 0 .. 2**2 - 1;
		MODE        : DMA2D_Mode := Memory_to_Memory; -- DMA2D mode
		Reserved_18 : Integer range 0 .. 2**14 - 1;
	end record with Size => 32;

	for Control_Register use record
		START       at 0 range  0 ..  0;
		SUSP        at 0 range  1 ..  1;
		ABRT        at 0 range  2 ..  2;
		Reserved_3  at 0 range  3 ..  7;
		E           at 0 range  8 .. 13;
		Reserved_14 at 0 range 14 .. 15;
		MODE        at 0 range 16 .. 17;
		Reserved_18 at 0 range 18 .. 31;
	end record;

	-- ISR, IFCR
	type Interrupt_Status_Register is record
		F           : Interrupt_Array := (others => False); -- Interrupt flags
		Reserved_6  : Integer range 0 .. 2**26 - 1 := 0;
	end record with Size => 32;

	for Interrupt_Status_Register use record
		F           at 0 range  0 ..  5;
		Reserved_6  at 0 range  6 .. 31;
	end record;

	-- FGOR, BGOR, OOR

	type Offset_Register is record
		LO         : Integer range 0 .. 2**14 - 1 := 0; -- Line offset
		Reserved   : Integer range 0 .. 2**18 - 1 := 0;
	end record with Size => 32;
	for Offset_Register use record
		LO         at 0 range  0 .. 13;
		Reserved   at 0 range 14 .. 31;
	end record;

	-- FGPFCCR, BGPFCCR

	type CLUT_Color_Mode is new Color_Mode range ARGB8888 .. RGB888
	with Size => 1;

	type Alpha_Mode is (
		No_Alpha_Modification,
		Replace_Alpha,
		Multiply_Alpha
	) with Size => 2;
	for Alpha_Mode use (
		No_Alpha_Modification => 2#00#,
		Replace_Alpha         => 2#01#,
		Multiply_Alpha        => 2#10#
	);

	type PFC_Control_Register is record
		CM          : Color_Mode                  := ARGB8888; -- Color mode
		CCM         : CLUT_Color_Mode             := ARGB8888; -- CLUT color mode
		START       : Boolean                     := False; -- Start loading of the CLUT
		Reserved_6  : Integer range 0 .. 2**2 - 1 := 0;
		CS          : Integer range 0 .. 2**8 - 1 := 0; -- CLUT size minus 1
		AM          : Alpha_Mode                  := No_Alpha_Modification; -- Alpha mode
		Reserved_18 : Integer range 0 .. 2**6 - 1 := 0;
		ALPHA       : Integer range 0 .. 2**8 - 1 := 0; -- Alpha value
	end record with Size => 32;
	for PFC_Control_Register use record
		CM          at 0 range  0 ..  3;
		CCM         at 0 range  4 ..  4;
		START       at 0 range  5 ..  5;
		Reserved_6  at 0 range  6 ..  7;
		CS          at 0 range  8 .. 15;
		AM          at 0 range 16 .. 17;
		Reserved_18 at 0 range 18 .. 23;
		ALPHA       at 0 range 24 .. 31;
	end record;

	-- FGCOLR, BGCOLR, FGCLUT(), BGCLUT() use Color_Register from STM32.Graphics
	subtype Color_Register is ARGB8888_Pixel;

	-- OPFCCR
	type Output_Color_Mode is new Color_Mode range ARGB8888 .. ARGB4444
	with Size => 3;

	type Output_PFC_Control_Register is record
		CM          : Output_Color_Mode := ARGB8888; -- Output color mode
		Reserved_3  : Integer range 0 .. 2**29 - 1 := 0;
	end record with Size => 32;
	for Output_PFC_Control_Register use record
		CM          at 0 range  0 ..  2;
		Reserved_3  at 0 range  3 .. 31;
	end record;

	-- OCOLR
	type Output_Color_Register (Mode : Output_Color_Mode := ARGB8888) is record
		case Mode is
		when ARGB8888 =>
			ARGB8888 : ARGB8888_Pixel;
		when RGB888 =>
			RGB888   : RGB888_Pixel;
		when RGB565 =>
			RGB565   : RGB565_Pixel;
		when ARGB1555 =>
			ARGB1555 : ARGB1555_Pixel;
		when ARGB4444 =>
			ARGB4444 : ARGB4444_Pixel;
		end case;
	end record with Size => 32, Unchecked_Union;

	for Output_Color_Register use record
		ARGB8888 at 0 range  0 .. 31;
		RGB888   at 0 range  0 .. 23;
		RGB565   at 0 range  0 .. 15;
		ARGB1555 at 0 range  0 .. 15;
		ARGB4444 at 0 range  0 .. 15;
	end record;
	-- should be implemented on more convenient way

	-- NLR
	type Number_of_Line_Register is record
		NL          : Integer range 0 .. 2**16 - 1 := 0; -- Number of lines
		PL          : Integer range 0 .. 2**14 - 1 := 0; -- Number of pixels per line
		Reserved    : Integer range 0 .. 3 := 0;
	end record with Size => 32;
	for Number_of_Line_Register use record
		NL          at 0 range  0 .. 15;
		PL          at 0 range 16 .. 29;
		Reserved    at 0 range 30 .. 31;
	end record;

	-- LWR
	type Line_Watermark_Register is record
		LW          : Integer range 0 .. 2**16 - 1 := 0; -- Line watermark
		Reserved    : Integer range 0 .. 2**16 - 1 := 0;
	end record with Size => 32;
	for Line_Watermark_Register use record
		LW          at 0 range  0 .. 15;
		Reserved    at 0 range 16 .. 31;
	end record;

	-- AMTCR
	type AHB_Master_Timer_Configuration_Register is record
		EN          : Boolean;
		Reserved_1  : Integer range 0 .. 2**7 - 1;
		DT          : Integer range 0 .. 2**8 - 1;
		Reserved_16 : Integer range 0 .. 2**16 - 1;
	end record with Size => 32;
	for AHB_Master_Timer_Configuration_Register use record
		EN          at 0 range  0 ..  0;
		Reserved_1  at 0 range  1 ..  7;
		DT          at 0 range  8 .. 15;
		Reserved_16 at 0 range 16 .. 31;
	end record;

	-- FGCLUT, BGCLUT arrays
	type Color_Lookup_Table is array (0 .. 2**8 - 1) of Color_Register;

	type DMA2D_Registers is record
		CR          : Control_Register;
		ISR         : Interrupt_Status_Register;
		IFCR        : Interrupt_Status_Register;
		FGMAR       : Address_Register;
		FGOR        : Offset_Register;
		BGMAR       : Address_Register;
		BGOR        : Offset_Register;
		FGPFCCR     : PFC_Control_Register;
		FGCOLR      : Color_Register;
		BGPFCCR     : PFC_Control_Register;
		BGCOLR      : Color_Register;
		FGCMAR      : Address_Register;
		BGCMAR      : Address_Register;
		OPFCCR      : Output_PFC_Control_Register;
		OCOLR       : Output_Color_Register;
		OMAR        : Address_Register;
		OOR         : Offset_Register;
		NLR         : Number_of_Line_Register;
		LWR         : Line_Watermark_Register;
		AMTCR       : AHB_Master_Timer_Configuration_Register;
		FGCLUT      : Color_Lookup_Table;
		BGCLUT      : Color_Lookup_Table;
	end record with Volatile;
	for DMA2D_Registers use record
		CR          at 16#0000# range 0 .. 31;
		ISR         at 16#0004# range 0 .. 31;
		IFCR        at 16#0008# range 0 .. 31;
		FGMAR       at 16#000C# range 0 .. 31;
		FGOR        at 16#0010# range 0 .. 31;
		BGMAR       at 16#0014# range 0 .. 31;
		BGOR        at 16#0018# range 0 .. 31;
		FGPFCCR     at 16#001C# range 0 .. 31;
		FGCOLR      at 16#0020# range 0 .. 31;
		BGPFCCR     at 16#0024# range 0 .. 31;
		BGCOLR      at 16#0028# range 0 .. 31;
		FGCMAR      at 16#002C# range 0 .. 31;
		BGCMAR      at 16#0030# range 0 .. 31;
		OPFCCR      at 16#0034# range 0 .. 31;
		OCOLR       at 16#0038# range 0 .. 31;
		OMAR        at 16#003C# range 0 .. 31;
		OOR         at 16#0040# range 0 .. 31;
		NLR         at 16#0044# range 0 .. 31;
		LWR         at 16#0048# range 0 .. 31;
		AMTCR       at 16#004C# range 0 .. 31;
		FGCLUT      at 16#0400# range 0 .. 2**8 * 32 - 1;
		BGCLUT      at 16#0800# range 0 .. 2**8 * 32 - 1;
	end record;

	DMA2D : DMA2D_Registers with Volatile, Import, Address => Address_Map.DMA2D;

end STM32.Graphic_Accelerator;
