with STM32.External_Interrupts, STM32.System_Configuration;
use  STM32.External_Interrupts, STM32.System_Configuration;

package body STM32.GPIO.Ports is

	package body GPIO_Port is

		procedure Enable (A : Boolean := True) is
		begin
			RCC.AHB1ENR (RCC_Index) := A;
		end Enable;

		procedure Reset is
		begin
			RCC.AHB1RSTR (RCC_Index) := True;
			RCC.AHB1RSTR (RCC_Index) := False;
		end Reset;

		-- Single GPIO pin
		package body Boolean_Port is

			procedure Set (Value : Boolean) is
			begin
				Register.BSRR := (
					BR => 2**Bit,
					BS => 2**Bit * Boolean'Pos (Value xor Invert)
				);
			end Set;

			function Value return Boolean is
			begin
				return (Register.IDR and 2**Bit) /= 0;
			end Value;

			procedure Set_MODER (Mode : Port_Mode) is
			begin
				Register.MODER (Bit) := Mode;
			end Set_MODER;

			procedure Set_OTYPER (Output : Output_Type) is
			begin
				Register.OTYPER (Bit) := Output;
			end Set_OTYPER;

			procedure Set_OSPEEDR (Speed : Output_Speed) is
			begin
				Register.OSPEEDR (Bit) := Speed;
			end Set_OSPEEDR;

			procedure Set_PUPDR (Pull : Port_Pull) is
			begin
				Register.PUPDR (Bit) := Pull;
			end Set_PUPDR;

			procedure Set_AFR (Value : Alternate_Function) is
			begin
				Register.AFR (Bit) := Value;
			end Set_AFR;

			procedure Enable is
			begin
				Enable (True); -- Call parent
			end Enable;

			procedure Set_EXTICR is
			begin
				case Bit is
				when  0 ..  3 =>
					SYSCFG.EXTICR1.EXTI (Bit) := Interrupt_Pin;
				when  4 ..  7 =>
					SYSCFG.EXTICR2.EXTI (Bit) := Interrupt_Pin;
				when  8 .. 11 =>
					SYSCFG.EXTICR3.EXTI (Bit) := Interrupt_Pin;
				when 12 .. 15 =>
					SYSCFG.EXTICR4.EXTI (Bit) := Interrupt_Pin;
				end case;
			end Set_EXTICR;

			procedure Set_IMR  (Value : Boolean) is
				R : Line_Set_Register := EXTI.IMR;
			begin
				R (Bit) := Value;
				EXTI.IMR := R;
			end Set_IMR;

			procedure Set_EMR  (Value : Boolean) is
				R : Line_Set_Register := EXTI.EMR;
			begin
				R (Bit) := Value;
				EXTI.EMR := R;
			end Set_EMR;

			procedure Set_RTSR (Value : Boolean) is
				R : Line_Set_Register := EXTI.RTSR;
			begin
				R (Bit) := Value;
				EXTI.RTSR := R;
			end Set_RTSR;

			procedure Set_FTSR (Value : Boolean) is
				R : Line_Set_Register := EXTI.FTSR;
			begin
				R (Bit) := Value;
				EXTI.FTSR := R;
			end Set_FTSR;

			procedure Clear_PR is
				R : Line_Set_Register := (others => False);
			begin
				R (Bit) := True;
				EXTI.PR := R;
			end Clear_PR;

		end Boolean_Port;

		package body Modular_Port is

			Size : constant Positive := 1 + Last_Bit - First_Bit;
			Mask : constant Unsigned_16 := 2**(Last_Bit + 1) - 2**First_Bit;

			procedure Set (Value : Value_Type) is
			begin
				Register.BSRR := (
					BR => Mask,
					BS => 2**First_Bit * Unsigned_16 (Value xor Invert)
				);
			end Set;

			function Value return Value_Type is
			begin
				return Value_Type ((Register.IDR / 2**First_Bit) and (2**Size - 1)) xor Invert;
			end Value;

			procedure Set_MODER (Mode : Port_Mode) is
				R : Port_Mode_Register := Register.MODER;
			begin
				for I in Port_Bit_Number range First_Bit .. Last_Bit loop
					R (I) := Mode;
				end loop;

				Register.MODER := R;
			end Set_MODER;

			procedure Set_OTYPER (Output : Output_Type) is
				R : Output_Type_Register := Register.OTYPER;
			begin
				for I in Port_Bit_Number range First_Bit .. Last_Bit loop
					R (I) := Output;
				end loop;

				Register.OTYPER := R;
			end Set_OTYPER;

			procedure Set_OSPEEDR (Speed : Output_Speed) is
				R : Output_Speed_Register := Register.OSPEEDR;
			begin
				for I in Port_Bit_Number range First_Bit .. Last_Bit loop
					R (I) := Speed;
				end loop;

				Register.OSPEEDR := R;
			end Set_OSPEEDR;

			procedure Set_PUPDR (Pull : Port_Pull) is
				R : Port_Pull_Register := Register.PUPDR;
			begin
				for I in Port_Bit_Number range First_Bit .. Last_Bit loop
					R (I) := Pull;
				end loop;

				Register.PUPDR := R;
			end Set_PUPDR;

			procedure Set_AFR (Value : Alternate_Function) is
				R : Alternate_Function_Register := Register.AFR;
			begin
				for I in Port_Bit_Number range First_Bit .. Last_Bit loop
					R (I) := Value;
				end loop;

				Register.AFR := R;
			end Set_AFR;

			procedure Enable is
			begin
				Enable (True); -- Call parent
			end Enable;

			procedure Set_EXTICR is
			begin
				for I in Port_Bit_Number range Port_Bit_Number'Max (First_Bit,  0)
				                            .. Port_Bit_Number'Min (Last_Bit,   3) loop
					SYSCFG.EXTICR1.EXTI (I) := Interrupt_Pin;
				end loop;
				for I in Port_Bit_Number range Port_Bit_Number'Max (First_Bit,  4)
				                            .. Port_Bit_Number'Min (Last_Bit,   7) loop
					SYSCFG.EXTICR2.EXTI (I) := Interrupt_Pin;
				end loop;
				for I in Port_Bit_Number range Port_Bit_Number'Max (First_Bit,  8)
				                            .. Port_Bit_Number'Min (Last_Bit,  11) loop
					SYSCFG.EXTICR3.EXTI (I) := Interrupt_Pin;
				end loop;
				for I in Port_Bit_Number range Port_Bit_Number'Max (First_Bit, 12)
				                            .. Port_Bit_Number'Min (Last_Bit,  15) loop
					SYSCFG.EXTICR4.EXTI (I) := Interrupt_Pin;
				end loop;
			end Set_EXTICR;

		end Modular_Port;

	end GPIO_Port;

end STM32.GPIO.Ports;
