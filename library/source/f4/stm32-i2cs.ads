with STM32.Address_Map;

-- Inter-Integrated Circuit Interface Controllers
--
-- These units are the same through the whole STM32F4 family
-- The only difference is FLTR register is absent in some models

package STM32.I2Cs is

	-- CR1

	type SMBus_Role is (
		Device,
		Host
	) with Size => 1;
	for SMBus_Role use (
		Device => 0,
		Host   => 1
	);

	type Acknowledge_Position is (
		Current_Byte,
		Next_Byte
	) with Size => 1;
	for Acknowledge_Position use (
		Current_Byte => 0,
		Next_Byte    => 1
	);

	type Control_Register_1 is record
		PE          : Boolean              := False; -- Peripheral enable
		SMBUS       : Boolean              := False; -- SMBus mode
		Reserved_2  : Integer range 0 .. 1 := 0;
		SMBTYPE     : SMBus_Role           := Device; -- SMBus type
		ENARP       : Boolean              := False; -- ARP enable
		ENPEC       : Boolean              := False; -- Packet error check calculation enable
		ENGC        : Boolean              := False; -- General call answer enable
		NOSTRETCH   : Boolean              := False; -- Clock stretching disable
		START       : Boolean              := False; -- START generation
		STOP        : Boolean              := False; -- STOP generation
		ACK         : Boolean              := False; -- Acknowledge enable
		POS         : Acknowledge_Position := Current_Byte; -- Acknowledge / PEC position
		PEC         : Boolean              := False; -- Packet error checking transfer
		ALERT       : Boolean              := False; -- Drives SMBA pin low
		Reserved_14 : Integer range 0 .. 1 := 0;
		SWRST       : Boolean              := False; -- Software reset
	end record with Size => 16;
	for Control_Register_1 use record
		PE          at 0 range  0 ..  0;
		SMBUS       at 0 range  1 ..  1;
		Reserved_2  at 0 range  2 ..  2;
		SMBTYPE     at 0 range  3 ..  3;
		ENARP       at 0 range  4 ..  4;
		ENPEC       at 0 range  5 ..  5;
		ENGC        at 0 range  6 ..  6;
		NOSTRETCH   at 0 range  7 ..  7;
		START       at 0 range  8 ..  8;
		STOP        at 0 range  9 ..  9;
		ACK         at 0 range 10 .. 10;
		POS         at 0 range 11 .. 11;
		PEC         at 0 range 12 .. 12;
		ALERT       at 0 range 13 .. 13;
		Reserved_14 at 0 range 14 .. 14;
		SWRST       at 0 range 15 .. 15;
	end record;

	-- CR2

	type Control_Register_2 is record
		FREQ        : Integer range 0 .. 50 := 0; -- Peripheral clock frequency in MHz
		Reserved_6  : Integer range 0 .. 3 := 0;
		ITERREN     : Boolean := False; -- Error interrupt enable
		ITEVTEN     : Boolean := False; -- Event interrupt enable
		ITBUFEN     : Boolean := False; -- Buffer interrupt enable
		DMAEN       : Boolean := False; -- DMA requests enable
		LAST        : Boolean := False; -- Next DMA is the last transfer
		Reserved_13 : Integer range 0 .. 7 := 0;
	end record with Size => 16;
	for Control_Register_2 use record
		FREQ        at 0 range  0 ..  5;
		Reserved_6  at 0 range  6 ..  7;
		ITERREN     at 0 range  8 ..  8;
		ITEVTEN     at 0 range  9 ..  9;
		ITBUFEN     at 0 range 10 .. 10;
		DMAEN       at 0 range 11 .. 11;
		LAST        at 0 range 12 .. 12;
		Reserved_13 at 0 range 13 .. 15;
	end record;

	-- OAR1
	type Addressing_Mode is (
		Address_7_Bit,
		Address_10_Bit
	) with Size => 1;
	for Addressing_Mode use (
		Address_7_Bit  => 0,
		Address_10_Bit => 1
	);

	type Own_Address_Register_1 is record
		ADD         : Integer range 0 .. 2**10 - 1 := 0; -- Own address. Bits 1 .. 7 used in 7 bit addressing mode
		Reserved_10 : Integer range 0 .. 2**5 - 1  := 0;
		ADDMODE     : Addressing_Mode              := Address_7_Bit; -- Addressing mode
	end record with Size => 16;
	for Own_Address_Register_1 use record
		ADD         at 0 range  0 ..  9;
		Reserved_10 at 0 range 10 .. 14;
		ADDMODE     at 0 range 15 .. 15;
	end record;

	-- OAR2

	type Own_Address_Register_2 is record
		ENDUAL      : Boolean := False; -- Second address enable
		ADD2        : Integer range 0 .. 2**7 - 1 := 0;
		Reserved    : Integer range 0 .. 2**8 - 1 := 0;
	end record with Size => 16;
	for Own_Address_Register_2 use record
		ENDUAL      at 0 range  0 ..  0;
		ADD2        at 0 range  1 ..  7;
		Reserved    at 0 range  8 .. 15;
	end record;

	-- SR1

	type Status_Register_1 is record
		SB          : Boolean := False; -- Start condition generated (in master mode)
		ADDR        : Boolean := False; -- Address sent (in master mode) or matched (in slave mode)
		BTF         : Boolean := False; -- Byte transfer finished
		ADD10       : Boolean := False; -- 10-bit header sent
		STOPF       : Boolean := False; -- Stop condition detected
		Reserved_5  : Integer range 0 .. 1 := 0;
		RxNE        : Boolean := False; -- Receiver data register not empty
		TxE         : Boolean := False; -- Transmitter data register empty
		BERR        : Boolean := True; -- Bus error
		ARLO        : Boolean := True; -- Arbitration lost
		AF          : Boolean := True; -- Acknowledge failure
		OVR         : Boolean := True; -- Overrun or underrun occured
		PECERR      : Boolean := True; -- PEC error
		Reserved_13 : Integer range 0 .. 1 := 0;
		TIMEOUT     : Boolean := True; -- Timeout or Tlow error
		SMBALERT    : Boolean := True; -- SMBus alert
	end record with Size => 16;
	for Status_Register_1 use record
		SB          at 0 range  0 ..  0;
		ADDR        at 0 range  1 ..  1;
		BTF         at 0 range  2 ..  2;
		ADD10       at 0 range  3 ..  3;
		STOPF       at 0 range  4 ..  4;
		Reserved_5  at 0 range  5 ..  5;
		RxNE        at 0 range  6 ..  6;
		TxE         at 0 range  7 ..  7;
		BERR        at 0 range  8 ..  8;
		ARLO        at 0 range  9 ..  9;
		AF          at 0 range 10 .. 10;
		OVR         at 0 range 11 .. 11;
		PECERR      at 0 range 12 .. 12;
		Reserved_13 at 0 range 13 .. 13;
		TIMEOUT     at 0 range 14 .. 14;
		SMBALERT    at 0 range 15 .. 15;
	end record;

	-- SR2

	type Role is (
		Slave,
		Master
	) with Size => 1;
	for Role use (
		Slave  => 0,
		Master => 1
	);

	type Direction is (
		Receive,
		Transmit
	) with Size => 1;
	for Direction use (
		Receive  => 0,
		Transmit => 1
	);

	type Status_Register_2 is record
		MSL         : Role;    -- Master/slave mode
		BUSY        : Boolean; -- Communication is ongoing on the bus
		TRA         : Direction; -- Transmitter mode set on R/W bit state of address byte
		Reserved    : Integer range 0 .. 1;
		GENCALL     : Boolean; -- General call address received when ENGC = True
		SMBDEFAULT  : Boolean; -- SMBus default address received when ENARP = True
		SMBHOST     : Boolean; -- SMBus host address received when SMBTYPE = Host and ENARP = True
		DUALF       : Boolean; -- Secondary address matched
		PEC         : Integer range 0 .. 2**8 - 1; -- Packer error checking register
	end record with Size => 16;
	for Status_Register_2 use record
		MSL         at 0 range  0 ..  0;
		BUSY        at 0 range  1 ..  1;
		TRA         at 0 range  2 ..  2;
		Reserved    at 0 range  3 ..  3;
		GENCALL     at 0 range  4 ..  4;
		SMBDEFAULT  at 0 range  5 ..  5;
		SMBHOST     at 0 range  6 ..  6;
		DUALF       at 0 range  7 ..  7;
		PEC         at 0 range  8 .. 15;
	end record;

	-- CCR
	type Duty_Cycle is (
		Fm_2_1,
		Fm_16_9
	) with Size => 1;
	for Duty_Cycle use (
		Fm_2_1  => 0,
		Fm_16_9 => 1
	);

	type Speed_Mode is (
		Sm_Mode,
		Fm_Mode
	) with Size => 1;
	for Speed_Mode use (
		Sm_Mode => 0,
		Fm_Mode => 1
	);

	type Clock_Control_Register is record
		CCR         : Integer range 0 .. 2**12 - 1 := 0;       -- Clock period
		Reserved_12 : Integer range 0 .. 3         := 0;
		DUTY        : Duty_Cycle                   := Fm_2_1;  -- Fast mode clock low / high ratio
		F_S         : Speed_Mode                   := Sm_Mode; -- Speed mode
	end record with Size => 16;
	for Clock_Control_Register use record
		CCR         at 0 range  0 .. 11;
		Reserved_12 at 0 range 12 .. 13;
		DUTY        at 0 range 14 .. 14;
		F_S         at 0 range 15 .. 15;
	end record;

	-- FLTR

	type Filter_Register is record
		DNF         : Integer range 0 .. 15 := 0; -- Digital noise filter period
		ANOFF       : Boolean := False; -- Analog noise filter disable
		Reserved    : Integer range 0 .. 2**11 - 1;
	end record with Size => 16;
	for Filter_Register use record
		DNF         at 0 range  0 ..  3;
		ANOFF       at 0 range  4 ..  4;
		Reserved    at 0 range  5 .. 15;
	end record;

	-- note: FLTR only present in models F413, F415, F417, F42x, F43x, F44x, F469 and F479
	--       FLTR is absent in           F40x, F410, F411, and F412

	--

	type I2C_Registers is record
		CR1         : Control_Register_1;
		pragma Volatile_Full_Access (CR1);
		CR2         : Control_Register_2;
		pragma Volatile_Full_Access (CR2);
		OAR1        : Own_Address_Register_1;
		pragma Volatile_Full_Access (OAR1);
		OAR2        : Own_Address_Register_2;
		pragma Volatile_Full_Access (OAR2);
		DR          : Integer range 0 .. 2**8 - 1 := 0;
		SR1         : Status_Register_1;
		pragma Volatile_Full_Access (SR1);
		SR2         : Status_Register_2;
		pragma Volatile_Full_Access (SR2);
		CCR         : Clock_Control_Register;
		pragma Volatile_Full_Access (CCR);
		TRISE       : Integer range 0 .. 2**6 - 1 := 2;
		FLTR        : Filter_Register;
		pragma Volatile_Full_Access (FLTR);
	end record;
	for I2C_Registers use record
		CR1         at 16#00# range  0 .. 15;
		CR2         at 16#04# range  0 .. 15;
		OAR1        at 16#08# range  0 .. 15;
		OAR2        at 16#0C# range  0 .. 15;
		DR          at 16#10# range  0 .. 15;
		SR1         at 16#14# range  0 .. 15;
		SR2         at 16#18# range  0 .. 15;
		CCR         at 16#1C# range  0 .. 15;
		TRISE       at 16#20# range  0 .. 15;
		FLTR        at 16#24# range  0 .. 15;
	end record;

	--

	I2C1   : aliased I2C_Registers with Volatile, Import, Address => Address_Map.I2C1;
	I2C2   : aliased I2C_Registers with Volatile, Import, Address => Address_Map.I2C2;
	I2C3   : aliased I2C_Registers with Volatile, Import, Address => Address_Map.I2C3;

end STM32.I2Cs;
