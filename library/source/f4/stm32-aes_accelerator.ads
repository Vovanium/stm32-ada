with STM32.Address_Map;

-- AES Hardware Accelerator
--
-- This module replaces CRYP module in STM32F4x3 devices.
-- It is completely not compatible with CRYP.

package STM32.AES_Accelerator is

	-- CR

	type Data_Type is (
		Data_32_Bit, -- No swapping of data
		Data_16_Bit, -- Each word consist of two half-words which are swapped with each other
		Data_8_Bit,  -- Each word consist of four bytes in reverse order
		Data_1_Bit)  -- Each word is reversed bit-wise
	with Size => 2;

	type Operating_Mode is (
		Encryption,
		Key_Derivation,
		Decryption,
		Key_Derivation_Single_Decryption)
	with Size => 2;

	type Chaining_Mode is (
		Electronic_Codebook,
		Cipher_Block_Chaining,
		Counter_Mode,
		Galois_Counter_Mode,
		Counter_with_CBC_MAC);

	type Chaining_Mode_0 is mod 4;
	type Chaining_Mode_2 is mod 2;

	function To_Mode_0 (Mode : Chaining_Mode) return Chaining_Mode_0 is (
		Chaining_Mode_0 (Unsigned_32 (Chaining_Mode'Pos (Mode)) and 2#011#)
	);
	-- CHMOD0 field value of a given algorithm mode

	function To_Mode_2 (Mode : Chaining_Mode) return Chaining_Mode_2 is (
		Chaining_Mode_2 (Shift_Right (Unsigned_32 (Chaining_Mode'Pos (Mode)), 2))
	);
	-- CHMOD3 field value of a given algorithm mode

	function To_Mode (Mode_2 : Chaining_Mode_2; Mode_0 : Chaining_Mode_0) return Chaining_Mode is (
		Chaining_Mode'Val (Shift_Left (Unsigned_32 (Mode_2), 2) or Unsigned_32 (Mode_0))
	);

	type Chaining_Phase is (
		Init_Phase,
		Header_Phase,
		Payload_Phase,
		Final_Phase)
	with Size => 2;

	type Key_Size is (
		Key_128_Bits,
		Key_256_Bits)
	with Size => 1;

	type Control_Register is record
		EN        : Boolean         := False;      -- AES enable
		DATATYPE  : Data_Type       := Data_32_Bit; -- Data type selection
		MODE      : Operating_Mode  := Encryption; -- AES operating mode
		CHMOD0    : Chaining_Mode_0 := To_Mode_0 (Electronic_Codebook); -- AES chaining mode
		CCFC      : Boolean         := False;             -- CCF clear
		ERRC      : Boolean         := False;             -- Error clear
		CCFIE     : Boolean         := False;             -- CCF interrupt enable
		ERRIE     : Boolean         := False;             -- Error interrupt enable
		DMAINEN   : Boolean         := False;             -- Enable DMA input
		DMAOUTEN  : Boolean         := False;             -- Enable DMA output
		GCMPH     : Chaining_Phase  := Init_Phase;        -- GCM or CCM phase selection
		Unused_15 : Unused_1_Bit    := 0;
		CHMOD2    : Chaining_Mode_2 := To_Mode_2 (Electronic_Codebook); -- AES chaining mode
		Unused_17 : Unused_1_Bit    := 0;
		KEYSIZE   : Key_Size        := Key_128_Bits;      -- Key size selection
		Unused_19 : Unused_13_Bits  := 0;
	end record with Size => 32;
	for Control_Register use record
		EN        at 0 range  0 ..  0;
		DATATYPE  at 0 range  1 ..  2;
		MODE      at 0 range  3 ..  4;
		CHMOD0    at 0 range  5 ..  6;
		CCFC      at 0 range  7 ..  7;
		ERRC      at 0 range  8 ..  8;
		CCFIE     at 0 range  9 ..  9;
		ERRIE     at 0 range 10 .. 10;
		DMAINEN   at 0 range 11 .. 11;
		DMAOUTEN  at 0 range 12 .. 12;
		GCMPH     at 0 range 13 .. 14;
		Unused_15 at 0 range 15 .. 15;
		CHMOD2    at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 17;
		KEYSIZE   at 0 range 18 .. 18;
		Unused_19 at 0 range 19 .. 31;
	end record;

	-- SR

	type Status_Register is record
		CCF       : Boolean; -- Computation complete flag
		RDERR     : Boolean; -- Read error flag
		WRERR     : Boolean; -- Write error flag
		BUSY      : Boolean; -- Busy flag
		Unused_4  : Unused_28_Bits;
	end record with Size => 32;
	for Status_Register use record
		CCF       at 0 range  0 ..  0;
		RDERR     at 0 range  1 ..  1;
		WRERR     at 0 range  2 ..  2;
		BUSY      at 0 range  3 ..  3;
		Unused_4  at 0 range  4 .. 31;
	end record;

	-- KEYRx

	subtype Key_Index is Integer range 0 .. 3;

	type Key_Registers is array (Key_Index) of Unsigned_32 with Size => 32 * 4;

	type AES_Registers is record
		CR    : Control_Register;
		SR    : Status_Register;
		DINR  : Unsigned_32;
		DOUTR : Unsigned_32;
		KEYR  : Key_Registers;
		IVR   : Key_Registers;
	end record;
	for AES_Registers use record
		CR    at 16#0# range 0 .. 31;
		SR    at 16#4# range 0 .. 31;
		DINR  at 16#8# range 0 .. 31;
		DOUTR at 16#C# range 0 .. 31;
		KEYR  at 16#10# range 0 .. 32 * 4 - 1;
		IVR   at 16#20# range 0 .. 32 * 4 - 1;
	end record;

	AES : AES_Registers with Volatile, Import, Address => Address_Map.CRYP;

end STM32.AES_Accelerator;
