with STM32.Address_Map;

package STM32.Random_Number_Generator is

	-- CR

	type Control_Register is record
		Unused_0    : Integer range 0 .. 3 := 0;
		RNGEN       : Boolean := False; -- Random number generator
		IE          : Boolean := False; -- Interrupt enable
		Unused_4    : Integer range 0 .. 2**28 - 1 := 0;
	end record with Size => 32;
	for Control_Register use record
		Unused_0    at 0 range  0 ..  1;
		RNGEN       at 0 range  2 ..  2;
		IE          at 0 range  3 ..  3;
		Unused_4    at 0 range  4 .. 31;
	end record;

	-- SR

	type Status_Register is record
		DRDY        : Boolean; -- Data ready
		CECS        : Boolean; -- Clock error current status
		SECS        : Boolean; -- Seed error current status
		Unused_3    : Integer range 0 .. 3;
		CEIS        : Boolean; -- Clock error interrupt
		SEIS        : Boolean; -- Seed error interrupt
		Unused_7    : Integer range 0 .. 2**25 - 1;
	end record with Size => 32;
	for Status_Register use record
		DRDY        at 0 range  0 ..  0;
		CECS        at 0 range  1 ..  1;
		SECS        at 0 range  2 ..  2;
		Unused_3    at 0 range  3 ..  4;
		CEIS        at 0 range  5 ..  5;
		SEIS        at 0 range  6 ..  6;
		Unused_7    at 0 range  7 .. 31;

	end record;

	--

	type RNG_Registers is record
		CR          : Control_Register;
		SR          : Status_Register;
		DR          : Unsigned_32;

	end record;
	for RNG_Registers use record
		CR          at 16#00# range 0 .. 31;
		SR          at 16#04# range 0 .. 31;
		DR          at 16#08# range 0 .. 31;

	end record;

	RNG : RNG_Registers with Volatile, Import, Address => Address_Map.RNG;

end STM32.Random_Number_Generator;
