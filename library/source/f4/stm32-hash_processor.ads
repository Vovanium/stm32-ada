with STM32.Address_Map;

-- Hash Processor

package STM32.Hash_Processor is

	-- CR

	type Data_Type is (
		Data_32_Bit, -- No swapping of data
		Data_16_Bit, -- Each word consist of two half-words which are swapped with each other
		Data_8_Bit,  -- Each word consist of four bytes in reverse order
		Data_1_Bit   -- Each word is reversed bit-wise
	) with Size => 2;

	type Mode_Select is (
		Hash_Mode,
		HMAC_Mode
	) with Size => 1;

	type Algorithm_0 is mod 2;
	type Algorithm_1 is mod 2;

	type Algorithm is (
		SHA_1,
		MD5,
		SHA224,
		SHA256
	) with Size => 2;
	for Algorithm use (
		SHA_1  => 2#00#,
		MD5    => 2#01#,
		SHA224 => 2#10#,
		SHA256 => 2#11#
	);

	function To_Algorithm_0 (Algo : Algorithm) return Algorithm_0 is (
		Algorithm_0 (Unsigned_32 (Algorithm'Pos (Algo)) and 2#01#)
	);
	-- ALGO0 field value of a given algorithm

	function To_Algorithm_1 (Algo : Algorithm) return Algorithm_1 is (
		Algorithm_1 (Shift_Right (Unsigned_32 (Algorithm'Pos (Algo)), 1))
	);
	-- ALGO1 field value of a given algorithm

	function To_Algorithm (Algo_1 : Algorithm_1; Algo_0 : Algorithm_0) return Algorithm is (
		Algorithm'Val (Shift_Left (Unsigned_32 (Algo_1), 1) or Unsigned_32 (Algo_0))
	);
	-- Composes ALGO0 and ALGO1 to a single value of algorithm

	type Control_Register is record
		Unused_0    : Integer range 0 .. 3  := 0;
		INIT        : Boolean               := False; -- Initialize message digest
		DMAE        : Boolean               := False; -- DMA enable
		DATATYPE    : Data_Type             := Data_32_Bit; -- Data type selection
		MODE        : Mode_Select           := Hash_Mode; -- Mode selection
		ALGO0       : Algorithm_0           := 0; -- Algorithm selection bit 0
		NBW         : Integer range 0 .. 15 := 0; -- Number of words already
		DINNE       : Boolean               := False; -- DIN not empty
		MDMAT       : Boolean               := False; -- Multiple DMA Transfers
		Unused_14   : Integer range 0 .. 3  := 0;
		LKEY        : Boolean               := False; -- Long key selection
		Unused_17   : Integer range 0 .. 1  := 0;
		ALGO1       : Algorithm_1           := 0; -- Algorithm selection bit 1
		Unused_19   : Integer range 0 .. 2**13 - 1 := 0;
	end record with Size => 32;
	for Control_Register use record
		Unused_0    at 0 range  0 ..  1;
		INIT        at 0 range  2 ..  2;
		DMAE        at 0 range  3 ..  3;
		DATATYPE    at 0 range  4 ..  5;
		MODE        at 0 range  6 ..  6;
		ALGO0       at 0 range  7 ..  7;
		NBW         at 0 range  8 .. 11;
		DINNE       at 0 range 12 .. 12;
		MDMAT       at 0 range 13 .. 13;
		Unused_14   at 0 range 14 .. 15;
		LKEY        at 0 range 16 .. 16;
		Unused_17   at 0 range 17 .. 17;
		ALGO1       at 0 range 18 .. 18;
		Unused_19   at 0 range 19 .. 31;
	end record;

	-- STR

	subtype Valid_Bit_Count is Integer range 0 .. 2**5 - 1;

	type Start_Register is record
		NBLW        : Valid_Bit_Count := 0; -- Number of valid bits in the la
		Unused_5    : Unused_3_Bits   := 0;
		DCAL        : Boolean         := False; -- Digest calculation
		Unused_9    : Unused_23_Bits  := 0;
	end record with Size => 32;
	for Start_Register use record
		NBLW        at 0 range  0 ..  4;
		Unused_5    at 0 range  5 ..  7;
		DCAL        at 0 range  8 ..  8;
		Unused_9    at 0 range  9 .. 31;
	end record;

	-- HRx

	type Unsigned_32_Array is array (Integer range <>) of Unsigned_32;

	-- IMR

	type Interrupt_Enable_Register is record
		DINIE       : Boolean := False; -- Data input interrupt enable
		DCIE        : Boolean := False; -- Digest calculation completion interrupt enable
		Unused      : Unused_30_Bits := 0;
	end record with Size => 32;
	for Interrupt_Enable_Register use record
		DINIE       at 0 range  0 ..  0;
		DCIE        at 0 range  1 ..  1;
		Unused      at 0 range  2 .. 31;
	end record;

	-- SR

	type Status_Register is record
		DINIS       : Boolean := True; -- Data input interrupt status
		DCIS        : Boolean := True; -- Digest calculation completion interrupt status
		DMAS        : Boolean := False; -- DMA Status
		BUSY        : Boolean := False; -- Busy bit
		Unused_4    : Unused_28_Bits := 0;
	end record with Size => 32;
	for Status_Register use record
		DINIS       at 0 range  0 ..  0;
		DCIS        at 0 range  1 ..  1;
		DMAS        at 0 range  2 ..  2;
		BUSY        at 0 range  3 ..  3;
		Unused_4    at 0 range  4 .. 31;
	end record;

	type HASH_Registers is record
		CR          : Control_Register;
		DIN         : Unsigned_32;
		STR         : Start_Register;
		HR          : Unsigned_32_Array (0 .. 4);
		IMR         : Interrupt_Enable_Register;
		SR          : Status_Register;
		CSR         : Unsigned_32_Array (0 .. 53);
		HASH_HR     : Unsigned_32_Array (0 .. 7);
	end record;
	for HASH_Registers use record
		CR          at 16#0000# range  0 .. 31;
		DIN         at 16#0004# range  0 .. 31;
		STR         at 16#0008# range  0 .. 31;
		HR          at 16#000C# range  0 .. 32 * 5 - 1;
		IMR         at 16#0020# range  0 .. 31;
		SR          at 16#0024# range  0 .. 31;
		CSR         at 16#00F8# range  0 .. 32 * 54 - 1;
		HASH_HR     at 16#0310# range  0 .. 32 * 8 - 1;
	end record;

	HASH : HASH_Registers with Volatile, Import, Address => Address_Map.HASH;

end STM32.Hash_Processor;
