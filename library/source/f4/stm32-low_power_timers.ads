with STM32.Address_Map;

-- Low Power Timers
--
-- Available in F410 and F4x3 devices

package STM32.Low_Power_Timers is

	-- ISR, ICR, IER

	type Interrupt_Register is record
		CMPM     : Boolean        := False; -- Compare match
		ARRM     : Boolean        := False; -- Autoreload match
		EXTTRIG  : Boolean        := False; -- External trigger edge
		CMPOK    : Boolean        := False; -- Compare register update OK
		ARROK    : Boolean        := False; -- Autoreload register update OK
		UP       : Boolean        := False; -- Counter direction change down to up
		DOWN     : Boolean        := False; -- Counter direction change up to down
		Unused_7 : Unused_25_Bits := 0;
	end record with Size => 32;
	for Interrupt_Register use record
		CMPM     at 0 range  0 ..  0;
		ARRM     at 0 range  1 ..  1;
		EXTTRIG  at 0 range  2 ..  2;
		CMPOK    at 0 range  3 ..  3;
		ARROK    at 0 range  4 ..  4;
		UP       at 0 range  5 ..  5;
		DOWN     at 0 range  6 ..  6;
		Unused_7 at 0 range  7 .. 31;
	end record;

	-- CFGR

	type Clock_Select is (
		Internal_Clock,
		External_Input_1)
	with Size => 1;

	type Clock_Polarity is (
		Rising_Edge_Clock,
		Falling_Edge_Clock,
		Both_Edges_Clock)
	with Size => 2;
	for Clock_Polarity use (
		Rising_Edge_Clock  => 2#00#,
		Falling_Edge_Clock => 2#01#,
		Both_Edges_Clock   => 2#10#);

	type Filter is new Logarithmic range Value_1 .. Value_8 with Size => 2;

	type Prescaler is new Logarithmic range Value_1 .. Value_128 with Size => 3;

	type Trigger_Select is range 0 .. 7 with Size => 3;

	type Trigger_Enable is (
		Software_Trigger,
		Rising_Edge_Trigger,
		Falling_Edge_Trigger,
		Both_Edges_Trigger)
	with Size => 2;

	type Waveform_Shape is (
		Normal,
		Set_Once)
	with Size => 1;

	type Waveform_Polarity is (
		Waveform_Positive,
		Waveform_Negative)
	with Size => 1;

	type Preload_Mode is (
		APB_Write,
		End_of_Cycle)
	with Size => 1;

	type Configuration_Register is record
		CKSEL     : Clock_Select      := Internal_Clock; -- Clock selector
		CKPOL     : Clock_Polarity    := Rising_Edge_Clock; -- Clock Polarity
		CKFLT     : Filter            := Value_1; -- Digital filter foe external clock
		Unused_5  : Unused_1_Bit      := 0;
		TRGFLT    : Filter            := Value_1; -- Digital filter for trigger
		Unused_8  : Unused_1_Bit      := 0;
		PRESC     : Prescaler         := Value_1; -- Clock prescaler
		Unused_12 : Unused_1_Bit      := 0;
		TRIGSEL   : Trigger_Select    := 0;       -- Trigger selector
		Unused_16 : Unused_1_Bit      := 0;
		TRIGEN    : Trigger_Enable    := Software_Trigger; -- Trigger enable and
		TIMOUT    : Boolean           := False; -- Timeout enable
		WAVE      : Waveform_Shape    := Normal; -- Waveform shape
		WAVPOL    : Waveform_Polarity := Waveform_Positive; -- Waveform shape polarity
		PRELOAD   : Preload_Mode      := APB_Write; -- Registers update mode
		COUNTMODE : Clock_Select      := Internal_Clock; -- Counter mode
		ENC       : Boolean           := False; -- Encoder mode enable
		Unused_25 : Unused_7_Bits     := 0;
	end record with Size => 32;
	for Configuration_Register use record
		CKSEL at 0 range 0 .. 0;
		CKPOL at 0 range 1 .. 2;
		CKFLT at 0 range 3 .. 4;
		Unused_5  at 0 range  5 ..  5;
		TRGFLT    at 0 range  6 ..  7;
		Unused_8  at 0 range  8 ..  8;
		PRESC     at 0 range  9 .. 11;
		Unused_12 at 0 range 12 .. 12;
		TRIGSEL   at 0 range 13 .. 15;
		Unused_16 at 0 range 16 .. 16;
		TRIGEN    at 0 range 17 .. 18;
		TIMOUT    at 0 range 19 .. 19;
		WAVE      at 0 range 20 .. 20;
		WAVPOL    at 0 range 21 .. 21;
		PRELOAD   at 0 range 22 .. 22;
		COUNTMODE at 0 range 23 .. 23;
		ENC       at 0 range 24 .. 24;
		Unused_25 at 0 range 25 .. 31;
	end record;

	-- CR

	type Control_Register is record
		ENABLE    : Boolean        := False; -- LPTIM Enable
		SNGSTRT   : Boolean        := False; -- Start in single mode
		CNTSTRT   : Boolean        := False; -- Start in continuous mode
		Unused_3  : Unused_29_Bits := 0;
	end record with Size => 32;
	for Control_Register use record
		ENABLE    at 0 range  0 ..  0;
		SNGSTRT   at 0 range  1 ..  1;
		CNTSTRT   at 0 range  2 ..  2;
		Unused_3  at 0 range  3 .. 31;
	end record;

	-- CMP

	type Count_Register is range 0 .. 2**16 - 1 with Size => 32;

	type CMP_Register is record
		CMP : Integer range 0 .. 2**16 - 1; -- Compare value
		Unused_16 : Integer range 0 .. 2**16 - 1;

	end record with Size => 32;
	for CMP_Register use record
		CMP at 0 range 0 .. 15;
		Unused_16 at 0 range 16 .. 31;

	end record;

	-- ARR

	type ARR_Register is record
		ARR : Integer range 0 .. 2**16 - 1; -- Auto reload value
		Unused_16 : Integer range 0 .. 2**16 - 1;

	end record with Size => 32;
	for ARR_Register use record
		ARR at 0 range 0 .. 15;
		Unused_16 at 0 range 16 .. 31;

	end record;

	-- CNT

	type CNT_Register is record
		CNT : Integer range 0 .. 2**16 - 1; -- Counter value
		Unused_16 : Integer range 0 .. 2**16 - 1;

	end record with Size => 32;
	for CNT_Register use record
		CNT at 0 range 0 .. 15;
		Unused_16 at 0 range 16 .. 31;

	end record;

	-- OPTR

	type Input_1_Remap is (
		Input_1_PB5_PC0,
		Input_1_PA4,
		Input_1_PB9,
		Input_1_TIM6_DAC)
	with Size => 2;

	type Option_Register is record
		OPT      : Input_1_Remap  := Input_1_PB5_PC0; -- (OR) Timer Input 1 Remap option
		Unused_2 : Unused_30_Bits := 0;
	end record with Size => 32;
	for Option_Register use record
		OPT       at 0 range 0 .. 1;
		Unused_2  at 0 range 2 .. 31;
	end record;

	-- OR renamed to OPT to avoid keyword clash

	-- OPTR is present only in F410

	--

	type LPTIM_Registers is record
		ISR  : Interrupt_Register;
		pragma Volatile_Full_Access (ISR);
		ICR  : Interrupt_Register;
		pragma Volatile_Full_Access (ICR);
		IER  : Interrupt_Register;
		pragma Volatile_Full_Access (IER);
		CFGR : Configuration_Register;
		pragma Volatile_Full_Access (CFGR);
		CR   : Control_Register;
		pragma Volatile_Full_Access (CR);
		CMP  : Count_Register;
		ARR  : Count_Register;
		CNT  : Count_Register;
		OPTR : Option_Register;
		pragma Volatile_Full_Access (OPTR);
	end record;
	for LPTIM_Registers use record
		ISR  at 16#00# range 0 .. 31;
		ICR  at 16#04# range 0 .. 31;
		IER  at 16#08# range 0 .. 31;
		CFGR at 16#0C# range 0 .. 31;
		CR   at 16#10# range 0 .. 31;
		CMP  at 16#14# range 0 .. 31;
		ARR  at 16#18# range 0 .. 31;
		CNT  at 16#1C# range 0 .. 31;
		OPTR at 16#20# range 0 .. 31;
	end record;

	LPTIM1 : LPTIM_Registers with Volatile, Import, Address => Address_Map.LPTIM1;

end STM32.Low_Power_Timers;
