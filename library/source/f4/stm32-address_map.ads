with System.Storage_Elements;
use  System.Storage_Elements, System;

package STM32.Address_Map is

	-- Regions / memories
	Aliased_Memory : constant Address := To_Address (16#0000_0000#);
	Flash_Memory   : constant Address := To_Address (16#0800_0000#);
	CCM_RAM        : constant Address := To_Address (16#1000_0000#);
	Option_Bytes   : constant Address := To_Address (16#1FFE_C000#);
	System_Memory  : constant Address := To_Address (16#1FFF_0000#);
	Unique_Id      : constant Address := System_Memory + 16#7A10#;
	Flash_Size     : constant Address := System_Memory + 16#7A22#;
	Package_Id     : constant Address := System_Memory + 16#7BF0#;
	Option_Bytes_2 : constant Address := To_Address (16#1FFF_C000#);
	SRAM_1         : constant Address := To_Address (16#2000_0000#);
	SRAM_2         : constant Address := To_Address (16#2001_C000#);
	SRAM_3         : constant Address := To_Address (16#2002_0000#);

	-- APB1
	APB1           : constant Address := To_Address (16#4000_0000#);
	TIM2           : constant Address := APB1 + 16#0000#;
	TIM3           : constant Address := APB1 + 16#0400#;
	TIM4           : constant Address := APB1 + 16#0800#;
	TIM5           : constant Address := APB1 + 16#0C00#;
	TIM6           : constant Address := APB1 + 16#1000#;
	TIM7           : constant Address := APB1 + 16#1400#;
	TIM12          : constant Address := APB1 + 16#1800#;
	TIM13          : constant Address := APB1 + 16#1C00#;
	TIM14          : constant Address := APB1 + 16#2000#;
	LPTIM1         : constant Address := APB1 + 16#2400#;
	RTC_and_BKP    : constant Address := APB1 + 16#2800#;
	WWDG           : constant Address := APB1 + 16#2C00#;
	IWDG           : constant Address := APB1 + 16#3000#;
	I2S2ext        : constant Address := APB1 + 16#3400#;
	SPI2_I2S2      : constant Address := APB1 + 16#3800#;
	SPI3_I2S3      : constant Address := APB1 + 16#3C00#;
	I2S3ext        : constant Address := APB1 + 16#4000#;
	USART2         : constant Address := APB1 + 16#4400#;
	USART3         : constant Address := APB1 + 16#4800#;
	UART4          : constant Address := APB1 + 16#4C00#;
	UART5          : constant Address := APB1 + 16#5000#;
	I2C1           : constant Address := APB1 + 16#5400#;
	I2C2           : constant Address := APB1 + 16#5800#;
	I2C3           : constant Address := APB1 + 16#5C00#;
	FMPI2C1        : constant Address := APB1 + 16#6000#;
	CAN1           : constant Address := APB1 + 16#6400#;
	CAN2           : constant Address := APB1 + 16#6800#;
	CAN3           : constant Address := APB1 + 16#6C00#;
	PWR            : constant Address := APB1 + 16#7000#;
	DAC            : constant Address := APB1 + 16#7400#;
	UART7          : constant Address := APB1 + 16#7800#;
	UART8          : constant Address := APB1 + 16#7C00#;

	-- APB2
	APB2           : constant Address := To_Address (16#4001_0000#);
	TIM1           : constant Address := APB2 + 16#0000#;
	TIM8           : constant Address := APB2 + 16#0400#;
	USART1         : constant Address := APB2 + 16#1000#;
	USART6         : constant Address := APB2 + 16#1400#;
	UART9          : constant Address := APB2 + 16#1800#;
	UART10         : constant Address := APB2 + 16#1C00#;
	ADC1           : constant Address := APB2 + 16#2000#;
	ADC2           : constant Address := APB2 + 16#2100#;
	ADC3           : constant Address := APB2 + 16#2200#;
	ADC_Common     : constant Address := APB2 + 16#2300#;
	SDIO           : constant Address := APB2 + 16#2C00#;
	SPI1           : constant Address := APB2 + 16#3000#;
	SPI4           : constant Address := APB2 + 16#3400#;
	SYSCFG         : constant Address := APB2 + 16#3800#;
	EXTI           : constant Address := APB2 + 16#3C00#;
	TIM9           : constant Address := APB2 + 16#4000#;
	TIM10          : constant Address := APB2 + 16#4400#;
	TIM11          : constant Address := APB2 + 16#4800#;
	SPI5           : constant Address := APB2 + 16#5000#;
	SPI6           : constant Address := APB2 + 16#5400#;
	SAI1           : constant Address := APB2 + 16#5800#;
	SAI2           : constant Address := APB2 + 16#5C00#;
	DFSDM1         : constant Address := APB2 + 16#6000#;
	DFSDM2         : constant Address := APB2 + 16#6400#;
	LTDC           : constant Address := APB2 + 16#6800#;

	-- AHB1
	AHB1           : constant Address := To_Address (16#4002_0000#);
	GPIOA          : constant Address := AHB1 + 16#0000#;
	GPIOB          : constant Address := AHB1 + 16#0400#;
	GPIOC          : constant Address := AHB1 + 16#0800#;
	GPIOD          : constant Address := AHB1 + 16#0C00#;
	GPIOE          : constant Address := AHB1 + 16#1000#;
	GPIOF          : constant Address := AHB1 + 16#1400#;
	GPIOG          : constant Address := AHB1 + 16#1800#;
	GPIOH          : constant Address := AHB1 + 16#1C00#;
	GPIOI          : constant Address := AHB1 + 16#2000#;
	GPIOJ          : constant Address := AHB1 + 16#2400#;
	GPIOK          : constant Address := AHB1 + 16#2800#;
	CRC            : constant Address := AHB1 + 16#3000#;
	RCC            : constant Address := AHB1 + 16#3800#;
	Flash_IR       : constant Address := AHB1 + 16#3C00#;
	BKPSRAM        : constant Address := AHB1 + 16#4000#;
	DMA1           : constant Address := AHB1 + 16#6000#;
	DMA2           : constant Address := AHB1 + 16#6400#;
	Ethernet_MAC   : constant Address := AHB1 + 16#8000#;
	DMA2D          : constant Address := AHB1 + 16#B000#;
	USB_OTG_HS     : constant Address := AHB1 + 16#2_0000#;

	-- AHB2
	AHB2           : constant Address := To_Address (16#5000_0000#);
	USB_OTG_FS     : constant Address := AHB2 + 16#0_0000#;
	DCMI           : constant Address := AHB2 + 16#5_0000#;
	CRYP           : constant Address := AHB2 + 16#6_0000#;
	HASH           : constant Address := AHB2 + 16#6_0400#;
	RNG            : constant Address := AHB2 + 16#6_0800#;

	-- AHB 3
	AHB3           : constant Address := To_Address (16#6000_0000#);
	FMC_Bank_1     : constant Address := AHB3;
	FSMC_Bank1_1   : constant Address := FMC_Bank_1;
	FSMC_Bank1_2   : constant Address := FMC_Bank_1 + 16#0400_0000#;
	FSMC_Bank1_3   : constant Address := FMC_Bank_1 + 16#0800_0000#;
	FSMC_Bank1_4   : constant Address := FMC_Bank_1 + 16#0C00_0000#;
	FMC_Bank_2     : constant Address := AHB3 + 16#1000_0000#;
	FMC_Bank_3     : constant Address := AHB3 + 16#2000_0000#;
	FMC_Bank_4     : constant Address := AHB3 + 16#3000_0000#;
	FMC_FSMC       : constant Address := AHB3 + 16#4000_0000#;
	FMC_Bank_5     : constant Address := AHB3 + 16#6000_0000#;
	FMC_Bank_6     : constant Address := AHB3 + 16#7000_0000#;

	CPU_Internal   : constant Address := To_Address (16#E000_0000#);
	DBG            : constant Address := CPU_Internal + 16#4_2000#;

end STM32.Address_Map;
