with STM32.Address_Map;

-- Real Time Clock

package STM32.Real_Time_Clock is

	-- RTC identical through the whole F4 family.

	-- TR, TSTR

	subtype BCD_Digit is Integer range 0 .. 9;
	subtype BCD_Tens is BCD_Digit range 0 .. 5;
	subtype Hour_Tens is BCD_Digit range 0 .. 3;

	type Time_Register is record
		SU        : BCD_Digit     := 0; -- Second units in BCD format
		ST        : BCD_Tens      := 0; -- Second tens in BCD format
		MNU       : BCD_Digit     := 0; -- Minute units in BCD format
		MNT       : BCD_Tens      := 0; -- Minute tens in BCD format
		HU        : BCD_Digit     := 0; -- Hour units in BCD format
		HT        : Hour_Tens     := 0; -- Hour tens in BCD format
		PM        : Boolean               := False; -- AM/PM notation
		Unused_23 : Unused_9_Bits := 0;
	end record with Size => 32;
	for Time_Register use record
		SU        at 0 range  0 ..  3;
		ST        at 0 range  4 ..  7;
		MNU       at 0 range  8 .. 11;
		MNT       at 0 range 12 .. 15;
		HU        at 0 range 16 .. 19;
		HT        at 0 range 20 .. 21;
		PM        at 0 range 22 .. 22;
		Unused_23 at 0 range 23 .. 31;
	end record;

	-- note: bug in SVD for f405, f407, f415, f417, f427, f429, f437, f439, f446 and f469 devices

	-- DR, TSDR

	Monday    : constant Integer := 1;
	Tuesday   : constant Integer := 2;
	Wednesday : constant Integer := 3;
	Thursday  : constant Integer := 4;
	Friday    : constant Integer := 5;
	Saturday  : constant Integer := 6;
	Sunday    : constant Integer := 7;

	type Date_Register is record
		DU        : Integer range 0 .. 9 := 1; -- Date units in BCD format
		DT        : Integer range 0 .. 3 := 0; -- Date tens in BCD format
		Unused_6  : Integer range 0 .. 3 := 0;
		MU        : Integer range 0 .. 9 := 1; -- Month units in BCD format
		MT        : Integer range 0 .. 1 := 0; -- Month tens in BCD format
		WDU       : Integer range 0 .. 7 := 1; -- Week day units
		YU        : Integer range 0 .. 9 := 0; -- Year units in BCD format
		YT        : Integer range 0 .. 9 := 0; -- Year tens in BCD format
		Unused_24 : Integer range 0 .. 2**8 - 1 := 0;
	end record with Size => 32;
	for Date_Register use record
		DU        at 0 range  0 ..  3;
		DT        at 0 range  4 ..  5;
		Unused_6  at 0 range  6 ..  7;
		MU        at 0 range  8 .. 11;
		MT        at 0 range 12 .. 12;
		WDU       at 0 range 13 .. 15;
		YU        at 0 range 16 .. 19;
		YT        at 0 range 20 .. 23;
		Unused_24 at 0 range 24 .. 31;
	end record;

	-- CR

	type Wakeup_Clock is (
		RTC_Div_16,
		RTC_Div_8,
		RTC_Div_4,
		RTC_Div_2,
		CK_SPRE,
		CK_SPRE_A,
		CK_SPRE_Add_2_Power_16,
		CK_SPRE_Add_2_Power_16_A
	) with Size => 3;
	for Wakeup_Clock use (
		RTC_Div_16               => 2#000#,
		RTC_Div_8                => 2#001#,
		RTC_Div_4                => 2#010#,
		RTC_Div_2                => 2#011#,
		CK_SPRE                  => 2#100#,
		CK_SPRE_A                => 2#101#,
		CK_SPRE_Add_2_Power_16   => 2#110#,
		CK_SPRE_Add_2_Power_16_A => 2#111#
	);

	type Timestamp_Edge is (
		Rising_Edge,
		Falling_Edge
	) with Size => 1;

	type Hour_Format is (
		Hour_24,
		Hour_AM_PM
	) with Size => 1;

	type Output_Polarity is (
		Active_High,
		Active_Low
	) with Size => 1;
	-- Note assignment (not compatible with LTDC and DCMI)

	type Alarm_Output is (
		Alarm_Disabled,
		Alarm_A,
		Alarm_B,
		Wakeup
	) with Size => 2;
	for Alarm_Output use (
		Alarm_Disabled => 2#00#,
		Alarm_A        => 2#01#,
		Alarm_B        => 2#10#,
		Wakeup         => 2#11#
	);

	type Control_Register is record
		WCKSEL    : Wakeup_Clock    := RTC_Div_16;     -- Wakeup clock selection
		TSEDGE    : Timestamp_Edge  := Rising_Edge;    -- Time-stamp event active
		REFCKON   : Boolean         := False;          -- Reference clock detection enable
		BYPSHAD   : Boolean         := False;          -- Byass the shadow registers
		FMT       : Hour_Format     := Hour_24;        -- Hour format
		DCE       : Boolean         := False;          -- Coarse digital calibration
		ALRAE     : Boolean         := False;          -- Alarm A enable
		ALRBE     : Boolean         := False;          -- Alarm B enable
		WUTE      : Boolean         := False;          -- Wakeup timer enable
		TSE       : Boolean         := False;          -- Time stamp enable
		ALRAIE    : Boolean         := False;          -- Alarm A interrupt enable
		ALRBIE    : Boolean         := False;          -- Alarm B interrupt enable
		WUTIE     : Boolean         := False;          -- Wakeup timer interrupt enable
		TSIE      : Boolean         := False;          -- Time-stamp interrupt enable
		ADD1H     : Boolean         := False;          -- Add 1 hour (summer time change)
		SUB1H     : Boolean         := False;          -- Subtract 1 hour (winter time change)
		BKP       : Boolean         := False;          -- Backup
		POL       : Output_Polarity := Active_High;    -- Output polarity
		OSEL      : Alarm_Output    := Alarm_Disabled; -- Output selection
		COE       : Boolean         := False;          -- Calibration output enable
		Unused_24 : Unused_8_Bits   := 0;
	end record with Size => 32;
	for Control_Register use record
		WCKSEL    at 0 range  0 ..  2;
		TSEDGE    at 0 range  3 ..  3;
		REFCKON   at 0 range  4 ..  4;
		BYPSHAD   at 0 range  5 ..  5; -- note this does not exist in SVD
		FMT       at 0 range  6 ..  6;
		DCE       at 0 range  7 ..  7;
		ALRAE     at 0 range  8 ..  8;
		ALRBE     at 0 range  9 ..  9;
		WUTE      at 0 range 10 .. 10;
		TSE       at 0 range 11 .. 11;
		ALRAIE    at 0 range 12 .. 12;
		ALRBIE    at 0 range 13 .. 13;
		WUTIE     at 0 range 14 .. 14;
		TSIE      at 0 range 15 .. 15;
		ADD1H     at 0 range 16 .. 16;
		SUB1H     at 0 range 17 .. 17;
		BKP       at 0 range 18 .. 18;
		POL       at 0 range 20 .. 20;
		OSEL      at 0 range 21 .. 22;
		COE       at 0 range 23 .. 23;
		Unused_24 at 0 range 24 .. 31;
	end record;

	-- ISR

	type Initialization_and_Status_Register is record
		ALRAWF    : Boolean := True;  -- Alarm A write flag
		ALRBWF    : Boolean := True;  -- Alarm B write flag
		WUTWF     : Boolean := True;  -- Wakeup timer write flag
		SHPF      : Boolean := False; -- Shift operation pending
		INITS     : Boolean := False; -- Initialization status flag
		RSF       : Boolean := False; -- Registers synchronization
		INITF     : Boolean := False; -- Initialization flag
		INIT      : Boolean := False; -- Initialization mode
		ALRAF     : Boolean := True;  -- Alarm A flag
		ALRBF     : Boolean := True;  -- Alarm B flag
		WUTF      : Boolean := True;  -- Wakeup timer flag
		TSF       : Boolean := True;  -- Time-stamp flag
		TSOVF     : Boolean := True;  -- Time-stamp overflow flag
		TAMP1F    : Boolean := True;  -- Tamper detection flag
		TAMP2F    : Boolean := True;  -- TAMPER2 detection flag
		Unused_15 : Integer range 0 .. 1 := 0;
		RECALPF   : Boolean := False; -- Recalibration pending Flag
		Unused_17 : Integer range 0 .. 2**15 - 1 := 0;
	end record with Size => 32;
	for Initialization_and_Status_Register use record
		ALRAWF    at 0 range  0 ..  0;
		ALRBWF    at 0 range  1 ..  1;
		WUTWF     at 0 range  2 ..  2;
		SHPF      at 0 range  3 ..  3;
		INITS     at 0 range  4 ..  4;
		RSF       at 0 range  5 ..  5;
		INITF     at 0 range  6 ..  6;
		INIT      at 0 range  7 ..  7;
		ALRAF     at 0 range  8 ..  8;
		ALRBF     at 0 range  9 ..  9;
		WUTF      at 0 range 10 .. 10;
		TSF       at 0 range 11 .. 11;
		TSOVF     at 0 range 12 .. 12;
		TAMP1F    at 0 range 13 .. 13;
		TAMP2F    at 0 range 14 .. 14;
		Unused_15 at 0 range 15 .. 15;
		RECALPF   at 0 range 16 .. 16;
		Unused_17 at 0 range 17 .. 31;
	end record;

	-- PRER

	type Prescaler_Register is record
		PREDIV_S  : Integer range 0 .. 2**15 - 1 := 16#00FF#; -- Synchronous prescaler factor minus 1
		Unused_15 : Unused_1_Bit                 := 0;
		PREDIV_A  : Integer range 0 .. 2**7 - 1  := 16#7F#; -- Asynchronous prescaler factor minus 1
		Unused_23 : Integer range 0 .. 2**9 - 1  := 0;
	end record with Size => 32;
	for Prescaler_Register use record
		PREDIV_S  at 0 range  0 .. 14;
		Unused_15 at 0 range 15 .. 15;
		PREDIV_A  at 0 range 16 .. 22;
		Unused_23 at 0 range 23 .. 31;

	end record;

	-- CALIBR

	type Calibration_Sign is (
		Increase_Frequency,
		Decrease_Frequency
	) with Size => 1;

	type CALIBR_Register is record
		DC        : Integer range 0 .. 31 := 0; -- Digital calibration (by 4 ppm plus / by 2 ppm minus)
		Unused_5  : Unused_2_Bits         := 0;
		DCS       : Calibration_Sign      := Increase_Frequency; -- Digital calibration sign
		Unused_8  : Unused_24_Bits        := 0;
	end record with Size => 32;
	for CALIBR_Register use record
		DC        at 0 range  0 ..  4;
		Unused_5  at 0 range  5 ..  6;
		DCS       at 0 range  7 ..  7;
		Unused_8  at 0 range  8 .. 31;
	end record;

	-- ALRMAR, ALRMBR

	type Day_Mode is (
		Month_Day,
		Week_Day
	) with Size => 1;
	for Day_Mode use (
		Month_Day => 0,
		Week_Day  => 1
	);

	type Alarm_Register is record
		SU          : Integer range 0 .. 9 := 0;         -- Second units in BCD format
		ST          : Integer range 0 .. 5 := 0;         -- Second tens in BCD format
		MSKS        : Boolean              := False;     -- Alarm A seconds mask
		MNU         : Integer range 0 .. 9 := 0;         -- Minute units in BCD format
		MNT         : Integer range 0 .. 5 := 0;         -- Minute tens in BCD format
		MSKMN       : Boolean              := False;     -- Alarm A minutes mask
		HU          : Integer range 0 .. 9 := 0;         -- Hour units in BCD format
		HT          : Integer range 0 .. 2 := 0;         -- Hour tens in BCD format
		PM          : Boolean              := False;     -- AM/PM notation
		MSKH        : Boolean              := False;     -- Alarm A hours mask
		DU          : Integer range 0 .. 9 := 0;         -- Date units or day in BCD
		DT          : Integer range 0 .. 3 := 0;         -- Date tens in BCD format
		WDSEL       : Day_Mode             := Month_Day; -- Week day selection
		MSKD        : Boolean              := False;     -- Alarm A date mask
	end record with Size => 32;
	for Alarm_Register use record
		SU          at 0 range  0 ..  3;
		ST          at 0 range  4 ..  6;
		MSKS        at 0 range  7 ..  7;
		MNU         at 0 range  8 .. 11;
		MNT         at 0 range 12 .. 14;
		MSKMN       at 0 range 15 .. 15;
		HU          at 0 range 16 .. 19;
		HT          at 0 range 20 .. 21;
		PM          at 0 range 22 .. 22;
		MSKH        at 0 range 23 .. 23;
		DU          at 0 range 24 .. 27;
		DT          at 0 range 28 .. 29;
		WDSEL       at 0 range 30 .. 30;
		MSKD        at 0 range 31 .. 31;
	end record;

	-- Note: MSKx flags are renamed to more meaningful names

	-- WPR

	type Write_Protection_Register is new Unsigned_32 range 0 .. 2**8 - 1;

	WPR_Unlock_1 : constant Write_Protection_Register := 16#CA#; -- Unlock sequence step 1
	WPR_Unlock_2 : constant Write_Protection_Register := 16#53#; -- Unlock sequence step 2

	WPR_Lock     : constant Write_Protection_Register := 16#00#; -- Whatever not equal to unlock sequence

	-- SHIFTR

	type Shift_Control_Register is record
		SUBFS       : Integer range 0 .. 2**15 - 1 := 0; -- Subtract a fraction of a
		Unused_15   : Unused_16_Bits := 0;
		ADD1S       : Boolean := False; -- Add one second
	end record with Size => 32;
	for Shift_Control_Register use record
		SUBFS       at 0 range  0 .. 14;
		Unused_15   at 0 range 15 .. 30;
		ADD1S       at 0 range 31 .. 31;
	end record;

	-- CALR

	type Calibration_Register is record
		CALM        : Integer range 0 .. 2**9 - 1  := 0;     -- Calibration minus decreases frequency by 0.9537 ppm (1/2**20) per one
		Unused_9    : Integer range 0 .. 15        := 0;
		CALW16      : Boolean                      := False; -- Use a 16-second calibration cy
		CALW8       : Boolean                      := False; -- Use an 8-second calibration cy
		CALP        : Boolean                      := False; -- Increase frequency of RTC by 488.5 ppm (1/2**11)
		Unused_16   : Integer range 0 .. 2**16 - 1 := 0;
	end record with Size => 32;
	for Calibration_Register use record
		CALM        at 0 range  0 ..  8;
		Unused_9    at 0 range  9 .. 12;
		CALW16      at 0 range 13 .. 13;
		CALW8       at 0 range 14 .. 14;
		CALP        at 0 range 15 .. 15;
		Unused_16   at 0 range 16 .. 31;
	end record;

	-- TAFCR

	type Tamper_Level is (
		Low_or_Rising,
		High_or_Falling
	) with Size => 1;
	for Tamper_Level use (
		Low_or_Rising   => 0,
		High_or_Falling => 1
	);

	type Tamper_Filter is (
		Tamper_on_Edge,
		Tamper_on_2_Samples,
		Tamper_on_4_Samples,
		Tamper_on_8_Samples
	) with Size => 2;
	for Tamper_Filter use (
		Tamper_on_Edge      => 2#00#,
		Tamper_on_2_Samples => 2#01#,
		Tamper_on_4_Samples => 2#10#,
		Tamper_on_8_Samples => 2#11#
	);

	type Precharge_Duration is new Logarithmic range Value_1 .. Value_8 with Size => 2;

	type Pin_Mapping is (
		AF1,
		AF2
	) with Size => 1;

	type Alarm_Output_Type is (
		Open_Drain,
		Push_Pull
	) with Size => 1;

	type Tamper_and_Alternate_Function_Register is record
		TAMP1E       : Boolean              := False; -- Tamper 1 detection enable
		TAMP1TRG     : Tamper_Level         := Low_or_Rising; -- Active level for tamper 1
		TAMPIE       : Boolean              := False; -- Tamper interrupt enable
		TAMP2E       : Boolean              := False; -- Tamper 2 detection enable
		TAMP2TRG     : Tamper_Level         := Low_or_Rising; -- Active level for tamper 2
		TAMPTS       : Boolean              := False; -- Activate timestamp on tamper d
		TAMPFREQ     : Integer range 0 .. 7 := 0; -- Tamper sampling frequency equals fRTC / 2**(15 - x)
		TAMPFLT      : Tamper_Filter        := Tamper_on_Edge; -- Tamper filter count
		TAMPPRCH     : Precharge_Duration   := Value_1; -- Tamper precharge duration
		TAMPPUDIS    : Boolean              := False; -- TAMPER pull-up disable
		TAMP1INSEL   : Pin_Mapping          := AF1; -- TAMPER1 mapping
		TSINSEL      : Pin_Mapping          := AF1; -- TIMESTAMP mapping
		ALARMOUTTYPE : Alarm_Output_Type    := Open_Drain; -- RTC_ALARM output type
		Reserved_19  : Integer range 0 .. 2**13 - 1 := 0;
	end record with Size => 32;
	for Tamper_and_Alternate_Function_Register use record
		TAMP1E       at 0 range  0 ..  0;
		TAMP1TRG     at 0 range  1 ..  1;
		TAMPIE       at 0 range  2 ..  2;
		TAMP2E       at 0 range  3 ..  3;
		TAMP2TRG     at 0 range  4 ..  4;
		TAMPTS       at 0 range  7 ..  7;
		TAMPFREQ     at 0 range  8 .. 10;
		TAMPFLT      at 0 range 11 .. 12;
		TAMPPRCH     at 0 range 13 .. 14;
		TAMPPUDIS    at 0 range 15 .. 15;
		TAMP1INSEL   at 0 range 16 .. 16;
		TSINSEL      at 0 range 17 .. 17;
		ALARMOUTTYPE at 0 range 18 .. 18;
		Reserved_19  at 0 range 19 .. 31;
	end record;

	-- ALRMASSR, ALRMBSSR

	type Alarm_Subsecond_Register is record
		SS          : Integer range 0 .. 2**15 - 1; -- Sub seconds value
		Unused_15   : Integer range 0 .. 2**9 - 1;
		MASKSS      : Integer range 0 .. 15; -- Mask the most-significant bits
		Unused_28   : Integer range 0 .. 15;
	end record with Size => 32;
	for Alarm_Subsecond_Register use record
		SS          at 0 range  0 .. 14;
		Unused_15   at 0 range 15 .. 23;
		MASKSS      at 0 range 24 .. 27;
		Unused_28   at 0 range 28 .. 31;
	end record;

	-- BKPxR

	type Backup_Registers is array (0 .. 19) of Unsigned_32;

	type RTC_Registers is record
		TR          : Time_Register;
		pragma Volatile_Full_Access (TR);
		DR          : Date_Register;
		pragma Volatile_Full_Access (DR);
		CR          : Control_Register;
		pragma Volatile_Full_Access (CR);
		ISR         : Initialization_and_Status_Register;
		pragma Volatile_Full_Access (ISR);
		PRER        : Prescaler_Register;
		pragma Volatile_Full_Access (PRER);
		WUTR        : Integer range 0 .. 2**16 - 1 := 16#FFFF#;
		pragma Volatile_Full_Access (WUTR);
		CALIBR      : CALIBR_Register;
		pragma Volatile_Full_Access (CALIBR);
		ALRMAR      : Alarm_Register;
		pragma Volatile_Full_Access (ALRMAR);
		ALRMBR      : Alarm_Register;
		pragma Volatile_Full_Access (ALRMBR);
		WPR         : Write_Protection_Register;
		pragma Volatile_Full_Access (WPR);
		SSR         : Integer range 0 .. 2**16 - 1;
		pragma Volatile_Full_Access (SSR);
		SHIFTR      : Shift_Control_Register;
		pragma Volatile_Full_Access (SHIFTR);
		TSTR        : Time_Register;
		pragma Volatile_Full_Access (TSTR);
		TSDR        : Date_Register;
		pragma Volatile_Full_Access (TSDR);
		TSSSR       : Integer range 0 .. 2**16 - 1;
		pragma Volatile_Full_Access (TSSSR);
		CALR        : Calibration_Register;
		pragma Volatile_Full_Access (CALR);
		TAFCR       : Tamper_and_Alternate_Function_Register;
		pragma Volatile_Full_Access (TAFCR);
		ALRMASSR    : Alarm_Subsecond_Register;
		pragma Volatile_Full_Access (ALRMASSR);
		ALRMBSSR    : Alarm_Subsecond_Register;
		pragma Volatile_Full_Access (ALRMBSSR);
		BKPR        : Backup_Registers;
	end record;
	for RTC_Registers use record
		TR          at 16#00# range  0 .. 31;
		DR          at 16#04# range  0 .. 31;
		CR          at 16#08# range  0 .. 31;
		ISR         at 16#0C# range  0 .. 31;
		PRER        at 16#10# range  0 .. 31;
		WUTR        at 16#14# range  0 .. 31;
		CALIBR      at 16#18# range  0 .. 31;
		ALRMAR      at 16#1C# range  0 .. 31;
		ALRMBR      at 16#20# range  0 .. 31;
		WPR         at 16#24# range  0 .. 31;
		SSR         at 16#28# range  0 .. 31;
		SHIFTR      at 16#2C# range  0 .. 31;
		TSTR        at 16#30# range  0 .. 31;
		TSDR        at 16#34# range  0 .. 31;
		TSSSR       at 16#38# range  0 .. 31;
		CALR        at 16#3C# range  0 .. 31;
		TAFCR       at 16#40# range  0 .. 31;
		ALRMASSR    at 16#44# range  0 .. 31;
		ALRMBSSR    at 16#48# range  0 .. 31;
		BKPR        at 16#50# range  0 .. 32 * 20 - 1;
	end record;

	RTC : RTC_Registers with Volatile, Import, Address => Address_Map.RTC_and_BKP;

end STM32.Real_Time_Clock;
