with STM32.System_Configuration, STM32.Reset_And_Clock;
use  STM32.System_Configuration, STM32.Reset_And_Clock;

package STM32.GPIO.Ports is


	generic
		Register      : in out GPIO_Registers;
		RCC_Index     :        Index.AHB1_Index;
		Interrupt_Pin :        Peripheral_Interrupt_Pin;
	package GPIO_Port is

		procedure Enable (A : Boolean := True);
		procedure Reset;

		-- Single GPIO pin
		generic
			Bit        : Port_Bit_Number;
			Invert     : Boolean         := False;
		package Boolean_Port is

			procedure Set (Value : Boolean) with Inline; -- Output to port
			function Value return Boolean with Inline; -- Read port

			-- Shortcuts to control port
			procedure Set_MODER (Mode : Port_Mode) with Inline;
			procedure Set_OTYPER (Output : Output_Type) with Inline;
			procedure Set_OSPEEDR (Speed : Output_Speed) with Inline;
			procedure Set_PUPDR (Pull : Port_Pull) with Inline;
			procedure Set_AFR (Value : Alternate_Function) with Inline;

			-- RCC shortcuts
			procedure Enable;
			-- Unlike whole port it is one-way

			-- Set specific EXTI port value
			procedure Set_EXTICR;

			-- Shortcuts to control External Interrupts
			procedure Set_IMR  (Value : Boolean) with Inline;
			procedure Set_EMR  (Value : Boolean) with Inline;
			procedure Set_RTSR (Value : Boolean) with Inline;
			procedure Set_FTSR (Value : Boolean) with Inline;
			procedure Clear_PR with Inline;
		end Boolean_Port;

		-- Multi-pin GPIO
		generic
			type Value_Type is mod <>;
			First_Bit : Port_Bit_Number;
			Last_Bit  : Port_Bit_Number;
			Invert    : Value_Type      := 0;
		package Modular_Port is
			procedure Set (Value : Value_Type) with Inline;
			function Value return Value_Type with Inline;

			-- Shortcuts to control port
			procedure Set_MODER (Mode : Port_Mode) with Inline;
			procedure Set_OTYPER (Output : Output_Type) with Inline;
			procedure Set_OSPEEDR (Speed : Output_Speed) with Inline;
			procedure Set_PUPDR (Pull : Port_Pull) with Inline;
			procedure Set_AFR (Value : Alternate_Function) with Inline;

			-- RCC shortcuts
			procedure Enable;
			-- Unlike whole port it is one-way

			-- Set specific EXTI port value
			procedure Set_EXTICR;
		end Modular_Port;

	end GPIO_Port;

end STM32.GPIO.Ports;
