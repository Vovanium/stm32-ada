function replace (s, A, n, r) {
	return substr (s, 1, A[n, "start"] - 1) r substr (s, A[n, "start"] + A[n, "length"])
}

/^ +/ {
	sub ("^ +", "");
}

/[0-9]+#[0-9A-Fa-f]#/ {
	while (match ($0, "[0-9]+#([0-9A-Fa-f])#", A)) {
		$0 = replace($0, A, 1, "0" A[1])
	}
}

/[A-Za-z0-9_] *:/ {
	if (match ($0, "^\\s*([A-Za-z0-9_]+ *):", A)) {
		$0 = replace($0, A, 1, A[1] substr ("           ", A[1, "length"]))
	}
}

/[A-Za-z0-9_] *at/ {
	if (match ($0, "^\\s*([A-Za-z0-9_]+ *)at", A)) {
		$0 = replace($0, A, 1, A[1] substr ("           ", A[1, "length"]))
	}
	if (match ($0, "at [0-9]+ range ([0-9]+) \\.\\. ([0-9]+);", A)) {
		$0 = replace($0, A, 2, substr (" ", A[2, "length"]) A[2])
		$0 = replace($0, A, 1, substr (" ", A[1, "length"]) A[1])
	}
}

/Integer range 0 / {
	gsub("range 0 .. 2\\*\\*1 - 1", "range 0 .. 1")
	gsub("range 0 .. 2\\*\\*2 - 1", "range 0 .. 3")
	gsub("range 0 .. 2\\*\\*3 - 1", "range 0 .. 7")
	gsub("range 0 .. 2\\*\\*4 - 1", "range 0 .. 15")
	gsub("Integer range 0 .. 2\\*\\*32 - 1", "Unsigned_32")
}

{
	print
}