<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:template name="cnumber">
  <xsl:param name="string" select="0"/>
  <xsl:choose>
   <xsl:when test="$string = '0'">0</xsl:when>
   <xsl:when test="substring ($string, 1, 2) = '0x'">16#<xsl:value-of select="substring ($string, 3)"/>#</xsl:when>
   <xsl:when test="substring ($string, 1, 1) = '0'">8#<xsl:value-of select="substring ($string, 2)"/>#</xsl:when>
   <xsl:otherwise><xsl:value-of select="$string"/></xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template name="numeric-value">
  <xsl:param name="string" select="0"/>
  <xsl:param name="radix" select="10"/>
  <xsl:param name="value" select="0"/>
  <xsl:variable name="digit" select="string-length (substring-after ('FEDCBA9876543210', substring ($string, 1, 1)))"/>
  <xsl:variable name="nextvalue" select="$value * $radix + $digit"/>
  <xsl:choose>
   <xsl:when test="string-length ($string) = 1"><xsl:value-of select="$nextvalue"/></xsl:when>
   <xsl:otherwise>
    <xsl:call-template name="numeric-value">
     <xsl:with-param name="string"><xsl:value-of select="substring ($string, 2)"/></xsl:with-param>
     <xsl:with-param name="radix"><xsl:value-of select="$radix"/></xsl:with-param>
     <xsl:with-param name="value"><xsl:value-of select="$nextvalue"/></xsl:with-param>
    </xsl:call-template>
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>
 <xsl:template name="cvalue">
  <xsl:param name="string" select="0"/>
  <xsl:choose>
   <xsl:when test="$string = '0'">0</xsl:when>
   <xsl:when test="substring ($string, 1, 2) = '0x'">
    <xsl:call-template name="numeric-value">
     <xsl:with-param name="string">0<xsl:value-of select="translate (substring ($string, 3), 'abcdef', 'ABCDEF')"/></xsl:with-param>
     <xsl:with-param name="radix">16</xsl:with-param>
    </xsl:call-template>
   </xsl:when>
   <xsl:when test="substring ($string, 1, 1) = '0'">
    <xsl:call-template name="numeric-value">
     <xsl:with-param name="string">0<xsl:value-of select="substring ($string, 2)"/></xsl:with-param>
     <xsl:with-param name="radix">8</xsl:with-param>
    </xsl:call-template>
   </xsl:when>
   <xsl:otherwise><xsl:value-of select="$string"/></xsl:otherwise>
  </xsl:choose>
 </xsl:template>

 <xsl:template name="bit-range-numeric">
  <xsl:param name="start" select="0"/>
  <xsl:param name="width" select="1"/>
  <xsl:value-of select="$start"/> .. <xsl:value-of select="$start + $width - 1"/>
 </xsl:template>

 <xsl:template name="bit-range">
  <xsl:param name="start" select="0"/>
  <xsl:param name="width" select="1"/>
  <xsl:call-template name="bit-range-numeric">
   <xsl:with-param name="start">
    <xsl:call-template name="cvalue">
     <xsl:with-param name="string"><xsl:value-of select="$start"/></xsl:with-param>
    </xsl:call-template>
   </xsl:with-param>
   <xsl:with-param name="width">
    <xsl:call-template name="cvalue">
     <xsl:with-param name="string"><xsl:value-of select="$width"/></xsl:with-param>
    </xsl:call-template>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:template>
</xsl:stylesheet>
