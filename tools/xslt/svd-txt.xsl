<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="text" omit-xml-declaration="yes"/>

 <xsl:include href="./common.xsl"/>

 <xsl:template name="peripheral">
  <xsl:for-each select="registers/register">
   <xsl:sort select="addressOffset"/>
REGISTER <xsl:value-of select="name"/> @ <xsl:value-of select="addressOffset"/><xsl:text> </xsl:text><xsl:call-template name="cvalue">
    <xsl:with-param name="string"><xsl:value-of select="size"/></xsl:with-param>
   </xsl:call-template>
   <xsl:for-each select="fields/field">
    <xsl:sort data-type="number" select="bitOffset"/>
- <xsl:value-of select="name"/> : <xsl:call-template name="bit-range">
     <xsl:with-param name="start"><xsl:value-of select="bitOffset"/></xsl:with-param>
     <xsl:with-param name="width"><xsl:value-of select="bitWidth"/></xsl:with-param>
    </xsl:call-template>
   </xsl:for-each>
<xsl:text>
</xsl:text>
  </xsl:for-each>
 </xsl:template>

 <xsl:template match="/">
  <xsl:for-each select="device">
   <xsl:variable name="file" select="concat(name, '.txt')"/>
   <xsl:document href="{$file}" method="text">
CHIP <xsl:value-of select="name"/>
INTERRUPTS
    <xsl:for-each select="peripherals/peripheral">
     <xsl:sort select="baseAddress"/>
     <xsl:for-each select="interrupt[not(value=preceding-sibling::interrupt/value)]">
     <xsl:sort data-type="number" select="value"/>
- <xsl:value-of select="name"/><xsl:text> : </xsl:text><xsl:value-of select="value"/>
     </xsl:for-each>
    </xsl:for-each>

BASE ADDRESSES
    <xsl:for-each select="peripherals/peripheral">
     <xsl:sort select="baseAddress"/>
- <xsl:value-of select="name"/> : <xsl:value-of select="baseAddress"/>
    </xsl:for-each>
   </xsl:document>

   <xsl:for-each select="peripherals/peripheral">
    <xsl:sort select="baseAddress"/>
    <xsl:variable name="pfile" select="concat(../../name, '/', name, '.txt')"/>
    <xsl:document href="{$pfile}" method="text">
     <xsl:choose>
      <xsl:when test="@derivedFrom">
	<xsl:variable name="origin" select="@derivedFrom"/>
	<xsl:for-each select="../peripheral">
	 <xsl:if test="name = $origin">
	  <xsl:call-template name="peripheral"/>
	 </xsl:if>
	</xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
       <xsl:call-template name="peripheral"/>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:document>
   </xsl:for-each>
  </xsl:for-each>
 </xsl:template>
</xsl:stylesheet>
