<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

 <xsl:include href="./common.xsl"/>
 <xsl:template name="unused">
  <xsl:param name="predOffset" select="0"/>
  <xsl:param name="predWidth" select="0"/>
  <xsl:param name="curOffset" select="0" />
  <xsl:variable name="uoffset" select="$predOffset + $predWidth"/>
  <xsl:variable name="uend" select="$curOffset - 1"/>
  <xsl:if test="$uoffset &lt; $curOffset">
<xsl:text>		</xsl:text>Unused_<xsl:value-of select="$uoffset"/> : Unused_<xsl:value-of select="$curOffset - $uoffset"/>_Bits;
  </xsl:if>
 </xsl:template>

 <xsl:template name="unused-for">
  <xsl:param name="predOffset" select="0"/>
  <xsl:param name="predWidth" select="0"/>
  <xsl:param name="curOffset" select="0" />
  <xsl:variable name="uoffset" select="$predOffset + $predWidth"/>
  <xsl:variable name="uend" select="$curOffset - 1"/>
  <xsl:if test="$uoffset &lt; $curOffset">
<xsl:text>		</xsl:text>Unused_<xsl:value-of select="$uoffset"/> at 0 range <xsl:value-of select="$uoffset"/> .. <xsl:value-of select="$uend"/>;
  </xsl:if>
 </xsl:template>

 <xsl:template name="peripheral">
  <xsl:for-each select="registers/register">
	-- <xsl:value-of select="name"/>
	<xsl:text>&#10;</xsl:text>
	type <xsl:value-of select="name"/>_Register is record
   <xsl:for-each select="fields/field">
    <xsl:sort data-type="number" select="bitOffset"/>
    <xsl:call-template name="unused">
     <xsl:with-param name="predOffset">0<xsl:value-of select="preceding-sibling::field[1]/bitOffset"/></xsl:with-param>
     <xsl:with-param name="predWidth">0<xsl:value-of select="preceding-sibling::field[1]/bitWidth"/></xsl:with-param>
     <xsl:with-param name="curOffset"><xsl:value-of select="bitOffset"/></xsl:with-param>
    </xsl:call-template>
<xsl:text>		</xsl:text><xsl:value-of select="name"/> : <xsl:choose>
     <xsl:when test="bitWidth = 1">Boolean</xsl:when>
     <xsl:otherwise>Integer range 0 .. 2**<xsl:call-template name="cvalue">
       <xsl:with-param name="string"><xsl:value-of select="bitWidth"/></xsl:with-param>
      </xsl:call-template> - 1</xsl:otherwise>
    </xsl:choose>; -- <xsl:value-of select="substring (description, 1, 30)"/><xsl:text>&#10;</xsl:text>
   </xsl:for-each>
   <xsl:call-template name="unused">
    <xsl:with-param name="predOffset">0<xsl:value-of select="fields/field[last()]/bitOffset"/></xsl:with-param>
    <xsl:with-param name="predWidth">0<xsl:value-of select="fields/field[last()]/bitWidth"/></xsl:with-param>
    <xsl:with-param name="curOffset"><xsl:call-template name="cvalue">
    <xsl:with-param name="string"><xsl:value-of select="size"/></xsl:with-param></xsl:call-template></xsl:with-param>
   </xsl:call-template>
	end record with Size =&gt; <xsl:call-template name="cvalue">
    <xsl:with-param name="string"><xsl:value-of select="size"/></xsl:with-param>
   </xsl:call-template>;
	for <xsl:value-of select="name"/>_Register use record
   <xsl:for-each select="fields/field">
    <xsl:sort data-type="number" select="bitOffset"/>
    <xsl:call-template name="unused-for">
     <xsl:with-param name="predOffset">0<xsl:value-of select="preceding-sibling::field[1]/bitOffset"/></xsl:with-param>
     <xsl:with-param name="predWidth">0<xsl:value-of select="preceding-sibling::field[1]/bitWidth"/></xsl:with-param>
     <xsl:with-param name="curOffset"><xsl:value-of select="bitOffset"/></xsl:with-param>
    </xsl:call-template>
    <xsl:text>		</xsl:text><xsl:value-of select="name"/> at 0 range <xsl:call-template name="bit-range">
     <xsl:with-param name="start"><xsl:value-of select="bitOffset"/></xsl:with-param>
     <xsl:with-param name="width"><xsl:value-of select="bitWidth"/></xsl:with-param>
    </xsl:call-template>;
   </xsl:for-each>
   <xsl:call-template name="unused-for">
    <xsl:with-param name="predOffset">0<xsl:value-of select="fields/field[last()]/bitOffset"/></xsl:with-param>
    <xsl:with-param name="predWidth">0<xsl:value-of select="fields/field[last()]/bitWidth"/></xsl:with-param>
    <xsl:with-param name="curOffset"><xsl:call-template name="cvalue">
    <xsl:with-param name="string"><xsl:value-of select="size"/></xsl:with-param></xsl:call-template></xsl:with-param>
   </xsl:call-template>
	end record;
  </xsl:for-each>
	type <xsl:value-of select="name"/>_Registers is record
  <xsl:for-each select="registers/register">
   <xsl:sort select="addressOffset"/>
<xsl:text>		</xsl:text><xsl:value-of select="name"/> : <xsl:value-of select="name"/>_Register;
  </xsl:for-each>
	end record;
	for <xsl:value-of select="name"/>_Registers use record
  <xsl:for-each select="registers/register">
   <xsl:sort select="addressOffset"/>
   <xsl:text>		</xsl:text>
   <xsl:value-of select="name"/> at 16#<xsl:value-of select="substring-after (addressOffset, 'x')"/># range <xsl:call-template name="bit-range">
    <xsl:with-param name="start">0</xsl:with-param>
    <xsl:with-param name="width"><xsl:value-of select="size"/></xsl:with-param>
   </xsl:call-template>;
  </xsl:for-each>
	end record;
 </xsl:template>

 <xsl:template match="/">
  <xsl:for-each select="device">
   <xsl:variable name="file" select="concat(name, '.ads')"/>
   <xsl:document href="{$file}" method="text">
package <xsl:value-of select="name"/> is
<xsl:text>	package Interrupts is
</xsl:text>
    <xsl:for-each select="peripherals/peripheral">
     <xsl:sort select="baseAddress"/>
     <xsl:for-each select="interrupt[not(value=preceding-sibling::interrupt/value)]">
      <xsl:sort data-type="number" select="value"/>
<xsl:text>		</xsl:text><xsl:value-of select="name"/><xsl:text> : constant := </xsl:text><xsl:value-of select="value"/>;
     </xsl:for-each>
    </xsl:for-each>
<xsl:text>	end Interrupts;</xsl:text>

	package Address_Map is
    <xsl:for-each select="peripherals/peripheral">
     <xsl:sort select="baseAddress"/>
<xsl:text>		</xsl:text><xsl:value-of select="name"/> : constant := 16#<xsl:value-of select="substring-after (baseAddress, 'x')"/>#;
    </xsl:for-each>
	end Address_Map;
end <xsl:value-of select="name"/>;
   </xsl:document>

   <xsl:for-each select="peripherals/peripheral">
    <xsl:sort select="baseAddress"/>
    <xsl:variable name="pfile" select="concat(../../name, '/', name, '.ads')"/>
    <xsl:document href="{$pfile}" method="text">
package <xsl:value-of select="../../name"/>.<xsl:value-of select="name"/> is
     <xsl:choose>
      <xsl:when test="@derivedFrom">
	-- derived from <xsl:value-of select="@derivedFrom"/>
	<xsl:variable name="origin" select="@derivedFrom"/>
	<xsl:for-each select="../peripheral">
	 <xsl:if test="name = $origin">
	  <xsl:call-template name="peripheral"/>
	 </xsl:if>
	</xsl:for-each>
      </xsl:when>
      <xsl:otherwise>
       <xsl:call-template name="peripheral"/>
      </xsl:otherwise>
     </xsl:choose>
end <xsl:value-of select="../../name"/>.<xsl:value-of select="name"/>;
    </xsl:document>
   </xsl:for-each>
  </xsl:for-each>
 </xsl:template>
</xsl:stylesheet>
