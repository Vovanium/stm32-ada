#!/usr/bin/awk -f

# This script compares different versions of modules

BEGIN {
	nfiles = 0;
}

FNR == 1 {
	nfiles = nfiles + 1;
	fn = FILENAME;
	sub("^\\./", "", fn);
	sub("^STM32", "", fn);
	sub(".txt$", "", fn);
	sub("_GLOBAL", "g", fn);
	sub("_HOST", "h", fn);
	sub("_DEVICE", "d", fn);
	sub("_HS", "h", fn);
	sub("_FS", "f", fn);
	FILE[nfiles] = fn;
}

$1 == "REGISTER" {
	name = $2;
	address = sprintf("%6s", substr($4, 3));
	size = $5;
	REGNAME[nfiles, address] = name;
	REGSIZE[nfiles, address] = size;
	ADDR[address] = 1;
}

$1 == "-" && $3 == ":" && $5 == ".." {
	name = $2;
	low = $4;
	high = $6;
	for(i = low; i <= high; i = i + 1) {
		FIELDNAME[nfiles, address, i] = name;
	}
}

END {
	# trying to group equal
	ngroups = 1;
	GROUPSIZE[1] = 1;
	GROUP[1] = 1;
	FILEG[1, 1] = 1;
	max_groupsize = 1;
	for(f = 2; f <= nfiles; f = f + 1) {
		for(g = 1; g <= ngroups; g = g + 1) {
			fg = FILEG[g, 1];
			#print f " vs " fg " (" g ")" > "/dev/stderr"
			equal = 1;
			for(address in ADDR) {
				if(REGNAME[f, address] != REGNAME[fg, address] || REGSIZE[f, address] != REGSIZE[fg, address]) {
					equal = 0;
				} else {
					for(b = 0; b < REGSIZE[f, address]; b = b + 1) {
						if(FIELDNAME[f, address, b] != FIELDNAME[fg, address, b]) {
							equal = 0;
							break;
						}
					}
				}
			}
			if(equal == 1) {
				GROUPSIZE[g] = GROUPSIZE[g] + 1;
				GROUP[f] = g;
				FILEG[g, GROUPSIZE[g]] = f;
				if(GROUPSIZE[g] > max_groupsize) {
					max_groupsize = GROUPSIZE[g];
				}
				break;
			}
		}
		if(equal == 0) {
			ngroups = ngroups + 1;
			GROUPSIZE[ngroups] = 1;
			GROUP[f] = ngroups;
			FILEG[ngroups, 1] = f;
		}
	}

	if(ngroups == 1) {
		print "All identical";
		exit;
	}

	column = 12; delim = " ";
	fc = 6;
	print "max_groupsize " max_groupsize;
	printf("g %-"fc"."fc"s", "---");
	for(g = 1; g <= ngroups; g = g + 1) {
		printf(delim "%-"column"."column"s", g);
	}
	printf("\n");
	for(n = 1; n <= max_groupsize; n = n + 1) {
		printf("f %-"fc"."fc"s", "---");
		for(g = 1; g <= ngroups; g = g + 1) {
			f = FILEG[g, n];
			printf(delim "%-"column"."column"s", substr(FILE[f],length(FILE[f])-column+1));
		}
		printf("\n");
	}
	n = asorti(ADDR);
	for(a = 1; a <= n; a = a + 1) {
		address = ADDR[a];
		printf("R %-"fc"."fc"s", address);
		equal = 1;
		max_size = 0;
		for(g = 1; g <= ngroups; g = g + 1) {
			f = FILEG[g, 1];
			printf(delim "%-"column"."column"s", REGNAME[f, address]);
			if(REGSIZE[f, address] > max_size) {
				max_size = REGSIZE[f, address]
			}
			if(REGSIZE[f, address] != REGSIZE[1, address]) {
				#print "Size mismatch " REGNAME[f, address] > "/dev/stderr"
				equal = 0;
			} else {
				for(b = 0; b < REGSIZE[f, address]; b = b + 1) {
					if(FIELDNAME[f, address, b] != FIELDNAME[1, address, b]) {
						equal = 0;
						#print "Field mismatch " f " " address " " b " " REGNAME[f, address] "." FIELDNAME[f, address, b] " "  FIELDNAME[1, address, b]> "/dev/stderr";
						break;
					}
				}
			}
		}
		printf("\n");
		#print "equal " equal > "/dev/stderr"
		if(equal == 0) {
			for(b = 0; b < max_size; b = b + 1) {
				printf("  % "fc"."fc"s", b);
				split ("", FIC);
				FIC[""] = "";
				ficnt = 0;
				for(g = 1; g <= ngroups; g = g + 1) {
					f = FILEG[g, 1];
					fin = FIELDNAME[f, address, b];
					if(!(fin in FIC)) {
						FIC[fin] = sprintf("%c[%dm", 27, 97 - ficnt);
						ficnt = ficnt + 1;
						if(ficnt > 7) {
							ficnt = 0;
						}
					}
					printf(delim "%s%-"column"."column"s", FIC[fin], fin);
				}
				printf("%c[0m\n", 27); # reset color
			}
		}
	}
}