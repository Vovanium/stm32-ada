with Ada.Real_Time; use Ada.Real_Time;
with Board.LCD.SPI_IO;
use Board;

with STM32.GPIO;

procedure SPI_LCD is

	Period : constant Time_Span := Milliseconds(2000);
	Now    : Time := Clock;

	--X, Y   : Integer := 0;
	--Colour : Integer := 0;

	Scale : Float := 1.0/60.0;
	CRe, CIm    : Float;
	ZRe, ZIm, T : Float;
	N : Integer;
begin
	-- LED to see what's happen
	LED.Enable;

	LED.Set_MODER   (STM32.GPIO.Output_Mode);
	LED.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED.Set (False);

	LED_2.Enable;

	LED_2.Set_MODER   (STM32.GPIO.Output_Mode);
	LED_2.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED_2.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED_2.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED_2.Set (False);

	loop

		for Y in Integer range 0 .. 319 loop
			for X in Integer range 0 .. 239 loop
				CRe := Float(- Y + 160) * Scale - 1.403;
				CIm := Float(X - 120) * Scale;
				N := 0;
				ZRe := CRe;
				ZIm := CIm;
				while ZRe * ZRe + ZIm * ZIm < 4.0 and N < 256 loop
					T   := ZRe * ZRe - ZIm * ZIm + CRe;
					ZIm := 2.0 * ZRe * ZIm + CIm;
					ZRe := T;
					N := N + 1;
				end loop;
				if N = 256 then
					LCD.SPI_IO.Plot_24 (X, Y, 0, 0, 0);
				else
					LCD.SPI_IO.Plot_24 (X, Y, 255 - N, N, 255 - N);
				end if;
			end loop;
		end loop;

		Scale := Scale * 0.63;

		Now := Clock + Period;
		delay until Now;

		--LED.Set (not LED_On);

	end loop;


end SPI_LCD;