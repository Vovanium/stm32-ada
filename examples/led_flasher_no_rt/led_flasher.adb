with Board; use Board;

with STM32.Reset_and_Clock; use STM32.Reset_and_Clock;
with STM32.GPIO;
use  STM32.GPIO;


procedure LED_Flasher is

	procedure Dly (X : Positive) is
	begin
		for J in 1 .. X loop
			LED_RCC_EN := True;
		end loop;
	end;
begin
	LED.Enable;

	LED.Set_MODER (Output_Mode);
	LED.Set_OTYPER (Push_Pull_Type);
	LED.Set_OSPEEDR (Very_High_Speed);
	LED.Set_PUPDR (No_Pull);
	loop
		LED.Set (True);
		Dly (1000000);
		LED.Set (False);
		Dly (3000000);
	end loop;
end Led_Flasher;

