with STM32.GPIO;
with Board; use Board;
with Board.UART.IO;
with Board.Memory; -- SDRAM initialization is here!

with Ada.Real_Time; use Ada.Real_Time;
with Interfaces;    use Interfaces;
with System, System.Storage_Elements;
use  System, System.Storage_Elements;

procedure SDRAM_Test is

	Now: Time := Clock;

	type Test_Type is array (Integer range 0 .. Board.Memory.Memory_Size / 32 - 1) of Unsigned_32 with Size => Board.Memory.Memory_Size;

	M : Test_Type with Import, Address => Board.Memory.Memory_Base;

	procedure Expect (Index : Integer; X, Y : Unsigned_32) is
	begin
		LED.Set   (Y =  X);
		LED_2.Set (Y /= X);

		if Y /= X then
			UART.IO.Transmit ("Error @" & Integer'Image (Index)
			& " = " & Unsigned_32'Image (Y)
			& " /= " & Unsigned_32'Image (X)
			& ASCII.CR & ASCII.LF);
		end if;
	end;

	procedure Memory_Test (X : Unsigned_32) is
	begin
		UART.IO.Transmit("SDRAM constant" & Unsigned_32'Image (X));
		for I in Integer range M'Range loop
			M (I) := X;
		end loop;

		UART.IO.Transmit (" ... Filled" & ASCII.CR & ASCII.LF);

		for I in Integer range M'Range loop
			Expect (I, X, M (I));
		end loop;
	
		UART.IO.Transmit ("SDRAM Tested" & ASCII.CR & ASCII.LF);

	end Memory_Test;

	procedure Memory_Test_I is
	begin
		UART.IO.Transmit("SDRAM address");
		for I in Integer range M'Range loop
			M (I) := Unsigned_32 (I);
		end loop;

		UART.IO.Transmit (" ... Filled" & ASCII.CR & ASCII.LF);

		for I in Integer range M'Range loop
			Expect (I, Unsigned_32 (I), M (I));
		end loop;
	
		UART.IO.Transmit ("SDRAM Tested" & ASCII.CR & ASCII.LF);
	end;

begin

	LED.Enable;

	LED.Set_MODER   (STM32.GPIO.Output_Mode);
	LED.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED.Set (False);

	LED_2.Enable;

	LED_2.Set_MODER   (STM32.GPIO.Output_Mode);
	LED_2.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED_2.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED_2.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED_2.Set (False);

	UART.IO.Transmit ("SDRAM Initialized" & ASCII.CR & ASCII.LF);

	-- Finally testing

	loop

		Memory_Test (16#0000_0000#);
		Memory_Test (16#FFFF_FFFF#);
		Memory_Test (16#0000_FFFF#);
		Memory_Test (16#FFFF_0000#);
		Memory_Test (16#00FF_00FF#);
		Memory_Test (16#FF00_FF00#);
		Memory_Test (16#0F0F_0F0F#);
		Memory_Test (16#F0F0_F0F0#);
		Memory_Test (16#3333_3333#);
		Memory_Test (16#CCCC_CCCC#);
		Memory_Test (16#5555_5555#);
		Memory_Test (16#AAAA_AAAA#);
		Memory_Test (16#9669_6996#);
		Memory_Test (16#6996_9669#);
		Memory_Test_I;

	end loop;

end SDRAM_Test;
