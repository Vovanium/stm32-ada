with Board; use Board;
with Board.UART.IO;

with Ada.Real_Time;     use Ada.Real_Time;
--with ASCII;

procedure Hello_World is

	Baud : constant := 115_200;

	Period: constant Time_Span := Milliseconds(1000);
	Now: Time := Clock;

begin
	Board.UART.IO.Set_Speed (Speed => Baud);

	loop

		UART.IO.Transmit ("Hello World!" & ASCII.CR & ASCII.LF);

		Now := Now + Period;
		delay until Now;

		UART.IO.Transmit ("Goodbye World!" & ASCII.CR & ASCII.LF);

		Now := Now + Period;
		delay until Now;
	end loop;

end Hello_World;
