with Board; use Board;
with Board.UART.IO;

--with Ada.Real_Time;     use Ada.Real_Time;
--with ASCII;

procedure Number_Game is

	subtype Number is Integer range 1 .. 100;
	subtype Number_Guess is Number'Base range Number'First - 1 .. Number'Last + 1;

	Bottom, Top, Sect : Number_Guess;

	Input : Character;

begin
	loop
		Bottom := Number'First;
		Top := Number'Last;

		UART.IO.Transmit ("Choose a number between "
		& Number'Image (Bottom) & " and " & Number'Image (Top)
		& " I'll guess it"
		& ASCII.CR & ASCII.LF
		& "Press any key when done."
		& ASCII.CR & ASCII.LF);

		UART.IO.Receive(Input);

		while Top > Bottom loop
			Sect := (Top + Bottom) / 2;
			UART.IO.Transmit ("Is it equal to "
			& Number'Image (Sect)
			& " (press =), less (press <), or greater (press >)?"
			& ASCII.CR & ASCII.LF);

			UART.IO.Receive (Input);

			case Input is
				when '=' =>
					Top := Sect;
					Bottom := Sect;
				when '<' =>
					Top := Sect - 1;
				when '>' =>
					Bottom := Sect + 1;
				when others =>
					UART.IO.Transmit ("Didn't understand"
					& ASCII.CR & ASCII.LF);
			end case;
		
		end loop;
		if Top = Bottom then
			UART.IO.Transmit ("Your number is "
			& Number'Image (Top)
			& ASCII.CR & ASCII.LF);
		else
			UART.IO.Transmit ("There's no such number, you're joking"
			& ASCII.CR & ASCII.LF);
		end if;
	end loop;
end Number_Game;
