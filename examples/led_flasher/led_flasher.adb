with Board; use Board;

with STM32.Reset_and_Clock; use STM32.Reset_and_Clock;
with STM32.GPIO;
use  STM32.GPIO;

with Ada.Real_Time;         use Ada.Real_Time;

procedure LED_Flasher is
	Period  : constant Time_Span := Milliseconds(250);
	On_Time : constant Time_Span := Milliseconds(10);
	Now     : Time := Clock;
begin
	LED.Enable;

	LED.Set_MODER (Output_Mode);
	LED.Set_OTYPER (Push_Pull_Type);
	LED.Set_OSPEEDR (Very_High_Speed);
	LED.Set_PUPDR (No_Pull);
	loop
		LED.Set (True);
		delay until Now + On_Time;
		LED.Set (False);
		Now := Now + Period;
		delay until Now;
	end loop;
end Led_Flasher;

