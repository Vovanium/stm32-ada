--
-- This example runs simple demo on the LCD screen.
-- It uses
-- * SPI to configure LCD internal controller,
-- * FMC to store frame buffer,
-- * LTDC to feed the pixeld in, and
-- * DMA2D to draw.
-- Also UART is used to output technical data while initialized

with Ada.Real_Time;
use  Ada.Real_Time;

with Board.LCD.SPI_IO;
with Board.LCD.Controller;
with Board.UART.IO;

with System.Storage_Elements;
use  type System.Storage_Elements.Storage_Offset;

with Pixels, Init_Layer, Put_Frequencies;
with Blit;

procedure LCD_Demo is
	Now    : Time := Clock;

	Frame : constant Pixels.Pixel_Array_Access := new Pixels.Pixel_Array (0 .. 319, 0 .. 239);
begin
	Put_Frequencies;

	Board.LCD.SPI_IO.Initialize_RGB_Interface;

	Init_Layer (Frame);

	--Board.LCD.Controller.Set_Background(255, 255, 255);

	Board.UART.IO.Transmit ("Pitch = "
	& System.Storage_Elements.Storage_Offset'Image (Frame (1, 0)'Address - Frame (0, 0)'Address)
	& ASCII.CR & ASCII.LF);

	Board.UART.IO.Transmit ("Frame (0,0)'Address = "
	& System.Storage_Elements.Integer_Address'Image (System.Storage_Elements.To_Integer (Frame (0, 0)'Address))
	& ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("Filling frame" & ASCII.CR & ASCII.LF);

	for X in 0 .. 239 loop
		for Y in 0 .. 319 loop
			--Board.UART.IO.Transmit ("@ (" & Integer'Image (X) & " " & Integer'Image (Y) & ")" & ASCII.CR & ASCII.LF);
			--Frame (Y, X) := (R => 0, G => 255, B => 0);
			if (X + Y) mod 50 = 0 or (X - Y) mod 50 = 0 then
				Frame (Y, X) := (R => X * 31 / 239 * 8, G => Y * 255 / 319, B => 0);
			else
				Frame (Y, X) := (others => 0);
			end if;
		end loop;
	end loop;

	Blit.Copy_Rect (Frame.all, 52, 102, 52, 102, 50, 50);

	for Bit in 0 .. 7 loop
		Blit.Blit_Rect (Frame.all, Bit * 30 + 1, Bit * 30 + 25,  1,  25, (R => 2**Bit, G =>      0, B => 0));
		Blit.Blit_Rect (Frame.all, Bit * 30 + 1, Bit * 30 + 25, 31,  55, (R =>      0, G => 2**Bit, B => 0));
		Blit.Blit_Rect (Frame.all, Bit * 30 + 1, Bit * 30 + 25, 61,  85, (R =>      0, G =>      0, B => 2**Bit));
		Blit.Blit_Rect (Frame.all, Bit * 30 + 1, Bit * 30 + 25, 91, 115, (R => 2**Bit, G => 2**Bit, B => 2**Bit));
	end loop;

	Board.UART.IO.Transmit ("Frame filled" & ASCII.CR & ASCII.LF);


	Now := Clock;

	loop
		Blit.Copy_Rect (Frame.all, 100, 200, 100, 200, 102, 102);

		Now := Now + Milliseconds (25);
		delay until Now;
	end loop;
end LCD_Demo;