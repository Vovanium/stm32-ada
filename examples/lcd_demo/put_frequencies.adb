with Board.UART.IO;
with Board.Frequencies;
use  Board.Frequencies;

procedure Put_Frequencies is

begin
	Board.UART.IO.Transmit ("Frequencies" & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("VCO In  " & Integer'Image (VCO_Input_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PLLCLK  " & Integer'Image (PLLCLK_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("HCLK    " & Integer'Image (HCLK_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("USBCLK  " & Integer'Image (USB_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PCLK1   " & Integer'Image (PCLK1_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PCLK2   " & Integer'Image (PCLK2_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PTIM1   " & Integer'Image (APB1_Timer_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PTIM2   " & Integer'Image (APB2_Timer_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("PTIM2   " & Integer'Image (APB2_Timer_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("I2S     " & Integer'Image (I2S_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("SAI1A   " & Integer'Image (SAI1A_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("SAI1B   " & Integer'Image (SAI1B_Clock_Frequency) & ASCII.CR & ASCII.LF);
	Board.UART.IO.Transmit ("LCD     " & Integer'Image (LCD_Clock_Frequency) & ASCII.CR & ASCII.LF);

end Put_Frequencies;
