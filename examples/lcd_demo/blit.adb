with Ada.Interrupts.Names;
use  Ada.Interrupts.Names;
--with System.Storage_Elements;
--use  type System.Storage_Elements.Storage_Offset;

with STM32.Reset_and_Clock;
use  STM32.Reset_and_Clock;

with STM32.Graphic_Accelerator;
use  STM32.Graphic_Accelerator;

with STM32.Graphics;
use  STM32.Graphics;

package body Blit is

	protected Interrupts is

		procedure Handler with Attach_Handler => DMA2D_Interrupt;
		entry Wait_Event (Status : out Interrupt_Array);

	private
		Event : Boolean := False;
		Ints : Interrupt_Array := (others => False);
	end Interrupts;


	protected body Interrupts is
		procedure Handler is
			R : constant Interrupt_Status_Register := DMA2D.ISR;
		begin
			DMA2D.IFCR := R;
			Ints := Ints or R.F;
			Event := True;
		end;
		entry Wait_Event (Status : out Interrupt_Array) when Event is
		begin
			Event := False;
			Status := Ints;
			Ints := (others => False);
			DMA2D.IFCR := (F => Status, others => <>);
		end;
	end Interrupts;

	procedure Blit_Rect (Target : in out Pixels.Pixel_Array; X1, X2, Y1, Y2 : Integer; Color : STM32.Graphics.RGB888_Pixel) is
		Status : Interrupt_Array;
	begin

		DMA2D.OMAR := Target (Y1, X1)'Address;
		DMA2D.OOR  := (LO => Target'Length(2) - (X2 - X1 + 1), others => <>);
		DMA2D.NLR  := (NL => Y2 - Y1 + 1, PL => X2 - X1 + 1, others => <>);

		DMA2D.OPFCCR := (CM => RGB888, others => <>);
		
		DMA2D.OCOLR.RGB888 := Color;
		
		declare
			R : Control_Register := DMA2D.CR;
		begin
			R.MODE := Register_to_Memory;
			R.START := True;
			DMA2D.CR := R;
		end;

		while DMA2D.CR.START loop
			Interrupts.Wait_Event (Status);
		end loop;
	end Blit_Rect;

	procedure Copy_Rect (Target : in out Pixels.Pixel_Array; X1, X2, Y1, Y2, X1_Source, Y1_Source : Integer) is
		Status : Interrupt_Array;
	begin
		DMA2D.FGMAR := Target (Y1_Source, X1_Source)'Address;
		DMA2D.FGOR  := (LO => Target'Length(2) - (X2 - X1 + 1), others => <>);
		DMA2D.FGPFCCR := (AM => No_Alpha_Modification, CM => RGB888, others => <>);
		
		DMA2D.OMAR := Target (Y1, X1)'Address;
		DMA2D.OOR  := (LO => Target'Length(2) - (X2 - X1 + 1), others => <>);
		DMA2D.NLR  := (NL => Y2 - Y1 + 1, PL => X2 - X1 + 1, others => <>);

		DMA2D.OPFCCR := (CM => RGB888, others => <>);
		
		declare
			R : Control_Register := DMA2D.CR;
		begin
			R.MODE := Memory_to_Memory;
			R.START := True;
			DMA2D.CR := R;
		end;

		while DMA2D.CR.START loop
			Interrupts.Wait_Event (Status);
		end loop;
	end;

begin

	RCC.AHB1ENR (Index.DMA2D) := True;
	RCC.AHB1RSTR (Index.DMA2D) := True;
	RCC.AHB1RSTR (Index.DMA2D) := False;

	declare
		R : Control_Register := DMA2D.CR;
	begin
		R.E (TCI) := True;
		-- Enable other interrupts here if required
		DMA2D.CR := R;
	end;
	
end Blit;