with STM32.Graphics;
with Pixels;

package Blit is

	procedure Blit_Rect (Target : in out Pixels.Pixel_Array; X1, X2, Y1, Y2 : Integer; Color : STM32.Graphics.RGB888_Pixel);
	procedure Copy_Rect (Target : in out Pixels.Pixel_Array; X1, X2, Y1, Y2, X1_Source, Y1_Source : Integer);

end Blit;
