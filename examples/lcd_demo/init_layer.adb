with STM32.Graphics;
with STM32.LCD_TFT;
use  STM32.LCD_TFT;

with System.Storage_Elements;
use  type System.Storage_Elements.Storage_Offset;

with Pixels;
use  Pixels;

procedure Init_Layer (Frame : access Pixel_Array) is

begin

	declare
		R : Layer_Horizontal_Position_Register := LTDC.L (1).WHPCR;
		Offset : constant Integer := LTDC.BPCR.HW + 1;
	begin
		R.WHSTPOS := 0 + Offset;
		R.WHSPPOS := 240 + Offset - 1;
		LTDC.L (1).WHPCR := R;
	end;

	declare
		R : Layer_Vertical_Position_Register := LTDC.L (1).WVPCR;
		Offset : constant Integer := LTDC.BPCR.VH + 1;
	begin
		R.WVSTPOS := 0 + Offset;
		R.WVSPPOS := 320 + Offset - 1;
		LTDC.L (1).WVPCR := R;
	end;

	declare	
		R : Pixel_Format_Configuration_Register := LTDC.L (1).PFCR;
	begin
		R.PF := STM32.Graphics.RGB888;
		LTDC.L (1).PFCR := R;
	end;

	declare
		C : Color_Register := LTDC.L (1).DCCR;
	begin
		C.R := 128;
		C.G := 0;
		C.B := 128;
		LTDC.L (1).DCCR := C;
	end;

	LTDC.L (1).CACR := 255;

	declare
		R : Layer_Blending_Factors_Config_Register := LTDC.L (1).BFCR;
	begin
		R.BF1 := Constant_Alpha;
		R.BF2 := One_Minus_Constant_Alpha;
		LTDC.L (1).BFCR := R;
	end;

	LTDC.L (1).CFBAR := Frame (0, 0)'Address;
	declare
		R : Layer_Buffer_Length_Register := LTDC.L (1).CFBLR;
	begin
		R.CFBP := Integer (Frame (1, 0)'Address - Frame (0, 0)'Address);
		R.CFBLL := 240 * 3 + 3;
		LTDC.L (1).CFBLR := R;
	end;

	LTDC.L (1).CFBLNR := 320;

	declare
		R : Layer_Control_Register := LTDC.L (1).CR;
	begin
		R.LEN := True;
		LTDC.L (1).CR := R;
	end;

	LTDC.SRCR := (IMR => True, others => <>);

end Init_Layer;
