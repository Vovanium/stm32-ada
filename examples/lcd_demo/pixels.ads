with Board.Memory;

with Simple_Pools;
use  Simple_Pools;

with STM32.Graphics;

with System.Storage_Elements;
use  System.Storage_Elements;

package Pixels is

	SDRAM_Pool : Simple_Pool := Make_Simple_Pool (
		Board.Memory.Memory_Base,
		Board.Memory.Memory_Size / System.Storage_Unit);

	type Pixel_Array is array (Integer range <>, Integer range <>) of aliased STM32.Graphics.RGB888_Pixel with Component_Size => 24;

	type Pixel_Array_Access is access Pixel_Array
	with Storage_Pool => SDRAM_Pool;

end Pixels;
