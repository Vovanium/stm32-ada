with STM32.GPIO.Ports.Names;
use  STM32.GPIO;

package Board is

	package LED is new Ports.Names.Port_D.Boolean_Port (3, Invert => True);

	APB2_Frequency : constant := 84_000_000; -- Set by board support
end Board;