with System.Storage_Elements;
with System.Storage_Pools; use System.Storage_Pools;

package Simple_Pools is
	-- A sbrk-like simple storage pool
	-- only allocate, do not release
	-- used for pool demostration only

	type Simple_Pool is new Root_Storage_Pool with private;

	procedure Allocate (
		Pool                     : in out Simple_Pool;
		Storage_Address          :    out System.Address;
		Size_In_Storage_Elements : in     System.Storage_Elements.Storage_Count;
		Alignment                : in     System.Storage_Elements.Storage_Count
	);
	procedure Deallocate (
		Pool                     : in out Simple_Pool;
		Storage_Address          : in     System.Address;
		Size_In_Storage_Elements : in     System.Storage_Elements.Storage_Count;
		Alignment                : in     System.Storage_Elements.Storage_Count
	);
	function Storage_Size (
		Pool                     : Simple_Pool
	) return System.Storage_Elements.Storage_Count;

	function Make_Simple_Pool (
		Base_Address             : System.Address;
		Size_In_Storage_Elements : System.Storage_Elements.Storage_Count
	) return Simple_Pool;

private

	type Simple_Pool is new Root_Storage_Pool with record
		Current : System.Address;
		Top     : System.Address;
	end record;

end Simple_Pools;