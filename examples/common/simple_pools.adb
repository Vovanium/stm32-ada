with System.Storage_Elements;
use System.Storage_Elements;
package body Simple_Pools is
	use type System.Storage_Elements.Storage_Offset;
	
	procedure Allocate (
		Pool                     : in out Simple_Pool;
		Storage_Address          :    out System.Address;
		Size_In_Storage_Elements : in     System.Storage_Elements.Storage_Count;
		Alignment                : in     System.Storage_Elements.Storage_Count
	) is
		This, New_Current : System.Address;
		
	begin
		This := Pool.Current + Alignment - 1;
		This := This - This mod Alignment;
		New_Current := This + Size_In_Storage_Elements;

		if New_Current - Pool.Top > 0 then
			raise Storage_Error with ("Insufficient memory");
		end if;

		Pool.Current := New_Current;
		Storage_Address := This;
	end;

	procedure Deallocate (
		Pool                     : in out Simple_Pool;
		Storage_Address          : in     System.Address;
		Size_In_Storage_Elements : in     System.Storage_Elements.Storage_Count;
		Alignment                : in     System.Storage_Elements.Storage_Count
	) is
	begin
		null; -- does not deallocate
	end;

	function Storage_Size (
		Pool                     : Simple_Pool
	) return System.Storage_Elements.Storage_Count is
	begin
		return Pool.Top - Pool.Current;
	end;

	function Make_Simple_Pool (
		Base_Address             : System.Address;
		Size_In_Storage_Elements : System.Storage_Elements.Storage_Count
	) return Simple_Pool is
	begin
		return Simple_Pool'(Root_Storage_Pool with
			Current => Base_Address,
			Top => Base_Address + Size_In_Storage_Elements
		);
	end;


end Simple_Pools;