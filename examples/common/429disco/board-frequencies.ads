with STM32.Reset_and_Clock;
with STM32.Reset_and_Clock.Frequencies;

package Board.Frequencies is new STM32.Reset_and_Clock.Frequencies (
	HSE_Frequency => HSE_Frequency,
	RCC => STM32.Reset_and_Clock.RCC
);