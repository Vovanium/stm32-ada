with STM32.USARTs, STM32.Reset_and_Clock;
use  STM32.USARTs, STM32.Reset_and_Clock;

package Board.UART is

	package Port renames Ports.Names.Port_A;
	package TX_Pin is new Port.Boolean_Port (9);
	package RX_Pin is new Port.Boolean_Port (10);

	Port_RCC_EN  : Boolean renames RCC.AHB1ENR (Index.GPIOA);
	Port_RCC_RST : Boolean renames RCC.AHB1RSTR (Index.GPIOA);

	AF : Alternate_Function renames Alternate_Functions.USART1;

	Module  : USART_Registers renames USART1;

	RCC_EN  : Boolean renames RCC.APB2ENR (Index.USART1);
	RCC_RST : Boolean renames RCC.APB2RSTR (Index.USART1);

end Board.UART;
