with Ada.Real_Time;
use  Ada.Real_Time;

package body Board.LCD.Generic_IO is
	type Unsigned_8_Array is array (Integer range <>) of Interfaces.Unsigned_8;

	procedure Write_Command (Command : ILI9341.Command; Data_1 : Interfaces.Unsigned_8) is
	begin
		Write_Command (Command);
		Write_Data (Data_1);
	end;

	procedure Write_Command (Command : ILI9341.Command; Data : Unsigned_8_Array) is
	begin
		Write_Command (Command);
		for I of Data loop
			Write_Data (I);
		end loop;
	end;

	procedure Initialize_Controller is
		Now    : Time := Clock;
	begin
		-- Initialization procedure per
		-- https://github.com/mongoose-os-libs/ili9341-spi
		-- released under Apache licence

		Write_Command (ILI9341.SWRESET);
		Now := Clock + Milliseconds (5);
		delay until Now;

		--Write_Command (ILI9341.Undocumented_CA,           (16#C3#, 16#08#, 16#50#));
		Write_Command (ILI9341.Power_Control_A,           (16#39#, 16#2C#, 16#00#, 16#34#, 16#02#));
		-- Vcore = 1.6, DDVDH = 5.6 (default)
		Write_Command (ILI9341.Power_Control_B,           (16#00#, 16#C1#, 16#30#));
		-- PCEQ = 1, DRV_ena = 0, Power_Control = 0, DRV_vml = 0, DRV_vmh = 0, DC_ena = 1
		--Write_Command (ILI9341.Undocumented_EF,           (16#03#, 16#80#, 16#02#));
		Write_Command (ILI9341.Driver_Timing_Control_A,   (16#85#, 16#00#, 16#78#));
		-- NOW = 1, EQ = 0, CR = 0, precharge = 2unit
		Write_Command (ILI9341.Driver_Timing_Control_B,   (16#00#, 16#00#));
		-- T1 = 0, T2 = 0, T3 = 0, T3 = 0
		Write_Command (ILI9341.Power_On_Sequence_Control, (16#64#, 16#03#, 16#12#, 16#81#));
		-- CP1 = 1frame, CP23 = 3frame, En_vcl = 1st, En_ddvdh = 4th, En_vgh = 2nd, En_vgl = 3rd, DDVDH = En
		Write_Command (ILI9341.Pump_Ratio_Control,        (16#20#));
		-- DDVDH = 2xVCI
		Write_Command (ILI9341.PWCTRL1,                   (16#23#));
		-- GVDD level = 4.45 V
		Write_Command (ILI9341.PWCTRL2,                   (16#10#));
		-- VGH = VCIx7, VGL = VCIx4
		Write_Command (ILI9341.VMCTRL1,                   (16#3E#, 16#28#));
		-- VCOMH = 4.250, VCOML = -1.500
		Write_Command (ILI9341.VMCTRL2,                   (16#86#));
		-- nVM = 1, VCOMH = VMH - 58
		Write_Command (ILI9341.MADCTL,                    (16#48#));
		-- MY = 0, MX = 1, MV = 0, ML = 0, BGR = 1, MH = 0
		Write_Command (ILI9341.PIXSET,                    (16#66#));
		-- RGB = 18bpp, MCU = 18bpp
		Write_Command (ILI9341.DINVOFF);
		Write_Command (ILI9341.FRMCTR1,                   (16#00#, 16#13#));
		-- DivA = fosc, RTNA = 19 clocks
		Write_Command (ILI9341.DISCTRL,                   (16#08#, 16#82#, 16#27#, 16#00#));
		-- PTG Intervalscan, PT = V63/V0/VCOMH/L, REV = White, ISC = 5 fr, GS = G1>G320,W, NL = 320, Fosc = DOTCLK/2
		Write_Command (ILI9341.PTLAR,                     (16#00#, 16#00#, 16#01#, 16#3F#));
		-- SR = 0, ER = 319
		Write_Command (ILI9341.Enable_3G,                 (16#02#));
		-- 3G = disable
		Write_Command (ILI9341.GAMSET,                    (16#01#));
		-- Curve = G2.2
		Write_Command (ILI9341.PGAMCTRL,                  (16#0F#, 16#31#, 16#2B#, 16#0C#, 16#0E#,
		   16#08#, 16#4E#, 16#F1#, 16#37#, 16#07#, 16#10#, 16#03#, 16#0E#, 16#09#, 16#00#));
		Write_Command (ILI9341.NGAMCTRL,                  (16#00#, 16#0E#, 16#14#, 16#03#, 16#11#,
		   16#07#, 16#31#, 16#C1#, 16#48#, 16#08#, 16#0F#, 16#0C#, 16#31#, 16#36#, 16#0F#));
		-- 16 bit?
		Now := Clock + Milliseconds (120);
		delay until Now;
		Write_Command (ILI9341.SLPOUT);
		Write_Command (ILI9341.DISPON);
	end Initialize_Controller;

	procedure Initialize_RGB_Interface is
	begin
		Write_Command (ILI9341.IFMODE,                   (16#C2#));
		-- EPL = true, DPL = Falling, HSPL = low, VSPL = low, RCM = 10, bypass = memory
		Write_Command (ILI9341.IFCTL,                    (16#01#, 16#00#, 16#06#));
		-- WEMODE = 1, BGR_EOR = 0, MV_EOR = 0, MX_EOR = 0, MY_EOR = 0, MDT = 0, EPF = MSB, RIM = 18/16, RM = RGB, DM = RGB, ENDIAN = MSB first
		Write_Command (ILI9341.PIXSET,                   (16#66#));
	end Initialize_RGB_Interface;

	procedure Plot_24 (X, Y, R, G, B: Integer) is
		-- Draw a pixel;
	begin
		Write_Command (ILI9341.CASET, (
			Interfaces.Unsigned_8 (X / 256),
			Interfaces.Unsigned_8 (X mod 256),
			Interfaces.Unsigned_8 ((X + 1) / 256),
			Interfaces.Unsigned_8 ((X + 1) mod 256)));
		Write_Command (ILI9341.PASET, (
			Interfaces.Unsigned_8 (Y / 256),
			Interfaces.Unsigned_8 (Y mod 256),
			Interfaces.Unsigned_8 ((Y + 1) / 256), 
			Interfaces.Unsigned_8 ((Y + 1) mod 256)));
		Write_Command (ILI9341.RAMWR, (
			Interfaces.Unsigned_8 (G),
			Interfaces.Unsigned_8 (B),
			Interfaces.Unsigned_8 (R)));
	end;
end Board.LCD.Generic_IO;