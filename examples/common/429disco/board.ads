with STM32.GPIO.Ports.Names;
use  STM32.GPIO;

package Board is

	-- LED

	package LED is new Ports.Names.Port_G.Boolean_Port (13, Invert => False);

	package LED_2 is new Ports.Names.Port_G.Boolean_Port (14, Invert => False);

	-- More internals
	HSE_Frequency : constant := 8_000_000;
	APB2_Frequency : constant := 90_000_000; -- Set by board support

end Board;