with STM32.SPIs, STM32.Reset_and_Clock;
use  STM32.SPIs, STM32.Reset_and_Clock;

package Board.LCD is

	package CSX_Pin is new Ports.Names.Port_C.Boolean_Port (2);

	package DCX_Pin is new Ports.Names.Port_D.Boolean_Port (13);

	package TE_Pin  is new Ports.Names.Port_D.Boolean_Port (11);

	package SCL_Pin is new Ports.Names.Port_F.Boolean_Port (7);

	package SDA_Pin is new Ports.Names.Port_F.Boolean_Port (9);

	SPI_Module : STM32.SPIs.SPI_Registers renames SPI5;
	SPI_AF     : STM32.GPIO.Alternate_Function renames STM32.GPIO.Alternate_Functions.SPI5;

	SPI_RCC_EN  : Boolean renames RCC.APB2ENR  (Index.SPI5);
	SPI_RCC_RST : Boolean renames RCC.APB2RSTR (Index.SPI5);

end Board.LCD;