with Ada.Unchecked_Conversion;

with Board.SPI.IO;
with Board.LCD.Generic_IO;
use  Board;

with ILI9341;

with Interfaces;

package body Board.LCD.SPI_IO is

	procedure Initialize_SPI is
	begin
		-- First initialize SPI to communicate onboard controller
		SPI_RCC_EN  := True;
		SPI_RCC_RST := True;
		SPI_RCC_RST := False;

		declare
			R : STM32.SPIs.Control_Register_1 := SPI_Module.CR1;
		begin
			R.BR       := STM32.SPIs.PCLK_DIV_16;
			R.CPHA     := STM32.SPIs.Late_Clock;
			R.CPOL     := STM32.SPIs.Positive_Clock;
			R.DFF      := STM32.SPIs.Frame_8_Bit;
			R.LSBFIRST := False;
			R.SSM      := True;
			R.SSI      := True;
			R.MSTR     := STM32.SPIs.Master;
			R.SPE      := True;
			R.RXONLY   := False;
			R.BIDIMODE := STM32.SPIs.Unidirectional;
			R.BIDIOE   := True;
			R.CRCEN    := False;
			R.CRCNEXT  := False;
		
			SPI_Module.CR1 := R;
		end;

		declare
			R : STM32.SPIs.Control_Register_2 := SPI_Module.CR2;
		begin
			R.FRF := STM32.SPIs.Motorola_Mode;
			SPI_Module.CR2 := R;
		end;

		-- Enabling pins
		RCC.AHB1ENR (Index.GPIOC) := True;
		RCC.AHB1ENR (Index.GPIOD) := True;
		RCC.AHB1ENR (Index.GPIOF) := True;

		CSX_Pin.Set_MODER   (Output_Mode);
		CSX_Pin.Set_OSPEEDR (Very_High_Speed);
		CSX_Pin.Set_OTYPER  (Push_Pull_Type);
		CSX_Pin.Set (True);

		DCX_Pin.Set_MODER   (Output_Mode);
		DCX_Pin.Set_OSPEEDR (Very_High_Speed);
		DCX_Pin.Set_OTYPER  (Push_Pull_Type);
		DCX_Pin.Set (True);

		SCL_Pin.Set_AFR     (SPI_AF);
		SCL_Pin.Set_MODER   (Alternate_Mode);
		SCL_Pin.Set_OSPEEDR (Very_High_Speed);
		SCL_Pin.Set_OTYPER  (Push_Pull_Type);
		SCL_Pin.Set_PUPDR   (Pull_Up);

		SDA_Pin.Set_AFR     (SPI_AF);
		SDA_Pin.Set_MODER   (Alternate_Mode);
		SDA_Pin.Set_OSPEEDR (Very_High_Speed);
		SDA_Pin.Set_OTYPER  (Push_Pull_Type);
		SDA_Pin.Set_PUPDR   (Pull_Up);
	end;

	procedure Command (Command : ILI9341.Command) is
		function To_Unsigned_8 is new Ada.Unchecked_Conversion (ILI9341.Command, Interfaces.Unsigned_8);
	begin
		CSX_Pin.Set (False);
		DCX_Pin.Set(False);
		SPI.IO.Transmit (LCD.SPI_Module, To_Unsigned_8 (Command));
		CSX_Pin.Set (True);
	end;

	procedure Data (Data : Interfaces.Unsigned_8) is
	begin
		CSX_Pin.Set (False);
		DCX_Pin.Set (True);
		SPI.IO.Transmit (LCD.SPI_Module, Data);
		CSX_Pin.Set (True);
	end;

	package IO is new LCD.Generic_IO(Command, Data);

	procedure Initialize_Controller renames IO.Initialize_Controller;
	procedure Initialize_RGB_Interface renames IO.Initialize_RGB_Interface;
	procedure Plot_24 (X, Y, R, G, B: Integer) renames IO.Plot_24;
begin
	Initialize_SPI;
	Initialize_Controller;

end Board.LCD.SPI_IO;
