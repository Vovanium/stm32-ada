with STM32.Flexible_Memory_Controller, STM32.Reset_and_Clock;
use  STM32.Flexible_Memory_Controller, STM32.Reset_and_Clock;
-- with STM32.RCC; use STM32.RCC;
-- with STM32.GPIO; use STM32.GPIO;
with Ada.Real_Time; use Ada.Real_Time;

package body Board.Memory is

	procedure Send_Command (R : SDRAM.Command_Mode_Register) is
	begin
		FMC.SDCMR := R;
		while FMC.SDSR.BUSY loop
			null;
		end loop;
	end;

	procedure Initialize is
		Now : Time;
	begin
		-- 64 Mbit SDRAM IS42S16400J

		RCC.AHB3ENR (Index.FMC) := True;

		-- Pins first

		RCC.AHB1ENR (Index.GPIOB .. Index.GPIOG) := (others => True);

		-- PB5  CKE1
		-- PB6  NE1

		GPIOB.AFR     (5 .. 6) := (others => Alternate_Functions.FMC);
		GPIOB.MODER   (5 .. 6) := (others => Alternate_Mode);
		GPIOB.OTYPER  (5 .. 6) := (others => Push_Pull_Type);
		GPIOB.OSPEEDR (5 .. 6) := (others => Very_High_Speed);
		GPIOB.PUPDR   (5 .. 6) := (others => No_Pull);
	
		-- PC0  NWE

		GPIOC.AFR     (0) := Alternate_Functions.FMC;
		GPIOC.MODER   (0) := Alternate_Mode;
		GPIOC.OTYPER  (0) := Push_Pull_Type;
		GPIOC.OSPEEDR (0) := Very_High_Speed;
		GPIOC.PUPDR   (0) := No_Pull;

		-- PD0  D2
		-- PD1  D3
		-- PD8  D13
		-- PD9  D14
		-- PD10 D15
		-- PD14 D0
		-- PD15 D1

		GPIOD.AFR     (0 .. 1) := (others => Alternate_Functions.FMC);
		GPIOD.MODER   (0 .. 1) := (others => Alternate_Mode);
		GPIOD.OTYPER  (0 .. 1) := (others => Push_Pull_Type);
		GPIOD.OSPEEDR (0 .. 1) := (others => Very_High_Speed);
		GPIOD.PUPDR   (0 .. 1) := (others => No_Pull);

		GPIOD.AFR     (8 .. 10) := (others => Alternate_Functions.FMC);
		GPIOD.MODER   (8 .. 10) := (others => Alternate_Mode);
		GPIOD.OTYPER  (8 .. 10) := (others => Push_Pull_Type);
		GPIOD.OSPEEDR (8 .. 10) := (others => Very_High_Speed);
		GPIOD.PUPDR   (8 .. 10) := (others => No_Pull);

		GPIOD.AFR     (14 .. 15) := (others => Alternate_Functions.FMC);
		GPIOD.MODER   (14 .. 15) := (others => Alternate_Mode);
		GPIOD.OTYPER  (14 .. 15) := (others => Push_Pull_Type);
		GPIOD.OSPEEDR (14 .. 15) := (others => Very_High_Speed);
		GPIOD.PUPDR   (14 .. 15) := (others => No_Pull);

		-- PE0  NBL0
		-- PE1  NBL1
		-- PE7  D4
		-- PE8  D5
		-- PE9  D6
		-- PE10 D7
		-- PE11 D8
		-- PE12 D9
		-- PE13 D10
		-- PE14 D11
		-- PE15 D12

		GPIOE.AFR     (0 .. 1) := (others => Alternate_Functions.FMC);
		GPIOE.MODER   (0 .. 1) := (others => Alternate_Mode);
		GPIOE.OTYPER  (0 .. 1) := (others => Push_Pull_Type);
		GPIOE.OSPEEDR (0 .. 1) := (others => Very_High_Speed);
		GPIOE.PUPDR   (0 .. 1) := (others => No_Pull);

		GPIOE.AFR     (7 .. 15) := (others => Alternate_Functions.FMC);
		GPIOE.MODER   (7 .. 15) := (others => Alternate_Mode);
		GPIOE.OTYPER  (7 .. 15) := (others => Push_Pull_Type);
		GPIOE.OSPEEDR (7 .. 15) := (others => Very_High_Speed);
		GPIOE.PUPDR   (7 .. 15) := (others => No_Pull);

		-- PF0  A0
		-- PF1  A1
		-- PF2  A2
		-- PF3  A3
		-- PF4  A4
		-- PF5  A5
		-- PF11 NRAS
		-- PF12 A6
		-- PF13 A7
		-- PF14 A8
		-- PF15 A9
	
		GPIOF.AFR     (0 .. 5) := (others => Alternate_Functions.FMC);
		GPIOF.MODER   (0 .. 5) := (others => Alternate_Mode);
		GPIOF.OTYPER  (0 .. 5) := (others => Push_Pull_Type);
		GPIOF.OSPEEDR (0 .. 5) := (others => Very_High_Speed);
		GPIOF.PUPDR   (0 .. 5) := (others => No_Pull);

		GPIOF.AFR     (11 .. 15) := (others => Alternate_Functions.FMC);
		GPIOF.MODER   (11 .. 15) := (others => Alternate_Mode);
		GPIOF.OTYPER  (11 .. 15) := (others => Push_Pull_Type);
		GPIOF.OSPEEDR (11 .. 15) := (others => Very_High_Speed);
		GPIOF.PUPDR   (11 .. 15) := (others => No_Pull);

		-- PG0  A10
		-- PG1  A11
		-- PG4  BA0
		-- PG5  BA1
		-- PG8  CLK
		-- PG15 NCAS

		GPIOG.AFR     (0 .. 1) := (others => Alternate_Functions.FMC);
		GPIOG.MODER   (0 .. 1) := (others => Alternate_Mode);
		GPIOG.OTYPER  (0 .. 1) := (others => Push_Pull_Type);
		GPIOG.OSPEEDR (0 .. 1) := (others => Very_High_Speed);
		GPIOG.PUPDR   (0 .. 1) := (others => No_Pull);

		GPIOG.AFR     (4 .. 5) := (others => Alternate_Functions.FMC);
		GPIOG.MODER   (4 .. 5) := (others => Alternate_Mode);
		GPIOG.OTYPER  (4 .. 5) := (others => Push_Pull_Type);
		GPIOG.OSPEEDR (4 .. 5) := (others => Very_High_Speed);
		GPIOG.PUPDR   (4 .. 5) := (others => No_Pull);

		GPIOG.AFR     (8) := Alternate_Functions.FMC;
		GPIOG.MODER   (8) := Alternate_Mode;
		GPIOG.OTYPER  (8) := Push_Pull_Type;
		GPIOG.OSPEEDR (8) := Very_High_Speed;
		GPIOG.PUPDR   (8) := No_Pull;

		GPIOG.AFR     (15) := Alternate_Functions.FMC;
		GPIOG.MODER   (15) := Alternate_Mode;
		GPIOG.OTYPER  (15) := Push_Pull_Type;
		GPIOG.OSPEEDR (15) := Very_High_Speed;
		GPIOG.PUPDR   (15) := No_Pull;

		-- 1. SDCRx Registers

		declare
			R : SDRAM.Control_Register := FMC.SDCR1;
		begin
			R.SDCLK := SDRAM.Period_2_HCLK;
			R.RBURST := False;
			R.RPIPE := 1;

			FMC.SDCR1 := R;
		end;

		declare
			R : SDRAM.Control_Register := FMC.SDCR2;
		begin
			R.NC := SDRAM.Column_8_Bits;
			R.NR := SDRAM.Row_12_Bits;
			R.MWID := Memory_16_Bits;
			R.NB := SDRAM.Four_Banks;
			R.CAS := 3;
			R.WP := False;

			FMC.SDCR2 := R;
		end;

		-- 2. SDTRx Registers

		declare
			R : SDRAM.Timing_Register := FMC.SDTR1;
		begin
			R.TRC := 7 - 1;
			R.TRP := 2 - 1;

			FMC.SDTR1 := R;
		end;

		declare
			R : SDRAM.Timing_Register := FMC.SDTR2;
		begin
			R.TMRD := 2 - 1;
			R.TXSR := 7 - 1;
			R.TRAS := 4 - 1;
			R.TWR  := 2 - 1;
			R.TRCD := 2 - 1;

			FMC.SDTR2 := R;
		end;

		-- 3. Set MODE to 001 and Target Bank Bits

		Send_Command((
			MODE => SDRAM.Clock_Configuration_Enable,
			CTB1 => False,
			CTB2 => True,
			NRFS => 1 - 1,
			MRD  => 0,
			others => <>
		));

		-- 4. Wait 100 us (see SDRAM datasheet)

		Now := Clock + Milliseconds(100);
		delay until Now;

		-- 5. Send Precharge All

		Send_Command((
			MODE => SDRAM.PALL,
			CTB1 => False,
			CTB2 => True,
			NRFS => 1 - 1,
			MRD  => 0,
			others => <>
		));

		-- 6. Send Auto-refresh

		Send_Command((
			MODE => SDRAM.Auto_Refresh,
			CTB1 => False,
			CTB2 => True,
			NRFS => 4 - 1,
			MRD  => 0,
			others => <>
		));

		-- 7. Configure MRD and send Load Mode Register

		Send_Command((
			MODE => SDRAM.Load_Mode_Register,
			CTB1 => False,
			CTB2 => True,
			NRFS => 1 - 1,
			MRD  => 16#0231#, -- Write_Burst_Single, Standard_Operation, CAS_Latency = 3, Burst_Mode_Sequential, Burst_Length_2
			others => <>
		));

		-- 8. Program refresh rate

		declare
			R : SDRAM.Refresh_Timer_Register := FMC.SDRTR;
		begin
			R.COUNT := 16#056A#;
			FMC.SDRTR := R;
		end;

		-- 9. Mobile SDRAM configure Extended mode register
		-- Not used yet
	end;
begin
	Initialize;
end Board.Memory;