with System;
use  System;
with STM32.Address_Map;

package Board.Memory is
	pragma Elaborate_Body;

	Memory_Base : constant Address := STM32.Address_Map.FMC_Bank_6;

	Memory_Size : constant := 64 * 1024**2; -- 64 Mbit

end Board.Memory;