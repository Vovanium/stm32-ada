with ILI9341;
with Interfaces;

generic
	with procedure Write_Command (Command : ILI9341.Command);
	with procedure Write_Data (Data : Interfaces.Unsigned_8);
package Board.LCD.Generic_IO is
	procedure Initialize_Controller;
	procedure Initialize_RGB_Interface;
	procedure Plot_24 (X, Y, R, G, B: Integer);
end Board.LCD.Generic_IO;