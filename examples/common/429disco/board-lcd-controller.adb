with STM32.LCD_TFT;
use  STM32.LCD_TFT;
with Board.Frequencies;

package body Board.LCD.Controller is
	procedure Initialize_Pins is
	begin
		RCC.AHB1ENR (Index.GPIOA .. Index.GPIOD) := (others => True);
		RCC.AHB1ENR (Index.GPIOF .. Index.GPIOG) := (others => True);

		-- PA3  B5
		-- PA4  VSYNC

		GPIOA.AFR     (3 .. 4) := (others => Alternate_Functions.LTDC_B);
		GPIOA.MODER   (3 .. 4) := (others => Alternate_Mode);
		GPIOA.OTYPER  (3 .. 4) := (others => Push_Pull_Type);
		GPIOA.OSPEEDR (3 .. 4) := (others => Very_High_Speed);
		GPIOA.PUPDR   (3 .. 4) := (others => No_Pull);

		-- PA6  G2

		GPIOA.AFR     (6) := Alternate_Functions.LTDC_B;
		GPIOA.MODER   (6) := Alternate_Mode;
		GPIOA.OTYPER  (6) := Push_Pull_Type;
		GPIOA.OSPEEDR (6) := Very_High_Speed;
		GPIOA.PUPDR   (6) := No_Pull;

		-- PA11 R4
		-- PA12 R5

		GPIOA.AFR     (11 .. 12) := (others => Alternate_Functions.LTDC_B);
		GPIOA.MODER   (11 .. 12) := (others => Alternate_Mode);
		GPIOA.OTYPER  (11 .. 12) := (others => Push_Pull_Type);
		GPIOA.OSPEEDR (11 .. 12) := (others => Very_High_Speed);
		GPIOA.PUPDR   (11 .. 12) := (others => No_Pull);
		
		-- PB0  R3
		-- PB1  R6

		GPIOB.AFR     (0 .. 1) := (others => Alternate_Functions.LTDC_A);
		GPIOB.MODER   (0 .. 1) := (others => Alternate_Mode);
		GPIOB.OTYPER  (0 .. 1) := (others => Push_Pull_Type);
		GPIOB.OSPEEDR (0 .. 1) := (others => Very_High_Speed);
		GPIOB.PUPDR   (0 .. 1) := (others => No_Pull);

		-- PB8  B6
		-- PB9  B7
		-- PB10 G4
		-- PB11 G5

		GPIOB.AFR     (8 .. 11) := (others => Alternate_Functions.LTDC_B);
		GPIOB.MODER   (8 .. 11) := (others => Alternate_Mode);
		GPIOB.OTYPER  (8 .. 11) := (others => Push_Pull_Type);
		GPIOB.OSPEEDR (8 .. 11) := (others => Very_High_Speed);
		GPIOB.PUPDR   (8 .. 11) := (others => No_Pull);

		-- PC6  HSYNC
		-- PC7  G6

		GPIOC.AFR     (6 .. 7) := (others => Alternate_Functions.LTDC_B);
		GPIOC.MODER   (6 .. 7) := (others => Alternate_Mode);
		GPIOC.OTYPER  (6 .. 7) := (others => Push_Pull_Type);
		GPIOC.OSPEEDR (6 .. 7) := (others => Very_High_Speed);
		GPIOC.PUPDR   (6 .. 7) := (others => No_Pull);

		-- PC10 R2

		GPIOC.AFR     (10) := Alternate_Functions.LTDC_B;
		GPIOC.MODER   (10) := Alternate_Mode;
		GPIOC.OTYPER  (10) := Push_Pull_Type;
		GPIOC.OSPEEDR (10) := Very_High_Speed;
		GPIOC.PUPDR   (10) := No_Pull;

		-- PD3  G7

		GPIOD.AFR     (3) := Alternate_Functions.LTDC_B;
		GPIOD.MODER   (3) := Alternate_Mode;
		GPIOD.OTYPER  (3) := Push_Pull_Type;
		GPIOD.OSPEEDR (3) := Very_High_Speed;
		GPIOD.PUPDR   (3) := No_Pull;

		-- PD6  B2

		GPIOD.AFR     (6) := Alternate_Functions.LTDC_B;
		GPIOD.MODER   (6) := Alternate_Mode;
		GPIOD.OTYPER  (6) := Push_Pull_Type;
		GPIOD.OSPEEDR (6) := Very_High_Speed;
		GPIOD.PUPDR   (6) := No_Pull;

		-- PF10 DE

		GPIOF.AFR     (10) := Alternate_Functions.LTDC_B;
		GPIOF.MODER   (10) := Alternate_Mode;
		GPIOF.OTYPER  (10) := Push_Pull_Type;
		GPIOF.OSPEEDR (10) := Very_High_Speed;
		GPIOF.PUPDR   (10) := No_Pull;

		-- PG6  R7
		-- PG7  CLK

		GPIOG.AFR     (6 .. 7) := (others => Alternate_Functions.LTDC_B);
		GPIOG.MODER   (6 .. 7) := (others => Alternate_Mode);
		GPIOG.OTYPER  (6 .. 7) := (others => Push_Pull_Type);
		GPIOG.OSPEEDR (6 .. 7) := (others => Very_High_Speed);
		GPIOG.PUPDR   (6 .. 7) := (others => No_Pull);

		-- PG10 G3
		-- PG11 B3
		-- PG12 B4

		GPIOG.AFR     (10)       := Alternate_Functions.LTDC_A;
		GPIOG.AFR     (11)       := Alternate_Functions.LTDC_B;
		GPIOG.AFR     (12)       := Alternate_Functions.LTDC_A;
		GPIOG.MODER   (10 .. 12) := (others => Alternate_Mode);
		GPIOG.OTYPER  (10 .. 12) := (others => Push_Pull_Type);
		GPIOG.OSPEEDR (10 .. 12) := (others => Very_High_Speed);
		GPIOG.PUPDR   (10 .. 12) := (others => No_Pull);
	end Initialize_Pins;

	procedure Initialize_Pixel_Clock is
		SAI_F : constant := 192_000_000;
		SAI_R : Natural;
		SAI_DIVR : STM32.Reset_and_Clock.PLLSAI_Division_Factor;
		Pixel_F : constant := 6_000_000;
	begin
		-- Disable
		RCC.CR.PLLSAION := False;
		-- Wait SAI disabled
		while RCC.CR.PLLSAIRDY loop
			null;
		end loop;
		-- Configure register
		RCC.PLLSAICFGR.N := Board.Frequencies.PLLSAI_N (SAI_F);
		Board.Frequencies.LCD_R(Pixel_F, SAI_R, SAI_DIVR);
		RCC.PLLSAICFGR.R := SAI_R; --4;
		RCC.DCKCFGR.PLLSAIDIVR := SAI_DIVR; --PLLSAI_Div_8;
		-- Enable
		RCC.CR.PLLSAION := True;
		-- Wait SAI ready
		while not RCC.CR.PLLSAIRDY loop
			null;
		end loop;
	end;

	procedure Initialize_Timings is
	begin
		declare
			R : Frame_Configuration_Register := LTDC.SSCR;
		begin
			R.VH :=  2 - 1;
			R.HW := 10 - 1;
			LTDC.SSCR := R;
			
			R := LTDC.BPCR;
			R.VH :=  4 - 1;
			R.HW := 30 - 1;
			LTDC.BPCR := R;

			R := LTDC.AWCR;
			R.VH := 324 - 1;
			R.HW := 270 - 1;
			LTDC.AWCR := R;

			R := LTDC.TWCR;
			R.VH := 328 - 1;
			R.HW := 280 - 1;
			LTDC.TWCR := R;
		end;

		declare
			R : Global_Control_Register := LTDC.GCR;
		begin
			R.PCPOL := True_Polarity;
			R.DEPOL := Active_Low;
			R.VSPOL := Active_Low;
			R.HSPOL := Active_Low;
			LTDC.GCR := R;
		end;

	end Initialize_Timings;

	procedure Initialize is
	begin
		Initialize_Pins;

		-- Enable LTDC clock
		RCC.APB2ENR  (Index.LTDC) := True;
		RCC.APB2RSTR (Index.LTDC) := True;
		RCC.APB2RSTR (Index.LTDC) := False;

		-- Configure Pixel clock
		Initialize_Pixel_Clock;

		-- Configure synchronous timings
		-- Configure synchronous signals in the LTDC_GCR
		Initialize_Timings;

		-- Configure the background color
		declare
			R : Color_Register := LTDC.BCCR;
		begin
			R.R := 16#AA#;
			R.G := 16#AA#;
			R.B := 16#55#;
			LTDC.BCCR := R;
		end;	
		-- Configure the needed interrupts
		-- Configure the Layer 1/2 parameters
		-- Enable Layer 1/2 and the CLUT
		-- Dithering and color keying
		-- Reload the shadow registers to active register
		LTDC.SRCR := (IMR => True, others => <>);
		-- Enable LCD-TFT controller in the LTDC_GCR
		declare
			R : Global_Control_Register := LTDC.GCR;
		begin
			R.LTDCEN := True;
			LTDC.GCR := R;
		end;
		-- All layer parameters may be modified...
	end Initialize;

	procedure Set_Background (R, G, B : Integer) is
	begin
		declare
			C : Color_Register := LTDC.BCCR;
		begin
			C.R := R;
			C.G := G;
			C.B := B;
			LTDC.BCCR := C;
		end;	
		LTDC.SRCR := (IMR => True, others => <>);
	end;
begin
	Initialize;
end Board.LCD.Controller;
