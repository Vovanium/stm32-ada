--with Interfaces; use Interfaces;
package ILI9341 is

	type Command is (
		NOP,
		SWRESET,
		RDDIDIF,
		RDDST,
		RDDPM,
		RDDMADCTL,
		RDDCOLMOD,
		RDDIM,
		RDDSM,
		RDDSDR,
		SLPIN,
		SLPOUT,
		PTLON,
		NORON,
		DINVOFF,
		DINVON,
		GAMSET,
		DISPOFF,
		DISPON,
		CASET,
		PASET,
		RAMWR,
		RGBSET,
		RAMRD,
		PTLAR,
		VSCRDEF,
		TEOFF,
		TEON,
		MADCTL,
		VSCRSADD,
		IDMOFF,
		IDMON,
		PIXSET,
		Write_Memory_Continue,
		Read_Memory_Continue,
		Set_Tear_Scanline,
		Get_Scanline,
		WRDISBV,
		RDDISBV,
		WRCTRLD,
		RDCTRLD,
		WRCABC,
		RDCABC,
		Write_CABC_Min_Brightness,
		Read_CABC_Min_Brightness,
		IFMODE ,
		FRMCTR1,
		FRMCTR2,
		FRMCTR3,
		INVTR,
		PRCTR,
		DISCTRL,
		ETMOD,
		Backlight_Control_1,
		Backlight_Control_2,
		Backlight_Control_3,
		Backlight_Control_4,
		Backlight_Control_5,
		Backlight_Control_7,
		Backlight_Control_8,
		PWCTRL1,
		PWCTRL2,
		VMCTRL1,
		VMCTRL2,
		Undocumented_CA,
		Power_Control_A,
		Power_Control_B,
		NVMWR,
		NVMPKEY,
		RDNVM,
		RDID4,
		RDID1,
		RDID2,
		RDID3,
		PGAMCTRL,
		NGAMCTRL,
		DGAMCTRL1,
		DGAMCTRL2,
		Driver_Timing_Control_A,
		Driver_Timing_Control_A1,
		Driver_Timing_Control_B,
		Power_On_Sequence_Control,
		Undocumented_EF,
		Enable_3G,
		IFCTL,
		Pump_Ratio_Control
	) with Size => 8;

	for Command use (
		NOP                       => 16#00#,
		SWRESET                   => 16#01#,
		RDDIDIF                   => 16#04#,
		RDDST                     => 16#09#,
		RDDPM                     => 16#0A#,
		RDDMADCTL                 => 16#0B#,
		RDDCOLMOD                 => 16#0C#,
		RDDIM                     => 16#0D#,
		RDDSM                     => 16#0E#,
		RDDSDR                    => 16#0F#,
		SLPIN                     => 16#10#,
		SLPOUT                    => 16#11#,
		PTLON                     => 16#12#,
		NORON                     => 16#13#,
		DINVOFF                   => 16#20#,
		DINVON                    => 16#21#,
		GAMSET                    => 16#26#,
		DISPOFF                   => 16#28#,
		DISPON                    => 16#29#,
		CASET                     => 16#2A#,
		PASET                     => 16#2B#,
		RAMWR                     => 16#2C#,
		RGBSET                    => 16#2D#,
		RAMRD                     => 16#2E#,
		PTLAR                     => 16#30#,
		VSCRDEF                   => 16#33#,
		TEOFF                     => 16#34#,
		TEON                      => 16#35#,
		MADCTL                    => 16#36#,
		VSCRSADD                  => 16#37#,
		IDMOFF                    => 16#38#,
		IDMON                     => 16#39#,
		PIXSET                    => 16#3A#,
		Write_Memory_Continue     => 16#3C#,
		Read_Memory_Continue      => 16#3E#,
		Set_Tear_Scanline         => 16#44#,
		Get_Scanline              => 16#45#,
		WRDISBV                   => 16#51#,
		RDDISBV                   => 16#52#,
		WRCTRLD                   => 16#53#,
		RDCTRLD                   => 16#54#,
		WRCABC                    => 16#55#,
		RDCABC                    => 16#56#,
		Write_CABC_Min_Brightness => 16#5E#,
		Read_CABC_Min_Brightness  => 16#5F#,
		IFMODE                    => 16#B0#,
		FRMCTR1                   => 16#B1#,
		FRMCTR2                   => 16#B2#,
		FRMCTR3                   => 16#B3#,
		INVTR                     => 16#B4#,
		PRCTR                     => 16#B5#,
		DISCTRL                   => 16#B6#,
		ETMOD                     => 16#B7#,
		Backlight_Control_1       => 16#B8#,
		Backlight_Control_2       => 16#B9#,
		Backlight_Control_3       => 16#BA#,
		Backlight_Control_4       => 16#BB#,
		Backlight_Control_5       => 16#BC#,
		Backlight_Control_7       => 16#BE#,
		Backlight_Control_8       => 16#BF#,
		PWCTRL1                   => 16#C0#,
		PWCTRL2                   => 16#C1#,
		VMCTRL1                   => 16#C5#,
		VMCTRL2                   => 16#C7#,
		Undocumented_CA           => 16#CA#,
		Power_Control_A           => 16#CB#,
		Power_Control_B           => 16#CF#,
		NVMWR                     => 16#D0#,
		NVMPKEY                   => 16#D1#,
		RDNVM                     => 16#D2#,
		RDID4                     => 16#D3#,
		RDID1                     => 16#DA#,
		RDID2                     => 16#DB#,
		RDID3                     => 16#DC#,
		PGAMCTRL                  => 16#E0#,
		NGAMCTRL                  => 16#E1#,
		DGAMCTRL1                 => 16#E2#,
		DGAMCTRL2                 => 16#E3#,
		Driver_Timing_Control_A   => 16#E8#,
		Driver_Timing_Control_A1  => 16#E9#,
		Driver_Timing_Control_B   => 16#EA#,
		Power_On_Sequence_Control => 16#ED#,
		Undocumented_EF           => 16#EF#,
		Enable_3G                 => 16#F2#,
		IFCTL                     => 16#F6#,
		Pump_Ratio_Control        => 16#F7#
	);
	-- Note: There are undocumented commands in drivers, see
	-- https://forums.adafruit.com/viewtopic.php?f=47&t=63229&p=320378&hilit=0xef+ili9341#p320378

end ILI9341;
