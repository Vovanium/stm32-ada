package body Board.SPI.IO is

	procedure Transmit_Receive (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_16;
		Input  : out Interfaces.Unsigned_16)
	is
	begin
		while not Unit.SR.TXE loop
			null;
		end loop;

		Unit.DR := Output;

		if Unit.SR.BSY then
			null;
		end if;

		while not Unit.SR.RXNE loop
			null;
		end loop;

		if Unit.SR.RXNE then
			Input := Unit.DR;
		end if;
	end;

	procedure Transmit (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_16)
	is
		pragma Warnings (Off, "variable ""X"" is assigned but never read");
		X : Interfaces.Unsigned_16;
		pragma Warnings (On, "variable ""X"" is assigned but never read");
	begin
		Transmit_Receive (Unit, Output, X);
	end;

	procedure Transmit_Receive (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_8;
		Input  : out Interfaces.Unsigned_8)
	is
		X : Interfaces.Unsigned_16;
	begin
		Transmit_Receive (Unit, Interfaces.Unsigned_16 (Output), X);
		Input := Interfaces.Unsigned_8 (X);
	end;

	procedure Transmit (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_8)
	is
		pragma Warnings (Off, "variable ""X"" is assigned but never read");
		X : Interfaces.Unsigned_16;
		pragma Warnings (On, "variable ""X"" is assigned but never read");
	begin
		Transmit_Receive (Unit, Interfaces.Unsigned_16 (Output), X);
	end;

end Board.SPI.IO;