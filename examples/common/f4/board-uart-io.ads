package Board.UART.IO is
	procedure Set_Speed (Speed : Integer);
	-- Note: there's no method to detect if transmission or receiving
	-- is active, thus user should stop operations manually
	-- before this subprogram is called.

	procedure Transmit (Data : String);

	procedure Receive (Data : out Character);
end Board.UART.IO;