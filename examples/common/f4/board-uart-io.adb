with STM32.USARTs;
use  STM32.USARTs;

package body Board.UART.IO is

	procedure Initialize (Baud : Integer := 115_200) is
	begin
		Port_RCC_EN := True;

		RCC_EN := True;

		RCC_RST := True;
		RCC_RST := False;
	
		TX_Pin.Set_AFR     (AF);
		TX_Pin.Set_MODER   (Alternate_Mode);
		TX_Pin.Set_OSPEEDR (Low_Speed);
		TX_Pin.Set_OTYPER  (Push_Pull_Type);
		TX_Pin.Set_PUPDR   (Pull_Down);

		RX_Pin.Set_AFR     (AF);
		RX_Pin.Set_MODER   (Alternate_Mode);
		RX_Pin.Set_OSPEEDR (Low_Speed);
		RX_Pin.Set_OTYPER  (Push_Pull_Type);
		RX_Pin.Set_PUPDR   (Pull_Down);

		declare
			R : Control_Register_1 := Module.CR1;
		begin
			R.UE := False;
			Module.CR1 := R;
		end;

		declare
			R : Control_Register_1 := Module.CR1;
		begin
			R.M := Word_8_Bits;
			R.PCE := False;
			R.PS := Even_Parity;
			R.TE := True;
			R.RE := True;
			R.OVER8 := False;
		
			Module.CR1 := R;
		end;

		declare
			R : Control_Register_2 := Module.CR2;
		begin
			R.STOP := Stop_1_Bit;
			R.LINEN := False;
			R.CLKEN := False;

			Module.CR2 := R;
		end;

		declare
			R : Control_Register_3 := Module.CR3;
		begin
			R.SCEN := False;
			R.HDSEL := False;
			R.IREN := False;
			R.RTSE := False;
			R.CTSE := False;

			Module.CR3 := R;
		end;

		Module.BRR := Baud_Rate (Speed => Baud, OVER8 => False, Bus_Frequency => Board.APB2_Frequency);

		declare
			R : Control_Register_1 := Module.CR1;
		begin
			R.UE := True;
			Module.CR1 := R;
		end;
	end;

	procedure Set_Speed (Speed : Integer) is
	begin
		Module.BRR := Baud_Rate (Speed => Speed, OVER8 => False, Bus_Frequency => Board.APB2_Frequency);
	end;

	procedure Transmit (Data : String) is
	begin
		for I of Data loop
			while not Module.SR.TXE loop
				null;
			end loop;
			Module.DR := Character'Pos (I);
		end loop;
	end;

	procedure Receive (Data : out Character) is
	begin
		while not Module.SR.RXNE loop
			null;
		end loop;
		Data := Character'Val(Module.DR);
	end;

begin
	Initialize;
end Board.UART.IO;