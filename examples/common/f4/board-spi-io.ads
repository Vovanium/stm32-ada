with Interfaces;
with STM32.SPIs;
package Board.SPI.IO is
	procedure Transmit_Receive (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_16;
		Input  : out Interfaces.Unsigned_16);

	procedure Transmit (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_16);

	procedure Transmit_Receive (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_8;
		Input  : out Interfaces.Unsigned_8);

	procedure Transmit (
		Unit   : in out STM32.SPIs.SPI_Registers;
		Output : Interfaces.Unsigned_8);
end Board.SPI.IO;
