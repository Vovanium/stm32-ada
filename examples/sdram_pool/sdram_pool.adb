with STM32.GPIO; use STM32.GPIO;
with STM32.GPIO.Ports; use STM32.GPIO.Ports;
with Chip.Units; use Chip.Units;
with Board; use Board;
with Board.UART.IO;
with Board.Memory; -- SDRAM initialization is here!
with Simple_Pools;

with Ada.Real_Time; use Ada.Real_Time;
with Interfaces;    use Interfaces;
with System;
with System.Storage_Elements;
use type System.Storage_Elements.Storage_Offset;

procedure SDRAM_Pool is

	Now: Time := Clock;

	--type Test_Type is array (Integer range 0 .. Board.Memory.Memory_Size / 32 - 1) of Unsigned_32 with Size => Board.Memory.Memory_Size;

	--M : Test_Type with Import, Address => System'To_Address(Board.Memory.Memory_Base);

	package LED is new GPIO_Port_Boolean (LED_Port, LED_Bit);

	package LED_2 is new GPIO_Port_Boolean (LED_2_Port, LED_2_Bit);

	SDRAM_Pool : Simple_Pools.Simple_Pool := Simple_Pools.Make_Simple_Pool (
		System'To_Address (Board.Memory.Memory_Base),
		Board.Memory.Memory_Size / System.Storage_Unit);

	type Integer_Array is array (Integer range <>) of Integer;

	type Integer_Array_Access is access Integer_Array with Storage_Pool => SDRAM_Pool;

	A, B, C : Integer_Array_Access := new Integer_Array(1 .. 500_000);

begin

	LED_Pin.Enable;

	LED.Set_MODER   (STM32.GPIO.Output_Mode);
	LED.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED.Set (not LED_On);

	LED_2_Pin.Enable;

	LED_2.Set_MODER   (STM32.GPIO.Output_Mode);
	LED_2.Set_OTYPER  (STM32.GPIO.Push_Pull_Type);
	LED_2.Set_OSPEEDR (STM32.GPIO.Very_High_Speed);
	LED_2.Set_PUPDR   (STM32.GPIO.No_Pull);

	LED_2.Set (not LED_2_On);

	UART.IO.Transmit ("SDRAM Initialized" & ASCII.CR & ASCII.LF);

	UART.IO.Transmit ("A(1)'Address = " & System.Storage_Elements.Integer_Address'Image (System.Storage_Elements.To_Integer (A(1)'Address)) & ASCII.CR & ASCII.LF);
	UART.IO.Transmit ("B(1)'Address = " & System.Storage_Elements.Integer_Address'Image (System.Storage_Elements.To_Integer (B(1)'Address)) & ASCII.CR & ASCII.LF);
	UART.IO.Transmit ("C(1)'Address = " & System.Storage_Elements.Integer_Address'Image (System.Storage_Elements.To_Integer (C(1)'Address)) & ASCII.CR & ASCII.LF);

	A (1) := 1;
	B (1) := 7;
	C (1) := 33;

	UART.IO.Transmit ("Variables assigned" & ASCII.CR & ASCII.LF);

	UART.IO.Transmit ("A (1) = " & Integer'Image (A (1)) & ASCII.CR & ASCII.LF);
	UART.IO.Transmit ("B (1) = " & Integer'Image (B (1)) & ASCII.CR & ASCII.LF);
	UART.IO.Transmit ("C (1) = " & Integer'Image (C (1)) & ASCII.CR & ASCII.LF);

	loop

		null;

	end loop;

end SDRAM_Pool;
